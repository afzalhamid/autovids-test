﻿namespace VideoViewer
{
    partial class frmHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHistory));
            this.btnExportExcel = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanelDopdownMenu = new System.Windows.Forms.TableLayoutPanel();
            this.panelDropdown = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.panelMain = new System.Windows.Forms.Panel();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.gridEvent = new DevExpress.XtraGrid.GridControl();
            this.gridViewEvent = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSrNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVIN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarker = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeStamp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tableLayoutPanelDopdownMenu.SuspendLayout();
            this.panelDropdown.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.tableLayoutPanelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEvent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEvent)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Appearance.Image")));
            this.btnExportExcel.Appearance.Options.UseImage = true;
            this.btnExportExcel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExportExcel.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnExportExcel.Location = new System.Drawing.Point(1202, 3);
            this.btnExportExcel.MaximumSize = new System.Drawing.Size(32, 32);
            this.btnExportExcel.MinimumSize = new System.Drawing.Size(32, 32);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(32, 32);
            this.btnExportExcel.TabIndex = 1;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // tableLayoutPanelDopdownMenu
            // 
            this.tableLayoutPanelDopdownMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.tableLayoutPanelDopdownMenu.ColumnCount = 4;
            this.tableLayoutPanelDopdownMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanelDopdownMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelDopdownMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanelDopdownMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanelDopdownMenu.Controls.Add(this.panelDropdown, 0, 0);
            this.tableLayoutPanelDopdownMenu.Controls.Add(this.btnExportExcel, 2, 0);
            this.tableLayoutPanelDopdownMenu.Controls.Add(this.btnClose, 3, 0);
            this.tableLayoutPanelDopdownMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelDopdownMenu.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelDopdownMenu.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelDopdownMenu.Name = "tableLayoutPanelDopdownMenu";
            this.tableLayoutPanelDopdownMenu.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tableLayoutPanelDopdownMenu.RowCount = 1;
            this.tableLayoutPanelDopdownMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelDopdownMenu.Size = new System.Drawing.Size(1260, 41);
            this.tableLayoutPanelDopdownMenu.TabIndex = 0;
            // 
            // panelDropdown
            // 
            this.panelDropdown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panelDropdown.Controls.Add(this.label1);
            this.panelDropdown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDropdown.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.panelDropdown.ForeColor = System.Drawing.Color.White;
            this.panelDropdown.Location = new System.Drawing.Point(0, 0);
            this.panelDropdown.Margin = new System.Windows.Forms.Padding(0);
            this.panelDropdown.Name = "panelDropdown";
            this.panelDropdown.Size = new System.Drawing.Size(200, 41);
            this.panelDropdown.TabIndex = 512;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(4, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 17);
            this.label1.TabIndex = 502;
            this.label1.Text = "Vehicle Visit Histroy";
            // 
            // btnClose
            // 
            this.btnClose.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnClose.Appearance.Options.UseBackColor = true;
            this.btnClose.Appearance.Options.UseFont = true;
            this.btnClose.Appearance.Options.UseForeColor = true;
            this.btnClose.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnClose.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.ImageOptions.Image")));
            this.btnClose.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnClose.Location = new System.Drawing.Point(1241, 3);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(16, 16);
            this.btnClose.TabIndex = 510;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.Transparent;
            this.panelMain.Controls.Add(this.tableLayoutPanelDopdownMenu);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(2, 14);
            this.panelMain.Margin = new System.Windows.Forms.Padding(2);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1260, 41);
            this.panelMain.TabIndex = 4;
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.BackColor = System.Drawing.Color.Gainsboro;
            this.tableLayoutPanelMain.ColumnCount = 1;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.gridEvent, 0, 3);
            this.tableLayoutPanelMain.Controls.Add(this.panelMain, 0, 1);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 4;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1264, 761);
            this.tableLayoutPanelMain.TabIndex = 6;
            // 
            // gridEvent
            // 
            this.gridEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEvent.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridEvent.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridEvent.Location = new System.Drawing.Point(25, 69);
            this.gridEvent.MainView = this.gridViewEvent;
            this.gridEvent.Margin = new System.Windows.Forms.Padding(25, 0, 25, 25);
            this.gridEvent.Name = "gridEvent";
            this.gridEvent.Size = new System.Drawing.Size(1214, 667);
            this.gridEvent.TabIndex = 35;
            this.gridEvent.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEvent});
            // 
            // gridViewEvent
            // 
            this.gridViewEvent.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridViewEvent.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridViewEvent.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridViewEvent.Appearance.Empty.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.Empty.Options.UseFont = true;
            this.gridViewEvent.Appearance.EvenRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.EvenRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.FilterCloseButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridViewEvent.Appearance.FilterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FilterPanel.Options.UseFont = true;
            this.gridViewEvent.Appearance.FixedLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FixedLine.Options.UseFont = true;
            this.gridViewEvent.Appearance.FocusedCell.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FocusedCell.Options.UseFont = true;
            this.gridViewEvent.Appearance.FocusedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.FooterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewEvent.Appearance.GroupButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.GroupButton.Options.UseFont = true;
            this.gridViewEvent.Appearance.GroupFooter.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.GroupFooter.Options.UseFont = true;
            this.gridViewEvent.Appearance.GroupPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.GroupRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.HeaderPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewEvent.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.HorzLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.HorzLine.Options.UseFont = true;
            this.gridViewEvent.Appearance.OddRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.OddRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.Preview.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.Preview.Options.UseFont = true;
            this.gridViewEvent.Appearance.Row.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.Row.Options.UseFont = true;
            this.gridViewEvent.Appearance.RowSeparator.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.RowSeparator.Options.UseFont = true;
            this.gridViewEvent.Appearance.SelectedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.SelectedRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.TopNewRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.TopNewRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.VertLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.VertLine.Options.UseFont = true;
            this.gridViewEvent.Appearance.ViewCaption.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.ViewCaption.Options.UseFont = true;
            this.gridViewEvent.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSrNo,
            this.colVIN,
            this.colMarker,
            this.colComment,
            this.colTimeStamp});
            this.gridViewEvent.GridControl = this.gridEvent;
            this.gridViewEvent.Name = "gridViewEvent";
            this.gridViewEvent.OptionsView.ShowGroupPanel = false;
            // 
            // colSrNo
            // 
            this.colSrNo.Caption = "Sr #.";
            this.colSrNo.FieldName = "SrNo";
            this.colSrNo.MaxWidth = 50;
            this.colSrNo.Name = "colSrNo";
            this.colSrNo.OptionsColumn.AllowEdit = false;
            this.colSrNo.Visible = true;
            this.colSrNo.VisibleIndex = 0;
            this.colSrNo.Width = 50;
            // 
            // colVIN
            // 
            this.colVIN.Caption = "VIN";
            this.colVIN.FieldName = "Vin";
            this.colVIN.MaxWidth = 300;
            this.colVIN.MinWidth = 300;
            this.colVIN.Name = "colVIN";
            this.colVIN.OptionsColumn.AllowEdit = false;
            this.colVIN.Visible = true;
            this.colVIN.VisibleIndex = 1;
            this.colVIN.Width = 300;
            // 
            // colMarker
            // 
            this.colMarker.Caption = "Marker";
            this.colMarker.FieldName = "Marker";
            this.colMarker.MaxWidth = 250;
            this.colMarker.MinWidth = 250;
            this.colMarker.Name = "colMarker";
            this.colMarker.OptionsColumn.AllowEdit = false;
            this.colMarker.Visible = true;
            this.colMarker.VisibleIndex = 2;
            this.colMarker.Width = 250;
            // 
            // colComment
            // 
            this.colComment.Caption = "Comment";
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            this.colComment.OptionsColumn.AllowEdit = false;
            this.colComment.Visible = true;
            this.colComment.VisibleIndex = 3;
            // 
            // colTimeStamp
            // 
            this.colTimeStamp.Caption = "Timestamp";
            this.colTimeStamp.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm:ss";
            this.colTimeStamp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeStamp.FieldName = "Timestamp";
            this.colTimeStamp.MaxWidth = 250;
            this.colTimeStamp.MinWidth = 250;
            this.colTimeStamp.Name = "colTimeStamp";
            this.colTimeStamp.OptionsColumn.AllowEdit = false;
            this.colTimeStamp.Visible = true;
            this.colTimeStamp.VisibleIndex = 4;
            this.colTimeStamp.Width = 250;
            // 
            // frmHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1264, 761);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmHistory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmHistory";
            this.Load += new System.EventHandler(this.frmHistory_Load);
            this.tableLayoutPanelDopdownMenu.ResumeLayout(false);
            this.panelDropdown.ResumeLayout(false);
            this.panelDropdown.PerformLayout();
            this.panelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEvent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEvent)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton btnExportExcel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelDopdownMenu;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private DevExpress.XtraGrid.GridControl gridEvent;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEvent;
        private DevExpress.XtraGrid.Columns.GridColumn colSrNo;
        private DevExpress.XtraGrid.Columns.GridColumn colVIN;
        private DevExpress.XtraGrid.Columns.GridColumn colMarker;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeStamp;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private System.Windows.Forms.Panel panelDropdown;
        private System.Windows.Forms.Label label1;
    }
}