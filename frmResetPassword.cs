﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoViewer
{
    public partial class frmResetPassword : Form
    {
        public int userID { get; set; }
        private string existingPassword { get; set; }
        private string userName { get; set; }
        DAL dataAccess = new DAL();

        public frmResetPassword()
        {
            InitializeComponent();
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            lblMessage.Text = string.Empty;

            if (string.Equals(txtNewPassword.Text.Trim(), txtConfirmPassword.Text.Trim()))
            {
                if (string.Equals(txtCurrentPassword.Text.Trim(), existingPassword.Trim()))
                {
                    if (dataAccess.ChangeUserPassword(userName, txtNewPassword.Text.Trim(), existingPassword))
                        lblMessage.Text = "Password changed successfully.";
                    else
                        lblMessage.Text = "Failed to update the password.";
                }
                else
                {
                    lblMessage.Text = "Current password is wrong.";
                }
            }
            else
            {
                lblMessage.Text = "Password didn't match.";
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtCurrentPassword.Text = string.Empty;
            txtNewPassword.Text = string.Empty;
            txtConfirmPassword.Text = string.Empty;
            lblMessage.Text = string.Empty;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void frmResetPassword_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable dtUsers = dataAccess.GetUsers();
                var results = from u in dtUsers.AsEnumerable()
                              where u.Field<int>("UID") == userID
                              select new { Password = u.Field<string>("Password"), UserName = u.Field<string>("UserID") };

                existingPassword = dataAccess.Decrypt(results.First().Password);
                userName = results.First().UserName;
                txtUserName.Text = userName;
            }
            catch (Exception ex)
            {
                dataAccess.Logging(Module.Application,LogClass.Error, ex.ToString());
            }
        }
    }
}
