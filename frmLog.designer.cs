﻿namespace VideoViewer
{
    partial class frmLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControlLog = new DevExpress.XtraGrid.GridControl();
            this.gridViewLog = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnModule = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnTimestamp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelSelectionControls = new System.Windows.Forms.TableLayoutPanel();
            this.panelSelectionControls = new System.Windows.Forms.Panel();
            this.rbException = new System.Windows.Forms.RadioButton();
            this.rbMessage = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLog)).BeginInit();
            this.tableLayoutPanelMain.SuspendLayout();
            this.tableLayoutPanelSelectionControls.SuspendLayout();
            this.panelSelectionControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControlLog
            // 
            this.gridControlLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlLog.Location = new System.Drawing.Point(0, 85);
            this.gridControlLog.MainView = this.gridViewLog;
            this.gridControlLog.Margin = new System.Windows.Forms.Padding(0);
            this.gridControlLog.Name = "gridControlLog";
            this.gridControlLog.Size = new System.Drawing.Size(1608, 621);
            this.gridControlLog.TabIndex = 0;
            this.gridControlLog.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLog});
            // 
            // gridViewLog
            // 
            this.gridViewLog.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridViewLog.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridViewLog.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridViewLog.Appearance.DetailTip.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.DetailTip.Options.UseFont = true;
            this.gridViewLog.Appearance.Empty.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.Empty.Options.UseFont = true;
            this.gridViewLog.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.gridViewLog.Appearance.EvenRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridViewLog.Appearance.EvenRow.Options.UseFont = true;
            this.gridViewLog.Appearance.FilterCloseButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridViewLog.Appearance.FilterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.FilterPanel.Options.UseFont = true;
            this.gridViewLog.Appearance.FixedLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.FixedLine.Options.UseFont = true;
            this.gridViewLog.Appearance.FocusedCell.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.FocusedCell.Options.UseFont = true;
            this.gridViewLog.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.gridViewLog.Appearance.FocusedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridViewLog.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewLog.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewLog.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewLog.Appearance.FooterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewLog.Appearance.GroupButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.GroupButton.Options.UseFont = true;
            this.gridViewLog.Appearance.GroupFooter.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.GroupFooter.Options.UseFont = true;
            this.gridViewLog.Appearance.GroupPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewLog.Appearance.GroupRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewLog.Appearance.HeaderPanel.Font = new System.Drawing.Font("Calibri", 12F);
            this.gridViewLog.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewLog.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridViewLog.Appearance.HorzLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.HorzLine.Options.UseFont = true;
            this.gridViewLog.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.gridViewLog.Appearance.OddRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewLog.Appearance.OddRow.Options.UseFont = true;
            this.gridViewLog.Appearance.Preview.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.Preview.Options.UseFont = true;
            this.gridViewLog.Appearance.Row.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.Row.Options.UseFont = true;
            this.gridViewLog.Appearance.RowSeparator.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.RowSeparator.Options.UseFont = true;
            this.gridViewLog.Appearance.SelectedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.SelectedRow.Options.UseFont = true;
            this.gridViewLog.Appearance.TopNewRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.TopNewRow.Options.UseFont = true;
            this.gridViewLog.Appearance.VertLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.VertLine.Options.UseFont = true;
            this.gridViewLog.Appearance.ViewCaption.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewLog.Appearance.ViewCaption.Options.UseFont = true;
            this.gridViewLog.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnModule,
            this.gridColumnClass,
            this.gridColumnMessage,
            this.gridColumnTimestamp});
            this.gridViewLog.GridControl = this.gridControlLog;
            this.gridViewLog.Name = "gridViewLog";
            this.gridViewLog.OptionsView.EnableAppearanceOddRow = true;
            // 
            // gridColumnModule
            // 
            this.gridColumnModule.Caption = "Module";
            this.gridColumnModule.FieldName = "Module";
            this.gridColumnModule.MaxWidth = 200;
            this.gridColumnModule.Name = "gridColumnModule";
            this.gridColumnModule.OptionsColumn.AllowEdit = false;
            this.gridColumnModule.Visible = true;
            this.gridColumnModule.VisibleIndex = 0;
            this.gridColumnModule.Width = 200;
            // 
            // gridColumnClass
            // 
            this.gridColumnClass.Caption = "Class";
            this.gridColumnClass.FieldName = "Class";
            this.gridColumnClass.MaxWidth = 200;
            this.gridColumnClass.Name = "gridColumnClass";
            this.gridColumnClass.OptionsColumn.AllowEdit = false;
            this.gridColumnClass.Visible = true;
            this.gridColumnClass.VisibleIndex = 1;
            this.gridColumnClass.Width = 200;
            // 
            // gridColumnMessage
            // 
            this.gridColumnMessage.Caption = "Message";
            this.gridColumnMessage.FieldName = "Message";
            this.gridColumnMessage.Name = "gridColumnMessage";
            this.gridColumnMessage.OptionsColumn.AllowEdit = false;
            this.gridColumnMessage.Visible = true;
            this.gridColumnMessage.VisibleIndex = 2;
            this.gridColumnMessage.Width = 397;
            // 
            // gridColumnTimestamp
            // 
            this.gridColumnTimestamp.Caption = "Timestamp";
            this.gridColumnTimestamp.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss";
            this.gridColumnTimestamp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumnTimestamp.FieldName = "Timestamp";
            this.gridColumnTimestamp.MaxWidth = 200;
            this.gridColumnTimestamp.Name = "gridColumnTimestamp";
            this.gridColumnTimestamp.OptionsColumn.AllowEdit = false;
            this.gridColumnTimestamp.Visible = true;
            this.gridColumnTimestamp.VisibleIndex = 3;
            this.gridColumnTimestamp.Width = 200;
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelMain.ColumnCount = 1;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.gridControlLog, 0, 3);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelSelectionControls, 0, 1);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 4;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1608, 706);
            this.tableLayoutPanelMain.TabIndex = 1;
            // 
            // tableLayoutPanelSelectionControls
            // 
            this.tableLayoutPanelSelectionControls.ColumnCount = 2;
            this.tableLayoutPanelSelectionControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanelSelectionControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelSelectionControls.Controls.Add(this.panelSelectionControls, 0, 0);
            this.tableLayoutPanelSelectionControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelSelectionControls.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanelSelectionControls.Name = "tableLayoutPanelSelectionControls";
            this.tableLayoutPanelSelectionControls.RowCount = 1;
            this.tableLayoutPanelSelectionControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelSelectionControls.Size = new System.Drawing.Size(1602, 49);
            this.tableLayoutPanelSelectionControls.TabIndex = 2;
            // 
            // panelSelectionControls
            // 
            this.panelSelectionControls.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panelSelectionControls.Controls.Add(this.rbException);
            this.panelSelectionControls.Controls.Add(this.rbMessage);
            this.panelSelectionControls.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.panelSelectionControls.ForeColor = System.Drawing.Color.White;
            this.panelSelectionControls.Location = new System.Drawing.Point(0, 0);
            this.panelSelectionControls.Margin = new System.Windows.Forms.Padding(0);
            this.panelSelectionControls.MaximumSize = new System.Drawing.Size(454, 50);
            this.panelSelectionControls.Name = "panelSelectionControls";
            this.panelSelectionControls.Size = new System.Drawing.Size(400, 49);
            this.panelSelectionControls.TabIndex = 0;
            // 
            // rbException
            // 
            this.rbException.AutoSize = true;
            this.rbException.Location = new System.Drawing.Point(160, 12);
            this.rbException.Name = "rbException";
            this.rbException.Size = new System.Drawing.Size(107, 25);
            this.rbException.TabIndex = 1;
            this.rbException.Text = "Exceptions";
            this.rbException.UseVisualStyleBackColor = true;
            this.rbException.CheckedChanged += new System.EventHandler(this.rbException_CheckedChanged);
            // 
            // rbMessage
            // 
            this.rbMessage.AutoSize = true;
            this.rbMessage.Checked = true;
            this.rbMessage.Location = new System.Drawing.Point(21, 12);
            this.rbMessage.Name = "rbMessage";
            this.rbMessage.Size = new System.Drawing.Size(101, 25);
            this.rbMessage.TabIndex = 0;
            this.rbMessage.TabStop = true;
            this.rbMessage.Text = "Messages";
            this.rbMessage.UseVisualStyleBackColor = true;
            this.rbMessage.CheckedChanged += new System.EventHandler(this.rbMessage_CheckedChanged);
            // 
            // frmLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1608, 706);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmLog";
            this.Text = "Log";
            this.Load += new System.EventHandler(this.frmLog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLog)).EndInit();
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelSelectionControls.ResumeLayout(false);
            this.panelSelectionControls.ResumeLayout(false);
            this.panelSelectionControls.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlLog;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLog;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnModule;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnClass;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMessage;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTimestamp;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.RadioButton rbException;
        private System.Windows.Forms.RadioButton rbMessage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelSelectionControls;
        private System.Windows.Forms.Panel panelSelectionControls;
    }
}