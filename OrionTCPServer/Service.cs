﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using tcpServer;
using System.Net.Sockets;
using System.Configuration;
using System.IO;
using System.Net;
using VideoOS.Platform;
using VideoOS.Platform.Data;
using VideoOS.Platform.Messaging;
using VideoOS.Platform.Proxy.Alarm;
using VideoOS.Platform.Proxy.AlarmClient;

namespace OrionTCPServer
{
    public partial class Service : ServiceBase
    {
        public delegate void invokeDelegate();
        TcpServer tcpServerANPR = new TcpServer();
        TcpServer tcpServerRFiD = new TcpServer();
        XMLBLL parseXml = new XMLBLL();
        System.Timers.Timer timer = new System.Timers.Timer();

        private object obj;
        private AlarmClientManager alarmClientManager;
        private MessageCommunication messageCommunication;
        static List<Camera> CamEntry = null;
        static List<Camera> CamExit = null;
        private static Socket clientConnect { get; set; }
        private static string milestoneUserName { get { return ConfigurationManager.AppSettings["milestoneUserName"]; } }
        private static string milestonePassword { get { return ConfigurationManager.AppSettings["milestonePassword"]; } }
        private static int milestonePort { get { return Convert.ToInt32(ConfigurationManager.AppSettings["milestonePort"]); } }
        private static string milestoneIPAddress { get { return ConfigurationManager.AppSettings["milestoneIP"]; } }
        private static int milestoneEventServerPort { get { return Convert.ToInt32(ConfigurationManager.AppSettings["MilestoneEventServerPort"]); } }
        private string IpAddress { get { return ConfigurationManager.AppSettings["TCPServerIP"]; } }
        private string Domain { get { return ConfigurationManager.AppSettings["Domain"]; } }
        private char Delimiter { get { return Convert.ToChar(ConfigurationManager.AppSettings["Delimiter"]); } }
        private int PortNo { get { return Convert.ToInt32(ConfigurationManager.AppSettings["TCPServerPort"]); } }
        private int PortNo2 { get { return PortNo + 1; } }

        private int Interval { get { return Convert.ToInt32(ConfigurationManager.AppSettings["RecInterval"]); } }
        private enum Recording
        {Start, Stop}

        DAL dbOperation = new DAL();

        public Service()
        {
            InitializeComponent();
        }
        
        protected override void OnStart(string[] args)
        {
            try
            {
                List<Camera> cameras = new List<Camera>();
                DataTable dataTable = dbOperation.GetAllCamera();

                foreach (DataRow row in dataTable.Rows)
                {
                    cameras.Add(new Camera() {

                        ID = Convert.ToInt32(row["ID"]),
                        RFID = Convert.ToInt32(row["RFID"]),
                        ServerId = Convert.ToInt32(row["ServerId"]),
                        Direction = Convert.ToString(row["Direction"]) ,
                        ANPR = Convert.ToString(row["ANPR"]),
                        Group = Convert.ToString(row["Group"]),                        
                        Marker = Convert.ToString(row["Marker"]),
                        Name = Convert.ToString(row["Camera"]),                       
                        Server = Convert.ToString(row["Server"])
                        
                    });
                }

                CamEntry = new List<Camera>();
                CamExit = new List<Camera>();
                CamEntry = (from cam in cameras.AsEnumerable() where cam.Direction == "IN" select cam).ToList();
                CamExit = (from cam in cameras.AsEnumerable() where cam.Direction == "OUT" select cam).ToList();
                
                tcpServerANPR.OnConnect += tcpServerANPR_OnConnect;
                tcpServerANPR.OnDataAvailable += tcpServerANPR_OnDataAvailable;

                tcpServerRFiD.OnConnect += TcpServerRFiD_OnConnect;
                tcpServerRFiD.OnDataAvailable += TcpServerRFiD_OnDataAvailable;

                StartTCPServer();
                MilestoneInitialization();

                timer.Elapsed += Timer_Elapsed;
                timer.Interval = 1000 * 60 * 10;
                timer.Enabled = true;
            }
            catch (Exception ex) { Logging.TraceLog(Module.TCPServer, LogClass.Error, ex.ToString()); }
            // TODO: Add code here to start your service.
        }

        private void TcpServerRFiD_OnDataAvailable(TcpServerConnection connection)
        {
            try
            {
                byte[] data = readStream(connection.Socket);

                if (data != null)
                {
                    XMLBLL.vinMessage = Encoding.UTF8.GetString(data);
                    VehicleRead vehRead = parseXml.DeserializeElements();

                    if (vehRead.direction.value == "IN")
                        TriggerCamerasRecordingByVIN(CamEntry, Recording.Start,vehRead.direction.value,DAL.Mode.Event, vehRead.vin.value, vehRead.marker.value ,DateTime.Now);
                    else if (vehRead.direction.value == "OUT")
                        TriggerCamerasRecordingByVIN(CamExit, Recording.Start, vehRead.direction.value, DAL.Mode.Event, vehRead.vin.value, vehRead.marker.value, DateTime.Now);
                    
                    Logging.TraceLog(Module.TCPServer, LogClass.Message, String.Format("VIN # :{0} | Timestamp :{1} | Marker :{2} | Direction: {3}", vehRead.vin.value, vehRead.timestamp.value, vehRead.marker.value, vehRead.direction.value));
                    
                    data = null;
                }
            }
            catch (Exception ex)
            {
                Logging.TraceLog(Module.TCPServer, LogClass.Error, String.Format("OnDataAvailable : {0}", ex.ToString()));
            }
        }

        private void TcpServerRFiD_OnConnect(TcpServerConnection connection)
        {
            try
            {
                string clientIP = connection.Socket.Client.RemoteEndPoint.ToString().Split(':').First();
                Logging.TraceLog(Module.TCPServer, LogClass.Message, String.Format("RFID Client {0} connected successfully at {1}", clientIP, DateTime.Now.ToString()));

            }
            catch { }
        }

        void tcpServerANPR_OnConnect(TcpServerConnection connection)
        {
            try
            {
                string clientIP = connection.Socket.Client.RemoteEndPoint.ToString().Split(':').First();
                Logging.TraceLog(Module.TCPServer, LogClass.Message, String.Format("ANPR Client {0} connected successfully at {1}", clientIP, DateTime.Now.ToString()));

            }
            catch { }

        }
        //Read Data from Client
        void tcpServerANPR_OnDataAvailable(TcpServerConnection connection)
        {
            try
            {
                byte[] data = readStream(connection.Socket);

                if (data != null)
                {
                    string dataStr = Encoding.ASCII.GetString(data);
                    string[] keys = dataStr.Split(Delimiter);
                    string direction = keys[0];
                    string vehicleNumber = keys[1];

                    if (direction == "IN")
                        TriggerCamerasRecording(CamEntry, Recording.Start, vehicleNumber, direction, DAL.Mode.Event);
                    else if (direction == "OUT")
                        TriggerCamerasRecording(CamExit, Recording.Start, vehicleNumber, direction, DAL.Mode.Event);

                    Logging.TraceLog(Module.TCPServer, LogClass.Message, String.Format("Packet Received : {0}", dataStr));

                    //Process Data received from NedApp TCP Connection

                    data = null;
                }
            }
            catch (Exception ex)
            {
                Logging.TraceLog(Module.TCPServer, LogClass.Error, String.Format("OnDataAvailable : {0}", ex.ToString()));
            }
        }
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                dbOperation.ClearLogsEvents();
            }
            catch
            {
                
            }
        }

        private void send(string data, string telegram)
        {
            try
            {
                data = data.Substring(0, data.Length);
                tcpServerANPR.Send(data);
                Logging.TraceLog(Module.TCPServer, LogClass.Message, string.Format("{0} sent at {1}", telegram, DateTime.Now.ToUniversalTime().ToString()));
            }
            catch (Exception ex) { Logging.TraceLog(Module.TCPServer, LogClass.Error, ex.ToString()); }
            //Log Event to OMW Database
        }
        
        protected byte[] readStream(TcpClient client)
        {
            NetworkStream stream = client.GetStream();
            if (stream.DataAvailable)
            {
                byte[] data = new byte[client.Available];

                int bytesRead = 0;
                try
                {
                    bytesRead = stream.Read(data, 0, data.Length);
                }
                catch (IOException)
                {
                }

                if (bytesRead < data.Length)
                {
                    byte[] lastData = data;
                    data = new byte[bytesRead];
                    Array.ConstrainedCopy(lastData, 0, data, 0, bytesRead);
                }
                return data;
            }
            return null;
        }

        

        protected override void OnStop()
        {
            try
            {
                tcpServerANPR.Close();
                tcpServerRFiD.Close();
                
            }
            catch (Exception ex)
            {
                Logging.TraceLog(Module.TCPServer, LogClass.Error, ex.ToString());
            }

        }

        void StartTCPServer()
        {
            try
            {
                //tcpServerANPR.IpAddress = IpAddress;
                //tcpServerANPR.Port = PortNo;

                tcpServerRFiD.IpAddress = IpAddress;
                tcpServerRFiD.Port = PortNo2;

                //tcpServerANPR.Open();
                tcpServerRFiD.Open();

                //Logging.TraceLog(Module.TCPServer, LogClass.Message, String.Format("ANPR TCP Server started at {0}:{1}", IpAddress, PortNo));
                Logging.TraceLog(Module.TCPServer, LogClass.Message, String.Format("RFiD TCP Server started at {0}:{1}", IpAddress, PortNo2));
            }
            catch (Exception ex)
            {
                Logging.TraceLog(Module.TCPServer, LogClass.Error, ex.ToString());
            }

        }

        #region Milestone

        private void TriggerCamerasRecordingByVIN(List<Camera> Camera, Recording rec, string direction, DAL.Mode mode, string vin,string marker, DateTime evtTime)
        {

            try
            {
                if (Camera.Any())
                {
                    IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(milestoneIPAddress), milestoneEventServerPort);
                    Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    server.Connect(remoteEP);

                    string message = string.Empty;


                    foreach (var cam in Camera)
                    {
                        message = String.IsNullOrEmpty(message) ? cam.Name : String.Format("{0},{1}", message, cam.Name);
                        server.Send(Encoding.ASCII.GetBytes(String.Format("{0}{1}", rec, cam.Name)));
                    }

                    dbOperation.LogEventByVIN(vin, marker, evtTime);

                    server.Shutdown(SocketShutdown.Both);
                    server.Close();
                    Logging.TraceLog(Module.TCPServer, LogClass.Message, String.Format("TCP Server sent packet to {0}:{1}: Messages : {2}", milestoneIPAddress, milestoneEventServerPort, message));
                }

            }
            catch (Exception ex)
            {
                Logging.TraceLog(Module.TCPServer, LogClass.Error, ex.ToString());
            }
        }

        private void TriggerCamerasRecording(List<Camera> Camera, Recording rec, string vehicleNumber, string direction, DAL.Mode mode)
        {

            try
            {
                if (Camera.Any())
                {
                    IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(milestoneIPAddress), milestoneEventServerPort);
                    Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    server.Connect(remoteEP);

                    string message = string.Empty;


                    foreach (var cam in Camera)
                    {
                        message = String.IsNullOrEmpty(message) ? cam.Name : String.Format("{0},{1}", message, cam.Name);
                        server.Send(Encoding.ASCII.GetBytes(String.Format("{0}{1}", rec, cam.Name)));
                    }

                    dbOperation.LogVehicleEvent(vehicleNumber, direction, DAL.Mode.Event);

                    server.Shutdown(SocketShutdown.Both);
                    server.Close();
                    Logging.TraceLog(Module.TCPServer, LogClass.Message, String.Format("TCP Server sent packet to {0}:{1}: Messages : {2}", milestoneIPAddress, milestoneEventServerPort, message));
                }
                
            }
            catch (Exception ex)
            {
                Logging.TraceLog(Module.TCPServer, LogClass.Error, ex.ToString());
            }
        }

        private void MilestoneInitialization()
        {
            try
            {

                VideoOS.Platform.SDK.Environment.Initialize();			// General initialize.  Always required

                string hostManagementService = milestoneIPAddress;

                Uri uri = new UriBuilder(hostManagementService).Uri;
                System.Net.NetworkCredential credentials = new NetworkCredential(milestoneUserName, milestonePassword, string.Empty);
                VideoOS.Platform.SDK.Environment.AddServer(uri, credentials);// CredentialCache.DefaultNetworkCredentials);
                bool msServerConnected = VideoOS.Platform.SDK.Environment.IsServerConnected(uri);
                MessageCommunicationManager.Start(EnvironmentManager.Instance.MasterSite.ServerId);
                messageCommunication = MessageCommunicationManager.Get(EnvironmentManager.Instance.MasterSite.ServerId);

                obj = messageCommunication.RegisterCommunicationFilter(NewAlarmMessageHandler,
                    new VideoOS.Platform.Messaging.CommunicationIdFilter(VideoOS.Platform.Messaging.MessageId.Server.NewAlarmIndication));
                //obj = messageCommunication.RegisterCommunicationFilter(ChangedEventMessageHandler,
                //new VideoOS.Platform.Messaging.CommunicationIdFilter(VideoOS.Platform.Messaging.MessageId.Server.ChangedAlarmIndication));

                alarmClientManager = new AlarmClientManager();

                if (msServerConnected)
                    Logging.TraceLog(Module.Milestone, LogClass.Message, String.Format("Successfully connected to Milestone server at {0}:{1}", milestoneIPAddress, milestonePort));
                else
                    Logging.TraceLog(Module.Milestone, LogClass.Message, String.Format("Failed to connect milestone server at {0}:{1}", milestoneIPAddress, milestonePort));

            }
            catch (Exception ex)
            {
                Logging.TraceLog(Module.Milestone, LogClass.Error, ex.ToString());
            }
        }

        private object NewAlarmMessageHandler(VideoOS.Platform.Messaging.Message message, FQID dest, FQID source)
        {
            try
            {
                Alarm alarm = message.Data as Alarm;
                if (alarm != null)
                {

                    string alarmDef = alarm.RuleList != null && alarm.RuleList.Count > 0 ? alarm.RuleList[0].Name : "";
                    //string messageToSend = String.Format("{0}{1}{2}{3}{4}{5}", alarmDef, alarm.EventHeader.Source.Name, alarm.EventHeader.Timestamp.ToLocalTime(), alarm.EventHeader.Message, alarm.EventHeader.Priority, alarm.State);
                    //Alarm State 1:New , 11: Closed
                    if (tcpServerANPR.IsOpen)
                    {
                       
                    }

                }
            }
            catch (Exception ex)
            {
                EnvironmentManager.Instance.ExceptionDialog("MessageHandler", ex);
            }

            return null;
        }

        private object ChangedEventMessageHandler(VideoOS.Platform.Messaging.Message message, FQID dest, FQID source)
        {
            try
            {
                Alarm alarm = message.Data as Alarm;
                if (alarm != null)
                {

                    string alarmDef = alarm.RuleList != null && alarm.RuleList.Count > 0 ? alarm.RuleList[0].Name : "";
                    //string messageToSend = String.Format("{0}{1}{2}{3}{4}{5}", alarmDef, alarm.EventHeader.Source.Name, alarm.EventHeader.Timestamp.ToLocalTime(), alarm.EventHeader.Message, alarm.EventHeader.Priority, alarm.State);
                    //Alarm State 1:New , 11: Closed
                    if (tcpServerANPR.IsOpen)
                    {
                        //Process Milestone Event
                    }
                }
            }
            catch (Exception ex)
            {
                EnvironmentManager.Instance.ExceptionDialog("MessageHandler", ex);
            }

            return null;
        }
        
        private void LogAlarm(string Source, string Message, ushort Priority, ushort State, DateTime Timestamp, string Definition)
        {
            DAL dbOperation = new DAL();
            Dictionary<string, string> paraCollection = new Dictionary<string, string>();
            paraCollection.Add("@Source", Source);
            paraCollection.Add("@Message", Message);
            paraCollection.Add("@Priority", Convert.ToString(Priority));
            paraCollection.Add("@State", Convert.ToString(State));
            paraCollection.Add("@Timestamp", Timestamp.ToString("M/d/yyyy HH:mm:ss"));
            paraCollection.Add("@Definition", Definition);

            //dbOperation(paraCollection);
        }

        #endregion Milestone
    }
}
