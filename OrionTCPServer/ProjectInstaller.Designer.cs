﻿namespace OrionTCPServer
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstallerTCP = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstallerTCP = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstallerTCP
            // 
            this.serviceProcessInstallerTCP.Account = System.ServiceProcess.ServiceAccount.NetworkService;
            this.serviceProcessInstallerTCP.Password = null;
            this.serviceProcessInstallerTCP.Username = null;
            // 
            // serviceInstallerTCP
            // 
            this.serviceInstallerTCP.Description = "AutoVIDS Server is a listener which received data and events from ANPR and RFiD S" +
    "ystem";
            this.serviceInstallerTCP.DisplayName = "Orion Server";
            this.serviceInstallerTCP.ServiceName = "OrionAutoVDSServer";
            this.serviceInstallerTCP.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceProcessInstallerTCP,
            this.serviceInstallerTCP});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstallerTCP;
        private System.ServiceProcess.ServiceInstaller serviceInstallerTCP;
    }
}