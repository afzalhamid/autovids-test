﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace TestProject
{
    class Program
    {
        static List<Camera> CamEntry = null;
        static List<Camera> CamExit = null;

       static Queue<Message> msgQ { get; set; }
       static int count { get; set; }
        static void Main(string[] args)
        {
            DAL dbOperation = new DAL();
            //List<Camera> cameras = dbOperation.GetAllCamera();
            //CamEntry = new List<Camera>();
            //CamExit = new List<Camera>();
            //CamEntry = (from cam in cameras.AsEnumerable() where cam.Direction == "IN" select cam).ToList();
            //CamExit = (from cam in cameras.AsEnumerable() where cam.Direction == "OUT" select cam).ToList();

            //XMLBLL.vinMessage = XmlMessage._value;
            //XMLBLL xmlMsg = new XMLBLL();
            //VehicleRead vinRead = xmlMsg.DeserializeElements();

            string val1 = "DEMO";

            string en = dbOperation.Encrypt(val1);
            string dc = dbOperation.Decrypt(en);


            count = 1;
            msgQ = new Queue<Message>();

            Timer tIn = new Timer();
            Timer tOut = new Timer();

            tIn.Interval = 1000;
            tOut.Interval = 1000;

            tIn.Elapsed += TIn_Elapsed;
            tOut.Elapsed += TOut_Elapsed;

            tIn.Start();
            tOut.Start();

            Console.ReadKey();
             

        }

        private static void TOut_Elapsed(object sender, ElapsedEventArgs e)
        {
            msgQ.Enqueue(new Message()
            {
                regno = String.Format("[ REG ]: {0}", count < 10 ? String.Format("00{0}",count) : count < 100 ? String.Format("0{0}", count) : count.ToString()),
                vin = String.Format("[ VIN ]: {0}", count < 10 ? String.Format("00{0}", count) : count < 100 ? String.Format("0{0}", count) : count.ToString()),
                timestamp = String.Format("[ TIMESTAMP ]: {0}", DateTime.Now)
            });

            count++;
        }

        private static void TIn_Elapsed(object sender, ElapsedEventArgs e)
        {
           if(msgQ.Count > 1)
            {
                Message oMsg = msgQ.Dequeue();
                Console.WriteLine(String.Format("{0} {1} {2}", oMsg.regno, oMsg.vin, oMsg.timestamp));
            }
            else
            {
                Console.WriteLine(" ************** NO MESSAGE  ************** ");
            }

        }
    }
    class Message
    {
        public string vin { get; set; }
        public string regno { get; set; }
        public string timestamp { get; set; }
    }
    
    class XmlMessage
    {
        public static string _value
        {
            get {
                return @"<?xml version='1.0' encoding='UTF-8'?>
                          <vehicleread>  
                                          <vin> ZFF73SKT4C0184194 </vin>  
                                          <timestamp> 1528810073403 </timestamp>  
                                          <marker> A0100000 </marker>
                          </vehicleread >";
            }
        }
    }
    
}
