﻿namespace VideoViewer
{
    partial class frmConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConfiguration));
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.gridControlCamGroup = new DevExpress.XtraGrid.GridControl();
            this.gridViewCamGroup = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnSrNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnGrp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox2 = new System.Windows.Forms.MaskedTextBox();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.txtGroupID = new System.Windows.Forms.MaskedTextBox();
            this.btnAddUpdateGroup = new DevExpress.XtraEditors.SimpleButton();
            this.btnResetGroup = new DevExpress.XtraEditors.SimpleButton();
            this.txtGroupName = new System.Windows.Forms.MaskedTextBox();
            this.btnDeleteGroup = new DevExpress.XtraEditors.SimpleButton();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.gridControlCamera = new DevExpress.XtraGrid.GridControl();
            this.gridViewCamera = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnCamera = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnServer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnMarker = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnVID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnGID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnRFID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tableLayoutPanelCamera = new System.Windows.Forms.TableLayoutPanel();
            this.panelCameraTitle = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panelCameraInputControls = new System.Windows.Forms.Panel();
            this.ddlRFiDMarker = new System.Windows.Forms.ComboBox();
            this.lblMarker = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.ddlCameraGroup = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.btnResetCamera = new DevExpress.XtraEditors.SimpleButton();
            this.ddlVMSServer = new System.Windows.Forms.ComboBox();
            this.btnDeleteCamera = new DevExpress.XtraEditors.SimpleButton();
            this.txtCameraName = new System.Windows.Forms.MaskedTextBox();
            this.btnAddUpdateCamera = new DevExpress.XtraEditors.SimpleButton();
            this.txtCameraID = new System.Windows.Forms.MaskedTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.panelVMSDDL = new System.Windows.Forms.Panel();
            this.panelCameraIP = new System.Windows.Forms.Panel();
            this.gridControlVMS = new DevExpress.XtraGrid.GridControl();
            this.gridViewVMS = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnVMSServer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnVMSIP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnVMSID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tableLayoutPanelVMSSection = new System.Windows.Forms.TableLayoutPanel();
            this.panleVMSTitle = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panelVMSInputControls = new System.Windows.Forms.Panel();
            this.txtServerIP = new System.Windows.Forms.MaskedTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtServerName = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnResetVMS = new DevExpress.XtraEditors.SimpleButton();
            this.txtServerID = new System.Windows.Forms.MaskedTextBox();
            this.btnDeleteVMS = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddUpdateVMS = new DevExpress.XtraEditors.SimpleButton();
            this.panelServerName = new System.Windows.Forms.Panel();
            this.tableLayoutPanelRFiDANPR = new System.Windows.Forms.TableLayoutPanel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.rbANPROUT = new System.Windows.Forms.RadioButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.rbANPRIN = new System.Windows.Forms.RadioButton();
            this.btnDeleteANPR = new DevExpress.XtraEditors.SimpleButton();
            this.label10 = new System.Windows.Forms.Label();
            this.txtANPRNameTag = new System.Windows.Forms.MaskedTextBox();
            this.txtANPRIP = new System.Windows.Forms.MaskedTextBox();
            this.btnResetANPR = new DevExpress.XtraEditors.SimpleButton();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAddUpdateANPR = new DevExpress.XtraEditors.SimpleButton();
            this.panel11 = new System.Windows.Forms.Panel();
            this.txtANPRID = new System.Windows.Forms.MaskedTextBox();
            this.panelRFiD = new System.Windows.Forms.Panel();
            this.btnResetMarker = new DevExpress.XtraEditors.SimpleButton();
            this.rbOUT = new System.Windows.Forms.RadioButton();
            this.btnDeleteMarker = new DevExpress.XtraEditors.SimpleButton();
            this.txtRFiDID = new System.Windows.Forms.MaskedTextBox();
            this.btnAddUpdateMarker = new DevExpress.XtraEditors.SimpleButton();
            this.rbIN = new System.Windows.Forms.RadioButton();
            this.txtRFiDMarker = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanelFRiDANPRGrid = new System.Windows.Forms.TableLayoutPanel();
            this.gridControlMarker = new DevExpress.XtraGrid.GridControl();
            this.gridViewMarker = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDirection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarker = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlANPR = new DevExpress.XtraGrid.GridControl();
            this.gridViewANPR = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTag = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.tableLayoutPanelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCamGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCamGroup)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCamera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCamera)).BeginInit();
            this.tableLayoutPanelCamera.SuspendLayout();
            this.panelCameraTitle.SuspendLayout();
            this.panelCameraInputControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVMS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVMS)).BeginInit();
            this.tableLayoutPanelVMSSection.SuspendLayout();
            this.panleVMSTitle.SuspendLayout();
            this.panelVMSInputControls.SuspendLayout();
            this.tableLayoutPanelRFiDANPR.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panelRFiD.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanelFRiDANPRGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMarker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMarker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlANPR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewANPR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelMain.ColumnCount = 4;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 397F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 403F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.gridControlCamGroup, 3, 0);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanel1, 2, 0);
            this.tableLayoutPanelMain.Controls.Add(this.gridControlCamera, 3, 1);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelCamera, 2, 1);
            this.tableLayoutPanelMain.Controls.Add(this.gridControlVMS, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelVMSSection, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelRFiDANPR, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelFRiDANPRGrid, 1, 1);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 2;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1829, 1028);
            this.tableLayoutPanelMain.TabIndex = 0;
            // 
            // gridControlCamGroup
            // 
            this.gridControlCamGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlCamGroup.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlCamGroup.Location = new System.Drawing.Point(1203, 4);
            this.gridControlCamGroup.MainView = this.gridViewCamGroup;
            this.gridControlCamGroup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlCamGroup.Name = "gridControlCamGroup";
            this.gridControlCamGroup.Size = new System.Drawing.Size(623, 351);
            this.gridControlCamGroup.TabIndex = 137;
            this.gridControlCamGroup.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCamGroup});
            // 
            // gridViewCamGroup
            // 
            this.gridViewCamGroup.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.DetailTip.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.DetailTip.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.Empty.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.Empty.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.gridViewCamGroup.Appearance.EvenRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridViewCamGroup.Appearance.EvenRow.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.FilterCloseButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.FilterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.FilterPanel.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.FixedLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.FixedLine.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.FocusedCell.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.FocusedCell.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.gridViewCamGroup.Appearance.FocusedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridViewCamGroup.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewCamGroup.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewCamGroup.Appearance.FooterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.GroupButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.GroupButton.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.GroupFooter.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.GroupFooter.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.GroupPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.GroupRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.HeaderPanel.Font = new System.Drawing.Font("Calibri", 12F);
            this.gridViewCamGroup.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.HorzLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.HorzLine.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.gridViewCamGroup.Appearance.OddRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewCamGroup.Appearance.OddRow.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.Preview.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.Preview.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.Row.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.Row.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.RowSeparator.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.RowSeparator.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.SelectedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.SelectedRow.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.TopNewRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.TopNewRow.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.VertLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.VertLine.Options.UseFont = true;
            this.gridViewCamGroup.Appearance.ViewCaption.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamGroup.Appearance.ViewCaption.Options.UseFont = true;
            this.gridViewCamGroup.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnSrNo,
            this.gridColumnGrp,
            this.gridColumnID});
            this.gridViewCamGroup.GridControl = this.gridControlCamGroup;
            this.gridViewCamGroup.Name = "gridViewCamGroup";
            this.gridViewCamGroup.OptionsView.EnableAppearanceOddRow = true;
            this.gridViewCamGroup.OptionsView.ShowGroupPanel = false;
            this.gridViewCamGroup.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridViewCamGroup_RowClick);
            // 
            // gridColumnSrNo
            // 
            this.gridColumnSrNo.Caption = "Sr.#";
            this.gridColumnSrNo.FieldName = "ROWID";
            this.gridColumnSrNo.MinWidth = 50;
            this.gridColumnSrNo.Name = "gridColumnSrNo";
            this.gridColumnSrNo.OptionsColumn.AllowEdit = false;
            this.gridColumnSrNo.Visible = true;
            this.gridColumnSrNo.VisibleIndex = 0;
            this.gridColumnSrNo.Width = 50;
            // 
            // gridColumnGrp
            // 
            this.gridColumnGrp.Caption = "Group";
            this.gridColumnGrp.FieldName = "Name";
            this.gridColumnGrp.MinWidth = 350;
            this.gridColumnGrp.Name = "gridColumnGrp";
            this.gridColumnGrp.OptionsColumn.AllowEdit = false;
            this.gridColumnGrp.Visible = true;
            this.gridColumnGrp.VisibleIndex = 1;
            this.gridColumnGrp.Width = 350;
            // 
            // gridColumnID
            // 
            this.gridColumnID.Caption = "ID";
            this.gridColumnID.FieldName = "ID";
            this.gridColumnID.Name = "gridColumnID";
            this.gridColumnID.OptionsColumn.AllowEdit = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel13, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel15, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel18, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(803, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(394, 353);
            this.tableLayoutPanel1.TabIndex = 136;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panel13.Controls.Add(this.label12);
            this.panel13.Controls.Add(this.simpleButton1);
            this.panel13.Controls.Add(this.maskedTextBox1);
            this.panel13.Controls.Add(this.maskedTextBox2);
            this.panel13.Controls.Add(this.simpleButton2);
            this.panel13.Controls.Add(this.simpleButton3);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Location = new System.Drawing.Point(3, 403);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(385, 256);
            this.panel13.TabIndex = 138;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(111, 17);
            this.label12.TabIndex = 222;
            this.label12.Text = "Marker ID / Name";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.simpleButton1.Appearance.ForeColor = System.Drawing.Color.White;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Appearance.Options.UseForeColor = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(219, 125);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(86, 30);
            this.simpleButton1.TabIndex = 236;
            this.simpleButton1.Text = "Reset";
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.maskedTextBox1.Location = new System.Drawing.Point(20, 65);
            this.maskedTextBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(275, 17);
            this.maskedTextBox1.TabIndex = 226;
            this.maskedTextBox1.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // maskedTextBox2
            // 
            this.maskedTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.maskedTextBox2.Location = new System.Drawing.Point(256, 27);
            this.maskedTextBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.maskedTextBox2.Name = "maskedTextBox2";
            this.maskedTextBox2.Size = new System.Drawing.Size(39, 17);
            this.maskedTextBox2.TabIndex = 223;
            this.maskedTextBox2.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.simpleButton2.Appearance.ForeColor = System.Drawing.Color.White;
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Appearance.Options.UseForeColor = true;
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(118, 125);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(86, 30);
            this.simpleButton2.TabIndex = 234;
            this.simpleButton2.Text = "Delete";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.simpleButton3.Appearance.ForeColor = System.Drawing.Color.White;
            this.simpleButton3.Appearance.Options.UseBackColor = true;
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Appearance.Options.UseForeColor = true;
            this.simpleButton3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.simpleButton3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.ImageOptions.Image")));
            this.simpleButton3.Location = new System.Drawing.Point(17, 125);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(86, 30);
            this.simpleButton3.TabIndex = 235;
            this.simpleButton3.Text = "Add / Edit";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.White;
            this.panel14.Location = new System.Drawing.Point(13, 58);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(288, 35);
            this.panel14.TabIndex = 219;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panel15.Controls.Add(this.label14);
            this.panel15.Controls.Add(this.txtGroupID);
            this.panel15.Controls.Add(this.btnAddUpdateGroup);
            this.panel15.Controls.Add(this.btnResetGroup);
            this.panel15.Controls.Add(this.txtGroupName);
            this.panel15.Controls.Add(this.btnDeleteGroup);
            this.panel15.Controls.Add(this.panel17);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(3, 53);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(388, 294);
            this.panel15.TabIndex = 136;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 17);
            this.label14.TabIndex = 222;
            this.label14.Text = "Group Name";
            // 
            // txtGroupID
            // 
            this.txtGroupID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtGroupID.Location = new System.Drawing.Point(258, 22);
            this.txtGroupID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtGroupID.Name = "txtGroupID";
            this.txtGroupID.Size = new System.Drawing.Size(39, 17);
            this.txtGroupID.TabIndex = 223;
            this.txtGroupID.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtGroupID.Visible = false;
            // 
            // btnAddUpdateGroup
            // 
            this.btnAddUpdateGroup.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnAddUpdateGroup.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnAddUpdateGroup.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnAddUpdateGroup.Appearance.Options.UseBackColor = true;
            this.btnAddUpdateGroup.Appearance.Options.UseFont = true;
            this.btnAddUpdateGroup.Appearance.Options.UseForeColor = true;
            this.btnAddUpdateGroup.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnAddUpdateGroup.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddUpdateGroup.ImageOptions.Image")));
            this.btnAddUpdateGroup.Location = new System.Drawing.Point(17, 195);
            this.btnAddUpdateGroup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddUpdateGroup.Name = "btnAddUpdateGroup";
            this.btnAddUpdateGroup.Size = new System.Drawing.Size(86, 30);
            this.btnAddUpdateGroup.TabIndex = 515;
            this.btnAddUpdateGroup.Text = "Add / Edit";
            this.btnAddUpdateGroup.Click += new System.EventHandler(this.btnAddUpdateGroup_Click);
            // 
            // btnResetGroup
            // 
            this.btnResetGroup.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnResetGroup.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnResetGroup.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnResetGroup.Appearance.Options.UseBackColor = true;
            this.btnResetGroup.Appearance.Options.UseFont = true;
            this.btnResetGroup.Appearance.Options.UseForeColor = true;
            this.btnResetGroup.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnResetGroup.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnResetGroup.ImageOptions.Image")));
            this.btnResetGroup.Location = new System.Drawing.Point(219, 195);
            this.btnResetGroup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnResetGroup.Name = "btnResetGroup";
            this.btnResetGroup.Size = new System.Drawing.Size(86, 30);
            this.btnResetGroup.TabIndex = 517;
            this.btnResetGroup.Text = "Reset";
            this.btnResetGroup.Click += new System.EventHandler(this.btnResetGroup_Click);
            // 
            // txtGroupName
            // 
            this.txtGroupName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtGroupName.Location = new System.Drawing.Point(22, 62);
            this.txtGroupName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtGroupName.Name = "txtGroupName";
            this.txtGroupName.Size = new System.Drawing.Size(275, 17);
            this.txtGroupName.TabIndex = 514;
            this.txtGroupName.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // btnDeleteGroup
            // 
            this.btnDeleteGroup.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnDeleteGroup.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnDeleteGroup.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnDeleteGroup.Appearance.Options.UseBackColor = true;
            this.btnDeleteGroup.Appearance.Options.UseFont = true;
            this.btnDeleteGroup.Appearance.Options.UseForeColor = true;
            this.btnDeleteGroup.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnDeleteGroup.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteGroup.ImageOptions.Image")));
            this.btnDeleteGroup.Location = new System.Drawing.Point(118, 195);
            this.btnDeleteGroup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDeleteGroup.Name = "btnDeleteGroup";
            this.btnDeleteGroup.Size = new System.Drawing.Size(86, 30);
            this.btnDeleteGroup.TabIndex = 516;
            this.btnDeleteGroup.Text = "Delete";
            this.btnDeleteGroup.Click += new System.EventHandler(this.btnDeleteGroup_Click);
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.White;
            this.panel17.Location = new System.Drawing.Point(15, 53);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(288, 35);
            this.panel17.TabIndex = 219;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panel18.Controls.Add(this.label15);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.ForeColor = System.Drawing.Color.White;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Margin = new System.Windows.Forms.Padding(0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(394, 50);
            this.panel18.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(13, 14);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(103, 17);
            this.label15.TabIndex = 2;
            this.label15.Text = "CAMERA GROUP";
            // 
            // gridControlCamera
            // 
            this.gridControlCamera.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlCamera.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlCamera.Location = new System.Drawing.Point(1203, 363);
            this.gridControlCamera.MainView = this.gridViewCamera;
            this.gridControlCamera.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlCamera.Name = "gridControlCamera";
            this.gridControlCamera.Size = new System.Drawing.Size(623, 661);
            this.gridControlCamera.TabIndex = 134;
            this.gridControlCamera.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCamera});
            // 
            // gridViewCamera
            // 
            this.gridViewCamera.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridViewCamera.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridViewCamera.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridViewCamera.Appearance.DetailTip.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.DetailTip.Options.UseFont = true;
            this.gridViewCamera.Appearance.Empty.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.Empty.Options.UseFont = true;
            this.gridViewCamera.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.gridViewCamera.Appearance.EvenRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridViewCamera.Appearance.EvenRow.Options.UseFont = true;
            this.gridViewCamera.Appearance.FilterCloseButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridViewCamera.Appearance.FilterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.FilterPanel.Options.UseFont = true;
            this.gridViewCamera.Appearance.FixedLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.FixedLine.Options.UseFont = true;
            this.gridViewCamera.Appearance.FocusedCell.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.FocusedCell.Options.UseFont = true;
            this.gridViewCamera.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.gridViewCamera.Appearance.FocusedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridViewCamera.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewCamera.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewCamera.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewCamera.Appearance.FooterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewCamera.Appearance.GroupButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.GroupButton.Options.UseFont = true;
            this.gridViewCamera.Appearance.GroupFooter.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.GroupFooter.Options.UseFont = true;
            this.gridViewCamera.Appearance.GroupPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewCamera.Appearance.GroupRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewCamera.Appearance.HeaderPanel.Font = new System.Drawing.Font("Calibri", 12F);
            this.gridViewCamera.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewCamera.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridViewCamera.Appearance.HorzLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.HorzLine.Options.UseFont = true;
            this.gridViewCamera.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.gridViewCamera.Appearance.OddRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewCamera.Appearance.OddRow.Options.UseFont = true;
            this.gridViewCamera.Appearance.Preview.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.Preview.Options.UseFont = true;
            this.gridViewCamera.Appearance.Row.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.Row.Options.UseFont = true;
            this.gridViewCamera.Appearance.RowSeparator.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.RowSeparator.Options.UseFont = true;
            this.gridViewCamera.Appearance.SelectedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.SelectedRow.Options.UseFont = true;
            this.gridViewCamera.Appearance.TopNewRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.TopNewRow.Options.UseFont = true;
            this.gridViewCamera.Appearance.VertLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.VertLine.Options.UseFont = true;
            this.gridViewCamera.Appearance.ViewCaption.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewCamera.Appearance.ViewCaption.Options.UseFont = true;
            this.gridViewCamera.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnCamera,
            this.gridColumnServer,
            this.gridColumnGroup,
            this.gridColumnMarker,
            this.gridColumnCID,
            this.gridColumnVID,
            this.gridColumnGID,
            this.gridColumnRFID});
            this.gridViewCamera.GridControl = this.gridControlCamera;
            this.gridViewCamera.Name = "gridViewCamera";
            this.gridViewCamera.OptionsView.EnableAppearanceOddRow = true;
            this.gridViewCamera.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridViewCamera_RowClick);
            // 
            // gridColumnCamera
            // 
            this.gridColumnCamera.Caption = "Camera";
            this.gridColumnCamera.FieldName = "Camera";
            this.gridColumnCamera.MinWidth = 150;
            this.gridColumnCamera.Name = "gridColumnCamera";
            this.gridColumnCamera.OptionsColumn.AllowEdit = false;
            this.gridColumnCamera.Visible = true;
            this.gridColumnCamera.VisibleIndex = 0;
            this.gridColumnCamera.Width = 206;
            // 
            // gridColumnServer
            // 
            this.gridColumnServer.Caption = "Server";
            this.gridColumnServer.FieldName = "Server";
            this.gridColumnServer.MaxWidth = 200;
            this.gridColumnServer.MinWidth = 75;
            this.gridColumnServer.Name = "gridColumnServer";
            this.gridColumnServer.OptionsColumn.AllowEdit = false;
            this.gridColumnServer.Visible = true;
            this.gridColumnServer.VisibleIndex = 1;
            this.gridColumnServer.Width = 104;
            // 
            // gridColumnGroup
            // 
            this.gridColumnGroup.Caption = "Group";
            this.gridColumnGroup.FieldName = "Group";
            this.gridColumnGroup.MaxWidth = 200;
            this.gridColumnGroup.MinWidth = 75;
            this.gridColumnGroup.Name = "gridColumnGroup";
            this.gridColumnGroup.OptionsColumn.AllowEdit = false;
            this.gridColumnGroup.Visible = true;
            this.gridColumnGroup.VisibleIndex = 2;
            this.gridColumnGroup.Width = 200;
            // 
            // gridColumnMarker
            // 
            this.gridColumnMarker.Caption = "Marker";
            this.gridColumnMarker.FieldName = "Marker";
            this.gridColumnMarker.MaxWidth = 200;
            this.gridColumnMarker.MinWidth = 75;
            this.gridColumnMarker.Name = "gridColumnMarker";
            this.gridColumnMarker.OptionsColumn.AllowEdit = false;
            this.gridColumnMarker.Visible = true;
            this.gridColumnMarker.VisibleIndex = 3;
            this.gridColumnMarker.Width = 95;
            // 
            // gridColumnCID
            // 
            this.gridColumnCID.Caption = "CID";
            this.gridColumnCID.FieldName = "ID";
            this.gridColumnCID.Name = "gridColumnCID";
            this.gridColumnCID.Width = 20;
            // 
            // gridColumnVID
            // 
            this.gridColumnVID.Caption = "VMSID";
            this.gridColumnVID.FieldName = "ServerId";
            this.gridColumnVID.Name = "gridColumnVID";
            this.gridColumnVID.Width = 20;
            // 
            // gridColumnGID
            // 
            this.gridColumnGID.Caption = "GroupID";
            this.gridColumnGID.FieldName = "GroupID";
            this.gridColumnGID.Name = "gridColumnGID";
            // 
            // gridColumnRFID
            // 
            this.gridColumnRFID.Caption = "RFID";
            this.gridColumnRFID.FieldName = "RFID";
            this.gridColumnRFID.Name = "gridColumnRFID";
            // 
            // tableLayoutPanelCamera
            // 
            this.tableLayoutPanelCamera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.tableLayoutPanelCamera.ColumnCount = 1;
            this.tableLayoutPanelCamera.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelCamera.Controls.Add(this.panelCameraTitle, 0, 0);
            this.tableLayoutPanelCamera.Controls.Add(this.panelCameraInputControls, 0, 1);
            this.tableLayoutPanelCamera.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelCamera.Location = new System.Drawing.Point(803, 363);
            this.tableLayoutPanelCamera.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanelCamera.Name = "tableLayoutPanelCamera";
            this.tableLayoutPanelCamera.RowCount = 2;
            this.tableLayoutPanelCamera.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelCamera.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelCamera.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelCamera.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelCamera.Size = new System.Drawing.Size(394, 661);
            this.tableLayoutPanelCamera.TabIndex = 132;
            // 
            // panelCameraTitle
            // 
            this.panelCameraTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panelCameraTitle.Controls.Add(this.label2);
            this.panelCameraTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCameraTitle.ForeColor = System.Drawing.Color.White;
            this.panelCameraTitle.Location = new System.Drawing.Point(0, 0);
            this.panelCameraTitle.Margin = new System.Windows.Forms.Padding(0);
            this.panelCameraTitle.Name = "panelCameraTitle";
            this.panelCameraTitle.Size = new System.Drawing.Size(394, 50);
            this.panelCameraTitle.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(13, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "CAMERA";
            // 
            // panelCameraInputControls
            // 
            this.panelCameraInputControls.Controls.Add(this.ddlRFiDMarker);
            this.panelCameraInputControls.Controls.Add(this.lblMarker);
            this.panelCameraInputControls.Controls.Add(this.panel8);
            this.panelCameraInputControls.Controls.Add(this.ddlCameraGroup);
            this.panelCameraInputControls.Controls.Add(this.label9);
            this.panelCameraInputControls.Controls.Add(this.panel5);
            this.panelCameraInputControls.Controls.Add(this.label20);
            this.panelCameraInputControls.Controls.Add(this.btnResetCamera);
            this.panelCameraInputControls.Controls.Add(this.ddlVMSServer);
            this.panelCameraInputControls.Controls.Add(this.btnDeleteCamera);
            this.panelCameraInputControls.Controls.Add(this.txtCameraName);
            this.panelCameraInputControls.Controls.Add(this.btnAddUpdateCamera);
            this.panelCameraInputControls.Controls.Add(this.txtCameraID);
            this.panelCameraInputControls.Controls.Add(this.label23);
            this.panelCameraInputControls.Controls.Add(this.panelVMSDDL);
            this.panelCameraInputControls.Controls.Add(this.panelCameraIP);
            this.panelCameraInputControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCameraInputControls.Location = new System.Drawing.Point(3, 53);
            this.panelCameraInputControls.Name = "panelCameraInputControls";
            this.panelCameraInputControls.Size = new System.Drawing.Size(388, 605);
            this.panelCameraInputControls.TabIndex = 2;
            // 
            // ddlRFiDMarker
            // 
            this.ddlRFiDMarker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlRFiDMarker.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ddlRFiDMarker.FormattingEnabled = true;
            this.ddlRFiDMarker.Location = new System.Drawing.Point(23, 276);
            this.ddlRFiDMarker.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ddlRFiDMarker.Name = "ddlRFiDMarker";
            this.ddlRFiDMarker.Size = new System.Drawing.Size(275, 23);
            this.ddlRFiDMarker.TabIndex = 521;
            // 
            // lblMarker
            // 
            this.lblMarker.AutoSize = true;
            this.lblMarker.Location = new System.Drawing.Point(16, 242);
            this.lblMarker.Name = "lblMarker";
            this.lblMarker.Size = new System.Drawing.Size(79, 17);
            this.lblMarker.TabIndex = 234;
            this.lblMarker.Text = "RFiD Marker";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.Location = new System.Drawing.Point(16, 270);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(288, 35);
            this.panel8.TabIndex = 233;
            // 
            // ddlCameraGroup
            // 
            this.ddlCameraGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCameraGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ddlCameraGroup.FormattingEnabled = true;
            this.ddlCameraGroup.Location = new System.Drawing.Point(23, 200);
            this.ddlCameraGroup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ddlCameraGroup.Name = "ddlCameraGroup";
            this.ddlCameraGroup.Size = new System.Drawing.Size(275, 23);
            this.ddlCameraGroup.TabIndex = 520;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 166);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 17);
            this.label9.TabIndex = 231;
            this.label9.Text = "Camera Group";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Location = new System.Drawing.Point(16, 194);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(288, 35);
            this.panel5.TabIndex = 230;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(13, 15);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(89, 17);
            this.label20.TabIndex = 222;
            this.label20.Text = "Camera Name";
            // 
            // btnResetCamera
            // 
            this.btnResetCamera.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnResetCamera.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnResetCamera.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnResetCamera.Appearance.Options.UseBackColor = true;
            this.btnResetCamera.Appearance.Options.UseFont = true;
            this.btnResetCamera.Appearance.Options.UseForeColor = true;
            this.btnResetCamera.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnResetCamera.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnResetCamera.ImageOptions.Image")));
            this.btnResetCamera.Location = new System.Drawing.Point(220, 352);
            this.btnResetCamera.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnResetCamera.Name = "btnResetCamera";
            this.btnResetCamera.Size = new System.Drawing.Size(86, 30);
            this.btnResetCamera.TabIndex = 526;
            this.btnResetCamera.Text = "Reset";
            this.btnResetCamera.Click += new System.EventHandler(this.btnResetCamera_Click);
            // 
            // ddlVMSServer
            // 
            this.ddlVMSServer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlVMSServer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ddlVMSServer.FormattingEnabled = true;
            this.ddlVMSServer.Location = new System.Drawing.Point(21, 125);
            this.ddlVMSServer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ddlVMSServer.Name = "ddlVMSServer";
            this.ddlVMSServer.Size = new System.Drawing.Size(275, 23);
            this.ddlVMSServer.TabIndex = 519;
            // 
            // btnDeleteCamera
            // 
            this.btnDeleteCamera.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnDeleteCamera.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnDeleteCamera.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnDeleteCamera.Appearance.Options.UseBackColor = true;
            this.btnDeleteCamera.Appearance.Options.UseFont = true;
            this.btnDeleteCamera.Appearance.Options.UseForeColor = true;
            this.btnDeleteCamera.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnDeleteCamera.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteCamera.ImageOptions.Image")));
            this.btnDeleteCamera.Location = new System.Drawing.Point(119, 352);
            this.btnDeleteCamera.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDeleteCamera.Name = "btnDeleteCamera";
            this.btnDeleteCamera.Size = new System.Drawing.Size(86, 30);
            this.btnDeleteCamera.TabIndex = 525;
            this.btnDeleteCamera.Text = "Delete";
            this.btnDeleteCamera.Click += new System.EventHandler(this.btnDeleteCamera_Click);
            // 
            // txtCameraName
            // 
            this.txtCameraName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCameraName.Location = new System.Drawing.Point(20, 53);
            this.txtCameraName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCameraName.Name = "txtCameraName";
            this.txtCameraName.Size = new System.Drawing.Size(275, 17);
            this.txtCameraName.TabIndex = 518;
            this.txtCameraName.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // btnAddUpdateCamera
            // 
            this.btnAddUpdateCamera.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnAddUpdateCamera.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnAddUpdateCamera.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnAddUpdateCamera.Appearance.Options.UseBackColor = true;
            this.btnAddUpdateCamera.Appearance.Options.UseFont = true;
            this.btnAddUpdateCamera.Appearance.Options.UseForeColor = true;
            this.btnAddUpdateCamera.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnAddUpdateCamera.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddUpdateCamera.ImageOptions.Image")));
            this.btnAddUpdateCamera.Location = new System.Drawing.Point(18, 352);
            this.btnAddUpdateCamera.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddUpdateCamera.Name = "btnAddUpdateCamera";
            this.btnAddUpdateCamera.Size = new System.Drawing.Size(86, 30);
            this.btnAddUpdateCamera.TabIndex = 524;
            this.btnAddUpdateCamera.Text = "Add / Edit";
            this.btnAddUpdateCamera.Click += new System.EventHandler(this.btnAddUpdateCamera_Click);
            // 
            // txtCameraID
            // 
            this.txtCameraID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCameraID.Location = new System.Drawing.Point(256, 13);
            this.txtCameraID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCameraID.Name = "txtCameraID";
            this.txtCameraID.Size = new System.Drawing.Size(39, 17);
            this.txtCameraID.TabIndex = 223;
            this.txtCameraID.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtCameraID.Visible = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(14, 91);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(73, 17);
            this.label23.TabIndex = 225;
            this.label23.Text = "VMS Server";
            // 
            // panelVMSDDL
            // 
            this.panelVMSDDL.BackColor = System.Drawing.Color.White;
            this.panelVMSDDL.Location = new System.Drawing.Point(14, 119);
            this.panelVMSDDL.Name = "panelVMSDDL";
            this.panelVMSDDL.Size = new System.Drawing.Size(288, 35);
            this.panelVMSDDL.TabIndex = 219;
            // 
            // panelCameraIP
            // 
            this.panelCameraIP.BackColor = System.Drawing.Color.White;
            this.panelCameraIP.Location = new System.Drawing.Point(13, 44);
            this.panelCameraIP.Name = "panelCameraIP";
            this.panelCameraIP.Size = new System.Drawing.Size(288, 35);
            this.panelCameraIP.TabIndex = 219;
            // 
            // gridControlVMS
            // 
            this.gridControlVMS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlVMS.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlVMS.Location = new System.Drawing.Point(400, 4);
            this.gridControlVMS.MainView = this.gridViewVMS;
            this.gridControlVMS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlVMS.Name = "gridControlVMS";
            this.gridControlVMS.Size = new System.Drawing.Size(397, 351);
            this.gridControlVMS.TabIndex = 126;
            this.gridControlVMS.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewVMS});
            // 
            // gridViewVMS
            // 
            this.gridViewVMS.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridViewVMS.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridViewVMS.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridViewVMS.Appearance.DetailTip.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.DetailTip.Options.UseFont = true;
            this.gridViewVMS.Appearance.Empty.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.Empty.Options.UseFont = true;
            this.gridViewVMS.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.gridViewVMS.Appearance.EvenRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridViewVMS.Appearance.EvenRow.Options.UseFont = true;
            this.gridViewVMS.Appearance.FilterCloseButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridViewVMS.Appearance.FilterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.FilterPanel.Options.UseFont = true;
            this.gridViewVMS.Appearance.FixedLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.FixedLine.Options.UseFont = true;
            this.gridViewVMS.Appearance.FocusedCell.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.FocusedCell.Options.UseFont = true;
            this.gridViewVMS.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.gridViewVMS.Appearance.FocusedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridViewVMS.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewVMS.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewVMS.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewVMS.Appearance.FooterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewVMS.Appearance.GroupButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.GroupButton.Options.UseFont = true;
            this.gridViewVMS.Appearance.GroupFooter.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.GroupFooter.Options.UseFont = true;
            this.gridViewVMS.Appearance.GroupPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewVMS.Appearance.GroupRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewVMS.Appearance.HeaderPanel.Font = new System.Drawing.Font("Calibri", 12F);
            this.gridViewVMS.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewVMS.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridViewVMS.Appearance.HorzLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.HorzLine.Options.UseFont = true;
            this.gridViewVMS.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.gridViewVMS.Appearance.OddRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewVMS.Appearance.OddRow.Options.UseFont = true;
            this.gridViewVMS.Appearance.Preview.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.Preview.Options.UseFont = true;
            this.gridViewVMS.Appearance.Row.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.Row.Options.UseFont = true;
            this.gridViewVMS.Appearance.RowSeparator.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.RowSeparator.Options.UseFont = true;
            this.gridViewVMS.Appearance.SelectedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.SelectedRow.Options.UseFont = true;
            this.gridViewVMS.Appearance.TopNewRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.TopNewRow.Options.UseFont = true;
            this.gridViewVMS.Appearance.VertLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.VertLine.Options.UseFont = true;
            this.gridViewVMS.Appearance.ViewCaption.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVMS.Appearance.ViewCaption.Options.UseFont = true;
            this.gridViewVMS.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnVMSServer,
            this.gridColumnVMSIP,
            this.gridColumnVMSID});
            this.gridViewVMS.GridControl = this.gridControlVMS;
            this.gridViewVMS.Name = "gridViewVMS";
            this.gridViewVMS.OptionsView.EnableAppearanceOddRow = true;
            this.gridViewVMS.OptionsView.ShowGroupPanel = false;
            this.gridViewVMS.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridViewVMS_RowClick);
            // 
            // gridColumnVMSServer
            // 
            this.gridColumnVMSServer.Caption = "Server";
            this.gridColumnVMSServer.FieldName = "ServerName";
            this.gridColumnVMSServer.MinWidth = 200;
            this.gridColumnVMSServer.Name = "gridColumnVMSServer";
            this.gridColumnVMSServer.OptionsColumn.AllowEdit = false;
            this.gridColumnVMSServer.Visible = true;
            this.gridColumnVMSServer.VisibleIndex = 0;
            this.gridColumnVMSServer.Width = 200;
            // 
            // gridColumnVMSIP
            // 
            this.gridColumnVMSIP.Caption = "IP";
            this.gridColumnVMSIP.FieldName = "IP";
            this.gridColumnVMSIP.MinWidth = 200;
            this.gridColumnVMSIP.Name = "gridColumnVMSIP";
            this.gridColumnVMSIP.OptionsColumn.AllowEdit = false;
            this.gridColumnVMSIP.Visible = true;
            this.gridColumnVMSIP.VisibleIndex = 1;
            this.gridColumnVMSIP.Width = 200;
            // 
            // gridColumnVMSID
            // 
            this.gridColumnVMSID.Caption = "ID";
            this.gridColumnVMSID.FieldName = "ID";
            this.gridColumnVMSID.Name = "gridColumnVMSID";
            this.gridColumnVMSID.OptionsColumn.AllowEdit = false;
            // 
            // tableLayoutPanelVMSSection
            // 
            this.tableLayoutPanelVMSSection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.tableLayoutPanelVMSSection.ColumnCount = 1;
            this.tableLayoutPanelVMSSection.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelVMSSection.Controls.Add(this.panleVMSTitle, 0, 0);
            this.tableLayoutPanelVMSSection.Controls.Add(this.panelVMSInputControls, 0, 1);
            this.tableLayoutPanelVMSSection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelVMSSection.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelVMSSection.Name = "tableLayoutPanelVMSSection";
            this.tableLayoutPanelVMSSection.RowCount = 2;
            this.tableLayoutPanelVMSSection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelVMSSection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelVMSSection.Size = new System.Drawing.Size(391, 353);
            this.tableLayoutPanelVMSSection.TabIndex = 4;
            // 
            // panleVMSTitle
            // 
            this.panleVMSTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panleVMSTitle.Controls.Add(this.label1);
            this.panleVMSTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panleVMSTitle.ForeColor = System.Drawing.Color.White;
            this.panleVMSTitle.Location = new System.Drawing.Point(0, 0);
            this.panleVMSTitle.Margin = new System.Windows.Forms.Padding(0);
            this.panleVMSTitle.Name = "panleVMSTitle";
            this.panleVMSTitle.Size = new System.Drawing.Size(391, 50);
            this.panleVMSTitle.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(13, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "VMS SERVER";
            // 
            // panelVMSInputControls
            // 
            this.panelVMSInputControls.Controls.Add(this.txtServerIP);
            this.panelVMSInputControls.Controls.Add(this.panel1);
            this.panelVMSInputControls.Controls.Add(this.txtServerName);
            this.panelVMSInputControls.Controls.Add(this.label3);
            this.panelVMSInputControls.Controls.Add(this.label16);
            this.panelVMSInputControls.Controls.Add(this.btnResetVMS);
            this.panelVMSInputControls.Controls.Add(this.txtServerID);
            this.panelVMSInputControls.Controls.Add(this.btnDeleteVMS);
            this.panelVMSInputControls.Controls.Add(this.btnAddUpdateVMS);
            this.panelVMSInputControls.Controls.Add(this.panelServerName);
            this.panelVMSInputControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelVMSInputControls.Location = new System.Drawing.Point(3, 53);
            this.panelVMSInputControls.Name = "panelVMSInputControls";
            this.panelVMSInputControls.Size = new System.Drawing.Size(385, 297);
            this.panelVMSInputControls.TabIndex = 1;
            // 
            // txtServerIP
            // 
            this.txtServerIP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtServerIP.Location = new System.Drawing.Point(21, 135);
            this.txtServerIP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtServerIP.Name = "txtServerIP";
            this.txtServerIP.Size = new System.Drawing.Size(275, 17);
            this.txtServerIP.TabIndex = 501;
            this.txtServerIP.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(14, 126);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(288, 35);
            this.panel1.TabIndex = 222;
            // 
            // txtServerName
            // 
            this.txtServerName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtServerName.Location = new System.Drawing.Point(20, 62);
            this.txtServerName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.Size = new System.Drawing.Size(275, 17);
            this.txtServerName.TabIndex = 500;
            this.txtServerName.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 17);
            this.label3.TabIndex = 220;
            this.label3.Text = "Server IP";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(13, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 17);
            this.label16.TabIndex = 106;
            this.label16.Text = "Server Name";
            // 
            // btnResetVMS
            // 
            this.btnResetVMS.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnResetVMS.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnResetVMS.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnResetVMS.Appearance.Options.UseBackColor = true;
            this.btnResetVMS.Appearance.Options.UseFont = true;
            this.btnResetVMS.Appearance.Options.UseForeColor = true;
            this.btnResetVMS.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnResetVMS.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnResetVMS.ImageOptions.Image")));
            this.btnResetVMS.Location = new System.Drawing.Point(216, 195);
            this.btnResetVMS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnResetVMS.Name = "btnResetVMS";
            this.btnResetVMS.Size = new System.Drawing.Size(86, 30);
            this.btnResetVMS.TabIndex = 504;
            this.btnResetVMS.Text = "Reset";
            this.btnResetVMS.Click += new System.EventHandler(this.btnResetVMS_Click);
            // 
            // txtServerID
            // 
            this.txtServerID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtServerID.Location = new System.Drawing.Point(246, 21);
            this.txtServerID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtServerID.Name = "txtServerID";
            this.txtServerID.Size = new System.Drawing.Size(39, 17);
            this.txtServerID.TabIndex = 110;
            this.txtServerID.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtServerID.Visible = false;
            // 
            // btnDeleteVMS
            // 
            this.btnDeleteVMS.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnDeleteVMS.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnDeleteVMS.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnDeleteVMS.Appearance.Options.UseBackColor = true;
            this.btnDeleteVMS.Appearance.Options.UseFont = true;
            this.btnDeleteVMS.Appearance.Options.UseForeColor = true;
            this.btnDeleteVMS.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnDeleteVMS.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteVMS.ImageOptions.Image")));
            this.btnDeleteVMS.Location = new System.Drawing.Point(115, 195);
            this.btnDeleteVMS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDeleteVMS.Name = "btnDeleteVMS";
            this.btnDeleteVMS.Size = new System.Drawing.Size(86, 30);
            this.btnDeleteVMS.TabIndex = 503;
            this.btnDeleteVMS.Text = "Delete";
            this.btnDeleteVMS.Click += new System.EventHandler(this.btnDeleteVMS_Click);
            // 
            // btnAddUpdateVMS
            // 
            this.btnAddUpdateVMS.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnAddUpdateVMS.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnAddUpdateVMS.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnAddUpdateVMS.Appearance.Options.UseBackColor = true;
            this.btnAddUpdateVMS.Appearance.Options.UseFont = true;
            this.btnAddUpdateVMS.Appearance.Options.UseForeColor = true;
            this.btnAddUpdateVMS.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnAddUpdateVMS.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddUpdateVMS.ImageOptions.Image")));
            this.btnAddUpdateVMS.Location = new System.Drawing.Point(14, 195);
            this.btnAddUpdateVMS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddUpdateVMS.Name = "btnAddUpdateVMS";
            this.btnAddUpdateVMS.Size = new System.Drawing.Size(86, 30);
            this.btnAddUpdateVMS.TabIndex = 502;
            this.btnAddUpdateVMS.Text = "Add / Edit";
            this.btnAddUpdateVMS.Click += new System.EventHandler(this.btnAddUpdateVMS_Click);
            // 
            // panelServerName
            // 
            this.panelServerName.BackColor = System.Drawing.Color.White;
            this.panelServerName.Location = new System.Drawing.Point(13, 53);
            this.panelServerName.Name = "panelServerName";
            this.panelServerName.Size = new System.Drawing.Size(288, 35);
            this.panelServerName.TabIndex = 218;
            // 
            // tableLayoutPanelRFiDANPR
            // 
            this.tableLayoutPanelRFiDANPR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.tableLayoutPanelRFiDANPR.ColumnCount = 1;
            this.tableLayoutPanelRFiDANPR.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelRFiDANPR.Controls.Add(this.panel10, 0, 2);
            this.tableLayoutPanelRFiDANPR.Controls.Add(this.panel4, 0, 3);
            this.tableLayoutPanelRFiDANPR.Controls.Add(this.panelRFiD, 0, 1);
            this.tableLayoutPanelRFiDANPR.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanelRFiDANPR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelRFiDANPR.Location = new System.Drawing.Point(3, 362);
            this.tableLayoutPanelRFiDANPR.Name = "tableLayoutPanelRFiDANPR";
            this.tableLayoutPanelRFiDANPR.RowCount = 4;
            this.tableLayoutPanelRFiDANPR.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelRFiDANPR.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanelRFiDANPR.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelRFiDANPR.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanelRFiDANPR.Size = new System.Drawing.Size(391, 663);
            this.tableLayoutPanelRFiDANPR.TabIndex = 133;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panel10.Controls.Add(this.label8);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.ForeColor = System.Drawing.Color.White;
            this.panel10.Location = new System.Drawing.Point(0, 350);
            this.panel10.Margin = new System.Windows.Forms.Padding(0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(391, 50);
            this.panel10.TabIndex = 139;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(13, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 17);
            this.label8.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panel4.Controls.Add(this.rbANPROUT);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.rbANPRIN);
            this.panel4.Controls.Add(this.btnDeleteANPR);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.txtANPRNameTag);
            this.panel4.Controls.Add(this.txtANPRIP);
            this.panel4.Controls.Add(this.btnResetANPR);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.btnAddUpdateANPR);
            this.panel4.Controls.Add(this.panel11);
            this.panel4.Controls.Add(this.txtANPRID);
            this.panel4.Location = new System.Drawing.Point(3, 403);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(385, 256);
            this.panel4.TabIndex = 138;
            // 
            // rbANPROUT
            // 
            this.rbANPROUT.AutoSize = true;
            this.rbANPROUT.Location = new System.Drawing.Point(146, 159);
            this.rbANPROUT.Name = "rbANPROUT";
            this.rbANPROUT.Size = new System.Drawing.Size(47, 21);
            this.rbANPROUT.TabIndex = 517;
            this.rbANPROUT.Text = "Exit";
            this.rbANPROUT.UseVisualStyleBackColor = true;
            this.rbANPROUT.Visible = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.Location = new System.Drawing.Point(41, 47);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(288, 35);
            this.panel6.TabIndex = 219;
            this.panel6.Visible = false;
            // 
            // rbANPRIN
            // 
            this.rbANPRIN.AutoSize = true;
            this.rbANPRIN.Checked = true;
            this.rbANPRIN.Location = new System.Drawing.Point(40, 159);
            this.rbANPRIN.Name = "rbANPRIN";
            this.rbANPRIN.Size = new System.Drawing.Size(82, 21);
            this.rbANPRIN.TabIndex = 516;
            this.rbANPRIN.TabStop = true;
            this.rbANPRIN.Text = "Enrtrance";
            this.rbANPRIN.UseVisualStyleBackColor = true;
            this.rbANPRIN.Visible = false;
            // 
            // btnDeleteANPR
            // 
            this.btnDeleteANPR.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnDeleteANPR.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnDeleteANPR.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnDeleteANPR.Appearance.Options.UseBackColor = true;
            this.btnDeleteANPR.Appearance.Options.UseFont = true;
            this.btnDeleteANPR.Appearance.Options.UseForeColor = true;
            this.btnDeleteANPR.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnDeleteANPR.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteANPR.ImageOptions.Image")));
            this.btnDeleteANPR.Location = new System.Drawing.Point(143, 200);
            this.btnDeleteANPR.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDeleteANPR.Name = "btnDeleteANPR";
            this.btnDeleteANPR.Size = new System.Drawing.Size(86, 30);
            this.btnDeleteANPR.TabIndex = 508;
            this.btnDeleteANPR.Text = "Delete";
            this.btnDeleteANPR.Visible = false;
            this.btnDeleteANPR.Click += new System.EventHandler(this.btnDeleteANPR_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(40, 91);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 17);
            this.label10.TabIndex = 231;
            this.label10.Text = "ANPR IP";
            this.label10.Visible = false;
            // 
            // txtANPRNameTag
            // 
            this.txtANPRNameTag.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtANPRNameTag.Location = new System.Drawing.Point(48, 54);
            this.txtANPRNameTag.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtANPRNameTag.Name = "txtANPRNameTag";
            this.txtANPRNameTag.Size = new System.Drawing.Size(275, 17);
            this.txtANPRNameTag.TabIndex = 505;
            this.txtANPRNameTag.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtANPRNameTag.Visible = false;
            // 
            // txtANPRIP
            // 
            this.txtANPRIP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtANPRIP.Location = new System.Drawing.Point(47, 127);
            this.txtANPRIP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtANPRIP.Name = "txtANPRIP";
            this.txtANPRIP.Size = new System.Drawing.Size(275, 17);
            this.txtANPRIP.TabIndex = 506;
            this.txtANPRIP.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtANPRIP.Visible = false;
            // 
            // btnResetANPR
            // 
            this.btnResetANPR.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnResetANPR.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnResetANPR.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnResetANPR.Appearance.Options.UseBackColor = true;
            this.btnResetANPR.Appearance.Options.UseFont = true;
            this.btnResetANPR.Appearance.Options.UseForeColor = true;
            this.btnResetANPR.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnResetANPR.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnResetANPR.ImageOptions.Image")));
            this.btnResetANPR.Location = new System.Drawing.Point(244, 200);
            this.btnResetANPR.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnResetANPR.Name = "btnResetANPR";
            this.btnResetANPR.Size = new System.Drawing.Size(86, 30);
            this.btnResetANPR.TabIndex = 509;
            this.btnResetANPR.Text = "Reset";
            this.btnResetANPR.Visible = false;
            this.btnResetANPR.Click += new System.EventHandler(this.btnResetANPR_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(41, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 17);
            this.label6.TabIndex = 222;
            this.label6.Text = "ANPR Tag (Name , Sr.#)";
            this.label6.Visible = false;
            // 
            // btnAddUpdateANPR
            // 
            this.btnAddUpdateANPR.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnAddUpdateANPR.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnAddUpdateANPR.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnAddUpdateANPR.Appearance.Options.UseBackColor = true;
            this.btnAddUpdateANPR.Appearance.Options.UseFont = true;
            this.btnAddUpdateANPR.Appearance.Options.UseForeColor = true;
            this.btnAddUpdateANPR.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnAddUpdateANPR.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddUpdateANPR.ImageOptions.Image")));
            this.btnAddUpdateANPR.Location = new System.Drawing.Point(42, 200);
            this.btnAddUpdateANPR.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddUpdateANPR.Name = "btnAddUpdateANPR";
            this.btnAddUpdateANPR.Size = new System.Drawing.Size(86, 30);
            this.btnAddUpdateANPR.TabIndex = 507;
            this.btnAddUpdateANPR.Text = "Add / Edit";
            this.btnAddUpdateANPR.Visible = false;
            this.btnAddUpdateANPR.Click += new System.EventHandler(this.btnAddUpdateANPR_Click);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.White;
            this.panel11.Location = new System.Drawing.Point(40, 120);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(288, 35);
            this.panel11.TabIndex = 230;
            this.panel11.Visible = false;
            // 
            // txtANPRID
            // 
            this.txtANPRID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtANPRID.Location = new System.Drawing.Point(284, 16);
            this.txtANPRID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtANPRID.Name = "txtANPRID";
            this.txtANPRID.Size = new System.Drawing.Size(39, 17);
            this.txtANPRID.TabIndex = 223;
            this.txtANPRID.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtANPRID.Visible = false;
            // 
            // panelRFiD
            // 
            this.panelRFiD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panelRFiD.Controls.Add(this.btnResetMarker);
            this.panelRFiD.Controls.Add(this.rbOUT);
            this.panelRFiD.Controls.Add(this.btnDeleteMarker);
            this.panelRFiD.Controls.Add(this.txtRFiDID);
            this.panelRFiD.Controls.Add(this.btnAddUpdateMarker);
            this.panelRFiD.Controls.Add(this.rbIN);
            this.panelRFiD.Controls.Add(this.txtRFiDMarker);
            this.panelRFiD.Controls.Add(this.label7);
            this.panelRFiD.Controls.Add(this.panel7);
            this.panelRFiD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRFiD.Location = new System.Drawing.Point(3, 53);
            this.panelRFiD.Name = "panelRFiD";
            this.panelRFiD.Size = new System.Drawing.Size(385, 294);
            this.panelRFiD.TabIndex = 136;
            // 
            // btnResetMarker
            // 
            this.btnResetMarker.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnResetMarker.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnResetMarker.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnResetMarker.Appearance.Options.UseBackColor = true;
            this.btnResetMarker.Appearance.Options.UseFont = true;
            this.btnResetMarker.Appearance.Options.UseForeColor = true;
            this.btnResetMarker.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnResetMarker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnResetMarker.ImageOptions.Image")));
            this.btnResetMarker.Location = new System.Drawing.Point(219, 167);
            this.btnResetMarker.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnResetMarker.Name = "btnResetMarker";
            this.btnResetMarker.Size = new System.Drawing.Size(86, 30);
            this.btnResetMarker.TabIndex = 513;
            this.btnResetMarker.Text = "Reset";
            this.btnResetMarker.Click += new System.EventHandler(this.btnResetMarker_Click);
            // 
            // rbOUT
            // 
            this.rbOUT.AutoSize = true;
            this.rbOUT.Location = new System.Drawing.Point(122, 88);
            this.rbOUT.Name = "rbOUT";
            this.rbOUT.Size = new System.Drawing.Size(47, 21);
            this.rbOUT.TabIndex = 515;
            this.rbOUT.Text = "Exit";
            this.rbOUT.UseVisualStyleBackColor = true;
            // 
            // btnDeleteMarker
            // 
            this.btnDeleteMarker.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnDeleteMarker.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnDeleteMarker.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnDeleteMarker.Appearance.Options.UseBackColor = true;
            this.btnDeleteMarker.Appearance.Options.UseFont = true;
            this.btnDeleteMarker.Appearance.Options.UseForeColor = true;
            this.btnDeleteMarker.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnDeleteMarker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteMarker.ImageOptions.Image")));
            this.btnDeleteMarker.Location = new System.Drawing.Point(118, 167);
            this.btnDeleteMarker.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDeleteMarker.Name = "btnDeleteMarker";
            this.btnDeleteMarker.Size = new System.Drawing.Size(86, 30);
            this.btnDeleteMarker.TabIndex = 512;
            this.btnDeleteMarker.Text = "Delete";
            this.btnDeleteMarker.Click += new System.EventHandler(this.btnDeleteMarker_Click);
            // 
            // txtRFiDID
            // 
            this.txtRFiDID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRFiDID.Location = new System.Drawing.Point(260, 18);
            this.txtRFiDID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRFiDID.Name = "txtRFiDID";
            this.txtRFiDID.Size = new System.Drawing.Size(39, 17);
            this.txtRFiDID.TabIndex = 223;
            this.txtRFiDID.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtRFiDID.Visible = false;
            // 
            // btnAddUpdateMarker
            // 
            this.btnAddUpdateMarker.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnAddUpdateMarker.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnAddUpdateMarker.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnAddUpdateMarker.Appearance.Options.UseBackColor = true;
            this.btnAddUpdateMarker.Appearance.Options.UseFont = true;
            this.btnAddUpdateMarker.Appearance.Options.UseForeColor = true;
            this.btnAddUpdateMarker.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnAddUpdateMarker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddUpdateMarker.ImageOptions.Image")));
            this.btnAddUpdateMarker.Location = new System.Drawing.Point(17, 167);
            this.btnAddUpdateMarker.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddUpdateMarker.Name = "btnAddUpdateMarker";
            this.btnAddUpdateMarker.Size = new System.Drawing.Size(86, 30);
            this.btnAddUpdateMarker.TabIndex = 511;
            this.btnAddUpdateMarker.Text = "Add / Edit";
            this.btnAddUpdateMarker.Click += new System.EventHandler(this.btnAddUpdateMarker_Click);
            // 
            // rbIN
            // 
            this.rbIN.AutoSize = true;
            this.rbIN.Checked = true;
            this.rbIN.Location = new System.Drawing.Point(16, 88);
            this.rbIN.Name = "rbIN";
            this.rbIN.Size = new System.Drawing.Size(82, 21);
            this.rbIN.TabIndex = 514;
            this.rbIN.TabStop = true;
            this.rbIN.Text = "Enrtrance";
            this.rbIN.UseVisualStyleBackColor = true;
            // 
            // txtRFiDMarker
            // 
            this.txtRFiDMarker.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRFiDMarker.Location = new System.Drawing.Point(24, 58);
            this.txtRFiDMarker.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRFiDMarker.Name = "txtRFiDMarker";
            this.txtRFiDMarker.Size = new System.Drawing.Size(275, 17);
            this.txtRFiDMarker.TabIndex = 510;
            this.txtRFiDMarker.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 17);
            this.label7.TabIndex = 222;
            this.label7.Text = "Marker ID / Name";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.Location = new System.Drawing.Point(17, 49);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(288, 35);
            this.panel7.TabIndex = 219;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panel3.Controls.Add(this.label5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(391, 50);
            this.panel3.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(13, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "RFID MARKER";
            // 
            // tableLayoutPanelFRiDANPRGrid
            // 
            this.tableLayoutPanelFRiDANPRGrid.ColumnCount = 1;
            this.tableLayoutPanelFRiDANPRGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelFRiDANPRGrid.Controls.Add(this.gridControlMarker, 0, 0);
            this.tableLayoutPanelFRiDANPRGrid.Controls.Add(this.gridControlANPR, 0, 1);
            this.tableLayoutPanelFRiDANPRGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelFRiDANPRGrid.Location = new System.Drawing.Point(400, 362);
            this.tableLayoutPanelFRiDANPRGrid.Name = "tableLayoutPanelFRiDANPRGrid";
            this.tableLayoutPanelFRiDANPRGrid.RowCount = 2;
            this.tableLayoutPanelFRiDANPRGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanelFRiDANPRGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelFRiDANPRGrid.Size = new System.Drawing.Size(397, 663);
            this.tableLayoutPanelFRiDANPRGrid.TabIndex = 135;
            // 
            // gridControlMarker
            // 
            this.gridControlMarker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlMarker.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlMarker.Location = new System.Drawing.Point(3, 4);
            this.gridControlMarker.MainView = this.gridViewMarker;
            this.gridControlMarker.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlMarker.Name = "gridControlMarker";
            this.gridControlMarker.Size = new System.Drawing.Size(391, 342);
            this.gridControlMarker.TabIndex = 128;
            this.gridControlMarker.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMarker});
            // 
            // gridViewMarker
            // 
            this.gridViewMarker.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridViewMarker.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridViewMarker.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridViewMarker.Appearance.DetailTip.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.DetailTip.Options.UseFont = true;
            this.gridViewMarker.Appearance.Empty.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.Empty.Options.UseFont = true;
            this.gridViewMarker.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.gridViewMarker.Appearance.EvenRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridViewMarker.Appearance.EvenRow.Options.UseFont = true;
            this.gridViewMarker.Appearance.FilterCloseButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridViewMarker.Appearance.FilterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.FilterPanel.Options.UseFont = true;
            this.gridViewMarker.Appearance.FixedLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.FixedLine.Options.UseFont = true;
            this.gridViewMarker.Appearance.FocusedCell.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.FocusedCell.Options.UseFont = true;
            this.gridViewMarker.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.gridViewMarker.Appearance.FocusedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridViewMarker.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewMarker.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewMarker.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewMarker.Appearance.FooterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewMarker.Appearance.GroupButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.GroupButton.Options.UseFont = true;
            this.gridViewMarker.Appearance.GroupFooter.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.GroupFooter.Options.UseFont = true;
            this.gridViewMarker.Appearance.GroupPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewMarker.Appearance.GroupRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewMarker.Appearance.HeaderPanel.Font = new System.Drawing.Font("Calibri", 12F);
            this.gridViewMarker.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewMarker.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridViewMarker.Appearance.HorzLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.HorzLine.Options.UseFont = true;
            this.gridViewMarker.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.gridViewMarker.Appearance.OddRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewMarker.Appearance.OddRow.Options.UseFont = true;
            this.gridViewMarker.Appearance.Preview.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.Preview.Options.UseFont = true;
            this.gridViewMarker.Appearance.Row.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.Row.Options.UseFont = true;
            this.gridViewMarker.Appearance.RowSeparator.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.RowSeparator.Options.UseFont = true;
            this.gridViewMarker.Appearance.SelectedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.SelectedRow.Options.UseFont = true;
            this.gridViewMarker.Appearance.TopNewRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.TopNewRow.Options.UseFont = true;
            this.gridViewMarker.Appearance.VertLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.VertLine.Options.UseFont = true;
            this.gridViewMarker.Appearance.ViewCaption.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewMarker.Appearance.ViewCaption.Options.UseFont = true;
            this.gridViewMarker.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDirection,
            this.colMarker,
            this.gridColumn6});
            this.gridViewMarker.GridControl = this.gridControlMarker;
            this.gridViewMarker.Name = "gridViewMarker";
            this.gridViewMarker.OptionsView.EnableAppearanceOddRow = true;
            this.gridViewMarker.OptionsView.ShowGroupPanel = false;
            this.gridViewMarker.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridViewMarker_RowClick);
            // 
            // colDirection
            // 
            this.colDirection.Caption = "Direction";
            this.colDirection.FieldName = "Description";
            this.colDirection.MinWidth = 100;
            this.colDirection.Name = "colDirection";
            this.colDirection.OptionsColumn.AllowEdit = false;
            this.colDirection.Visible = true;
            this.colDirection.VisibleIndex = 0;
            this.colDirection.Width = 100;
            // 
            // colMarker
            // 
            this.colMarker.Caption = "Marker";
            this.colMarker.FieldName = "Marker";
            this.colMarker.MinWidth = 275;
            this.colMarker.Name = "colMarker";
            this.colMarker.OptionsColumn.AllowEdit = false;
            this.colMarker.Visible = true;
            this.colMarker.VisibleIndex = 1;
            this.colMarker.Width = 275;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "ID";
            this.gridColumn6.FieldName = "ServerId";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            // 
            // gridControlANPR
            // 
            this.gridControlANPR.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlANPR.Location = new System.Drawing.Point(3, 354);
            this.gridControlANPR.MainView = this.gridViewANPR;
            this.gridControlANPR.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlANPR.Name = "gridControlANPR";
            this.gridControlANPR.Size = new System.Drawing.Size(391, 305);
            this.gridControlANPR.TabIndex = 128;
            this.gridControlANPR.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewANPR});
            this.gridControlANPR.Visible = false;
            // 
            // gridViewANPR
            // 
            this.gridViewANPR.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridViewANPR.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridViewANPR.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridViewANPR.Appearance.DetailTip.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.DetailTip.Options.UseFont = true;
            this.gridViewANPR.Appearance.Empty.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.Empty.Options.UseFont = true;
            this.gridViewANPR.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.gridViewANPR.Appearance.EvenRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridViewANPR.Appearance.EvenRow.Options.UseFont = true;
            this.gridViewANPR.Appearance.FilterCloseButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridViewANPR.Appearance.FilterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.FilterPanel.Options.UseFont = true;
            this.gridViewANPR.Appearance.FixedLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.FixedLine.Options.UseFont = true;
            this.gridViewANPR.Appearance.FocusedCell.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.FocusedCell.Options.UseFont = true;
            this.gridViewANPR.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.gridViewANPR.Appearance.FocusedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridViewANPR.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewANPR.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewANPR.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewANPR.Appearance.FooterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewANPR.Appearance.GroupButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.GroupButton.Options.UseFont = true;
            this.gridViewANPR.Appearance.GroupFooter.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.GroupFooter.Options.UseFont = true;
            this.gridViewANPR.Appearance.GroupPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewANPR.Appearance.GroupRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewANPR.Appearance.HeaderPanel.Font = new System.Drawing.Font("Calibri", 12F);
            this.gridViewANPR.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewANPR.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridViewANPR.Appearance.HorzLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.HorzLine.Options.UseFont = true;
            this.gridViewANPR.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.gridViewANPR.Appearance.OddRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewANPR.Appearance.OddRow.Options.UseFont = true;
            this.gridViewANPR.Appearance.Preview.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.Preview.Options.UseFont = true;
            this.gridViewANPR.Appearance.Row.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.Row.Options.UseFont = true;
            this.gridViewANPR.Appearance.RowSeparator.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.RowSeparator.Options.UseFont = true;
            this.gridViewANPR.Appearance.SelectedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.SelectedRow.Options.UseFont = true;
            this.gridViewANPR.Appearance.TopNewRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.TopNewRow.Options.UseFont = true;
            this.gridViewANPR.Appearance.VertLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.VertLine.Options.UseFont = true;
            this.gridViewANPR.Appearance.ViewCaption.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewANPR.Appearance.ViewCaption.Options.UseFont = true;
            this.gridViewANPR.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colTag,
            this.colIP,
            this.colID});
            this.gridViewANPR.GridControl = this.gridControlANPR;
            this.gridViewANPR.Name = "gridViewANPR";
            this.gridViewANPR.OptionsView.EnableAppearanceOddRow = true;
            this.gridViewANPR.OptionsView.ShowGroupPanel = false;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Direction";
            this.colDescription.FieldName = "Description";
            this.colDescription.MinWidth = 100;
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 100;
            // 
            // colTag
            // 
            this.colTag.Caption = "Name / Sr#";
            this.colTag.FieldName = "SRNO";
            this.colTag.MinWidth = 150;
            this.colTag.Name = "colTag";
            this.colTag.OptionsColumn.AllowEdit = false;
            this.colTag.Visible = true;
            this.colTag.VisibleIndex = 1;
            this.colTag.Width = 150;
            // 
            // colIP
            // 
            this.colIP.Caption = "IP";
            this.colIP.FieldName = "IP";
            this.colIP.MinWidth = 150;
            this.colIP.Name = "colIP";
            this.colIP.OptionsColumn.AllowEdit = false;
            this.colIP.Visible = true;
            this.colIP.VisibleIndex = 2;
            this.colIP.Width = 150;
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // frmConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1829, 1028);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Font = new System.Drawing.Font("Calibri", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmConfiguration";
            this.Text = "VMS Settings";
            this.Load += new System.EventHandler(this.frmConfiguration_Load);
            this.tableLayoutPanelMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCamGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCamGroup)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCamera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCamera)).EndInit();
            this.tableLayoutPanelCamera.ResumeLayout(false);
            this.panelCameraTitle.ResumeLayout(false);
            this.panelCameraTitle.PerformLayout();
            this.panelCameraInputControls.ResumeLayout(false);
            this.panelCameraInputControls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVMS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVMS)).EndInit();
            this.tableLayoutPanelVMSSection.ResumeLayout(false);
            this.panleVMSTitle.ResumeLayout(false);
            this.panleVMSTitle.PerformLayout();
            this.panelVMSInputControls.ResumeLayout(false);
            this.panelVMSInputControls.PerformLayout();
            this.tableLayoutPanelRFiDANPR.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panelRFiD.ResumeLayout(false);
            this.panelRFiD.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tableLayoutPanelFRiDANPRGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMarker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMarker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlANPR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewANPR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private DevExpress.XtraGrid.GridControl gridControlVMS;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewVMS;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnVMSServer;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnVMSIP;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnVMSID;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.SimpleButton btnResetVMS;
        private DevExpress.XtraEditors.SimpleButton btnDeleteVMS;
        private DevExpress.XtraEditors.SimpleButton btnAddUpdateVMS;
        private System.Windows.Forms.MaskedTextBox txtServerName;
        private System.Windows.Forms.MaskedTextBox txtServerID;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelVMSSection;
        private System.Windows.Forms.Panel panleVMSTitle;
        private System.Windows.Forms.Panel panelVMSInputControls;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelServerName;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraGrid.GridControl gridControlCamera;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCamera;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCamera;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnServer;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnVID;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCamera;
        private System.Windows.Forms.Panel panelCameraTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelCameraInputControls;
        private System.Windows.Forms.ComboBox ddlRFiDMarker;
        private System.Windows.Forms.Label lblMarker;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.ComboBox ddlCameraGroup;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraEditors.SimpleButton btnResetCamera;
        private System.Windows.Forms.ComboBox ddlVMSServer;
        private DevExpress.XtraEditors.SimpleButton btnDeleteCamera;
        private System.Windows.Forms.MaskedTextBox txtCameraName;
        private DevExpress.XtraEditors.SimpleButton btnAddUpdateCamera;
        private System.Windows.Forms.MaskedTextBox txtCameraID;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panelVMSDDL;
        private System.Windows.Forms.Panel panelCameraIP;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRFiDANPR;
        private System.Windows.Forms.Panel panelRFiD;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox txtRFiDMarker;
        private System.Windows.Forms.MaskedTextBox txtRFiDID;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelFRiDANPRGrid;
        private DevExpress.XtraGrid.GridControl gridControlMarker;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMarker;
        private DevExpress.XtraGrid.Columns.GridColumn colDirection;
        private DevExpress.XtraGrid.Columns.GridColumn colMarker;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.SimpleButton btnResetMarker;
        private DevExpress.XtraEditors.SimpleButton btnDeleteMarker;
        private DevExpress.XtraEditors.SimpleButton btnAddUpdateMarker;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox txtANPRIP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.MaskedTextBox txtANPRID;
        private DevExpress.XtraEditors.SimpleButton btnAddUpdateANPR;
        private DevExpress.XtraEditors.SimpleButton btnResetANPR;
        private System.Windows.Forms.MaskedTextBox txtANPRNameTag;
        private DevExpress.XtraEditors.SimpleButton btnDeleteANPR;
        private System.Windows.Forms.Panel panel6;
        private DevExpress.XtraGrid.GridControl gridControlCamGroup;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCamGroup;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSrNo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnGrp;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnID;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.MaskedTextBox maskedTextBox2;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.MaskedTextBox txtGroupID;
        private DevExpress.XtraEditors.SimpleButton btnAddUpdateGroup;
        private DevExpress.XtraEditors.SimpleButton btnResetGroup;
        private System.Windows.Forms.MaskedTextBox txtGroupName;
        private DevExpress.XtraEditors.SimpleButton btnDeleteGroup;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnGroup;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMarker;
        private System.Windows.Forms.MaskedTextBox txtServerIP;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnGID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnRFID;
        private System.Windows.Forms.RadioButton rbOUT;
        private System.Windows.Forms.RadioButton rbIN;
        private System.Windows.Forms.RadioButton rbANPROUT;
        private System.Windows.Forms.RadioButton rbANPRIN;
        private DevExpress.XtraGrid.GridControl gridControlANPR;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewANPR;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTag;
        private DevExpress.XtraGrid.Columns.GridColumn colIP;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
    }
}