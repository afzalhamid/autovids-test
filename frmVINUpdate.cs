﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccessLayer;

namespace VideoViewer
{
    public partial class frmVINUpdate : Form
    {
        public int vehicleID { get; set; }
        DAL dataAccess = new DAL();

        public frmVINUpdate()
        {
            InitializeComponent();
        }

        private void frmVINUpdate_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        void LoadData()
        {
            try
            {
                DataTable dataTable = dataAccess.GetVehicleInfoByID(vehicleID);

                if (dataTable.Rows.Count > 0)
                {
                    txtVIN.Text = dataTable.Rows[0]["VIN"].ToString();
                    txtComment.Text = dataTable.Rows[0]["Comments"].ToString();
                    cbBlackList.Checked = Convert.ToBoolean(dataTable.Rows[0]["Status"]);

                    if (cbBlackList.Checked)
                        this.BackColor = Color.FromArgb(255, 192, 192);
                }
            }
            catch { }
        }

        void SaveData()
        {
            string vin = txtVIN.Text.Trim();
            string comment = txtComment.Text.Trim();
            bool blacklist = cbBlackList.Checked;

            if (dataAccess.UpdateVehicleInfoByID(vehicleID, comment, vin, blacklist))
            {
                this.Close();
            }
            else
            {
                lblMessage.Text = "Information updated failed !";
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            SaveData();
        }
    }
}
