﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoViewer
{
    public partial class frmLog : Form
    {
        DAL dataAccess = new DAL();
        public frmLog()
        {
            InitializeComponent();
        }

        private void frmLog_Load(object sender, EventArgs e)
        {
            GetLogs("Message");
        }

        private void rbMessage_CheckedChanged(object sender, EventArgs e)
        {
            gridControlLog.DataSource = null;
            GetLogs("Message");
        }

        private void rbException_CheckedChanged(object sender, EventArgs e)
        {
            gridControlLog.DataSource = null;
            GetLogs("Error");
        }

        void GetLogs(string logType)
        {
            try
            {
                gridControlLog.DataSource = dataAccess.GetLogs(logType).Tables[0];
            }
            catch (Exception ex)
            {
                dataAccess.Logging(Module.Application,LogClass.Error, ex.ToString());
            }
        }
    }
}
