﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using VideoOS.Platform;
using VideoOS.Platform.SDK.UI.LoginDialog;

namespace VideoViewer
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			VideoOS.Platform.SDK.Environment.Initialize();		// Initialize the standalone Environment
																// NOTE: This dll requires the application to be in x86 due to the ActiveX
			VideoOS.Platform.SDK.UI.Environment.Initialize();	// new MIPSDK 2015

			EnvironmentManager.Instance.TraceFunctionCalls = true;

			//DialogLoginForm loginForm = new DialogLoginForm(SetLoginResult);
			//loginForm.AutoLogin = false;				// Can overrride the tick mark
			//loginForm.LoginLogoImage = someImage;		// Could add my own image here
			//Application.Run(loginForm);
			if (true)
			{
				try
				{
					Application.Run(new frmLogin());
				}
				catch (Exception e)
				{
				    MessageBox.Show("Program.cs:" + e.Message);
				}
			}

		}

		private static bool Connected = false;
		private static void SetLoginResult(bool connected)
		{
			Connected = connected;
		}
	}
}
