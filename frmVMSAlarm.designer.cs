﻿namespace VideoViewer
{
    partial class frmVMSAlarm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVMSAlarm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.panelGrid = new System.Windows.Forms.Panel();
            this.gridControlAlarm = new DevExpress.XtraGrid.GridControl();
            this.show_recording = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.tableLayoutPanelSelectionControl = new System.Windows.Forms.TableLayoutPanel();
            this.panelDropdown = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panelServerName = new System.Windows.Forms.Panel();
            this.dateEditFrom = new DevExpress.XtraEditors.DateEdit();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSource = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefinition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimestamp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tableLayoutPanelMain.SuspendLayout();
            this.panelGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAlarm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.show_recording)).BeginInit();
            this.tableLayoutPanelSelectionControl.SuspendLayout();
            this.panelDropdown.SuspendLayout();
            this.panelServerName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 1;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.panelGrid, 0, 3);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelSelectionControl, 0, 1);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 4;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1220, 612);
            this.tableLayoutPanelMain.TabIndex = 0;
            // 
            // panelGrid
            // 
            this.panelGrid.Controls.Add(this.gridControlAlarm);
            this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGrid.Location = new System.Drawing.Point(2, 71);
            this.panelGrid.Margin = new System.Windows.Forms.Padding(2);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(1216, 539);
            this.panelGrid.TabIndex = 0;
            // 
            // gridControlAlarm
            // 
            this.gridControlAlarm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAlarm.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            gridLevelNode1.RelationName = "Level1";
            this.gridControlAlarm.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControlAlarm.Location = new System.Drawing.Point(0, 0);
            this.gridControlAlarm.MainView = this.gridView1;
            this.gridControlAlarm.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlAlarm.Name = "gridControlAlarm";
            this.gridControlAlarm.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.show_recording});
            this.gridControlAlarm.Size = new System.Drawing.Size(1216, 539);
            this.gridControlAlarm.TabIndex = 5;
            this.gridControlAlarm.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // show_recording
            // 
            this.show_recording.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            this.show_recording.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.show_recording.Name = "show_recording";
            this.show_recording.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // tableLayoutPanelSelectionControl
            // 
            this.tableLayoutPanelSelectionControl.ColumnCount = 2;
            this.tableLayoutPanelSelectionControl.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanelSelectionControl.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelSelectionControl.Controls.Add(this.panelDropdown, 0, 0);
            this.tableLayoutPanelSelectionControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelSelectionControl.Location = new System.Drawing.Point(2, 14);
            this.tableLayoutPanelSelectionControl.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelSelectionControl.Name = "tableLayoutPanelSelectionControl";
            this.tableLayoutPanelSelectionControl.RowCount = 1;
            this.tableLayoutPanelSelectionControl.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelSelectionControl.Size = new System.Drawing.Size(1216, 41);
            this.tableLayoutPanelSelectionControl.TabIndex = 1;
            // 
            // panelDropdown
            // 
            this.panelDropdown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panelDropdown.Controls.Add(this.label1);
            this.panelDropdown.Controls.Add(this.panelServerName);
            this.panelDropdown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDropdown.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.panelDropdown.ForeColor = System.Drawing.Color.White;
            this.panelDropdown.Location = new System.Drawing.Point(0, 0);
            this.panelDropdown.Margin = new System.Windows.Forms.Padding(0);
            this.panelDropdown.MaximumSize = new System.Drawing.Size(345, 41);
            this.panelDropdown.Name = "panelDropdown";
            this.panelDropdown.Size = new System.Drawing.Size(300, 41);
            this.panelDropdown.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(4, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Date :";
            // 
            // panelServerName
            // 
            this.panelServerName.BackColor = System.Drawing.Color.White;
            this.panelServerName.Controls.Add(this.dateEditFrom);
            this.panelServerName.Controls.Add(this.btnSearch);
            this.panelServerName.Location = new System.Drawing.Point(59, 6);
            this.panelServerName.Margin = new System.Windows.Forms.Padding(2);
            this.panelServerName.Name = "panelServerName";
            this.panelServerName.Size = new System.Drawing.Size(216, 28);
            this.panelServerName.TabIndex = 502;
            // 
            // dateEditFrom
            // 
            this.dateEditFrom.EditValue = new System.DateTime(2018, 6, 26, 0, 0, 0, 0);
            this.dateEditFrom.Location = new System.Drawing.Point(5, 4);
            this.dateEditFrom.Margin = new System.Windows.Forms.Padding(2);
            this.dateEditFrom.Name = "dateEditFrom";
            this.dateEditFrom.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.dateEditFrom.Properties.Appearance.Options.UseFont = true;
            this.dateEditFrom.Properties.AppearanceCalendar.DayCellSelected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dateEditFrom.Properties.AppearanceCalendar.DayCellSelected.Options.UseBackColor = true;
            this.dateEditFrom.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.dateEditFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFrom.Size = new System.Drawing.Size(171, 20);
            this.dateEditFrom.TabIndex = 5;
            // 
            // btnSearch
            // 
            this.btnSearch.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSearch.Appearance.Options.UseBackColor = true;
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSearch.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnSearch.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.ImageOptions.Image")));
            this.btnSearch.Location = new System.Drawing.Point(190, 2);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(24, 24);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FilterCloseButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridView1.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridView1.Appearance.FilterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridView1.Appearance.FilterPanel.Options.UseFont = true;
            this.gridView1.Appearance.ViewCaption.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridView1.Appearance.ViewCaption.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSource,
            this.colMessage,
            this.colDefinition,
            this.colPriority,
            this.colState,
            this.colTimestamp});
            this.gridView1.GridControl = this.gridControlAlarm;
            this.gridView1.Name = "gridView1";
            // 
            // colSource
            // 
            this.colSource.Caption = "Source";
            this.colSource.FieldName = "Source";
            this.colSource.Name = "colSource";
            this.colSource.OptionsColumn.AllowEdit = false;
            this.colSource.Visible = true;
            this.colSource.VisibleIndex = 0;
            this.colSource.Width = 150;
            // 
            // colMessage
            // 
            this.colMessage.Caption = "Message";
            this.colMessage.FieldName = "Message";
            this.colMessage.Name = "colMessage";
            this.colMessage.OptionsColumn.AllowEdit = false;
            this.colMessage.Visible = true;
            this.colMessage.VisibleIndex = 1;
            this.colMessage.Width = 200;
            // 
            // colDefinition
            // 
            this.colDefinition.Caption = "Definition";
            this.colDefinition.FieldName = "Definition";
            this.colDefinition.Name = "colDefinition";
            this.colDefinition.OptionsColumn.AllowEdit = false;
            this.colDefinition.Visible = true;
            this.colDefinition.VisibleIndex = 2;
            this.colDefinition.Width = 200;
            // 
            // colPriority
            // 
            this.colPriority.Caption = "Priority";
            this.colPriority.FieldName = "Priority";
            this.colPriority.MaxWidth = 150;
            this.colPriority.Name = "colPriority";
            this.colPriority.OptionsColumn.AllowEdit = false;
            this.colPriority.Visible = true;
            this.colPriority.VisibleIndex = 3;
            this.colPriority.Width = 150;
            // 
            // colState
            // 
            this.colState.Caption = "State";
            this.colState.FieldName = "State";
            this.colState.MaxWidth = 150;
            this.colState.Name = "colState";
            this.colState.OptionsColumn.AllowEdit = false;
            this.colState.Visible = true;
            this.colState.VisibleIndex = 4;
            this.colState.Width = 150;
            // 
            // colTimestamp
            // 
            this.colTimestamp.Caption = "Timestamp";
            this.colTimestamp.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss";
            this.colTimestamp.FieldName = "Time";
            this.colTimestamp.MaxWidth = 150;
            this.colTimestamp.Name = "colTimestamp";
            this.colTimestamp.OptionsColumn.AllowEdit = false;
            this.colTimestamp.Visible = true;
            this.colTimestamp.VisibleIndex = 5;
            this.colTimestamp.Width = 150;
            // 
            // frmVMSAlarm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1220, 612);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmVMSAlarm";
            this.Text = "frmAlarm";
            this.Load += new System.EventHandler(this.frmAlarm_Load);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.panelGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAlarm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.show_recording)).EndInit();
            this.tableLayoutPanelSelectionControl.ResumeLayout(false);
            this.panelDropdown.ResumeLayout(false);
            this.panelDropdown.PerformLayout();
            this.panelServerName.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.Panel panelGrid;
        private DevExpress.XtraGrid.GridControl gridControlAlarm;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit show_recording;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelSelectionControl;
        private System.Windows.Forms.Panel panelDropdown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelServerName;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
        private DevExpress.XtraEditors.DateEdit dateEditFrom;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colSource;
        private DevExpress.XtraGrid.Columns.GridColumn colMessage;
        private DevExpress.XtraGrid.Columns.GridColumn colDefinition;
        private DevExpress.XtraGrid.Columns.GridColumn colPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colState;
        private DevExpress.XtraGrid.Columns.GridColumn colTimestamp;
    }
}