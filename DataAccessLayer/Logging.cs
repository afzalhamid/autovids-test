﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public enum Module
    {
        TCPServer,
        Milestone,
        DataBase,
        Application
    }

    public enum LogClass
    {
        Error,
        Message,
        Event
    }

    public class Logging
    {
        public static string connectionString { get; set; }

        public static void TraceLog(Module module, LogClass logClass, string message)
        {
            DAL DAL = new DAL();
            DAL.Logging(module, logClass, message);
        }

    }
}
