﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DataAccessLayer
{
    public class XMLBLL
    {
        public static string vinMessage { get; set; }
        protected XmlDocument doc = null;

        public XMLBLL()
        {            
           
        }

        public VehicleRead DeserializeElements()
        {
            doc = new XmlDocument();
            doc.LoadXml(vinMessage);

            XmlNodeReader mappingNode = new XmlNodeReader(doc.SelectSingleNode("/vehicleread"));
            XmlSerializer sElement = new XmlSerializer(typeof(VehicleRead));
            return (VehicleRead)sElement.Deserialize(mappingNode);
        }
    }

    [XmlRoot("vehicleread")]
    public class VehicleRead
    {
        [XmlElement("vin")]
        public Vin vin { get; set; }

        [XmlElement("timestamp")]
        public TimeStamp timestamp { get; set; }

        [XmlElement("marker")]
        public Marker marker { get; set; }

        [XmlElement("direction")]
        public Marker direction { get; set; }
    }
    public class Vin
    {
        [XmlText]
        public string value { get; set; }
    }

    public class TimeStamp
    {
        [XmlText]
        public string value { get; set; }
    }

    public class Marker
    {
        [XmlText]
        public string value { get; set; }
    }
}
