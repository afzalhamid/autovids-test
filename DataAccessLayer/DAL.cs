﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Security.Cryptography;
namespace DataAccessLayer
{
    public class DAL
    {
        public enum Mode
        {
            Event,Manual
        }

        private byte[] key = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F };

        private SqlConnection sqlConnection { get; set; }
        public string connectionString { get { return System.Configuration.ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString; } }
        public SqlConnection GetSQLConnection()
        {
            if (sqlConnection == null || sqlConnection.State == System.Data.ConnectionState.Closed)
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                return sqlConnection;
            }
            else
            {
                return sqlConnection;
            }
        }

        public DataSet SearchVehicle(string searchID)
        {
            DataSet dsVehicle = new DataSet();

            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                    sqlCommand.Connection = sqlConn;

                    sqlCommand.CommandText = "SearchVehicle";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@Keyword", searchID)
                    });

                    sqlAdapter.Fill(dsVehicle);
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.Message.ToString());
            }

            return dsVehicle;
        }


        public DataSet GetAllVehicles()
        {
            DataSet dsVehicle = new DataSet();

            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                    sqlCommand.Connection = sqlConn;

                    sqlCommand.CommandText = "GetAllVehicles";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlAdapter.Fill(dsVehicle);
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.Message.ToString());
            }

            return dsVehicle;
        }


        public DataTable GetVehicleInfoByID(int vehicleID)
        {
            DataTable dsVehicle = new DataTable();

            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                    sqlCommand.Connection = sqlConn;

                    sqlCommand.CommandText = "GetVehicleDetailByID";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@VehicleID", vehicleID)
                    });

                    sqlAdapter.Fill(dsVehicle);
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.Message.ToString());
            }

            return dsVehicle;
        }

        public DataTable GetMilestoneAlarms(DateTime date)
        {
            DataTable dataTable = new DataTable();

            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                    sqlCommand.Connection = sqlConn;

                    sqlCommand.CommandText = "GetMilestoneAlarms";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@Date", date)
                    });

                    sqlAdapter.Fill(dataTable);
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.Message.ToString());
            }

            return dataTable;
        }

        public bool UpdateVehicleInfoByID(int vehicleID, string comment, string vin, bool blacklist)
        {           

            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = sqlConn;

                    sqlCommand.CommandText = "UpdateVehicleDetailByID";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@VehicleID", vehicleID),
                        new SqlParameter("@Comments", comment),
                        new SqlParameter("@VIN", vin),
                        new SqlParameter("@BlackList", blacklist)
                    });

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.Message.ToString());
                return false;
            }

        }
        public DataTable GetEventsByVehicleID(string vehicleID, DateTime fromDate, DateTime toDate)
        {
            DataTable dtVehicle = new DataTable();

            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                    sqlCommand.Connection = sqlConn;

                    sqlCommand.CommandText = "GetEventsByVehicleID";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@VehicleID", vehicleID),
                        new SqlParameter("@FromDate", fromDate),
                        new SqlParameter("@ToDate", toDate)
                    });

                    sqlAdapter.Fill(dtVehicle);
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.Message.ToString());
            }

            return dtVehicle;
        }

        public DataTable GetAllCamera()
        {
            DataTable dtCamera = new DataTable();

            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                   
                    sqlCommand.Connection = sqlConn;

                    sqlCommand.CommandText = "GetAllCamera";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    
                    sqlAdapter.Fill(dtCamera);                                

                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.Message.ToString());
            }

            return dtCamera;
        }

        public DataTable GetVMSServer()
        {
            DataTable dt = new DataTable("VMS");
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;

                    sqlCommand.CommandText = "GetVMSServer";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                    sqlAdapter.Fill(dt);

                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.Message.ToString());
            }

            return dt;
        }

        public DataTable GetAllMarkers()
        {
            DataTable dt = new DataTable("RFiD");
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;

                    sqlCommand.CommandText = "GetRFiD";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                    sqlAdapter.Fill(dt);

                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.Message.ToString());
            }

            return dt;
        }

        public DataTable GetAllANPR()
        {
            DataTable dt = new DataTable("ANPR");
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;

                    sqlCommand.CommandText = "GetANPR";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                    sqlAdapter.Fill(dt);

                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.Message.ToString());
            }

            return dt;
        }

        public DataTable GetAllGroups()
        {
            DataTable dt = new DataTable("Groups");
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;

                    sqlCommand.CommandText = "GetCameraGroup";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                    sqlAdapter.Fill(dt);

                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.Message.ToString());
            }

            return dt;
        }

        public Dictionary<string, string> GetConfiguration()
        {
            Dictionary<string, string> config = new Dictionary<string, string>();

            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    DataSet ds = new DataSet();
                    sqlCommand.Connection = sqlConn;

                    sqlCommand.CommandText = "GetConfiguration";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;

                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                    sqlAdapter.Fill(ds);

                    config.Add("VMSServerName", (from k in ds.Tables[0].AsEnumerable() where k.Field<String>("Key") == "VMSServerName" select k).ElementAt(0).Field<String>("Value"));
                    config.Add("VMSUserName", (from k in ds.Tables[0].AsEnumerable() where k.Field<String>("Key") == "VMSUserName" select k).ElementAt(0).Field<String>("Value"));
                    config.Add("VMSPassword", (from k in ds.Tables[0].AsEnumerable() where k.Field<String>("Key") == "VMSPassword" select k).ElementAt(0).Field<String>("Value"));
                    config.Add("VMSPort", (from k in ds.Tables[0].AsEnumerable() where k.Field<String>("Key") == "VMSPort" select k).ElementAt(0).Field<String>("Value"));
                    config.Add("VMSIP", (from k in ds.Tables[0].AsEnumerable() where k.Field<String>("Key") == "VMSIP" select k).ElementAt(0).Field<String>("Value"));
                    config.Add("VMSRetentionTime", (from k in ds.Tables[0].AsEnumerable() where k.Field<String>("Key") == "VMSRetentionTime" select k).ElementAt(0).Field<String>("Value"));
                    config.Add("VMSEventServerPort", (from k in ds.Tables[0].AsEnumerable() where k.Field<String>("Key") == "VMSEventServerPort" select k).ElementAt(0).Field<String>("Value"));

                    config.Add("TCPServerIP", (from k in ds.Tables[0].AsEnumerable() where k.Field<String>("Key") == "TCPServerIP" select k).ElementAt(0).Field<String>("Value"));
                    config.Add("TCPPort", (from k in ds.Tables[0].AsEnumerable() where k.Field<String>("Key") == "TCPPort" select k).ElementAt(0).Field<String>("Value"));
                    config.Add("VMSEdition", (from k in ds.Tables[0].AsEnumerable() where k.Field<String>("Key") == "VMSEdition" select k).ElementAt(0).Field<String>("Value"));

                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.Message.ToString());
            }

            return config;
        }
        public bool SaveConfiguration(Dictionary<string, string> paraCollection)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandText = "AddUpdateConfiguration";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;

                    foreach (var para in paraCollection)
                    {
                        sqlCommand.Parameters.Add(new SqlParameter(para.Key, para.Value));
                    }

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();

                    return true;
                }
            }
            catch(Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.Message.ToString());
                return false;
            }
        }

        public void Logging(Module module, LogClass logClass, string message)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    DataSet ds = new DataSet();
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandText = "Logging";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@Module",module.ToString()),
                        new SqlParameter("@Class", logClass.ToString()),
                        new SqlParameter("@Message", message)
                    });

                    sqlCommand.ExecuteNonQuery();
                    sqlConn.Close();
                }
            }
            catch
            {
            }
        }        

        public DataSet GetLogs(string logType)
        {
            DataSet dsLog = new DataSet();
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandText = "GetAllLogs";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@Type",logType)
                    });
                    sqlCommand.CommandTimeout = 0;

                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(dsLog);

                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
            }

            return dsLog;
        }

        public DataTable GetLatestEvents()
        {
            DataTable dtResult = new DataTable();
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandText = "GetLatestEvents";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;

                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(dtResult);

                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
            }

            return dtResult;
        }

        public DataTable GetEventsByDate(DateTime datetime)
        {
            DataTable dtResult = new DataTable();
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandText = "GetEventsByDate";
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@Timestamp",datetime)
                    });
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;
                    
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(dtResult);

                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
            }

            return dtResult;
        }

        public DataTable GetHistoryByVehicleID(int vehicleID)
        {
            DataTable dtResult = new DataTable();
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandText = "GetHistoryByVehicleID";
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@VehicleID",vehicleID)
                    });
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;

                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(dtResult);

                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
            }

            return dtResult;
        }

        public void ClearLogsEvents()
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    DataSet ds = new DataSet();
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandText = "Clear_Logs";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    
                    sqlCommand.ExecuteNonQuery();
                    sqlConn.Close();
                }
            }
            catch
            {
            }
        }

        public void LogVehicleEvent(string regNumber, string gateDirection, Mode mode)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    DataSet ds = new DataSet();
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandText = "LogVehicleEvent";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@RegNumber",regNumber.Trim().ToUpper()),
                        new SqlParameter("@Gate", gateDirection),
                        new SqlParameter("@Mode", mode.ToString())
                    });

                    sqlCommand.ExecuteNonQuery();
                    sqlConn.Close();
                }
            }
            catch
            {
            }
        }

        public void LogEventByVIN(string vin, string marker, DateTime evttime)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    DataSet ds = new DataSet();
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandText = "LogEventByVIN";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@VIN",vin),
                        new SqlParameter("@Marker", marker),
                        new SqlParameter("@Timestamp", evttime)
                    });

                    sqlCommand.ExecuteNonQuery();
                    sqlConn.Close();
                }
            }
            catch(Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
            }
        }

        public bool AddUpdateVMSServer(int id, string name, string ip)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();

                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.CommandText = "AddUpdate_VMS_Server";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@ServerID",id),
                        new SqlParameter("@Name",name),
                        new SqlParameter("@IP",ip)
                    });

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();


                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString()); 
                return false;
            }
        }

        public bool AddUpdateGroup(int id, string name)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();

                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.CommandText = "AddUpdate_Group";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@ID",id),
                        new SqlParameter("@Name",name)
                    });

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();


                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
                return false;
            }
        }

        public bool AddUpdateANPR(int id, string name, string ip, string desc)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();

                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.CommandText = "AddUpdate_ANPR";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@ID",id),
                        new SqlParameter("@Name",name),
                        new SqlParameter("@Description",desc),
                        new SqlParameter("@IP",ip)
                    });

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();


                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
                return false;
            }
        }

        public bool AddUpdateRFiD(int id, string marker, string Desc)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();

                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.CommandText = "AddUpdate_RFiD";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@ID",id),
                        new SqlParameter("@Marker",marker),
                         new SqlParameter("@Description",Desc)
                    });

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();


                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
                return false;
            }
        }

        public bool AddUpdateCamera(int id, int serverid,int rfid, int grpid, string name)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();

                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.CommandText = "AddUpdate_Camera";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@GroupID",grpid),
                        new SqlParameter("@MarkerID",rfid),
                         new SqlParameter("@ServerID",serverid),
                        new SqlParameter("@CamID",id),
                        new SqlParameter("@CameraName",name)
                    });

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();


                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
                return false;
            }
        }

        public bool DeleteVMSServer(int id)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "DeleteVMServer";
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@ID",id)
                    });

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
                return false;
            }
        }

        public bool DeleteCamera(int id)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "DeleteCamera";
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@ID",id)
                    });

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
                return false;
            }
        }

        public bool DeleteCameraGroup(int id)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "DeleteCameraGroup";
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@ID",id)
                    });

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
                return false;
            }
        }

        public bool DeleteANPR(int id)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "DeleteANPR";
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@ID",id)
                    });

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
                return false;
            }
        }

        public bool DeleteRFiD(int id)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "DeleteRFiD";
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@ID",id)
                    });

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
                return false;
            }
        }

        #region User Management

        public DataTable GetPermissions()
        {
            DataTable dtResult = new DataTable("Permission");
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;

                    sqlCommand.CommandText = "GetPermissions";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                    sqlAdapter.Fill(dtResult);

                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
            }

            return dtResult;
        }

        public DataTable GetRoles()
        {
            DataTable dtRole = new DataTable("Roles");
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;

                    sqlCommand.CommandText = "GetRole";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                    sqlAdapter.Fill(dtRole);

                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
            }

            return dtRole;
        }

        public DataTable GetUsers()
        {
            DataTable dtUser = new DataTable("Users");
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();

                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.CommandText = "GetUsers";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                    sqlAdapter.Fill(dtUser);

                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
            }

            return dtUser;
        }

        public DataTable GetRoleAndPermission()
        {
            DataTable dtRolePm = new DataTable("Roles");
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;

                    sqlCommand.CommandText = "GetRoleAndPermission";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                    sqlAdapter.Fill(dtRolePm);

                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
            }

            return dtRolePm;
        }

        public bool AddUpdateUser(int id, int roleid, string fName, string lName, string uName, string email, string password)
        {
            try
            {
                if (id == 0 && string.IsNullOrEmpty(password))
                    return false;

                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();

                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.CommandText = "AddUpdate_User";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@RoleID",roleid),
                        new SqlParameter("@FirstName",fName),
                        new SqlParameter("@LastName",lName),
                        new SqlParameter("@UserName",uName),
                        new SqlParameter("@Email",email),
                         new SqlParameter("@UserID",id),
                        new SqlParameter("@Password",password)
                    });
                    sqlCommand.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();


                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
                return false;
            }
        }

        public bool DeleteUser(int userID)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "DeleteUser";
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@ID",userID)
                    });

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
                return false;
            }
        }

        public bool AddUpdateRole(int id, string roleName, string permissionIds)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();

                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.CommandText = "AddUpdate_Role";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@RoleID",id),
                        new SqlParameter("@RoleName",roleName),
                        new SqlParameter("@PermissionsList",permissionIds)
                    });

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();


                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
                return false;
            }
        }

        public bool DeleteRole(int roleID)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "DeleteRole";
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@ID",roleID)
                    });

                    sqlCommand.ExecuteNonQuery();

                    sqlConn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
                return false;
            }
        }

        public bool AuthenticateUser(string userName, string password, ref int userID)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "ValidateUser";
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@Username", userName),
                        new SqlParameter("@Password",Encrypt(password))
                    });

                    userID = Convert.ToInt32(sqlCommand.ExecuteScalar());

                    sqlConn.Close();

                    if (userID > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString()); return false;
            }
        }
        public bool ChangeUserPassword(string userName, string newPassword, string oldPassword)
        {
            try
            {
                using (SqlConnection sqlConn = GetSQLConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "ChangePassword";
                    sqlCommand.Parameters.AddRange(new SqlParameter[]{
                        new SqlParameter("@Username", userName),
                        new SqlParameter("@OldPassword",Encrypt(oldPassword)),
                        new SqlParameter("@Password",Encrypt(newPassword))
                    });

                    int val = Convert.ToInt32(sqlCommand.ExecuteScalar());

                    sqlConn.Close();

                    if (val == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                Logging(Module.DataBase, LogClass.Error, ex.ToString());
                return false;
            }
        }

        #endregion

        public string Encrypt(string input)
        {
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = key;// UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public string Decrypt(string input)
        {
            byte[] inputArray = Convert.FromBase64String(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = key;// UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }

    public class Camera
    {
        public int ID { get; set; }
        public int ServerId { get; set; }
        public string Name { get; set; }
        public string Server { get; set; }
        public string Group { get; set; }
        public int RFID { get; set; }
        public string Marker { get; set; }
        public string ANPR { get; set; }
        public string Direction { get; set; }
    }
}
