﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DataAccessLayer;

using VideoOS.Platform;
using VideoOS.Platform.Client;
using VideoOS.Platform.Messaging;
using DevExpress.XtraEditors;
using System.Text.RegularExpressions;
using System.Net;
using System.Diagnostics;
using System.IO;

namespace VideoViewer
{    
    public partial class frmMain : Form
    {
        public enum PlayBackDirection
        {
            Forward, Backward
        }

        DAL dataAccess = new DAL();
        List<DateTime> _visitDates { get; set; }
        private SimpleButton btnSelected { get; set; }

        Timer timer = new Timer();
        Timer timerEvent = new Timer();
        Timer timerService = new Timer();

        private bool chkServiceStatus { get; set; }
        Regex rgx = new Regex("[^a-zA-Z0-9 -]");

        public int UserID { get; set; }
        public bool VmsConnected { get; set; }
        private bool IsAdvanceEdt { get; set; }
        private string VMSUserName { get; set; }
        private string VMSPassword { get; set; }
        private string VMSPort { get; set; }
        private string VMSIP { get; set; }
        private string selVIN { get; set; }
        private bool EnableServiceStatus { get { return System.Configuration.ConfigurationManager.AppSettings["ServiceStatus"] == "1" ? true : false; } }
        private bool RefreshEventList { get { return System.Configuration.ConfigurationManager.AppSettings["EnableLatestEventAutoRefresh"] == "1" ? true : false; } }
        private bool DebuggingEnabled { get { return System.Configuration.ConfigurationManager.AppSettings["DebugMode"] == "1" ? true : false; } }
        private int PreRecPlayback { get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PreRecPlayback"]); } }
        private string ParentFolder { get { return System.Configuration.ConfigurationManager.AppSettings["FolderPath"]; } }


        private Item _selectItem1;
        private Item _selectItem2;
        private ImageViewerControl _imageViewerControl1;       
        private ImageViewerControl _imageViewerControl2;
        private FQID _playbackControllerFQID;

        private double _speed1 = 0.0;
        private double _speed2 = 0.0;
                       
        
        public List<Camera> InwardCameras { get; set; }
        public List<Camera> OutwardCameras { get; set; }
        private string Camera01 { get; set; }
        private string Camera02 { get; set; }
        public DateTime RecStartTime01 { get; set; }
        public DateTime RecStartTime02 { get; set; }
        public PlayBackDirection CurrentPlayback { get; set; }

        public frmMain()
        {
            InitializeComponent();

            EnvironmentManager.Instance.RegisterReceiver(PlaybackTimeChangedHandler,
                                                         new MessageIdFilter(MessageId.SmartClient.PlaybackCurrentTimeIndication));

            backgroundWorker.WorkerReportsProgress = true;
            backgroundWorker.WorkerSupportsCancellation = true;
            backgroundWorkerVMSStatus.WorkerReportsProgress = true;
            backgroundWorkerVMSStatus.WorkerSupportsCancellation = true;
            backgroundWorkerServiceStatus.WorkerReportsProgress = true;
            backgroundWorkerServiceStatus.WorkerSupportsCancellation = true;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            dateEditFrom.DateTime = DateTime.Now.AddDays(-6);
            dateEditTo.DateTime = DateTime.Now;

            btnSelected = btnHMI;
            FormatButton(btnHMI, true);

            timerEvent.Interval = 1000 * 15;
            timerService.Interval = 1000 * 15;
            timer.Interval = 1000 * 5;

            timer.Tick += Timer_Tick;
            timerEvent.Tick += TimerEvent_Tick;
            timerService.Tick += TimerService_Tick;

            tabCameraGroups.Hide();

            LoadEvents();
            LoadCameras();
            LoadConfiguration();
            //ConnectMilestone();
            SetDefault();

            panel3.BackColor = Color.Maroon;
            panel5.BackColor = Color.Maroon;
            panel6.BackColor = Color.Maroon;
            panel7.BackColor = Color.Maroon;

            if (EnableServiceStatus)
            {
                timerService.Enabled = true;
                timerService.Start();
            }

            if(RefreshEventList)
            {
                timerEvent.Enabled = true;
                timerEvent.Start();
            }

            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {

            if (!backgroundWorkerVMSStatus.IsBusy && !VmsConnected)
                backgroundWorkerVMSStatus.RunWorkerAsync();

            if (VmsConnected)
                timer.Stop();
        }

        private void TimerService_Tick(object sender, EventArgs e)
        {
            if (!backgroundWorkerServiceStatus.IsBusy)
                backgroundWorkerServiceStatus.RunWorkerAsync();
        }

        private void TimerEvent_Tick(object sender, EventArgs e)
        {
            if (!backgroundWorker.IsBusy)
                backgroundWorker.RunWorkerAsync();
        }

        private void btnCloseApplication_Click(object sender, EventArgs e)
        {
            try
            {
                frmLogin frmLogin = new frmLogin();
                frmLogin.Close();

                try
                {

                    foreach (Form frm in this.MdiChildren)
                    {
                        frm.Dispose();
                        frm.Close();
                    }
                }
                catch { }

                Application.ExitThread();
            }
            catch { }
        }

        void LoadConfiguration()
        {
            try
            {
                Dictionary<string, string> fields = dataAccess.GetConfiguration();

                VMSUserName = fields["VMSUserName"];
                VMSPassword = fields["VMSPassword"];
                VMSPort = fields["VMSPort"];
                VMSIP = fields["VMSIP"];

                if (fields["VMSEdition"] == "Advanced")
                    IsAdvanceEdt = true;
                else
                    IsAdvanceEdt = false;
            }
            catch (Exception ex)
            {
                dataAccess.Logging(Module.Application, LogClass.Error, ex.ToString());
            }
        }

        void LoadEvents()
        {
            try
            {
                gridEvent.DataSource = dataAccess.GetLatestEvents();
            }
            catch
            {

            }
                   
        }
        
        void LoadCameras()
        {
            DataTable dtCamera = dataAccess.GetAllCamera();

            if (dtCamera.Rows.Count > 0)
            {
                InwardCameras = (from evn in dtCamera.AsEnumerable()
                                 orderby evn.Field<string>("Camera") descending
                                 where evn.Field<string>("Direction") == "IN"
                                 select new Camera
                                 {
                                     ID = evn.Field<int>("ID"),
                                     Name = evn.Field<string>("Camera"),
                                     Group = evn.Field<string>("Group"),
                                     ServerId = evn.Field<int>("ServerId"),
                                     RFID = evn.Field<int>("RFID"),
                                     Marker = evn.Field<string>("Marker"),
                                     Direction = evn.Field<string>("Direction")
                                 }).ToList();

                OutwardCameras = (from evn in dtCamera.AsEnumerable()
                                  orderby evn.Field<string>("Camera") descending
                                  where evn.Field<string>("Direction") == "OUT"
                                  select new Camera
                                  {
                                      ID = evn.Field<int>("ID"),
                                      Name = evn.Field<string>("Camera"),
                                      Group = evn.Field<string>("Group"),
                                      ServerId = evn.Field<int>("ServerId"),
                                      RFID = evn.Field<int>("RFID"),
                                      Marker = evn.Field<string>("Marker"),
                                      Direction = evn.Field<string>("Direction")
                                  }).ToList();

                InwardCameras.ForEach(x => lbInwardCam.Items.Add(x.Name));
                OutwardCameras.ForEach(x => lbOutwardCam.Items.Add(x.Name));

            }
        }

            //private void dateEditFrom_DrawItem(object sender, DevExpress.XtraEditors.Calendar.CustomDrawDayNumberCellEventArgs e)
            //{
            //    if (_visitDates.Contains(e.Date.Date))
            //    {
            //        e.Style.ForeColor = Color.White;
            //        e.Style.BackColor = Color.FromArgb(0, 92, 185);
            //    }
            //    else
            //    {
            //        e.Style.ForeColor = Color.Gray;
            //    }
            //}

            //private void dateEditTo_DrawItem(object sender, DevExpress.XtraEditors.Calendar.CustomDrawDayNumberCellEventArgs e)
            //{
            //    if (_visitDates.Contains(e.Date.Date))
            //    {
            //        e.Style.ForeColor = Color.White;
            //        e.Style.BackColor = Color.FromArgb(0, 92, 185);
            //    }
            //    else
            //    {
            //        e.Style.ForeColor = Color.Gray;
            //    }
            //}

        void SetButtonSeletedState(SimpleButton btn)
        {
            foreach (var item in panelMenu.Controls)
            {
                SimpleButton sbtn = item as SimpleButton;
                if (btn == sbtn)
                    FormatButton(sbtn, true);
                else
                    FormatButton(sbtn, false);
            }
        }

        void FormatButton(SimpleButton btn, bool hover)
        {
            btn.Image = GetButtonImage(btn, hover);
            btn.Appearance.BackColor = hover ? Color.FromArgb(0, 92, 185) : Color.FromArgb(230, 230, 230);
            btn.Appearance.ForeColor = hover ? Color.White : Color.FromArgb(0, 92, 185);

        }

        Bitmap GetButtonImage(SimpleButton btn, bool hover)
        {
            Bitmap img = null;

            switch (btn.Text)
            {
                case "Main Layout":
                    img = hover ? Icons.hmiLayoutHover : Icons.hmiLayout;
                    break;

                case "Visit Log":
                    img = hover ? Icons.alarmEventsHover : Icons.alarmEvents;
                    break;

                case "System Configuration":
                    img = hover ? Icons.systemConfiguratoinHover : Icons.systemConfiguration;
                    break;

                case "User Management":
                    img = hover ? Icons.userManagementHover : Icons.userManagement;
                    break;

                case "Logs":
                    img = hover ? Icons.logsHover : Icons.logs;
                    break;
                default:
                    break;
            }

            return img;
        }

        #region Main Form Controls

        private void btnHMI_Click(object sender, EventArgs e)
        {
            
        }

        private void btnAlarm_Click(object sender, EventArgs e)
        {
            frmApplication objForm = new frmApplication();
            objForm.subForm = new frmVIN();
            objForm.UserID = this.UserID;             
            objForm.Show();
            this.Hide();
        }

        private void btnConfiguration_Click(object sender, EventArgs e)
        {
            frmApplication objForm = new frmApplication();
            objForm.subForm = new frmConfiguration();
            objForm.UserID = this.UserID;
            objForm.Show();
            this.Hide();
        }

        private void btnUserManagement_Click(object sender, EventArgs e)
        {
            frmApplication objForm = new frmApplication();
            objForm.subForm = new frmUserManagement();
            objForm.UserID = this.UserID;
            objForm.Show();
            this.Hide();
        }

        private void btnHelpTroubleshoot_Click(object sender, EventArgs e)
        {
            frmApplication objForm = new frmApplication();
            objForm.subForm = new frmLog();
            objForm.UserID = this.UserID;
            objForm.Show();
            this.Hide();
        }

        private void btnHMI_MouseLeave(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, false);
        }

        private void btnHMI_MouseEnter(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, true);
        }

        private void btnAlarm_MouseEnter(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, true);
        }

        private void btnAlarm_MouseLeave(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, false);
        }

        private void btnConfiguration_MouseEnter(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, true);
        }

        private void btnConfiguration_MouseLeave(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, false);
        }

        private void btnUserManagement_MouseEnter(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, true);
        }

        private void btnUserManagement_MouseLeave(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, false);
        }

        private void btnHelpTroubleshoot_MouseEnter(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, true);
        }

        private void btnHelpTroubleshoot_MouseLeave(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, false);
        }
        
        private void btnSearch_Click(object sender, EventArgs e)
        {
            string searchKeyword = txtSearch.Text.Trim();

            SetDefault();

            try
            {

                lbVIN.DataSource = null;
                lbTimeIN.DataSource = null;
                lbTimeOut.DataSource = null;
            }
            catch { }

            if (searchKeyword.Length > 0)
            {
                DataSet dsVehicle = dataAccess.SearchVehicle(searchKeyword);
                if (dsVehicle.Tables.Count > 0)
                {
                    if (dsVehicle.Tables[0].Rows.Count > 0)
                    {
                        lbVIN.DataSource = dsVehicle.Tables[0];
                        lbVIN.ValueMember = "ID";
                        lbVIN.DisplayMember = "VIN";

                        lblMessage.Visible = false;
                    }
                    else
                    {
                        lblMessage.Visible = true;
                    }
                }
                else
                {
                    lblMessage.Visible = true;
                }
            }

            btnShowInwardCam.Enabled = false;
            btnShowOutwardCam.Enabled = false;
            tabCameraGroups.Visible = false;
        }

        private void btnGetVisits_Click(object sender, EventArgs e)
        {
            SetDefault();
            selVIN = lbVIN.Text;
            try
            { 
                lbTimeIN.DataSource = null;
                lbTimeOut.DataSource = null;
                DateTime frmDate = dateEditFrom.DateTime.Date;
                DateTime toDate = dateEditTo.DateTime.Date;

                DataTable dtEvents = dataAccess.GetEventsByVehicleID(lbVIN.SelectedValue.ToString(), frmDate, toDate);
                if (dtEvents.Rows.Count > 0)
                {
                    var inEvents = from evn in dtEvents.AsEnumerable()
                                   orderby evn.Field<DateTime>("EventTime") descending
                                   where evn.Field<string>("Gate") == "IN"                                   
                                   select new { ID = evn.Field<int>("ID"), EventTime = evn.Field<DateTime>("EventTime").ToString("MM/dd/yyyy HH:mm:ss") };
                    var outEvents = from evn in dtEvents.AsEnumerable()
                                    orderby evn.Field<DateTime>("EventTime") descending
                                    where evn.Field<string>("Gate") == "OUT"
                                    select new { ID = evn.Field<int>("ID"), EventTime = evn.Field<DateTime>("EventTime").ToString("MM/dd/yyyy HH:mm:ss") };

                    if (inEvents.Any())
                    {
                        lbTimeIN.DataSource = inEvents.ToList();
                        lbTimeIN.DisplayMember = "EventTime";
                        lbTimeIN.ValueMember = "EventTime";
                        lbTimeIN.ClearSelected();
                        lbTimeIN.Enabled = true;
                        lbTimeIN.SelectedIndex = 0;
                    }
                    else
                    {
                        lbTimeIN.DataSource = null;
                        lbTimeIN.Enabled = false;
                        btnShowInwardCam.Enabled = false;
                        btnShowOutwardCam.Enabled = false;
                        tabCameraGroups.Visible = false;
                    }

                    if (outEvents.Any())
                    {
                        lbTimeOut.DataSource = outEvents.ToList();
                        lbTimeOut.DisplayMember = "EventTime";
                        lbTimeOut.ValueMember = "EventTime";
                        lbTimeOut.ClearSelected();
                        lbTimeOut.Enabled = true;
                        lbTimeOut.SelectedIndex = 0;
                    }
                    else
                    {
                        lbTimeOut.DataSource = null;
                        lbTimeOut.Enabled = false;
                        btnShowInwardCam.Enabled = false;
                        btnShowOutwardCam.Enabled = false;
                        tabCameraGroups.Visible = false;
                    }
                }
                else
                {
                    MessageBox.Show("No record found.", "Information!");
                    btnShowInwardCam.Enabled = false;
                    btnShowOutwardCam.Enabled = false;
                    tabCameraGroups.Visible = false;
                }
            }
            catch
            {
                MessageBox.Show("No record found.", "Information!");
                btnShowInwardCam.Enabled = false;
                btnShowOutwardCam.Enabled = false;
                tabCameraGroups.Visible = false;
            }
        }

        private void lbVIN_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnGetVisits.Enabled = true;
            try
            {
                lbTimeIN.DataSource = null;
                lbTimeOut.DataSource = null;
                SetDefault();
            }
            catch { }

            
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void backgroundWorkerServiceStatus_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void backgroundWorkerServiceStatus_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }
        
        private void btnCloseOutwardSeletion_Click(object sender, EventArgs e)
        {
            tabCameraGroups.Hide();
        }

        private void btnCloseInwardSeletion_Click(object sender, EventArgs e)
        {
            tabCameraGroups.Hide();
        }

        private void btnShowInwardCam_Click(object sender, EventArgs e)
        {
            if (tabCameraGroups.Visible && tabCameraGroups.SelectedPageIndex == 0)
                tabCameraGroups.Hide();
            else
                tabCameraGroups.Show();
            
            tabCameraGroups.Pages[0].PageVisible = true;
            tabCameraGroups.Pages[1].PageVisible = false;
            tabCameraGroups.SelectedPageIndex = 0;
        }

        private void btnShowOutwardCam_Click(object sender, EventArgs e)
        {
            if (tabCameraGroups.Visible && tabCameraGroups.SelectedPageIndex == 1)
                tabCameraGroups.Hide();
            else
                tabCameraGroups.Show();

            tabCameraGroups.Pages[0].PageVisible = false;
            tabCameraGroups.Pages[1].PageVisible = true;
            tabCameraGroups.SelectedPageIndex = 1;
        }

        private void btnSelectOutwardCam_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            Camera02 = lbOutwardCam.Text;
            RecStartTime02 = Convert.ToDateTime(lbTimeOut.Text);
            
            tabCameraGroups.Hide();
            SelectCamera02();

            Cursor = Cursors.Default;
        }

        private void btnSelectInwardCam_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            
            Camera01 = lbInwardCam.Text;
            RecStartTime01 = Convert.ToDateTime(lbTimeIN.Text);

            tabCameraGroups.Hide();
            SelectCamera01();

            Cursor = Cursors.Default;
        }

        private void lbInwardCam_DrawItem(object sender, ListBoxDrawItemEventArgs e)
        {
            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
            {
                e.Appearance.BackColor = Color.FromArgb(0, 92, 185);
                e.Appearance.ForeColor = Color.White;
            }
        }

        private void lbOutwardCam_DrawItem(object sender, ListBoxDrawItemEventArgs e)
        {
            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
            {
                e.Appearance.BackColor = Color.FromArgb(0, 92, 185);
                e.Appearance.ForeColor = Color.White;
            }
        }

        #endregion

        #region Time changed event handler

        private object PlaybackTimeChangedHandler(VideoOS.Platform.Messaging.Message message, FQID dest, FQID sender)
        {
            DateTime time = (DateTime)message.Data;
            int utcDiff = DateTime.Now.Subtract(DateTime.UtcNow).Hours;
            time = time.AddHours(utcDiff);

            // The built-in PlaybackController has FQID==null
            if (sender == null)
            {
                lblTime01.Text = time.ToString("MM/dd/yyyy hh:mm:ss tt");  //time.ToShortDateString() + "  " + time.ToLongTimeString();
                if (_imageViewerControl1 != null)
                    _imageViewerControl1.Invalidate();
            }

            // The PlaybackController on the right hand side has a specific FQID
            if (sender != null && _playbackControllerFQID != null && sender.ObjectId == _playbackControllerFQID.ObjectId)
            {
                lblTime02.Text = time.ToString("MM/dd/yyyy hh:mm:ss tt");  //time.ToShortDateString() + "  " + time.ToLongTimeString();
                if (_imageViewerControl2 != null)
                    _imageViewerControl2.Invalidate();
            }
            return null;
        }

        #endregion

        #region Load Plyback 

        void ConnectMilestone()
        {
            try
            {
                Uri uri = new Uri(String.Format("http://{0}:{1}/", VMSIP, VMSPort));
                NetworkCredential credentials = new NetworkCredential(VMSUserName, dataAccess.Decrypt(VMSPassword), string.Empty);
                VideoOS.Platform.SDK.Environment.RemoveAllServers();
                VideoOS.Platform.SDK.Environment.AddServer(uri, credentials);
                VideoOS.Platform.SDK.Environment.Login(uri, true);

                panel3.BackColor = Color.FromArgb(0, 92, 185);
                panel5.BackColor = Color.FromArgb(0, 92, 185);
                panel6.BackColor = Color.FromArgb(0, 92, 185);
                panel7.BackColor = Color.FromArgb(0, 92, 185);

                this.Invoke(new MethodInvoker(delegate ()
                {
                    btnReconnect.Visible = false;
                }));


                VmsConnected = true;
            }
            catch
            {
                panel3.BackColor = Color.Maroon;
                panel5.BackColor = Color.Maroon;
                panel6.BackColor = Color.Maroon;
                panel7.BackColor = Color.Maroon;

                this.Invoke(new MethodInvoker(delegate ()
                {
                    btnReconnect.Visible = true;
                }));

                VmsConnected = false;
            }
        }

        private void SelectCamera01()
        {
            try
            {

                if (_imageViewerControl1 != null)
                {
                    _imageViewerControl1.Disconnect();
                }

                List<Item> camCollection = null;

                if (IsAdvanceEdt)
                {
                    string[] camGrps = InwardCameras.Where(x => x.Name == Camera01).Select(x => x.Group).First().Split('|');
                    camCollection = EnvironmentManager.Instance.MasterSite.ServerId.UserContext.Configuration.GetItems()[0].GetChildren()[0].GetChildren();

                    int idx = 0;

                    foreach (string grp in camGrps)
                    {
                        if (idx == 0)
                        {
                            camCollection = camCollection.Where(x => x.Name.Trim() == grp.Trim()).ToList();
                            idx = 1;
                        }
                        else
                        {
                            camCollection = camCollection[0].GetChildren().Where(x => x.Name.Trim() == grp.Trim()).ToList();
                        }
                    }

                    camCollection = camCollection[0].GetChildren();

                }
                else
                {
                    camCollection = EnvironmentManager.Instance.MasterSite.ServerId.UserContext.Configuration.GetItems()[0].GetChildren()[4].GetChildren()[0].GetChildren();
                }

                foreach (Item cam in camCollection)
                {
                    if (cam.Name == Camera01)
                        _selectItem1 = cam;
                }


                _imageViewerControl1 = ClientControl.Instance.GenerateImageViewerControl();
                _imageViewerControl1.Dock = DockStyle.Fill;

                _imageViewerControl1.ClickEvent += new EventHandler(ImageViewerControl1Click);
                panel1.Controls.Clear();
                panel1.Controls.Add(_imageViewerControl1);
                _imageViewerControl1.CameraFQID = _selectItem1.FQID;
                _imageViewerControl1.Initialize();
                _imageViewerControl1.Connect();
                _imageViewerControl1.Selected = true;
                _imageViewerControl1.EnableMouseControlledPtz = true;
                _imageViewerControl1.EnableMousePtzEmbeddedHandler = true;
                _imageViewerControl1.EnableDigitalZoom = true;
                _imageViewerControl1.BackColor = System.Drawing.Color.Black;
                
                EnvironmentManager.Instance.Mode = Mode.ClientPlayback;

                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = RecStartTime01, Speed = _speed1 }));

                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                        MessageId.SmartClient.PlaybackCommand,
                                                        new PlaybackCommandData() { Command = PlaybackData.PlayStop, Speed = _speed1 }));

                lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);           

            }
            catch
            {
                MessageBox.Show(String.Format("Failed to connect camera {0}", Camera01));
            }

        }

        void ImageViewerControl1Click(object sender, EventArgs e)
        {
            if (_imageViewerControl2 != null)
                _imageViewerControl2.Selected = false;

            if (!btnPlayForward01.Enabled)
            {
                btnReset01.Enabled = true;
                btnExport1.Enabled = true;
                trackBar01.Enabled = false;

                btnPlayForward01.BringToFront();
                btnPlayBackward01.BringToFront();

                btnPlayForward02.BringToFront();
                btnPlayBackward02.BringToFront();

                btnPlayForward01.Enabled = true;
                btnPauseForward01.Enabled = true;
                btnFrameForward01.Enabled = true;
                btnPlayBackward01.Enabled = true;
                btnPauseBackward01.Enabled = true;
                btnFrameBackward01.Enabled = true;

                trackBar02.Enabled = false;
                btnPlayForward02.Enabled = false;
                btnPauseForward02.Enabled = false;
                btnFrameForward02.Enabled = false;
                btnPlayBackward02.Enabled = false;
                btnPauseBackward02.Enabled = false;
                btnFrameBackward02.Enabled = false;
            }

            _imageViewerControl1.Selected = true;

            _speed2 = 0.0;
            lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayStop }), _playbackControllerFQID);
            
        }
            
        private void SelectCamera02()
        {
            try
            {

                if (_imageViewerControl2 != null)
                {
                    _imageViewerControl2.Disconnect();
                }

                List<Item> camCollection = null;

                if (IsAdvanceEdt)
                {
                    string[] camGrps = OutwardCameras.Where(x => x.Name == Camera02).Select(x => x.Group).First().Split('|');
                    camCollection = EnvironmentManager.Instance.MasterSite.ServerId.UserContext.Configuration.GetItems()[0].GetChildren()[0].GetChildren();

                    int idx = 0;

                    foreach (string grp in camGrps)
                    {
                        if (idx == 0)
                        {
                            camCollection = camCollection.Where(x => x.Name.Trim() == grp.Trim()).ToList();
                            idx = 1;
                        }
                        else
                        {
                            camCollection = camCollection[0].GetChildren().Where(x => x.Name.Trim() == grp.Trim()).ToList();
                        }
                    }

                    camCollection = camCollection[0].GetChildren();

                }
                else
                {
                    camCollection = EnvironmentManager.Instance.MasterSite.ServerId.UserContext.Configuration.GetItems()[0].GetChildren()[4].GetChildren()[0].GetChildren();
                }

                foreach (Item cam in camCollection)
                {
                    if (cam.Name == Camera02)
                        _selectItem2 = cam;
                }

                if (_playbackControllerFQID == null)
                    _playbackControllerFQID = ClientControl.Instance.GeneratePlaybackController();

                _imageViewerControl2 = ClientControl.Instance.GenerateImageViewerControl();
                _imageViewerControl2.Dock = DockStyle.Fill;

                _imageViewerControl2.ClickEvent += new EventHandler(ImageViewerControl2Click);
                _imageViewerControl2.PlaybackControllerFQID = _playbackControllerFQID;
                panel2.Controls.Clear();
                panel2.Controls.Add(_imageViewerControl2);               
                _imageViewerControl2.CameraFQID = _selectItem2.FQID;
                _imageViewerControl2.Initialize();
                _imageViewerControl2.Connect();
                _imageViewerControl2.Selected = true;
                _imageViewerControl2.EnableMouseControlledPtz = true;
                _imageViewerControl2.EnableMousePtzEmbeddedHandler = true;
                _imageViewerControl2.EnableDigitalZoom = true;
                _imageViewerControl2.BackColor = System.Drawing.Color.Black;

                EnvironmentManager.Instance.Mode = Mode.ClientPlayback;

                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = RecStartTime02, Speed = _speed2 }), _playbackControllerFQID);

                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                        MessageId.SmartClient.PlaybackCommand,
                                                        new PlaybackCommandData() { Command = PlaybackData.PlayStop, Speed = _speed2 }), _playbackControllerFQID);

                lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);

            }
            catch
            {
                MessageBox.Show(String.Format("Failed to connect camera {0}", Camera02));
            }

        }

        void ImageViewerControl2Click(object sender, EventArgs e)
        {
            if (_imageViewerControl1 != null)
                _imageViewerControl1.Selected = false;

            if (!btnPlayForward02.Enabled)
            {
                btnPlayForward01.BringToFront();
                btnPlayBackward01.BringToFront();

                btnPlayForward02.BringToFront();
                btnPlayBackward02.BringToFront();

                btnReset02.Enabled = true;
                btnExport2.Enabled = true;

                trackBar02.Enabled = false;
                btnPlayForward02.Enabled = true;
                btnPauseForward02.Enabled = true;
                btnFrameForward02.Enabled = true;
                btnPlayBackward02.Enabled = true;
                btnPauseBackward02.Enabled = true;
                btnFrameBackward02.Enabled = true;

                trackBar01.Enabled = false;
                btnPlayForward01.Enabled = false;
                btnPauseForward01.Enabled = false;
                btnFrameForward01.Enabled = false;
                btnPlayBackward01.Enabled = false;
                btnPauseBackward01.Enabled = false;
                btnFrameBackward01.Enabled = false;
            }

            _imageViewerControl2.Selected = true;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                        MessageId.SmartClient.PlaybackCommand,
                                                        new PlaybackCommandData() { Command = PlaybackData.PlayStop }));

            
        }

        #endregion

        private void btnReconnect_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            ConnectMilestone();
            Cursor = Cursors.Default;
        }

        #region Video Playback Menu Controls Event

        private void trackBar01_EditValueChanged(object sender, EventArgs e)
        {
            if (_imageViewerControl1.Selected)
            {
                if (CurrentPlayback == PlayBackDirection.Forward)
                {
                    _speed1 = GetSpeedByIdx(trackBar01.Value);
                    EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed1 }));


                    lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);
                }
                else
                {
                    _speed1 = GetSpeedByIdx(trackBar01.Value);
                    EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed1 }));


                    lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);
                }
            }
        }

        private void btnPlayForward01_Click(object sender, EventArgs e)
        {
            _speed1 = 0.0;
            btnPauseForward01.BringToFront();
            btnPlayBackward01.BringToFront();
            trackBar01.Enabled = true;
            trackBar01.Value = 2;

            CurrentPlayback = PlayBackDirection.Forward;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed1 }));


            lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);
        }

        private void btnPauseForward01_Click(object sender, EventArgs e)
        {
            _speed1 = 0.0;
            btnPlayForward01.BringToFront();
            trackBar01.Enabled = false;
            trackBar01.Value = 2;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayStop }));

            lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);
        }

        private void btnFrameForward01_Click(object sender, EventArgs e)
        {
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.Next }));
        }

        private void btnPlayBackward01_Click(object sender, EventArgs e)
        {
            _speed1 = 0.0;
            btnPauseBackward01.BringToFront();
            btnPlayForward01.BringToFront();
            CurrentPlayback = PlayBackDirection.Backward;
            trackBar01.Enabled = true;
            trackBar01.Value = 2;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayReverse, Speed = _speed1 }));


            lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);
        }

        private void btnPauseBackward01_Click(object sender, EventArgs e)
        {
            _speed1 = 0.0;
            btnPlayBackward01.BringToFront();
            trackBar01.Enabled = false;
            trackBar01.Value = 2;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayStop }));

            lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);
        }

        private void btnFrameBackward01_Click(object sender, EventArgs e)
        {
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                         MessageId.SmartClient.PlaybackCommand,
                                                         new PlaybackCommandData() { Command = PlaybackData.Previous }));
        }

        private void trackBar02_EditValueChanged(object sender, EventArgs e)
        {
            if (_imageViewerControl2.Selected)
            {
                if (CurrentPlayback == PlayBackDirection.Forward)
                {
                    _speed2 = GetSpeedByIdx(trackBar02.Value);
                    EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed2 }), _playbackControllerFQID);


                    lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);
                }
                else
                {
                    _speed2 = GetSpeedByIdx(trackBar02.Value);
                    EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed2 }), _playbackControllerFQID);


                    lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);
                }
            }
        }

        private void btnPlayForward02_Click(object sender, EventArgs e)
        {
            _speed2 = 0.0;
            btnPauseForward02.BringToFront();
            btnPlayBackward02.BringToFront();
            CurrentPlayback = PlayBackDirection.Forward;
            trackBar02.Enabled = true;
            trackBar02.Value = 2;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed2 }),_playbackControllerFQID);


            lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);
        }

        private void btnPauseForward02_Click(object sender, EventArgs e)
        {
            _speed2 = 0.0;
            btnPlayForward02.BringToFront();
            trackBar02.Enabled = false;
            trackBar02.Value = 2;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayStop }),_playbackControllerFQID);

            lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);
        }

        private void btnFrameForward02_Click(object sender, EventArgs e)
        {
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                        MessageId.SmartClient.PlaybackCommand,
                                                        new PlaybackCommandData() { Command = PlaybackData.Next }), _playbackControllerFQID);
        }

        private void btnPlayBackward02_Click(object sender, EventArgs e)
        {
            _speed2 = 0.0;
            btnPauseBackward02.BringToFront();
            btnPlayForward02.BringToFront();
            CurrentPlayback = PlayBackDirection.Backward;
            trackBar02.Enabled = true;
            trackBar02.Value = 2;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayReverse, Speed = _speed2 }), _playbackControllerFQID);


            lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);
        }

        private void btnPauseBackward02_Click(object sender, EventArgs e)
        {
            _speed2 = 0.0;
            btnPlayBackward02.BringToFront();
            trackBar02.Enabled = false;
            trackBar02.Value = 2;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayStop }), _playbackControllerFQID);

            lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);
        }

        private void btnFrameBackward02_Click(object sender, EventArgs e)
        {
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                        MessageId.SmartClient.PlaybackCommand,
                                                        new PlaybackCommandData() { Command = PlaybackData.Previous }), _playbackControllerFQID);
        }

        #endregion
     
        double GetSpeedByIdx(int idx)
        {
            double speed = 0;

            switch (idx)
            {
                case 0:
                    speed = 0.25;
                    break;
                case 1:
                    speed = 0.5;
                    break;
                case 2:
                    speed = 1;
                    break;
                case 3:
                    speed = 2;
                    break;
                case 4:
                    speed = 4;
                    break;
                case 5:
                    speed = 8;
                    break;
                case 6:
                    speed = 16;
                    break;
                default:
                    break;
            }

            return speed;
        }

        void SetDefault()
        {
            _selectItem1 = null;
            _selectItem2 = null;
            _imageViewerControl1 = null;
            _imageViewerControl2 = null;
            _playbackControllerFQID = null;

            _speed1 = 0.0;
            _speed2 = 0.0;

            panel1.Controls.Clear();
            panel2.Controls.Clear();

            lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);
            lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);

            lblTime01.Text = string.Empty;
            lblTime02.Text = string.Empty;

            trackBar02.Enabled = false;
            btnPlayForward02.Enabled = false;
            btnPauseForward02.Enabled = false;
            btnFrameForward02.Enabled = false;
            btnPlayBackward02.Enabled = false;
            btnPauseBackward02.Enabled = false;
            btnFrameBackward02.Enabled = false;
            btnReset02.Enabled = false;
            btnExport2.Enabled = false;

            trackBar01.Enabled = false;
            btnPlayForward01.Enabled = false;
            btnPauseForward01.Enabled = false;
            btnFrameForward01.Enabled = false;
            btnPlayBackward01.Enabled = false;
            btnPauseBackward01.Enabled = false;
            btnFrameBackward01.Enabled = false;
            btnReset01.Enabled = false;
            btnExport1.Enabled = false;

            btnShowInwardCam.Enabled = false;
            btnShowOutwardCam.Enabled = false;
            tabCameraGroups.Visible = false;
        }

        private void btnReset01_Click(object sender, EventArgs e)
        {
            _speed1 = 0.0;
            
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                        MessageId.SmartClient.PlaybackCommand,
                                                        new PlaybackCommandData() { Command = PlaybackData.PlayStop }));

            EnvironmentManager.Instance.Mode = Mode.ClientPlayback;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                   MessageId.SmartClient.PlaybackCommand,
                                                   new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = RecStartTime01, Speed = _speed1 }));

            lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);
        }

        private void btnReset02_Click(object sender, EventArgs e)
        {
            _speed2 = 0.0;
            
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                        MessageId.SmartClient.PlaybackCommand,
                                                        new PlaybackCommandData() { Command = PlaybackData.PlayStop }), _playbackControllerFQID);

            EnvironmentManager.Instance.Mode = Mode.ClientPlayback;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                   MessageId.SmartClient.PlaybackCommand,
                                                   new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = RecStartTime02, Speed = _speed2 }), _playbackControllerFQID);

            lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);
        }

        private void btnExport1_Click(object sender, EventArgs e)
        {
            try
            {


                string folder = String.Format("{0}\\{1}", ParentFolder, selVIN);
                if (!System.IO.Directory.Exists(folder))
                    System.IO.Directory.CreateDirectory(folder);

                if (_imageViewerControl1 != null)
                {
                    try
                    {
                        if (!Directory.Exists(folder))
                            Directory.CreateDirectory(folder);
                    }
                    catch { }

                    folder = String.Format("{0}\\{1} {2}.jpg", folder, rgx.Replace(Camera01, "-"), Convert.ToDateTime(lblTime01.Text).ToString("MM-dd-yy hhmmss"));

                    Bitmap bitmap = _imageViewerControl1.GetCurrentDisplayedImageAsBitmap(false);
                    EnlargeImage form = new EnlargeImage();
                    form.Text = String.Format("Camera: {0}  Timestamp: {1}",Camera01, Convert.ToDateTime(lblTime01.Text).ToString("MM-dd-yy hh:mm:ss tt"));
                    form.SetBitmap(bitmap, folder);
                    form.WindowState = FormWindowState.Maximized;
                    form.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (DebuggingEnabled)
                    MessageBox.Show(ex.ToString());
            }
        }

        private void btnExport2_Click(object sender, EventArgs e)
        {
            try
            {


                string folder = String.Format("{0}\\{1}", ParentFolder, selVIN);
                if (!System.IO.Directory.Exists(folder))
                    System.IO.Directory.CreateDirectory(folder);
                
                if (_imageViewerControl2 != null)
                {
                    try
                    {
                        if (!Directory.Exists(folder))
                            Directory.CreateDirectory(folder);
                    }
                    catch { }

                    folder = String.Format("{0}\\{1} {2}.jpg", folder, rgx.Replace(Camera02, "-"), Convert.ToDateTime(lblTime02.Text).ToString("MM-dd-yy hhmmss"));

                    Bitmap bitmap = _imageViewerControl2.GetCurrentDisplayedImageAsBitmap(false);
                    EnlargeImage form = new EnlargeImage();
                    form.Text = String.Format("Camera: {0}  Timestamp: {1}", Camera02, Convert.ToDateTime(lblTime02.Text).ToString("MM-dd-yy hh:mm:ss tt"));
                    form.SetBitmap(bitmap, folder);
                    form.WindowState = FormWindowState.Maximized;
                    form.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (DebuggingEnabled)
                    MessageBox.Show(ex.ToString());
            }
        }

        private void btnBrowseFolder_Click(object sender, EventArgs e)
        {
            string dir;

            try
            {
                dir = String.IsNullOrEmpty(selVIN) ? string.Format("{0}", ParentFolder) : string.Format("{0}\\{1}", ParentFolder, selVIN);

                if (!System.IO.Directory.Exists(dir))
                    System.IO.Directory.CreateDirectory(dir);


                Process.Start(dir);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnService_Click(object sender, EventArgs e)
        {

        }

        private void backgroundWorkerVMSStatus_DoWork(object sender, DoWorkEventArgs e)
        {
            ConnectMilestone();
        }

        private void backgroundWorkerVMSStatus_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void lbTimeIN_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnShowInwardCam.Enabled = true;
        }

        private void lbTimeOut_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnShowOutwardCam.Enabled = true;
        }
    }
}
