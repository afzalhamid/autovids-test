﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoViewer
{
    public partial class frmConfiguration : Form
    {
        DAL dataAccess = new DAL();
        public frmConfiguration()
        {
            InitializeComponent();
        }

        private void frmConfiguration_Load(object sender, EventArgs e)
        {
            try
            {
                LoadVMS();
                LoadGroups();
                LoadRFiD();
                LoadCameras();

                txtServerID.Visible = false;
                txtRFiDID.Visible = false;
                txtANPRID.Visible = false;
                txtGroupID.Visible = false;
                txtCameraID.Visible = false;

            }
            catch (Exception ex)
            {
                dataAccess.Logging(Module.Application,LogClass.Error, ex.ToString());
            }
        }
                
        void LoadVMS()
        {
            ddlVMSServer.DataSource = null;
            ddlVMSServer.Items.Clear();

            DataTable dtVMS = dataAccess.GetVMSServer();
            gridControlVMS.DataSource = dtVMS;
            ddlVMSServer.DataSource = dtVMS;
            ddlVMSServer.DisplayMember = "ServerName";
            ddlVMSServer.ValueMember = "ID";            
        }

        void LoadRFiD()
        {
            ddlRFiDMarker.DataSource = null;
            ddlRFiDMarker.Items.Clear();

            DataTable dtRFiD = dataAccess.GetAllMarkers();
            gridControlMarker.DataSource = dtRFiD;
            ddlRFiDMarker.DataSource = dtRFiD;
            ddlRFiDMarker.DisplayMember = "Label";
            ddlRFiDMarker.ValueMember = "ID";

        }
        
        void LoadGroups()
        {
            ddlCameraGroup.DataSource = null;
            ddlCameraGroup.Items.Clear();

            DataTable dtCameraGroups = dataAccess.GetAllGroups();
            gridControlCamGroup.DataSource = dtCameraGroups;
            ddlCameraGroup.DataSource = dtCameraGroups;
            ddlCameraGroup.DisplayMember = "Name";
            ddlCameraGroup.ValueMember = "ID";
        }

        void LoadCameras()
        {
            DataTable dtCamera = dataAccess.GetAllCamera();
            gridControlCamera.DataSource = dtCamera;
        }

        void ClearServerFields()
        {
            txtServerName.Text = string.Empty;
            txtServerIP.Text = string.Empty;
            txtServerID.Text = string.Empty;
        }

        void ClearCameraFields()
        {
            txtCameraName.Text = string.Empty;
            ddlVMSServer.SelectedIndex = 0;
            ddlCameraGroup.SelectedIndex = 0;
            ddlRFiDMarker.SelectedIndex = 0;
            txtCameraID.Text = string.Empty;
        }

        void ClearANPRFields()
        {
            txtANPRID.Text = string.Empty;
            txtANPRIP.Text = string.Empty;
            txtANPRNameTag.Text = string.Empty;
        }
     
        void ClearRFiDFields()
        {
            txtRFiDID.Text = string.Empty;
            txtRFiDMarker.Text = string.Empty;
        }

        void ClearGroupFields()
        {
            txtGroupID.Text = string.Empty;
            txtGroupName.Text = string.Empty;
        }

        private void btnAddUpdateVMS_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtServerName.Text) && !string.IsNullOrEmpty(txtServerIP.Text))
                {
                    bool result = dataAccess.AddUpdateVMSServer(string.IsNullOrEmpty(txtServerID.Text) ? 0 : Convert.ToInt32(txtServerID.Text), txtServerName.Text, txtServerIP.Text);
                    if (result)
                    {
                        LoadVMS();

                        ClearServerFields();
                    }
                    else
                        MessageBox.Show("Failed to add/update the server.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                    MessageBox.Show("Enter server name / IP.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                dataAccess.Logging(Module.Application, LogClass.Error, ex.ToString());
                MessageBox.Show("Exception: Failed to add/update the server.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAddUpdateCamera_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtCameraName.Text))
                {
                    int vmsid = Convert.ToInt32(ddlVMSServer.SelectedValue);
                    int grpid = Convert.ToInt32(ddlCameraGroup.SelectedValue);
                    int rfid = Convert.ToInt32(ddlRFiDMarker.SelectedValue);
                    int camid = string.IsNullOrEmpty(txtCameraID.Text) ? 0 : Convert.ToInt32(txtCameraID.Text);
                    string camname = txtCameraName.Text;

                    bool result = dataAccess.AddUpdateCamera(camid, vmsid, rfid, grpid, camname);

                    if (result)
                    {
                        LoadCameras();
                        ClearCameraFields();
                    }
                    else
                        MessageBox.Show("Failed to add/update the camera.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                    MessageBox.Show("Provide all required information.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                dataAccess.Logging(Module.Application, LogClass.Error, ex.ToString());
                MessageBox.Show("Exception: Failed to add/update the camera.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        private void btnAddUpdateANPR_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtANPRNameTag.Text))
                {
                    string direction = rbANPRIN.Checked ? "IN" : "OUT";
                    bool result = dataAccess.AddUpdateANPR(string.IsNullOrEmpty(txtANPRID.Text) ? 0 : Convert.ToInt32(txtANPRID.Text), txtANPRNameTag.Text, txtANPRIP.Text, direction);
                    if (result)
                    {

                        ClearANPRFields();
                    }
                    else
                        MessageBox.Show("Failed to add/update the ANPR.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                    MessageBox.Show("Enter Name /Tag / Sr#.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                dataAccess.Logging(Module.Application, LogClass.Error, ex.ToString());
                MessageBox.Show("Exception: Failed to add/update the ANPR.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        private void btnAddUpdateMarker_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtRFiDMarker.Text))
                {
                    string direction = rbIN.Checked ? "IN" : "OUT";

                    bool result = dataAccess.AddUpdateRFiD(string.IsNullOrEmpty(txtRFiDID.Text) ? 0 : Convert.ToInt32(txtRFiDID.Text), txtRFiDMarker.Text, direction);
                    if (result)
                    {
                        LoadRFiD();

                        ClearRFiDFields();
                    }
                    else
                        MessageBox.Show("Failed to add/update the RFiD.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                    MessageBox.Show("Enter Marker ID / Number.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                dataAccess.Logging(Module.Application, LogClass.Error, ex.ToString());
                MessageBox.Show("Exception: Failed to add/update the RFiD.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        private void btnAddUpdateGroup_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtGroupName.Text))
                {
                    bool result = dataAccess.AddUpdateGroup(string.IsNullOrEmpty(txtGroupID.Text) ? 0 : Convert.ToInt32(txtGroupID.Text), txtGroupName.Text);
                    if (result)
                    {
                        LoadGroups();

                        ClearGroupFields();
                    }
                    else
                        MessageBox.Show("Failed to add/update the Group.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                    MessageBox.Show("Enter Group Name.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                dataAccess.Logging(Module.Application, LogClass.Error, ex.ToString());
                MessageBox.Show("Exception: Failed to add/update the Group.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDeleteCamera_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Are you sure to continue with delete process ?", "Delete Item", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            int ID = string.IsNullOrEmpty(txtCameraID.Text) ? 0 : Convert.ToInt32(txtCameraID.Text);

            if (ID > 0 && result == DialogResult.Yes)
                if (dataAccess.DeleteCamera(ID))
                {
                    LoadCameras();
                    ClearCameraFields();
                }
        }

        private void btnDeleteVMS_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Are you sure to continue with delete process ?", "Delete Item", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            int ID = string.IsNullOrEmpty(txtServerID.Text) ? 0 : Convert.ToInt32(txtServerID.Text);

            if (ID > 0 && result == DialogResult.Yes)
                if (dataAccess.DeleteVMSServer(ID))
                {
                    LoadVMS();
                    ClearServerFields();
                }
        }

        private void btnDeleteMarker_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Are you sure to continue with delete process ?", "Delete Item", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            int ID = string.IsNullOrEmpty(txtRFiDID.Text) ? 0 : Convert.ToInt32(txtRFiDID.Text);

            if (ID > 0 && result == DialogResult.Yes)
                if (dataAccess.DeleteRFiD(ID))
                {
                    LoadRFiD();
                    ClearRFiDFields();
                }
        }

        private void btnDeleteGroup_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Are you sure to continue with delete process ?", "Delete Item", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            int ID = string.IsNullOrEmpty(txtGroupID.Text) ? 0 : Convert.ToInt32(txtGroupID.Text);

            if (ID > 0 && result == DialogResult.Yes)
                if (dataAccess.DeleteCameraGroup(ID))
                {
                    LoadGroups();
                    ClearGroupFields();
                }
        }

        private void btnDeleteANPR_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Are you sure to continue with delete process ?", "Delete Item", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            int ID = string.IsNullOrEmpty(txtANPRID.Text) ? 0 : Convert.ToInt32(txtANPRID.Text);

            if (ID > 0 && result == DialogResult.Yes)
                if (dataAccess.DeleteANPR(ID))
                {
                    ClearANPRFields();
                }
        }

        private void btnResetMarker_Click(object sender, EventArgs e)
        {
            ClearRFiDFields();
        }

        private void btnResetGroup_Click(object sender, EventArgs e)
        {
            ClearGroupFields();
        }

        private void btnResetANPR_Click(object sender, EventArgs e)
        {
            ClearANPRFields();
        }

        private void btnResetVMS_Click(object sender, EventArgs e)
        {
            ClearServerFields();
        }

        private void btnResetCamera_Click(object sender, EventArgs e)
        {
            ClearCameraFields();
        }

        private void gridViewCamera_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            ClearCameraFields();

            string cameraName = Convert.ToString(gridViewCamera.GetRowCellValue(e.RowHandle, "Camera"));
            string camid = Convert.ToString(gridViewCamera.GetRowCellValue(e.RowHandle, "ID"));
            string vmsid = Convert.ToString(gridViewCamera.GetRowCellValue(e.RowHandle, "ServerId"));
            string groupid = Convert.ToString(gridViewCamera.GetRowCellValue(e.RowHandle, "GroupID"));
            string rfid = Convert.ToString(gridViewCamera.GetRowCellValue(e.RowHandle, "RFID"));

            txtCameraID.Text = camid;
            txtCameraName.Text = cameraName;

            for (int i = 0; i < ddlVMSServer.Items.Count; i++)
            {
                DataRowView item = ddlVMSServer.Items[i] as DataRowView;
                if (item.Row["ID"].ToString() == vmsid)
                    ddlVMSServer.SelectedValue = vmsid;
            }
            
            for (int i = 0; i < ddlCameraGroup.Items.Count; i++)
            {
                DataRowView item = ddlCameraGroup.Items[i] as DataRowView;
                if (item.Row["ID"].ToString() == groupid)
                    ddlCameraGroup.SelectedValue = groupid;
            }

            for (int i = 0; i < ddlRFiDMarker.Items.Count; i++)
            {
                DataRowView item = ddlRFiDMarker.Items[i] as DataRowView;
                if (item.Row["ID"].ToString() == rfid)
                    ddlRFiDMarker.SelectedValue = rfid;
            }

        }

        private void gridViewVMS_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            ClearServerFields();

            string name = Convert.ToString(gridViewVMS.GetRowCellValue(e.RowHandle, "ServerName"));
            string ip = Convert.ToString(gridViewVMS.GetRowCellValue(e.RowHandle, "IP"));
            string id = Convert.ToString(gridViewVMS.GetRowCellValue(e.RowHandle, "ID"));

            txtServerName.Text = name;
            txtServerIP.Text = ip;
            txtServerID.Text = id;
        }

        private void gridViewANPR_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            ClearANPRFields();

            string name = Convert.ToString(gridViewANPR.GetRowCellValue(e.RowHandle, "SRNO"));
            string ip = Convert.ToString(gridViewANPR.GetRowCellValue(e.RowHandle, "IP"));
            string id = Convert.ToString(gridViewANPR.GetRowCellValue(e.RowHandle, "ID"));
            string desc = Convert.ToString(gridViewANPR.GetRowCellValue(e.RowHandle, "Description"));
            
            if (desc == "IN")
                rbANPRIN.Checked = true;
            else
                rbANPROUT.Checked = true;

            txtANPRID.Text = id;
            txtANPRNameTag.Text = name;
            txtANPRIP.Text = ip;
        }

        private void gridViewMarker_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            ClearRFiDFields();

            string marker = Convert.ToString(gridViewMarker.GetRowCellValue(e.RowHandle, "Marker"));
            string id = Convert.ToString(gridViewMarker.GetRowCellValue(e.RowHandle, "ID"));
            string desc = Convert.ToString(gridViewMarker.GetRowCellValue(e.RowHandle, "Description"));

            txtRFiDID.Text = id;
            txtRFiDMarker.Text = marker;
            if (desc == "IN")
                rbIN.Checked = true;
            else
                rbOUT.Checked = true;
        }

        private void gridViewCamGroup_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            ClearGroupFields();

            string name = Convert.ToString(gridViewCamGroup.GetRowCellValue(e.RowHandle, "Name"));
            string id = Convert.ToString(gridViewCamGroup.GetRowCellValue(e.RowHandle, "ID"));

            txtGroupID.Text = id;
            txtGroupName.Text = name;
        }
    }
}
