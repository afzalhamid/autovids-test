﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DataAccessLayer;
using DevExpress.XtraEditors;

namespace VideoViewer
{    
    public partial class frmApplication : Form
    {
        List<DateTime> _visitDates { get; set; }
        private SimpleButton btnSelected { get; set; }
        public int UserID { get; set; }
        public bool vmsConnected { get; set; }
        public Form subForm { get; set; }

        #region private fields

        DAL DAL = new DAL();
      
        private string ParentFolder { get { return System.Configuration.ConfigurationManager.AppSettings["FolderPath"]; } }

        private bool IsAdvanceEdt { get { return System.Configuration.ConfigurationManager.AppSettings["AdvanceEdt"] == "1" ? true : false; } }

        private bool DebuggingEnabled { get { return System.Configuration.ConfigurationManager.AppSettings["DebugMode"] == "1" ? true : false; } }
        private int PreRecPlayback { get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PreRecPlayback"]); } }
        
        #endregion
        public frmApplication()
        {
            InitializeComponent();
        }

        private void frmApplication_Load(object sender, EventArgs e)
        {
            if (subForm != null)
            {                
                ShowSubForm(subForm);

                switch (subForm.Text)
                {
                    case "User Management":
                        btnSelected = btnUserManagement;
                        FormatButton(btnUserManagement, true);
                        break;
                    case "Log":
                        btnSelected = btnHelpTroubleshoot;
                        FormatButton(btnHelpTroubleshoot, true);
                        break;
                    case "VMS Settings":
                        btnSelected = btnConfiguration;
                        FormatButton(btnConfiguration, true);
                        break;
                    case "Events":
                        btnSelected = btnAlarm;
                        FormatButton(btnAlarm, true);
                        break;
                    default:
                        break;
                }
                subForm = null;
            }
        }

        void ShowSubForm(Form objForm)
        {
            try
            {
                panelDisplay.Controls.Clear();
                objForm.TopLevel = false;
                panelDisplay.Controls.Add(objForm);
                objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                objForm.Dock = DockStyle.Fill;
                objForm.Show();
            }
            catch (Exception ex)
            {
                DAL.Logging(Module.Application,LogClass.Error, ex.ToString());
            }
        }

        private void btnCloseApplication_Click(object sender, EventArgs e)
        {
            frmLogin frmLogin = new frmLogin();
            frmLogin.Close();

            try
            {
                foreach (Form frm in this.MdiChildren)
                {
                    frm.Dispose();
                    frm.Close();
                }
            }
            catch { }

            Application.ExitThread();
        }

        private void accordionControlElementHMI1_Click(object sender, EventArgs e)
        {
            btnSelected = btnHMI;
            DisposeForms();
            frmMain objForm = new frmMain();
            objForm.Show();
            this.Close();
        }

        private void accordionControlMilestoneAlarm_Click(object sender, EventArgs e)
        {
            btnSelected = btnAlarm;
            DisposeForms();
            frmVMSAlarm objForm = new frmVMSAlarm();
            ShowSubForm(objForm);
            SetButtonSeletedState(btnSelected);
        }
        

        private void accordionControlElementUM_Click(object sender, EventArgs e)
        {
            btnSelected = btnUserManagement;

            DisposeForms();
            frmUserManagement objForm = new frmUserManagement();
            ShowSubForm(objForm);

            SetButtonSeletedState(btnSelected);
        }
               
        private void accordionControlVisits_Click(object sender, EventArgs e)
        {
            btnSelected = btnAlarm;
            DisposeForms();
            frmVIN objForm = new frmVIN();
            ShowSubForm(objForm);
            SetButtonSeletedState(btnSelected);
        }

        private void accordionControlEevents_Click(object sender, EventArgs e)
        {
            btnSelected = btnAlarm;
            DisposeForms();
            frmEvents objForm = new frmEvents();
            ShowSubForm(objForm);
            SetButtonSeletedState(btnSelected);
        }

        private void accordionControlElementVmsCamera_Click(object sender, EventArgs e)
        {
            btnSelected = btnConfiguration;

            DisposeForms();
            frmConfiguration objForm = new frmConfiguration();
            ShowSubForm(objForm);

            SetButtonSeletedState(btnConfiguration);
        }

        private void accordionControlElementSysSettings_Click(object sender, EventArgs e)
        {
            btnSelected = btnConfiguration;

            DisposeForms();
            frmConnections objForm = new frmConnections();
            ShowSubForm(objForm);

            SetButtonSeletedState(btnConfiguration);
        }

        private void accordionControlElementResetPassword_Click(object sender, EventArgs e)
        {
            SimpleButton btnTemp = new SimpleButton();
            btnSelected = btnTemp;
            panelDisplay.Controls.Clear();

            DisposeForms();
            frmResetPassword objForm = new frmResetPassword();
            objForm.userID = UserID;
            objForm.ShowDialog();

            SetButtonSeletedState(btnTemp);
        }


        void SetButtonSeletedState(SimpleButton btn)
        {
            foreach (var item in panelMenu.Controls)
            {
                SimpleButton sbtn = item as SimpleButton;
                if (btn == sbtn)
                    FormatButton(sbtn, true);
                else
                    FormatButton(sbtn, false);
            }
        }

        void FormatButton(SimpleButton btn, bool hover)
        {
            btn.Image = GetButtonImage(btn, hover);
            btn.Appearance.BackColor = hover ? Color.FromArgb(0, 92, 185) : Color.FromArgb(230, 230, 230);
            btn.Appearance.ForeColor = hover ? Color.White : Color.FromArgb(0, 92, 185);

        }

        Bitmap GetButtonImage(SimpleButton btn, bool hover)
        {
            Bitmap img = null;

            switch (btn.Text)
            {
                case "Main Layout":
                    img = hover ? Icons.hmiLayoutHover : Icons.hmiLayout;
                    break;

                case "Visit Log":
                    img = hover ? Icons.alarmEventsHover : Icons.alarmEvents;
                    break;

                case "System Configuration":
                    img = hover ? Icons.systemConfiguratoinHover : Icons.systemConfiguration;
                    break;

                case "User Management":
                    img = hover ? Icons.userManagementHover : Icons.userManagement;
                    break;

                case "Logs":
                    img = hover ? Icons.logsHover : Icons.logs;
                    break;
                default:
                    break;
            }

            return img;
        }

        private void btnHMI_Click(object sender, EventArgs e)
        {
            btnSelected = sender as SimpleButton;

            frmMain objForm = new frmMain();
            objForm.Show();
            SetButtonSeletedState(sender as SimpleButton);
            this.Close();
        }

        private void btnAlarm_Click(object sender, EventArgs e)
        {
            btnSelected = sender as SimpleButton;
            DisposeForms();
            frmVIN objForm = new frmVIN();
            ShowSubForm(objForm);
            SetButtonSeletedState(sender as SimpleButton);
        }

        private void btnConfiguration_Click(object sender, EventArgs e)
        {
            btnSelected = sender as SimpleButton;
            DisposeForms();

            frmConfiguration objForm = new frmConfiguration();
            ShowSubForm(objForm);
            SetButtonSeletedState(sender as SimpleButton);
        }

        private void btnUserManagement_Click(object sender, EventArgs e)
        {
            btnSelected = sender as SimpleButton;
            DisposeForms();
            frmUserManagement objForm = new frmUserManagement();
            ShowSubForm(objForm);
            SetButtonSeletedState(sender as SimpleButton);
        }

        private void btnHelpTroubleshoot_Click(object sender, EventArgs e)
        {
            btnSelected = sender as SimpleButton;
            frmLog objForm = new frmLog();
            ShowSubForm(objForm);
            SetButtonSeletedState(sender as SimpleButton);
        }

        private void btnHMI_MouseLeave(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, false);
        }

        private void btnHMI_MouseEnter(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, true);
        }

        private void btnAlarm_MouseEnter(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, true);
        }

        private void btnAlarm_MouseLeave(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, false);
        }

        private void btnConfiguration_MouseEnter(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, true);
        }

        private void btnConfiguration_MouseLeave(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, false);
        }

        private void btnUserManagement_MouseEnter(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, true);
        }

        private void btnUserManagement_MouseLeave(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, false);
        }

        private void btnHelpTroubleshoot_MouseEnter(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, true);
        }

        private void btnHelpTroubleshoot_MouseLeave(object sender, EventArgs e)
        {
            SimpleButton sBtn = sender as SimpleButton;
            if (btnSelected != sBtn)
                FormatButton(sBtn, false);
        }

        void DisposeForms()
        {
            try
            {
                //if (formHMI != null)
                //{
                //    formHMI.timer.Stop();
                //    formHMI.timer.Enabled = false;
                //    formHMI.Dispose();
                //    formHMI = null;
                //}
                //if (formAlarm != null)
                //{
                //    formAlarm.timer.Stop();
                //    formAlarm.timer.Enabled = false;
                //    formAlarm.Dispose();
                //    formAlarm = null;
                //}
            }
            catch { }
        }

        private void btnBrowseFolder_Click(object sender, EventArgs e)
        {
            string dir;

            try
            {
                dir = string.Format("{0}", ParentFolder);

                if (!System.IO.Directory.Exists(dir))
                    System.IO.Directory.CreateDirectory(dir);


                Process.Start(dir);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

    }
}
