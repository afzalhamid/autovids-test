﻿namespace VideoViewer
{
    partial class frmConnections
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConnections));
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelOPCTitle = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panelOPCControls = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.txtTCPServerIP = new System.Windows.Forms.MaskedTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtANPRPortNumber = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtEventServerPortNumber = new System.Windows.Forms.MaskedTextBox();
            this.txtVMSRetentionTime = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtVMSuserName = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtVMSPortNumber = new System.Windows.Forms.MaskedTextBox();
            this.txtVMSPassword = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtVMSServerName = new System.Windows.Forms.MaskedTextBox();
            this.txtVMSIP = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnAddUpdate = new DevExpress.XtraEditors.SimpleButton();
            this.label14 = new System.Windows.Forms.Label();
            this.panelMsgDelimiter = new System.Windows.Forms.Panel();
            this.panelVMSEventPortNumber = new System.Windows.Forms.Panel();
            this.panelVMSRetention = new System.Windows.Forms.Panel();
            this.panelTCPPort = new System.Windows.Forms.Panel();
            this.panelTCPIP = new System.Windows.Forms.Panel();
            this.panelVMPort = new System.Windows.Forms.Panel();
            this.panelVMPassword = new System.Windows.Forms.Panel();
            this.panelVMSServerIP = new System.Windows.Forms.Panel();
            this.panelVMSUserName = new System.Windows.Forms.Panel();
            this.panelVMSServerName = new System.Windows.Forms.Panel();
            this.panelVMSControls = new System.Windows.Forms.Panel();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.ddlVMSEdition = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanelMain.SuspendLayout();
            this.panelOPCTitle.SuspendLayout();
            this.panelOPCControls.SuspendLayout();
            this.panelMsgDelimiter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelMain.ColumnCount = 3;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanelMain.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.panelOPCTitle, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.panelOPCControls, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.panelVMSControls, 1, 1);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 2;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1829, 1028);
            this.tableLayoutPanelMain.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(551, 58);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(542, 967);
            this.panel1.TabIndex = 5;
            // 
            // panelOPCTitle
            // 
            this.panelOPCTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panelOPCTitle.Controls.Add(this.label1);
            this.panelOPCTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelOPCTitle.ForeColor = System.Drawing.Color.White;
            this.panelOPCTitle.Location = new System.Drawing.Point(3, 3);
            this.panelOPCTitle.Name = "panelOPCTitle";
            this.panelOPCTitle.Size = new System.Drawing.Size(542, 49);
            this.panelOPCTitle.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "SYSTEM SETTINGS";
            // 
            // panelOPCControls
            // 
            this.panelOPCControls.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panelOPCControls.Controls.Add(this.label24);
            this.panelOPCControls.Controls.Add(this.txtTCPServerIP);
            this.panelOPCControls.Controls.Add(this.label21);
            this.panelOPCControls.Controls.Add(this.txtANPRPortNumber);
            this.panelOPCControls.Controls.Add(this.label11);
            this.panelOPCControls.Controls.Add(this.txtEventServerPortNumber);
            this.panelOPCControls.Controls.Add(this.txtVMSRetentionTime);
            this.panelOPCControls.Controls.Add(this.label12);
            this.panelOPCControls.Controls.Add(this.label13);
            this.panelOPCControls.Controls.Add(this.txtVMSuserName);
            this.panelOPCControls.Controls.Add(this.label10);
            this.panelOPCControls.Controls.Add(this.txtVMSPortNumber);
            this.panelOPCControls.Controls.Add(this.txtVMSPassword);
            this.panelOPCControls.Controls.Add(this.label7);
            this.panelOPCControls.Controls.Add(this.label8);
            this.panelOPCControls.Controls.Add(this.txtVMSServerName);
            this.panelOPCControls.Controls.Add(this.txtVMSIP);
            this.panelOPCControls.Controls.Add(this.label9);
            this.panelOPCControls.Controls.Add(this.btnAddUpdate);
            this.panelOPCControls.Controls.Add(this.label14);
            this.panelOPCControls.Controls.Add(this.panelMsgDelimiter);
            this.panelOPCControls.Controls.Add(this.panelVMSEventPortNumber);
            this.panelOPCControls.Controls.Add(this.panelVMSRetention);
            this.panelOPCControls.Controls.Add(this.panelTCPPort);
            this.panelOPCControls.Controls.Add(this.panelTCPIP);
            this.panelOPCControls.Controls.Add(this.panelVMPort);
            this.panelOPCControls.Controls.Add(this.panelVMPassword);
            this.panelOPCControls.Controls.Add(this.panelVMSServerIP);
            this.panelOPCControls.Controls.Add(this.panelVMSUserName);
            this.panelOPCControls.Controls.Add(this.panelVMSServerName);
            this.panelOPCControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelOPCControls.Location = new System.Drawing.Point(3, 58);
            this.panelOPCControls.Name = "panelOPCControls";
            this.panelOPCControls.Size = new System.Drawing.Size(542, 967);
            this.panelOPCControls.TabIndex = 3;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(36, 233);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(106, 17);
            this.label24.TabIndex = 273;
            this.label24.Text = "Milestone Edition";
            // 
            // txtTCPServerIP
            // 
            this.txtTCPServerIP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTCPServerIP.Location = new System.Drawing.Point(43, 606);
            this.txtTCPServerIP.Name = "txtTCPServerIP";
            this.txtTCPServerIP.Size = new System.Drawing.Size(275, 17);
            this.txtTCPServerIP.TabIndex = 269;
            this.txtTCPServerIP.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(36, 579);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(94, 17);
            this.label21.TabIndex = 271;
            this.label21.Text = "OMW Server IP";
            // 
            // txtANPRPortNumber
            // 
            this.txtANPRPortNumber.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtANPRPortNumber.Location = new System.Drawing.Point(43, 677);
            this.txtANPRPortNumber.Name = "txtANPRPortNumber";
            this.txtANPRPortNumber.Size = new System.Drawing.Size(275, 17);
            this.txtANPRPortNumber.TabIndex = 270;
            this.txtANPRPortNumber.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(36, 649);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(197, 17);
            this.label11.TabIndex = 261;
            this.label11.Text = "OMW Port Number [TCP Listener]";
            // 
            // txtEventServerPortNumber
            // 
            this.txtEventServerPortNumber.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEventServerPortNumber.Location = new System.Drawing.Point(43, 468);
            this.txtEventServerPortNumber.Name = "txtEventServerPortNumber";
            this.txtEventServerPortNumber.Size = new System.Drawing.Size(275, 17);
            this.txtEventServerPortNumber.TabIndex = 267;
            this.txtEventServerPortNumber.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // txtVMSRetentionTime
            // 
            this.txtVMSRetentionTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtVMSRetentionTime.Location = new System.Drawing.Point(43, 539);
            this.txtVMSRetentionTime.Name = "txtVMSRetentionTime";
            this.txtVMSRetentionTime.Size = new System.Drawing.Size(275, 17);
            this.txtVMSRetentionTime.TabIndex = 268;
            this.txtVMSRetentionTime.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(36, 440);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(113, 17);
            this.label12.TabIndex = 259;
            this.label12.Text = "VMS Port  Number";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(36, 511);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(162, 17);
            this.label13.TabIndex = 260;
            this.label13.Text = "Recording Retention (Days)";
            // 
            // txtVMSuserName
            // 
            this.txtVMSuserName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtVMSuserName.Location = new System.Drawing.Point(43, 330);
            this.txtVMSuserName.Name = "txtVMSuserName";
            this.txtVMSuserName.Size = new System.Drawing.Size(275, 17);
            this.txtVMSuserName.TabIndex = 265;
            this.txtVMSuserName.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(36, 302);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 17);
            this.label10.TabIndex = 258;
            this.label10.Text = "User Name";
            // 
            // txtVMSPortNumber
            // 
            this.txtVMSPortNumber.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtVMSPortNumber.Location = new System.Drawing.Point(43, 196);
            this.txtVMSPortNumber.Name = "txtVMSPortNumber";
            this.txtVMSPortNumber.Size = new System.Drawing.Size(275, 17);
            this.txtVMSPortNumber.TabIndex = 264;
            this.txtVMSPortNumber.Text = "80";
            this.txtVMSPortNumber.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtVMSPortNumber.ValidatingType = typeof(int);
            // 
            // txtVMSPassword
            // 
            this.txtVMSPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtVMSPassword.Location = new System.Drawing.Point(43, 399);
            this.txtVMSPassword.Name = "txtVMSPassword";
            this.txtVMSPassword.PasswordChar = '*';
            this.txtVMSPassword.Size = new System.Drawing.Size(275, 17);
            this.txtVMSPassword.TabIndex = 266;
            this.txtVMSPassword.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtVMSPassword.UseSystemPasswordChar = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 17);
            this.label7.TabIndex = 256;
            this.label7.Text = "VSM Port Number";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(36, 370);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 17);
            this.label8.TabIndex = 257;
            this.label8.Text = "Password";
            // 
            // txtVMSServerName
            // 
            this.txtVMSServerName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtVMSServerName.Location = new System.Drawing.Point(43, 61);
            this.txtVMSServerName.Name = "txtVMSServerName";
            this.txtVMSServerName.Size = new System.Drawing.Size(275, 17);
            this.txtVMSServerName.TabIndex = 262;
            this.txtVMSServerName.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // txtVMSIP
            // 
            this.txtVMSIP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtVMSIP.Location = new System.Drawing.Point(43, 126);
            this.txtVMSIP.Name = "txtVMSIP";
            this.txtVMSIP.Size = new System.Drawing.Size(275, 17);
            this.txtVMSIP.TabIndex = 263;
            this.txtVMSIP.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(36, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 17);
            this.label9.TabIndex = 254;
            this.label9.Text = "VMS Server Name";
            // 
            // btnAddUpdate
            // 
            this.btnAddUpdate.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnAddUpdate.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnAddUpdate.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnAddUpdate.Appearance.Options.UseBackColor = true;
            this.btnAddUpdate.Appearance.Options.UseFont = true;
            this.btnAddUpdate.Appearance.Options.UseForeColor = true;
            this.btnAddUpdate.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnAddUpdate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddUpdate.ImageOptions.Image")));
            this.btnAddUpdate.Location = new System.Drawing.Point(36, 730);
            this.btnAddUpdate.Name = "btnAddUpdate";
            this.btnAddUpdate.Size = new System.Drawing.Size(100, 30);
            this.btnAddUpdate.TabIndex = 272;
            this.btnAddUpdate.Text = "Save All";
            this.btnAddUpdate.Click += new System.EventHandler(this.btnAddUpdate_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(36, 98);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 17);
            this.label14.TabIndex = 255;
            this.label14.Text = "VMS Server IP";
            // 
            // panelMsgDelimiter
            // 
            this.panelMsgDelimiter.BackColor = System.Drawing.Color.White;
            this.panelMsgDelimiter.Controls.Add(this.ddlVMSEdition);
            this.panelMsgDelimiter.Location = new System.Drawing.Point(36, 252);
            this.panelMsgDelimiter.Name = "panelMsgDelimiter";
            this.panelMsgDelimiter.Size = new System.Drawing.Size(288, 35);
            this.panelMsgDelimiter.TabIndex = 281;
            // 
            // panelVMSEventPortNumber
            // 
            this.panelVMSEventPortNumber.BackColor = System.Drawing.Color.White;
            this.panelVMSEventPortNumber.Location = new System.Drawing.Point(36, 459);
            this.panelVMSEventPortNumber.Name = "panelVMSEventPortNumber";
            this.panelVMSEventPortNumber.Size = new System.Drawing.Size(288, 35);
            this.panelVMSEventPortNumber.TabIndex = 275;
            // 
            // panelVMSRetention
            // 
            this.panelVMSRetention.BackColor = System.Drawing.Color.White;
            this.panelVMSRetention.Location = new System.Drawing.Point(36, 530);
            this.panelVMSRetention.Name = "panelVMSRetention";
            this.panelVMSRetention.Size = new System.Drawing.Size(288, 35);
            this.panelVMSRetention.TabIndex = 282;
            // 
            // panelTCPPort
            // 
            this.panelTCPPort.BackColor = System.Drawing.Color.White;
            this.panelTCPPort.Location = new System.Drawing.Point(36, 668);
            this.panelTCPPort.Name = "panelTCPPort";
            this.panelTCPPort.Size = new System.Drawing.Size(288, 35);
            this.panelTCPPort.TabIndex = 283;
            // 
            // panelTCPIP
            // 
            this.panelTCPIP.BackColor = System.Drawing.Color.White;
            this.panelTCPIP.Location = new System.Drawing.Point(36, 598);
            this.panelTCPIP.Name = "panelTCPIP";
            this.panelTCPIP.Size = new System.Drawing.Size(288, 35);
            this.panelTCPIP.TabIndex = 284;
            // 
            // panelVMPort
            // 
            this.panelVMPort.BackColor = System.Drawing.Color.White;
            this.panelVMPort.Location = new System.Drawing.Point(36, 187);
            this.panelVMPort.Name = "panelVMPort";
            this.panelVMPort.Size = new System.Drawing.Size(288, 35);
            this.panelVMPort.TabIndex = 276;
            // 
            // panelVMPassword
            // 
            this.panelVMPassword.BackColor = System.Drawing.Color.White;
            this.panelVMPassword.Location = new System.Drawing.Point(36, 390);
            this.panelVMPassword.Name = "panelVMPassword";
            this.panelVMPassword.Size = new System.Drawing.Size(288, 35);
            this.panelVMPassword.TabIndex = 277;
            // 
            // panelVMSServerIP
            // 
            this.panelVMSServerIP.BackColor = System.Drawing.Color.White;
            this.panelVMSServerIP.Location = new System.Drawing.Point(36, 117);
            this.panelVMSServerIP.Name = "panelVMSServerIP";
            this.panelVMSServerIP.Size = new System.Drawing.Size(288, 35);
            this.panelVMSServerIP.TabIndex = 278;
            // 
            // panelVMSUserName
            // 
            this.panelVMSUserName.BackColor = System.Drawing.Color.White;
            this.panelVMSUserName.Location = new System.Drawing.Point(36, 321);
            this.panelVMSUserName.Name = "panelVMSUserName";
            this.panelVMSUserName.Size = new System.Drawing.Size(288, 35);
            this.panelVMSUserName.TabIndex = 279;
            // 
            // panelVMSServerName
            // 
            this.panelVMSServerName.BackColor = System.Drawing.Color.White;
            this.panelVMSServerName.Location = new System.Drawing.Point(36, 52);
            this.panelVMSServerName.Name = "panelVMSServerName";
            this.panelVMSServerName.Size = new System.Drawing.Size(288, 35);
            this.panelVMSServerName.TabIndex = 280;
            // 
            // panelVMSControls
            // 
            this.panelVMSControls.BackColor = System.Drawing.Color.Transparent;
            this.panelVMSControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelVMSControls.Location = new System.Drawing.Point(1099, 58);
            this.panelVMSControls.Name = "panelVMSControls";
            this.panelVMSControls.Size = new System.Drawing.Size(727, 967);
            this.panelVMSControls.TabIndex = 4;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "circle_green.png");
            this.imageList.Images.SetKeyName(1, "circle_grey.png");
            this.imageList.Images.SetKeyName(2, "circle_red.png");
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // ddlVMSEdition
            // 
            this.ddlVMSEdition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlVMSEdition.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ddlVMSEdition.FormattingEnabled = true;
            this.ddlVMSEdition.Items.AddRange(new object[] {
            "Professional",
            "Advanced"});
            this.ddlVMSEdition.Location = new System.Drawing.Point(6, 6);
            this.ddlVMSEdition.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ddlVMSEdition.Name = "ddlVMSEdition";
            this.ddlVMSEdition.Size = new System.Drawing.Size(275, 23);
            this.ddlVMSEdition.TabIndex = 520;
            // 
            // frmConnections
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1829, 1028);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Font = new System.Drawing.Font("Calibri", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmConnections";
            this.Text = "frmConnections";
            this.Load += new System.EventHandler(this.frmConnections_Load);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.panelOPCTitle.ResumeLayout(false);
            this.panelOPCTitle.PerformLayout();
            this.panelOPCControls.ResumeLayout(false);
            this.panelOPCControls.PerformLayout();
            this.panelMsgDelimiter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.Panel panelOPCTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelOPCControls;
        private System.Windows.Forms.Panel panelVMSControls;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.MaskedTextBox txtTCPServerIP;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.MaskedTextBox txtANPRPortNumber;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox txtEventServerPortNumber;
        private System.Windows.Forms.MaskedTextBox txtVMSRetentionTime;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox txtVMSuserName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox txtVMSPortNumber;
        private System.Windows.Forms.MaskedTextBox txtVMSPassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox txtVMSServerName;
        private System.Windows.Forms.MaskedTextBox txtVMSIP;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.SimpleButton btnAddUpdate;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panelMsgDelimiter;
        private System.Windows.Forms.Panel panelVMSEventPortNumber;
        private System.Windows.Forms.Panel panelVMSRetention;
        private System.Windows.Forms.Panel panelTCPPort;
        private System.Windows.Forms.Panel panelTCPIP;
        private System.Windows.Forms.Panel panelVMPort;
        private System.Windows.Forms.Panel panelVMPassword;
        private System.Windows.Forms.Panel panelVMSServerIP;
        private System.Windows.Forms.Panel panelVMSUserName;
        private System.Windows.Forms.Panel panelVMSServerName;
        private System.Windows.Forms.ComboBox ddlVMSEdition;
    }
}