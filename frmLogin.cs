﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoViewer
{
    public partial class frmLogin : Form
    {
        DAL dataAccess = new DAL();
        private bool milestoneConnected { get; set; }
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        Cursor defaultCursor = Cursor.Current;

        bool prevAutoLoginStatus { get; set; }
        string prevAutoLoginUserID { get; set; }

        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Cursor defaultCurose = Cursor.Current;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                Login();
            }
            catch
            {

            }
            finally
            {
                Cursor.Current = defaultCurose;
            }

        }

        void Login()
        {
            
            int userID = 0;

            if (dataAccess.AuthenticateUser(txtUserName.Text.Trim(), txtPassword.Text.Trim(), ref userID))
            {
                try
                {
                    if (prevAutoLoginStatus != checkBoxAutoLogin.Checked || prevAutoLoginUserID.Trim().ToUpper() != txtUserName.Text.Trim().ToUpper() && !string.IsNullOrEmpty(prevAutoLoginUserID))
                        SetAutoLogin();

                    this.Hide();

                    frmMain mainApp = new frmMain();
                    mainApp.UserID = userID;
                    mainApp.Show();
                }

                catch (Exception ex)
                {
                    dataAccess.Logging(Module.Application,LogClass.Error, ex.ToString());
                }

            }
            else
            {
                lblMessage.Text = "Invalid username or password.";
            }
           
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            if (timer.Enabled)
            {
                timer.Stop();
                timer.Enabled = false;

                txtPassword.Enabled = true;
                txtUserName.Enabled = true;
                btnLogin.Enabled = true;
                checkBoxAutoLogin.Checked = false;
               
            }

            txtUserName.Text = string.Empty;
            txtPassword.Text = string.Empty;
            lblMessage.Text = string.Empty;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            this.WindowState = FormWindowState.Maximized;
            //Get App.Config Keys
            this.AcceptButton = btnLogin;
            txtUserName.Focus();

            timer.Tick += Timer_Tick;
            timer.Interval = 5000;


            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            string autoLogin = config.AppSettings.Settings["AutoLogin"].Value;
            string userName = config.AppSettings.Settings["UserName"].Value;
            string password = config.AppSettings.Settings["Password"].Value;
            string title = System.Configuration.ConfigurationManager.AppSettings["Title"];

            //config.AppSettings.Settings["UserId"].Value = "myUserId";
            //config.Save(ConfigurationSaveMode.Modified);

            //if (!milestoneConnected)
            //    lblMessage.Text = "Database / VMS services are down.";

            checkBoxAutoLogin.Checked = autoLogin == "1" ? true : false;

            if (autoLogin == "1")
            {

                txtUserName.Text = userName;
                txtPassword.Text = dataAccess.Decrypt(password);

                txtPassword.Enabled = false;
                txtUserName.Enabled = false;

                prevAutoLoginUserID = userName;

                btnLogin.Enabled = false;


                timer.Start();
            }

            prevAutoLoginStatus = checkBoxAutoLogin.Checked;
            prevAutoLoginUserID = txtUserName.Text;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();
            timer.Enabled = false;

            Login();
        }

        void SetAutoLogin()
        {
            try
            {
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["AutoLogin"].Value = checkBoxAutoLogin.Checked ? "1" : "0";
                config.AppSettings.Settings["UserName"].Value = txtUserName.Text.Trim();
                config.AppSettings.Settings["Password"].Value = dataAccess.Encrypt(txtPassword.Text.Trim());

                config.Save(ConfigurationSaveMode.Modified);
            }
            catch { }

        }
    }
}
