﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoViewer
{
    public partial class EnlargeImage : Form
    {
        public EnlargeImage()
        {
            InitializeComponent();
        }

        public void SetBitmap(Bitmap bitmap, string path)
        {
            this.Size = new Size(bitmap.Width + 5, bitmap.Height + 40);
            pictureBox1.Size =  bitmap.Size;
            pictureBox1.Image = bitmap;
            pictureBox1.Dock = DockStyle.Fill;
            bitmap.Save(path);
        }
    }
}
