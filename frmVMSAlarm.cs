﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoViewer
{   

    public partial class frmVMSAlarm : Form
    {
        
        Timer timer = new Timer();
        DAL dataAccess = new DAL();
        public frmVMSAlarm()
        {
            InitializeComponent();
        }

        private void frmAlarm_Load(object sender, EventArgs e)
        {
            dateEditFrom.DateTime = DateTime.Now;
            //ddlPaging.DataSource = dataAccess.GetVMSAlarmDate();
            //ddlPaging.ValueMember = "Date";
            //ddlPaging.DisplayMember = "Date";

            LoadGrid();
            //timer.Interval = 2000;
            //timer.Tick += Timer_Tick;
            //timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {            
            LoadGrid();
        }

        void LoadGrid()
        {
            try
            {
                DateTime date = dateEditFrom.DateTime;
                DataTable dtResult = dataAccess.GetMilestoneAlarms(date);

                gridControlAlarm.DataSource = dtResult;
            }
            catch 
            {
               
            }
        }

        private void ddlPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadGrid();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGrid();
        }
    }
}
