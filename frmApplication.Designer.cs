﻿namespace VideoViewer
{
    partial class frmApplication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmApplication));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.imageListIndications = new System.Windows.Forms.ImageList(this.components);
            this.topPanelCloseMenu = new System.Windows.Forms.Panel();
            this.btnCloseApplication = new System.Windows.Forms.Button();
            this.tableLayoutTopControlsMenu = new System.Windows.Forms.TableLayoutPanel();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.btnHelpTroubleshoot = new DevExpress.XtraEditors.SimpleButton();
            this.btnUserManagement = new DevExpress.XtraEditors.SimpleButton();
            this.btnConfiguration = new DevExpress.XtraEditors.SimpleButton();
            this.btnAlarm = new DevExpress.XtraEditors.SimpleButton();
            this.btnHMI = new DevExpress.XtraEditors.SimpleButton();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.panelTopStraignLine = new System.Windows.Forms.Panel();
            this.panelBottomStraignLine = new System.Windows.Forms.Panel();
            this.cisServiceController = new System.ServiceProcess.ServiceController();
            this.imageListButton = new System.Windows.Forms.ImageList(this.components);
            this.backgroundWorkerServiceStatus = new System.ComponentModel.BackgroundWorker();
            this.tableLayoutPanelParent = new System.Windows.Forms.TableLayoutPanel();
            this.panelControlLMenu = new DevExpress.XtraEditors.PanelControl();
            this.accordionControlMain = new DevExpress.XtraBars.Navigation.AccordionControl();
            this.accordionControlElement1 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElementHMI1 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement2 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlVisits = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlEevents = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlMilestoneAlarm = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement4 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElementSystemConfiguration = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElementVmsCamera = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElementSysSettings = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElementUM = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElementResetPassword = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.panelDisplay = new System.Windows.Forms.Panel();
            this.tableLayoutMain = new System.Windows.Forms.TableLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnBrowseFolder = new DevExpress.XtraEditors.SimpleButton();
            this.btnService = new DevExpress.XtraEditors.SimpleButton();
            this.lblOrion = new DevExpress.XtraEditors.LabelControl();
            this.show_recording = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.topPanelCloseMenu.SuspendLayout();
            this.tableLayoutTopControlsMenu.SuspendLayout();
            this.panelMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.tableLayoutPanelParent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlLMenu)).BeginInit();
            this.panelControlLMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.accordionControlMain)).BeginInit();
            this.tableLayoutMain.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.show_recording)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // imageListIndications
            // 
            this.imageListIndications.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIndications.ImageStream")));
            this.imageListIndications.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIndications.Images.SetKeyName(0, "circle_green.png");
            this.imageListIndications.Images.SetKeyName(1, "circle_grey.png");
            this.imageListIndications.Images.SetKeyName(2, "circle_red.png");
            // 
            // topPanelCloseMenu
            // 
            this.topPanelCloseMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.topPanelCloseMenu.Controls.Add(this.btnCloseApplication);
            this.topPanelCloseMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topPanelCloseMenu.Location = new System.Drawing.Point(2, 2);
            this.topPanelCloseMenu.Margin = new System.Windows.Forms.Padding(2);
            this.topPanelCloseMenu.Name = "topPanelCloseMenu";
            this.topPanelCloseMenu.Size = new System.Drawing.Size(1916, 28);
            this.topPanelCloseMenu.TabIndex = 3;
            // 
            // btnCloseApplication
            // 
            this.btnCloseApplication.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.btnCloseApplication.FlatAppearance.BorderSize = 0;
            this.btnCloseApplication.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseApplication.Image = ((System.Drawing.Image)(resources.GetObject("btnCloseApplication.Image")));
            this.btnCloseApplication.Location = new System.Drawing.Point(1887, 1);
            this.btnCloseApplication.Margin = new System.Windows.Forms.Padding(2);
            this.btnCloseApplication.Name = "btnCloseApplication";
            this.btnCloseApplication.Size = new System.Drawing.Size(24, 28);
            this.btnCloseApplication.TabIndex = 0;
            this.btnCloseApplication.UseVisualStyleBackColor = false;
            this.btnCloseApplication.Click += new System.EventHandler(this.btnCloseApplication_Click);
            // 
            // tableLayoutTopControlsMenu
            // 
            this.tableLayoutTopControlsMenu.ColumnCount = 2;
            this.tableLayoutTopControlsMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 244F));
            this.tableLayoutTopControlsMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutTopControlsMenu.Controls.Add(this.panelMenu, 1, 0);
            this.tableLayoutTopControlsMenu.Controls.Add(this.pictureBoxLogo, 0, 0);
            this.tableLayoutTopControlsMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutTopControlsMenu.Location = new System.Drawing.Point(2, 34);
            this.tableLayoutTopControlsMenu.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutTopControlsMenu.Name = "tableLayoutTopControlsMenu";
            this.tableLayoutTopControlsMenu.RowCount = 1;
            this.tableLayoutTopControlsMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutTopControlsMenu.Size = new System.Drawing.Size(1916, 94);
            this.tableLayoutTopControlsMenu.TabIndex = 4;
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panelMenu.Controls.Add(this.btnHelpTroubleshoot);
            this.panelMenu.Controls.Add(this.btnUserManagement);
            this.panelMenu.Controls.Add(this.btnConfiguration);
            this.panelMenu.Controls.Add(this.btnAlarm);
            this.panelMenu.Controls.Add(this.btnHMI);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMenu.Location = new System.Drawing.Point(246, 2);
            this.panelMenu.Margin = new System.Windows.Forms.Padding(2);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(1668, 90);
            this.panelMenu.TabIndex = 0;
            // 
            // btnHelpTroubleshoot
            // 
            this.btnHelpTroubleshoot.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.btnHelpTroubleshoot.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.btnHelpTroubleshoot.Appearance.Options.UseFont = true;
            this.btnHelpTroubleshoot.Appearance.Options.UseForeColor = true;
            this.btnHelpTroubleshoot.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnHelpTroubleshoot.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnHelpTroubleshoot.ImageOptions.Image")));
            this.btnHelpTroubleshoot.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btnHelpTroubleshoot.Location = new System.Drawing.Point(554, 2);
            this.btnHelpTroubleshoot.Margin = new System.Windows.Forms.Padding(2);
            this.btnHelpTroubleshoot.Name = "btnHelpTroubleshoot";
            this.btnHelpTroubleshoot.Size = new System.Drawing.Size(136, 81);
            this.btnHelpTroubleshoot.TabIndex = 4;
            this.btnHelpTroubleshoot.Text = "Logs";
            this.btnHelpTroubleshoot.Click += new System.EventHandler(this.btnHelpTroubleshoot_Click);
            this.btnHelpTroubleshoot.MouseEnter += new System.EventHandler(this.btnHelpTroubleshoot_MouseEnter);
            this.btnHelpTroubleshoot.MouseLeave += new System.EventHandler(this.btnHelpTroubleshoot_MouseLeave);
            // 
            // btnUserManagement
            // 
            this.btnUserManagement.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.btnUserManagement.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.btnUserManagement.Appearance.Options.UseFont = true;
            this.btnUserManagement.Appearance.Options.UseForeColor = true;
            this.btnUserManagement.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnUserManagement.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnUserManagement.ImageOptions.Image")));
            this.btnUserManagement.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btnUserManagement.Location = new System.Drawing.Point(416, 2);
            this.btnUserManagement.Margin = new System.Windows.Forms.Padding(2);
            this.btnUserManagement.Name = "btnUserManagement";
            this.btnUserManagement.Size = new System.Drawing.Size(136, 81);
            this.btnUserManagement.TabIndex = 4;
            this.btnUserManagement.Text = "User Management";
            this.btnUserManagement.Click += new System.EventHandler(this.btnUserManagement_Click);
            this.btnUserManagement.MouseEnter += new System.EventHandler(this.btnUserManagement_MouseEnter);
            this.btnUserManagement.MouseLeave += new System.EventHandler(this.btnUserManagement_MouseLeave);
            // 
            // btnConfiguration
            // 
            this.btnConfiguration.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.btnConfiguration.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.btnConfiguration.Appearance.Options.UseFont = true;
            this.btnConfiguration.Appearance.Options.UseForeColor = true;
            this.btnConfiguration.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnConfiguration.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfiguration.ImageOptions.Image")));
            this.btnConfiguration.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btnConfiguration.Location = new System.Drawing.Point(278, 2);
            this.btnConfiguration.Margin = new System.Windows.Forms.Padding(2);
            this.btnConfiguration.Name = "btnConfiguration";
            this.btnConfiguration.Size = new System.Drawing.Size(136, 81);
            this.btnConfiguration.TabIndex = 4;
            this.btnConfiguration.Text = "System Configuration";
            this.btnConfiguration.Click += new System.EventHandler(this.btnConfiguration_Click);
            this.btnConfiguration.MouseEnter += new System.EventHandler(this.btnConfiguration_MouseEnter);
            this.btnConfiguration.MouseLeave += new System.EventHandler(this.btnConfiguration_MouseLeave);
            // 
            // btnAlarm
            // 
            this.btnAlarm.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.btnAlarm.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.btnAlarm.Appearance.Options.UseFont = true;
            this.btnAlarm.Appearance.Options.UseForeColor = true;
            this.btnAlarm.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnAlarm.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAlarm.ImageOptions.Image")));
            this.btnAlarm.ImageOptions.ImageIndex = 0;
            this.btnAlarm.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btnAlarm.Location = new System.Drawing.Point(140, 2);
            this.btnAlarm.Margin = new System.Windows.Forms.Padding(2);
            this.btnAlarm.Name = "btnAlarm";
            this.btnAlarm.Size = new System.Drawing.Size(136, 81);
            this.btnAlarm.TabIndex = 4;
            this.btnAlarm.Text = "Visit Log";
            this.btnAlarm.Click += new System.EventHandler(this.btnAlarm_Click);
            this.btnAlarm.MouseEnter += new System.EventHandler(this.btnAlarm_MouseEnter);
            this.btnAlarm.MouseLeave += new System.EventHandler(this.btnAlarm_MouseLeave);
            // 
            // btnHMI
            // 
            this.btnHMI.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.btnHMI.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.btnHMI.Appearance.Options.UseFont = true;
            this.btnHMI.Appearance.Options.UseForeColor = true;
            this.btnHMI.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnHMI.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnHMI.ImageOptions.Image")));
            this.btnHMI.ImageOptions.ImageList = this.imageListIndications;
            this.btnHMI.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btnHMI.Location = new System.Drawing.Point(2, 2);
            this.btnHMI.Margin = new System.Windows.Forms.Padding(2);
            this.btnHMI.Name = "btnHMI";
            this.btnHMI.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.True;
            this.btnHMI.Size = new System.Drawing.Size(136, 81);
            this.btnHMI.TabIndex = 4;
            this.btnHMI.Text = "Main Layout";
            this.btnHMI.Click += new System.EventHandler(this.btnHMI_Click);
            this.btnHMI.MouseEnter += new System.EventHandler(this.btnHMI_MouseEnter);
            this.btnHMI.MouseLeave += new System.EventHandler(this.btnHMI_MouseLeave);
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLogo.Image")));
            this.pictureBoxLogo.Location = new System.Drawing.Point(4, 4);
            this.pictureBoxLogo.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(236, 84);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxLogo.TabIndex = 8;
            this.pictureBoxLogo.TabStop = false;
            // 
            // panelTopStraignLine
            // 
            this.panelTopStraignLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panelTopStraignLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTopStraignLine.Location = new System.Drawing.Point(2, 132);
            this.panelTopStraignLine.Margin = new System.Windows.Forms.Padding(2);
            this.panelTopStraignLine.MinimumSize = new System.Drawing.Size(75, 2);
            this.panelTopStraignLine.Name = "panelTopStraignLine";
            this.panelTopStraignLine.Size = new System.Drawing.Size(1916, 2);
            this.panelTopStraignLine.TabIndex = 5;
            // 
            // panelBottomStraignLine
            // 
            this.panelBottomStraignLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panelBottomStraignLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBottomStraignLine.Location = new System.Drawing.Point(2, 1037);
            this.panelBottomStraignLine.Margin = new System.Windows.Forms.Padding(2);
            this.panelBottomStraignLine.Name = "panelBottomStraignLine";
            this.panelBottomStraignLine.Size = new System.Drawing.Size(1916, 1);
            this.panelBottomStraignLine.TabIndex = 6;
            // 
            // cisServiceController
            // 
            this.cisServiceController.ServiceName = "Orion.Server";
            // 
            // imageListButton
            // 
            this.imageListButton.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListButton.ImageStream")));
            this.imageListButton.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListButton.Images.SetKeyName(0, "alarmEvents.png");
            this.imageListButton.Images.SetKeyName(1, "alarmEventsHover.png");
            this.imageListButton.Images.SetKeyName(2, "hmiLayout.png");
            this.imageListButton.Images.SetKeyName(3, "hmiLayoutHover.png");
            this.imageListButton.Images.SetKeyName(4, "logs.png");
            this.imageListButton.Images.SetKeyName(5, "logsHover.png");
            this.imageListButton.Images.SetKeyName(6, "systemConfiguration.png");
            this.imageListButton.Images.SetKeyName(7, "systemConfiguratoinHover.png");
            this.imageListButton.Images.SetKeyName(8, "userManagement.png");
            this.imageListButton.Images.SetKeyName(9, "userManagementHover.png");
            // 
            // tableLayoutPanelParent
            // 
            this.tableLayoutPanelParent.ColumnCount = 2;
            this.tableLayoutPanelParent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 244F));
            this.tableLayoutPanelParent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelParent.Controls.Add(this.panelControlLMenu, 0, 0);
            this.tableLayoutPanelParent.Controls.Add(this.panelDisplay, 1, 0);
            this.tableLayoutPanelParent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelParent.Location = new System.Drawing.Point(2, 136);
            this.tableLayoutPanelParent.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelParent.Name = "tableLayoutPanelParent";
            this.tableLayoutPanelParent.RowCount = 1;
            this.tableLayoutPanelParent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelParent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 897F));
            this.tableLayoutPanelParent.Size = new System.Drawing.Size(1916, 897);
            this.tableLayoutPanelParent.TabIndex = 2;
            // 
            // panelControlLMenu
            // 
            this.panelControlLMenu.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panelControlLMenu.Appearance.Options.UseBackColor = true;
            this.panelControlLMenu.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControlLMenu.ContentImage = ((System.Drawing.Image)(resources.GetObject("panelControlLMenu.ContentImage")));
            this.panelControlLMenu.ContentImageAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.panelControlLMenu.Controls.Add(this.accordionControlMain);
            this.panelControlLMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlLMenu.Location = new System.Drawing.Point(2, 2);
            this.panelControlLMenu.Margin = new System.Windows.Forms.Padding(2);
            this.panelControlLMenu.Name = "panelControlLMenu";
            this.panelControlLMenu.Size = new System.Drawing.Size(240, 893);
            this.panelControlLMenu.TabIndex = 9;
            // 
            // accordionControlMain
            // 
            this.accordionControlMain.AnimationType = DevExpress.XtraBars.Navigation.AnimationType.Spline;
            this.accordionControlMain.Appearance.AccordionControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.accordionControlMain.Appearance.AccordionControl.Font = new System.Drawing.Font("Calibri", 14F);
            this.accordionControlMain.Appearance.AccordionControl.Options.UseBackColor = true;
            this.accordionControlMain.Appearance.AccordionControl.Options.UseBorderColor = true;
            this.accordionControlMain.Appearance.AccordionControl.Options.UseFont = true;
            this.accordionControlMain.Appearance.Group.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(80)))), ((int)(((byte)(145)))));
            this.accordionControlMain.Appearance.Group.Hovered.BorderColor = System.Drawing.Color.White;
            this.accordionControlMain.Appearance.Group.Hovered.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Underline);
            this.accordionControlMain.Appearance.Group.Hovered.ForeColor = System.Drawing.Color.White;
            this.accordionControlMain.Appearance.Group.Hovered.Options.UseBackColor = true;
            this.accordionControlMain.Appearance.Group.Hovered.Options.UseBorderColor = true;
            this.accordionControlMain.Appearance.Group.Hovered.Options.UseFont = true;
            this.accordionControlMain.Appearance.Group.Hovered.Options.UseForeColor = true;
            this.accordionControlMain.Appearance.Group.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(80)))), ((int)(((byte)(145)))));
            this.accordionControlMain.Appearance.Group.Normal.BorderColor = System.Drawing.Color.White;
            this.accordionControlMain.Appearance.Group.Normal.Font = new System.Drawing.Font("Calibri", 14F);
            this.accordionControlMain.Appearance.Group.Normal.ForeColor = System.Drawing.Color.White;
            this.accordionControlMain.Appearance.Group.Normal.Options.UseBackColor = true;
            this.accordionControlMain.Appearance.Group.Normal.Options.UseBorderColor = true;
            this.accordionControlMain.Appearance.Group.Normal.Options.UseFont = true;
            this.accordionControlMain.Appearance.Group.Normal.Options.UseForeColor = true;
            this.accordionControlMain.Appearance.Group.Pressed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(80)))), ((int)(((byte)(145)))));
            this.accordionControlMain.Appearance.Group.Pressed.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(80)))), ((int)(((byte)(145)))));
            this.accordionControlMain.Appearance.Group.Pressed.Font = new System.Drawing.Font("Calibri", 14F);
            this.accordionControlMain.Appearance.Group.Pressed.Options.UseBackColor = true;
            this.accordionControlMain.Appearance.Group.Pressed.Options.UseBorderColor = true;
            this.accordionControlMain.Appearance.Group.Pressed.Options.UseFont = true;
            this.accordionControlMain.Appearance.Item.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.accordionControlMain.Appearance.Item.Hovered.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Underline);
            this.accordionControlMain.Appearance.Item.Hovered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(209)))), ((int)(((byte)(249)))));
            this.accordionControlMain.Appearance.Item.Hovered.Options.UseBackColor = true;
            this.accordionControlMain.Appearance.Item.Hovered.Options.UseFont = true;
            this.accordionControlMain.Appearance.Item.Hovered.Options.UseForeColor = true;
            this.accordionControlMain.Appearance.Item.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.accordionControlMain.Appearance.Item.Normal.Font = new System.Drawing.Font("Calibri", 12F);
            this.accordionControlMain.Appearance.Item.Normal.ForeColor = System.Drawing.Color.White;
            this.accordionControlMain.Appearance.Item.Normal.Options.UseBackColor = true;
            this.accordionControlMain.Appearance.Item.Normal.Options.UseFont = true;
            this.accordionControlMain.Appearance.Item.Normal.Options.UseForeColor = true;
            this.accordionControlMain.Appearance.Item.Pressed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.accordionControlMain.Appearance.Item.Pressed.BorderColor = System.Drawing.Color.White;
            this.accordionControlMain.Appearance.Item.Pressed.Font = new System.Drawing.Font("Calibri", 12F);
            this.accordionControlMain.Appearance.Item.Pressed.ForeColor = System.Drawing.Color.White;
            this.accordionControlMain.Appearance.Item.Pressed.Options.UseBackColor = true;
            this.accordionControlMain.Appearance.Item.Pressed.Options.UseBorderColor = true;
            this.accordionControlMain.Appearance.Item.Pressed.Options.UseFont = true;
            this.accordionControlMain.Appearance.Item.Pressed.Options.UseForeColor = true;
            this.accordionControlMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.accordionControlMain.DistanceBetweenRootGroups = 6;
            this.accordionControlMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.accordionControlMain.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement1,
            this.accordionControlElement2,
            this.accordionControlElement4});
            this.accordionControlMain.ExpandElementMode = DevExpress.XtraBars.Navigation.ExpandElementMode.Single;
            this.accordionControlMain.Location = new System.Drawing.Point(0, 0);
            this.accordionControlMain.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.accordionControlMain.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.accordionControlMain.Margin = new System.Windows.Forms.Padding(2);
            this.accordionControlMain.Name = "accordionControlMain";
            this.accordionControlMain.ScaleImages = DevExpress.Utils.DefaultBoolean.False;
            this.accordionControlMain.ScrollBarMode = DevExpress.XtraBars.Navigation.ScrollBarMode.Hidden;
            this.accordionControlMain.Size = new System.Drawing.Size(240, 371);
            this.accordionControlMain.TabIndex = 11;
            this.accordionControlMain.Text = "accordionControlMain";
            // 
            // accordionControlElement1
            // 
            this.accordionControlElement1.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElementHMI1});
            this.accordionControlElement1.Expanded = true;
            this.accordionControlElement1.Name = "accordionControlElement1";
            this.accordionControlElement1.Tag = "";
            this.accordionControlElement1.Text = "Home";
            // 
            // accordionControlElementHMI1
            // 
            this.accordionControlElementHMI1.HeaderTemplate.AddRange(new DevExpress.XtraBars.Navigation.HeaderElementInfo[] {
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.Text),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.Image),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.HeaderControl),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.ContextButtons)});
            this.accordionControlElementHMI1.Name = "accordionControlElementHMI1";
            this.accordionControlElementHMI1.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElementHMI1.Tag = "HMIView";
            this.accordionControlElementHMI1.Text = "    Main Layout";
            this.accordionControlElementHMI1.Click += new System.EventHandler(this.accordionControlElementHMI1_Click);
            // 
            // accordionControlElement2
            // 
            this.accordionControlElement2.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlVisits,
            this.accordionControlEevents,
            this.accordionControlMilestoneAlarm});
            this.accordionControlElement2.Expanded = true;
            this.accordionControlElement2.Name = "accordionControlElement2";
            this.accordionControlElement2.Text = "Listing and Logs";
            // 
            // accordionControlVisits
            // 
            this.accordionControlVisits.HeaderTemplate.AddRange(new DevExpress.XtraBars.Navigation.HeaderElementInfo[] {
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.Text),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.Image),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.HeaderControl),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.ContextButtons)});
            this.accordionControlVisits.Name = "accordionControlVisits";
            this.accordionControlVisits.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlVisits.Tag = "AlarmView";
            this.accordionControlVisits.Text = "    Vehicles Listing";
            this.accordionControlVisits.Click += new System.EventHandler(this.accordionControlVisits_Click);
            // 
            // accordionControlEevents
            // 
            this.accordionControlEevents.Name = "accordionControlEevents";
            this.accordionControlEevents.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlEevents.Tag = "AlarmView";
            this.accordionControlEevents.Text = "    Visit Logs";
            this.accordionControlEevents.Click += new System.EventHandler(this.accordionControlEevents_Click);
            // 
            // accordionControlMilestoneAlarm
            // 
            this.accordionControlMilestoneAlarm.Name = "accordionControlMilestoneAlarm";
            this.accordionControlMilestoneAlarm.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlMilestoneAlarm.Tag = "AlarmView";
            this.accordionControlMilestoneAlarm.Text = "    VMS Events";
            this.accordionControlMilestoneAlarm.Click += new System.EventHandler(this.accordionControlMilestoneAlarm_Click);
            // 
            // accordionControlElement4
            // 
            this.accordionControlElement4.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElementSystemConfiguration,
            this.accordionControlElementUM,
            this.accordionControlElementResetPassword});
            this.accordionControlElement4.Expanded = true;
            this.accordionControlElement4.HeaderTemplate.AddRange(new DevExpress.XtraBars.Navigation.HeaderElementInfo[] {
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.Text),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.Image),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.HeaderControl),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.ContextButtons)});
            this.accordionControlElement4.Name = "accordionControlElement4";
            this.accordionControlElement4.Text = "Settings";
            // 
            // accordionControlElementSystemConfiguration
            // 
            this.accordionControlElementSystemConfiguration.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElementVmsCamera,
            this.accordionControlElementSysSettings});
            this.accordionControlElementSystemConfiguration.Name = "accordionControlElementSystemConfiguration";
            this.accordionControlElementSystemConfiguration.Text = "System Configuration";
            // 
            // accordionControlElementVmsCamera
            // 
            this.accordionControlElementVmsCamera.Name = "accordionControlElementVmsCamera";
            this.accordionControlElementVmsCamera.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElementVmsCamera.Tag = "SystemConfiguration";
            this.accordionControlElementVmsCamera.Text = "    VMS Settings";
            this.accordionControlElementVmsCamera.Click += new System.EventHandler(this.accordionControlElementVmsCamera_Click);
            // 
            // accordionControlElementSysSettings
            // 
            this.accordionControlElementSysSettings.Name = "accordionControlElementSysSettings";
            this.accordionControlElementSysSettings.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElementSysSettings.Tag = "SystemConfiguration";
            this.accordionControlElementSysSettings.Text = "    Connection Settings";
            this.accordionControlElementSysSettings.Click += new System.EventHandler(this.accordionControlElementSysSettings_Click);
            // 
            // accordionControlElementUM
            // 
            this.accordionControlElementUM.Name = "accordionControlElementUM";
            this.accordionControlElementUM.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElementUM.Tag = "UserManagement";
            this.accordionControlElementUM.Text = "    User Management";
            this.accordionControlElementUM.Click += new System.EventHandler(this.accordionControlElementUM_Click);
            // 
            // accordionControlElementResetPassword
            // 
            this.accordionControlElementResetPassword.Name = "accordionControlElementResetPassword";
            this.accordionControlElementResetPassword.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElementResetPassword.Tag = "ResetPassword";
            this.accordionControlElementResetPassword.Text = "    Reset Password";
            this.accordionControlElementResetPassword.Click += new System.EventHandler(this.accordionControlElementResetPassword_Click);
            // 
            // panelDisplay
            // 
            this.panelDisplay.AutoSize = true;
            this.panelDisplay.BackColor = System.Drawing.Color.Transparent;
            this.panelDisplay.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelDisplay.BackgroundImage")));
            this.panelDisplay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDisplay.Location = new System.Drawing.Point(246, 2);
            this.panelDisplay.Margin = new System.Windows.Forms.Padding(2);
            this.panelDisplay.MinimumSize = new System.Drawing.Size(600, 488);
            this.panelDisplay.Name = "panelDisplay";
            this.panelDisplay.Padding = new System.Windows.Forms.Padding(0, 0, 38, 0);
            this.panelDisplay.Size = new System.Drawing.Size(1668, 893);
            this.panelDisplay.TabIndex = 7;
            // 
            // tableLayoutMain
            // 
            this.tableLayoutMain.BackColor = System.Drawing.Color.White;
            this.tableLayoutMain.ColumnCount = 1;
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutMain.Controls.Add(this.tableLayoutPanelParent, 0, 3);
            this.tableLayoutMain.Controls.Add(this.panel4, 0, 5);
            this.tableLayoutMain.Controls.Add(this.topPanelCloseMenu, 0, 0);
            this.tableLayoutMain.Controls.Add(this.tableLayoutTopControlsMenu, 0, 1);
            this.tableLayoutMain.Controls.Add(this.panelTopStraignLine, 0, 2);
            this.tableLayoutMain.Controls.Add(this.panelBottomStraignLine, 0, 4);
            this.tableLayoutMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutMain.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutMain.Name = "tableLayoutMain";
            this.tableLayoutMain.RowCount = 6;
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 98F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 4F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 4F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutMain.Size = new System.Drawing.Size(1920, 1080);
            this.tableLayoutMain.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.btnBrowseFolder);
            this.panel4.Controls.Add(this.btnService);
            this.panel4.Controls.Add(this.lblOrion);
            this.panel4.Location = new System.Drawing.Point(2, 1041);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1916, 36);
            this.panel4.TabIndex = 1;
            // 
            // btnBrowseFolder
            // 
            this.btnBrowseFolder.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnBrowseFolder.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnBrowseFolder.Appearance.Options.UseBackColor = true;
            this.btnBrowseFolder.Appearance.Options.UseForeColor = true;
            this.btnBrowseFolder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnBrowseFolder.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnBrowseFolder.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBrowseFolder.ImageOptions.Image")));
            this.btnBrowseFolder.Location = new System.Drawing.Point(10, 4);
            this.btnBrowseFolder.Margin = new System.Windows.Forms.Padding(2);
            this.btnBrowseFolder.Name = "btnBrowseFolder";
            this.btnBrowseFolder.Size = new System.Drawing.Size(123, 27);
            this.btnBrowseFolder.TabIndex = 233;
            this.btnBrowseFolder.Text = "Browse Folder";
            this.btnBrowseFolder.Click += new System.EventHandler(this.btnBrowseFolder_Click);
            // 
            // btnService
            // 
            this.btnService.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnService.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnService.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.btnService.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnService.Appearance.Options.UseBackColor = true;
            this.btnService.Appearance.Options.UseFont = true;
            this.btnService.Appearance.Options.UseForeColor = true;
            this.btnService.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.btnService.ImageOptions.ImageList = this.imageListIndications;
            this.btnService.Location = new System.Drawing.Point(1787, 4);
            this.btnService.Margin = new System.Windows.Forms.Padding(2);
            this.btnService.Name = "btnService";
            this.btnService.Size = new System.Drawing.Size(118, 28);
            this.btnService.TabIndex = 8;
            this.btnService.Text = "Orion Server";
            // 
            // lblOrion
            // 
            this.lblOrion.Location = new System.Drawing.Point(12, 11);
            this.lblOrion.Margin = new System.Windows.Forms.Padding(2);
            this.lblOrion.Name = "lblOrion";
            this.lblOrion.Size = new System.Drawing.Size(0, 13);
            this.lblOrion.TabIndex = 0;
            // 
            // show_recording
            // 
            this.show_recording.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            this.show_recording.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.show_recording.Name = "show_recording";
            this.show_recording.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // frmApplication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.Controls.Add(this.tableLayoutMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmApplication";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmApplication";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmApplication_Load);
            this.topPanelCloseMenu.ResumeLayout(false);
            this.tableLayoutTopControlsMenu.ResumeLayout(false);
            this.panelMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.tableLayoutPanelParent.ResumeLayout(false);
            this.tableLayoutPanelParent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlLMenu)).EndInit();
            this.panelControlLMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.accordionControlMain)).EndInit();
            this.tableLayoutMain.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.show_recording)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.ImageList imageListIndications;
        private System.Windows.Forms.Panel topPanelCloseMenu;
        private System.Windows.Forms.Button btnCloseApplication;
        private System.Windows.Forms.TableLayoutPanel tableLayoutTopControlsMenu;
        private System.Windows.Forms.Panel panelMenu;
        private DevExpress.XtraEditors.SimpleButton btnHelpTroubleshoot;
        private DevExpress.XtraEditors.SimpleButton btnUserManagement;
        private DevExpress.XtraEditors.SimpleButton btnConfiguration;
        private DevExpress.XtraEditors.SimpleButton btnAlarm;
        private DevExpress.XtraEditors.SimpleButton btnHMI;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.Panel panelTopStraignLine;
        private System.Windows.Forms.Panel panelBottomStraignLine;
        private System.ServiceProcess.ServiceController cisServiceController;
        private System.Windows.Forms.ImageList imageListButton;
        private System.ComponentModel.BackgroundWorker backgroundWorkerServiceStatus;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelParent;
        private System.Windows.Forms.Panel panelDisplay;
        private System.Windows.Forms.TableLayoutPanel tableLayoutMain;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.SimpleButton btnService;
        private DevExpress.XtraEditors.LabelControl lblOrion;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit show_recording;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.PanelControl panelControlLMenu;
        private DevExpress.XtraBars.Navigation.AccordionControl accordionControlMain;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElementHMI1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement2;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlVisits;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlMilestoneAlarm;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement4;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElementSystemConfiguration;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElementVmsCamera;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElementSysSettings;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElementUM;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElementResetPassword;
        private DevExpress.XtraEditors.SimpleButton btnBrowseFolder;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlEevents;
    }
}