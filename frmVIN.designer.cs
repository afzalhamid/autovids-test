﻿namespace VideoViewer
{
    partial class frmVIN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVIN));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridControlVIN = new DevExpress.XtraGrid.GridControl();
            this.gridViewVIN = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRowNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVIN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLastVisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBlackList = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnEdit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.update_info = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colHistory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.show_history = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.panelMain = new System.Windows.Forms.Panel();
            this.tableLayoutPanelDopdownMenu = new System.Windows.Forms.TableLayoutPanel();
            this.panelDropdown = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.btnExportExcel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.update_info)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.show_history)).BeginInit();
            this.tableLayoutPanelMain.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.tableLayoutPanelDopdownMenu.SuspendLayout();
            this.panelDropdown.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControlVIN
            // 
            this.gridControlVIN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlVIN.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            gridLevelNode2.RelationName = "Level1";
            this.gridControlVIN.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gridControlVIN.Location = new System.Drawing.Point(2, 71);
            this.gridControlVIN.MainView = this.gridViewVIN;
            this.gridControlVIN.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlVIN.Name = "gridControlVIN";
            this.gridControlVIN.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.show_history,
            this.update_info});
            this.gridControlVIN.Size = new System.Drawing.Size(1216, 539);
            this.gridControlVIN.TabIndex = 3;
            this.gridControlVIN.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewVIN});
            // 
            // gridViewVIN
            // 
            this.gridViewVIN.Appearance.ViewCaption.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewVIN.Appearance.ViewCaption.Options.UseFont = true;
            this.gridViewVIN.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRowNumber,
            this.colVIN,
            this.gridColLastVisitDate,
            this.colBlackList,
            this.colComment,
            this.gridColumnEdit,
            this.colHistory,
            this.gridColActive,
            this.colVehicleID});
            this.gridViewVIN.GridControl = this.gridControlVIN;
            this.gridViewVIN.Name = "gridViewVIN";
            this.gridViewVIN.OptionsView.ShowGroupPanel = false;
            // 
            // colRowNumber
            // 
            this.colRowNumber.Caption = "Sr#";
            this.colRowNumber.FieldName = "SN";
            this.colRowNumber.MaxWidth = 35;
            this.colRowNumber.MinWidth = 35;
            this.colRowNumber.Name = "colRowNumber";
            this.colRowNumber.OptionsColumn.AllowEdit = false;
            this.colRowNumber.Visible = true;
            this.colRowNumber.VisibleIndex = 0;
            this.colRowNumber.Width = 35;
            // 
            // colVIN
            // 
            this.colVIN.Caption = "VIN Number";
            this.colVIN.FieldName = "VIN";
            this.colVIN.Name = "colVIN";
            this.colVIN.OptionsColumn.AllowEdit = false;
            this.colVIN.Visible = true;
            this.colVIN.VisibleIndex = 1;
            this.colVIN.Width = 200;
            // 
            // gridColLastVisitDate
            // 
            this.gridColLastVisitDate.Caption = "Last Visit Date";
            this.gridColLastVisitDate.DisplayFormat.FormatString = "MM/dd/yyyy";
            this.gridColLastVisitDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColLastVisitDate.FieldName = "LastVisitDate";
            this.gridColLastVisitDate.MinWidth = 200;
            this.gridColLastVisitDate.Name = "gridColLastVisitDate";
            this.gridColLastVisitDate.OptionsColumn.AllowEdit = false;
            this.gridColLastVisitDate.Visible = true;
            this.gridColLastVisitDate.VisibleIndex = 2;
            this.gridColLastVisitDate.Width = 200;
            // 
            // colBlackList
            // 
            this.colBlackList.Caption = "Status";
            this.colBlackList.FieldName = "Status";
            this.colBlackList.MaxWidth = 100;
            this.colBlackList.MinWidth = 100;
            this.colBlackList.Name = "colBlackList";
            this.colBlackList.OptionsColumn.AllowEdit = false;
            this.colBlackList.Visible = true;
            this.colBlackList.VisibleIndex = 3;
            this.colBlackList.Width = 100;
            // 
            // colComment
            // 
            this.colComment.Caption = "Comments";
            this.colComment.DisplayFormat.FormatString = "HH:mm:ss dd/MM/yyyy";
            this.colComment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colComment.FieldName = "Comments";
            this.colComment.Name = "colComment";
            this.colComment.OptionsColumn.AllowEdit = false;
            this.colComment.Visible = true;
            this.colComment.VisibleIndex = 4;
            this.colComment.Width = 611;
            // 
            // gridColumnEdit
            // 
            this.gridColumnEdit.Caption = " ";
            this.gridColumnEdit.ColumnEdit = this.update_info;
            this.gridColumnEdit.FieldName = "ID";
            this.gridColumnEdit.MaxWidth = 50;
            this.gridColumnEdit.MinWidth = 50;
            this.gridColumnEdit.Name = "gridColumnEdit";
            this.gridColumnEdit.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnEdit.Visible = true;
            this.gridColumnEdit.VisibleIndex = 5;
            this.gridColumnEdit.Width = 50;
            // 
            // update_info
            // 
            this.update_info.AutoHeight = false;
            editorButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions7.Image")));
            serializableAppearanceObject25.Image = ((System.Drawing.Image)(resources.GetObject("serializableAppearanceObject25.Image")));
            serializableAppearanceObject25.Options.UseImage = true;
            serializableAppearanceObject26.Image = ((System.Drawing.Image)(resources.GetObject("serializableAppearanceObject26.Image")));
            serializableAppearanceObject26.Options.UseImage = true;
            serializableAppearanceObject27.Image = ((System.Drawing.Image)(resources.GetObject("serializableAppearanceObject27.Image")));
            serializableAppearanceObject27.Options.UseImage = true;
            serializableAppearanceObject28.Image = ((System.Drawing.Image)(resources.GetObject("serializableAppearanceObject28.Image")));
            serializableAppearanceObject28.Options.UseImage = true;
            this.update_info.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.update_info.Name = "update_info";
            this.update_info.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.update_info.Click += new System.EventHandler(this.show_Description_Click);
            // 
            // colHistory
            // 
            this.colHistory.Caption = " ";
            this.colHistory.ColumnEdit = this.show_history;
            this.colHistory.FieldName = "ID";
            this.colHistory.MaxWidth = 50;
            this.colHistory.MinWidth = 50;
            this.colHistory.Name = "colHistory";
            this.colHistory.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.colHistory.Visible = true;
            this.colHistory.VisibleIndex = 6;
            this.colHistory.Width = 50;
            // 
            // show_history
            // 
            this.show_history.AutoHeight = false;
            editorButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions8.Image")));
            this.show_history.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Show Visit History", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.show_history.Name = "show_history";
            this.show_history.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.show_history.Click += new System.EventHandler(this.show_history_Click);
            // 
            // gridColActive
            // 
            this.gridColActive.Caption = "Active";
            this.gridColActive.FieldName = "Active";
            this.gridColActive.Name = "gridColActive";
            // 
            // colVehicleID
            // 
            this.colVehicleID.Caption = "VehicleID";
            this.colVehicleID.FieldName = "ID";
            this.colVehicleID.Name = "colVehicleID";
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelMain.ColumnCount = 1;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.gridControlVIN, 0, 3);
            this.tableLayoutPanelMain.Controls.Add(this.panelMain, 0, 1);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 4;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1220, 612);
            this.tableLayoutPanelMain.TabIndex = 4;
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.Transparent;
            this.panelMain.Controls.Add(this.tableLayoutPanelDopdownMenu);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(2, 14);
            this.panelMain.Margin = new System.Windows.Forms.Padding(2);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1216, 41);
            this.panelMain.TabIndex = 4;
            // 
            // tableLayoutPanelDopdownMenu
            // 
            this.tableLayoutPanelDopdownMenu.ColumnCount = 3;
            this.tableLayoutPanelDopdownMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 338F));
            this.tableLayoutPanelDopdownMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelDopdownMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelDopdownMenu.Controls.Add(this.panelDropdown, 0, 0);
            this.tableLayoutPanelDopdownMenu.Controls.Add(this.btnExportExcel, 2, 0);
            this.tableLayoutPanelDopdownMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelDopdownMenu.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelDopdownMenu.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelDopdownMenu.Name = "tableLayoutPanelDopdownMenu";
            this.tableLayoutPanelDopdownMenu.RowCount = 1;
            this.tableLayoutPanelDopdownMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelDopdownMenu.Size = new System.Drawing.Size(1216, 41);
            this.tableLayoutPanelDopdownMenu.TabIndex = 0;
            // 
            // panelDropdown
            // 
            this.panelDropdown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panelDropdown.Controls.Add(this.label16);
            this.panelDropdown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDropdown.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.panelDropdown.ForeColor = System.Drawing.Color.White;
            this.panelDropdown.Location = new System.Drawing.Point(0, 0);
            this.panelDropdown.Margin = new System.Windows.Forms.Padding(0);
            this.panelDropdown.Name = "panelDropdown";
            this.panelDropdown.Size = new System.Drawing.Size(338, 41);
            this.panelDropdown.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(4, 12);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 17);
            this.label16.TabIndex = 502;
            this.label16.Text = "Vehicle Log";
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Appearance.Image")));
            this.btnExportExcel.Appearance.Options.UseImage = true;
            this.btnExportExcel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExportExcel.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnExportExcel.Location = new System.Drawing.Point(1169, 3);
            this.btnExportExcel.MaximumSize = new System.Drawing.Size(32, 32);
            this.btnExportExcel.MinimumSize = new System.Drawing.Size(32, 32);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(32, 32);
            this.btnExportExcel.TabIndex = 1;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // frmVIN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1220, 612);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmVIN";
            this.Text = "Events";
            this.Load += new System.EventHandler(this.frmVIN_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.update_info)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.show_history)).EndInit();
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.panelMain.ResumeLayout(false);
            this.tableLayoutPanelDopdownMenu.ResumeLayout(false);
            this.panelDropdown.ResumeLayout(false);
            this.panelDropdown.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlVIN;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit show_history;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelDopdownMenu;
        private System.Windows.Forms.Panel panelDropdown;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit update_info;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewVIN;
        private DevExpress.XtraGrid.Columns.GridColumn colRowNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colVIN;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLastVisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colBlackList;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colHistory;
        private DevExpress.XtraGrid.Columns.GridColumn gridColActive;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleID;
        private DevExpress.XtraEditors.SimpleButton btnExportExcel;
    }
}