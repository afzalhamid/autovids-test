﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoViewer
{
    public partial class frmUserManagement : Form
    {
        DAL dataAccess = new DAL();
        public frmUserManagement()
        {
            InitializeComponent();
        }
        private void frmUserManagement_Load(object sender, EventArgs e)
        {
            try
            {
                clbPermission.DataSource = dataAccess.GetPermissions();
                clbPermission.DisplayMember = "Permission";
                clbPermission.ValueMember = "ID";

                ddlRole.DataSource = dataAccess.GetRoles();
                ddlRole.DisplayMember = "Role";
                ddlRole.ValueMember = "ID";

                gridControlUser.DataSource = dataAccess.GetUsers();
                gridControlRole.DataSource = dataAccess.GetRoleAndPermission();

                txtRoleID.Visible = false;
                txtUserID.Visible = false;
            }
            catch (Exception ex)
            {
                dataAccess.Logging(Module.Application,LogClass.Error, ex.ToString());
            }
        }
        private void gridViewUser_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            ClearUserFields();

            string firstName = Convert.ToString(gridViewUser.GetRowCellValue(e.RowHandle, "FirstName"));
            string lastName = Convert.ToString(gridViewUser.GetRowCellValue(e.RowHandle, "LastName"));
            string email = Convert.ToString(gridViewUser.GetRowCellValue(e.RowHandle, "Email"));
            string username = Convert.ToString(gridViewUser.GetRowCellValue(e.RowHandle, "UserID"));
            string password = Convert.ToString(gridViewUser.GetRowCellValue(e.RowHandle, "Password"));
            string roleid = Convert.ToString(gridViewUser.GetRowCellValue(e.RowHandle, "RID"));
            string userid = Convert.ToString(gridViewUser.GetRowCellValue(e.RowHandle, "UID"));

            txtFirstName.Text = firstName;
            txtLastName.Text = lastName;
            txtUserName.Text = username;
            txtEmail.Text = email;
            txtUserID.Text = userid;

            for (int i = 0; i < ddlRole.Items.Count; i++)
            {
                DataRowView item = ddlRole.Items[i] as DataRowView;
                if(item.Row["ID"].ToString() == roleid)
                    ddlRole.SelectedValue = roleid;
            }

            if (userid == "1" || username == "administrator")
            {
                SetUsersButtonAppearance(false);
            }
            else
            {
                SetUsersButtonAppearance(true);
            }

        }

        private void gridViewRole_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            ClearRoleFields();

            string role = Convert.ToString(gridViewRole.GetRowCellValue(e.RowHandle, "Role"));
            string roleid = Convert.ToString(gridViewRole.GetRowCellValue(e.RowHandle, "ID"));
            string pids = Convert.ToString(gridViewRole.GetRowCellValue(e.RowHandle, "PermissionID"));

            if(role == "Administrator")
            {
                SetRolesButtonAppearance(false);
            }
            else
            {
                SetRolesButtonAppearance(true);
            }

            txtRoleName.Text = role;
            txtRoleID.Text = roleid;
            string[] roles = pids.Split(',');
                      

            for (int i = 0; i < clbPermission.ItemCount; i++)
            {
                DataRowView item = clbPermission.GetItem(i) as DataRowView;
                if (roles.Contains(item.Row["ID"].ToString()))
                    clbPermission.SetItemChecked(i, true);
            }

        }
        private void btnAddUpdateUser_Click(object sender, EventArgs e)
        {
            try
            {
                string password = txtPassword.Text.Trim();
                string confirmPassword = txtConfirmPassword.Text.Trim();
                if (!string.Equals(password, confirmPassword))
                    password = string.Empty;
                bool result = dataAccess.AddUpdateUser(string.IsNullOrEmpty(txtUserID.Text) ? 0 : Convert.ToInt32(txtUserID.Text), Convert.ToInt32(ddlRole.SelectedValue), txtFirstName.Text, txtLastName.Text, txtUserName.Text, txtEmail.Text, password);
                if (result)
                {
                    gridControlUser.DataSource = dataAccess.GetUsers();
                    ClearUserFields();
                }
                else
                    MessageBox.Show("Failed to add/update user.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            catch (Exception ex)
            {
                dataAccess.Logging(Module.Application, LogClass.Error, ex.ToString());
            }
            finally { }
        }
        private void btnResetUser_Click(object sender, EventArgs e)
        {
            ClearUserFields();
        }
        private void btnDeleteUser_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Are you sure to continue with delete process ?", "Delete Sequence", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            int userID = string.IsNullOrEmpty(txtUserID.Text) ? 0 : Convert.ToInt32(txtUserID.Text);

            if (userID > 0 && result == DialogResult.Yes)
                if (dataAccess.DeleteUser(userID))
                {
                    gridControlUser.DataSource = dataAccess.GetUsers();
                    ClearUserFields();
                }
        }
        private void btnAddUpdateRole_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtRoleName.Text))
            {
                string selectedPermissions = string.Empty;

                for (int i = 0; i < clbPermission.CheckedItems.Count; i++)
                {
                    selectedPermissions = string.IsNullOrEmpty(selectedPermissions) ? clbPermission.CheckedItems[i].ToString() : String.Format("{0},{1}", selectedPermissions, clbPermission.CheckedItems[i]);
                }                

                bool result = dataAccess.AddUpdateRole(string.IsNullOrEmpty(txtRoleID.Text) ? 0 : Convert.ToInt32(txtRoleID.Text), txtRoleName.Text, selectedPermissions);
                if (result)
                {
                    gridControlRole.DataSource = dataAccess.GetRoleAndPermission();
                    ClearRoleFields();

                    ddlRole.DataSource = dataAccess.GetRoles();
                    ddlRole.DisplayMember = "Role";
                    ddlRole.ValueMember = "ID";
                }
                else
                    MessageBox.Show("Failed to add/update the role.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                MessageBox.Show("Enter role name.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
        private void btnResetRole_Click(object sender, EventArgs e)
        {
            ClearRoleFields();
        }
        private void btnDeleteRole_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Are you sure to continue with delete process ?", "Delete Step", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            int roleID = string.IsNullOrEmpty(txtRoleID.Text) ? 0 : Convert.ToInt32(txtRoleID.Text);

            if (roleID > 0 && result == DialogResult.Yes)
                if (dataAccess.DeleteRole(roleID))
                {
                    gridControlRole.DataSource = dataAccess.GetRoleAndPermission();
                    ClearRoleFields();
                }
        }
        void ClearUserFields()
        {
            gridViewUser.ClearSelection();
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtUserName.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtConfirmPassword.Text = string.Empty;
            txtUserID.Text = string.Empty;
            SetUsersButtonAppearance(true);
            ddlRole.SelectedIndex = 0;
        }
        void ClearRoleFields()
        {
            txtRoleName.Text = string.Empty;
            txtRoleID.Text = string.Empty;
            SetRolesButtonAppearance(true);
            clbPermission.UnCheckAll();
        }

        private void txtConfirmPassword_Leave(object sender, EventArgs e)
        {
            string password = txtPassword.Text.Trim();
            string confirmPassword = txtConfirmPassword.Text.Trim();
            if (!string.Equals(password, confirmPassword))
                txtConfirmPassword.BackColor = Color.FromArgb(253, 238, 238);
            else
                txtConfirmPassword.BackColor = Color.White;
        }

        
        void SetUsersButtonAppearance(bool enabled)
        {
            btnAddUpdateUser.Enabled = enabled;
            btnDeleteUser.Enabled = enabled;

            btnAddUpdateUser.Appearance.BackColor = SetButtonBackColor(enabled);
            btnDeleteUser.Appearance.BackColor = SetButtonBackColor(enabled);

            btnAddUpdateUser.Appearance.ForeColor = SetButtonForeColor(enabled);
            btnDeleteUser.Appearance.ForeColor = SetButtonForeColor(enabled);
        }

        void SetRolesButtonAppearance(bool enabled)
        {
            btnAddUpdateRole.Enabled = enabled;
            btnDeleteRole.Enabled = enabled;

            btnAddUpdateRole.Appearance.BackColor = SetButtonBackColor(enabled);
            btnDeleteRole.Appearance.BackColor = SetButtonBackColor(enabled);

            btnAddUpdateRole.Appearance.ForeColor = SetButtonForeColor(enabled);
            btnDeleteRole.Appearance.ForeColor = SetButtonForeColor(enabled);
        }

        Color SetButtonBackColor(bool enabled)
        {
            return enabled ? Color.FromArgb(111, 114, 113) : Color.FromArgb(179, 179, 179);
        }

        Color SetButtonForeColor(bool enabled)
        {
            return enabled ? Color.White : Color.FromArgb(204, 204, 204);
        }
    }
}
