﻿namespace VideoViewer
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.imageListIndications = new System.Windows.Forms.ImageList(this.components);
            this.topPanelCloseMenu = new System.Windows.Forms.Panel();
            this.btnCloseApplication = new System.Windows.Forms.Button();
            this.tableLayoutTopControlsMenu = new System.Windows.Forms.TableLayoutPanel();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.btnReconnect = new DevExpress.XtraEditors.SimpleButton();
            this.btnHelpTroubleshoot = new DevExpress.XtraEditors.SimpleButton();
            this.btnUserManagement = new DevExpress.XtraEditors.SimpleButton();
            this.btnConfiguration = new DevExpress.XtraEditors.SimpleButton();
            this.btnAlarm = new DevExpress.XtraEditors.SimpleButton();
            this.btnHMI = new DevExpress.XtraEditors.SimpleButton();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.panelTopStraignLine = new System.Windows.Forms.Panel();
            this.panelBottomStraignLine = new System.Windows.Forms.Panel();
            this.cisServiceController = new System.ServiceProcess.ServiceController();
            this.imageListButton = new System.Windows.Forms.ImageList(this.components);
            this.backgroundWorkerServiceStatus = new System.ComponentModel.BackgroundWorker();
            this.tableLayoutPanelParent = new System.Windows.Forms.TableLayoutPanel();
            this.panelControlLMenu = new DevExpress.XtraEditors.PanelControl();
            this.tabCameraGroups = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabInwardCamGrp = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.btnCloseInwardSeletion = new DevExpress.XtraEditors.SimpleButton();
            this.btnSelectInwardCam = new DevExpress.XtraEditors.SimpleButton();
            this.lbInwardCam = new DevExpress.XtraEditors.ListBoxControl();
            this.tabOutwardCamGrp = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.btnCloseOutwardSeletion = new DevExpress.XtraEditors.SimpleButton();
            this.btnSelectOutwardCam = new DevExpress.XtraEditors.SimpleButton();
            this.lbOutwardCam = new DevExpress.XtraEditors.ListBoxControl();
            this.lblMessageDateSearch = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnShowOutwardCam = new DevExpress.XtraEditors.SimpleButton();
            this.btnShowInwardCam = new DevExpress.XtraEditors.SimpleButton();
            this.lbTimeOut = new System.Windows.Forms.ListBox();
            this.lbTimeIN = new System.Windows.Forms.ListBox();
            this.dateEditFrom = new DevExpress.XtraEditors.DateEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.lbVIN = new System.Windows.Forms.ListBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.MaskedTextBox();
            this.panelServerName = new System.Windows.Forms.Panel();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnGetVisits = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditTo = new DevExpress.XtraEditors.DateEdit();
            this.panelDisplay = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnPlayBackward02 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPauseBackward02 = new DevExpress.XtraEditors.SimpleButton();
            this.trackBar02 = new DevExpress.XtraEditors.ZoomTrackBarControl();
            this.btnExport2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPlayForward02 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPauseForward02 = new DevExpress.XtraEditors.SimpleButton();
            this.btnFrameBackward02 = new DevExpress.XtraEditors.SimpleButton();
            this.btnFrameForward02 = new DevExpress.XtraEditors.SimpleButton();
            this.btnReset02 = new DevExpress.XtraEditors.SimpleButton();
            this.gridEvent = new DevExpress.XtraGrid.GridControl();
            this.gridViewEvent = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSrNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVIN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarker = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeStamp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnPlayBackward01 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPauseBackward01 = new DevExpress.XtraEditors.SimpleButton();
            this.trackBar01 = new DevExpress.XtraEditors.ZoomTrackBarControl();
            this.btnExport1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPlayForward01 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPauseForward01 = new DevExpress.XtraEditors.SimpleButton();
            this.btnFrameBackward01 = new DevExpress.XtraEditors.SimpleButton();
            this.btnFrameForward01 = new DevExpress.XtraEditors.SimpleButton();
            this.btnReset01 = new DevExpress.XtraEditors.SimpleButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblTime01 = new System.Windows.Forms.Label();
            this.lblSpeed01 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblSpeed02 = new System.Windows.Forms.Label();
            this.lblTime02 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutMain = new System.Windows.Forms.TableLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnBrowseFolder = new DevExpress.XtraEditors.SimpleButton();
            this.btnService = new DevExpress.XtraEditors.SimpleButton();
            this.lblOrion = new DevExpress.XtraEditors.LabelControl();
            this.show_recording = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.dateTimeChartRangeControlClient1 = new DevExpress.XtraEditors.DateTimeChartRangeControlClient();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.backgroundWorkerVMSStatus = new System.ComponentModel.BackgroundWorker();
            this.topPanelCloseMenu.SuspendLayout();
            this.tableLayoutTopControlsMenu.SuspendLayout();
            this.panelMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.tableLayoutPanelParent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlLMenu)).BeginInit();
            this.panelControlLMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabCameraGroups)).BeginInit();
            this.tabCameraGroups.SuspendLayout();
            this.tabInwardCamGrp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbInwardCam)).BeginInit();
            this.tabOutwardCamGrp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbOutwardCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).BeginInit();
            this.panelServerName.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).BeginInit();
            this.panelDisplay.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar02.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEvent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEvent)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar01.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tableLayoutMain.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.show_recording)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimeChartRangeControlClient1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // imageListIndications
            // 
            this.imageListIndications.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIndications.ImageStream")));
            this.imageListIndications.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIndications.Images.SetKeyName(0, "circle_green.png");
            this.imageListIndications.Images.SetKeyName(1, "circle_grey.png");
            this.imageListIndications.Images.SetKeyName(2, "circle_red.png");
            // 
            // topPanelCloseMenu
            // 
            this.topPanelCloseMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.topPanelCloseMenu.Controls.Add(this.btnCloseApplication);
            this.topPanelCloseMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topPanelCloseMenu.Location = new System.Drawing.Point(2, 2);
            this.topPanelCloseMenu.Margin = new System.Windows.Forms.Padding(2);
            this.topPanelCloseMenu.Name = "topPanelCloseMenu";
            this.topPanelCloseMenu.Size = new System.Drawing.Size(1508, 28);
            this.topPanelCloseMenu.TabIndex = 3;
            // 
            // btnCloseApplication
            // 
            this.btnCloseApplication.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.btnCloseApplication.FlatAppearance.BorderSize = 0;
            this.btnCloseApplication.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseApplication.Image = ((System.Drawing.Image)(resources.GetObject("btnCloseApplication.Image")));
            this.btnCloseApplication.Location = new System.Drawing.Point(1887, 1);
            this.btnCloseApplication.Margin = new System.Windows.Forms.Padding(2);
            this.btnCloseApplication.Name = "btnCloseApplication";
            this.btnCloseApplication.Size = new System.Drawing.Size(24, 28);
            this.btnCloseApplication.TabIndex = 0;
            this.btnCloseApplication.UseVisualStyleBackColor = false;
            this.btnCloseApplication.Click += new System.EventHandler(this.btnCloseApplication_Click);
            // 
            // tableLayoutTopControlsMenu
            // 
            this.tableLayoutTopControlsMenu.ColumnCount = 2;
            this.tableLayoutTopControlsMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 244F));
            this.tableLayoutTopControlsMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutTopControlsMenu.Controls.Add(this.panelMenu, 1, 0);
            this.tableLayoutTopControlsMenu.Controls.Add(this.pictureBoxLogo, 0, 0);
            this.tableLayoutTopControlsMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutTopControlsMenu.Location = new System.Drawing.Point(2, 34);
            this.tableLayoutTopControlsMenu.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutTopControlsMenu.Name = "tableLayoutTopControlsMenu";
            this.tableLayoutTopControlsMenu.RowCount = 1;
            this.tableLayoutTopControlsMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutTopControlsMenu.Size = new System.Drawing.Size(1508, 94);
            this.tableLayoutTopControlsMenu.TabIndex = 4;
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panelMenu.Controls.Add(this.btnReconnect);
            this.panelMenu.Controls.Add(this.btnHelpTroubleshoot);
            this.panelMenu.Controls.Add(this.btnUserManagement);
            this.panelMenu.Controls.Add(this.btnConfiguration);
            this.panelMenu.Controls.Add(this.btnAlarm);
            this.panelMenu.Controls.Add(this.btnHMI);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMenu.Location = new System.Drawing.Point(246, 2);
            this.panelMenu.Margin = new System.Windows.Forms.Padding(2);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(1260, 90);
            this.panelMenu.TabIndex = 0;
            // 
            // btnReconnect
            // 
            this.btnReconnect.ImageOptions.ImageUri.Uri = "Refresh;Office2013";
            this.btnReconnect.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnReconnect.Location = new System.Drawing.Point(1628, 53);
            this.btnReconnect.Name = "btnReconnect";
            this.btnReconnect.Size = new System.Drawing.Size(32, 32);
            this.btnReconnect.TabIndex = 5;
            this.btnReconnect.ToolTip = "Reconnect VMS Server";
            this.btnReconnect.Click += new System.EventHandler(this.btnReconnect_Click);
            // 
            // btnHelpTroubleshoot
            // 
            this.btnHelpTroubleshoot.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.btnHelpTroubleshoot.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.btnHelpTroubleshoot.Appearance.Options.UseFont = true;
            this.btnHelpTroubleshoot.Appearance.Options.UseForeColor = true;
            this.btnHelpTroubleshoot.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnHelpTroubleshoot.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnHelpTroubleshoot.ImageOptions.Image")));
            this.btnHelpTroubleshoot.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btnHelpTroubleshoot.Location = new System.Drawing.Point(554, 2);
            this.btnHelpTroubleshoot.Margin = new System.Windows.Forms.Padding(2);
            this.btnHelpTroubleshoot.Name = "btnHelpTroubleshoot";
            this.btnHelpTroubleshoot.Size = new System.Drawing.Size(136, 81);
            this.btnHelpTroubleshoot.TabIndex = 4;
            this.btnHelpTroubleshoot.Text = "Logs";
            this.btnHelpTroubleshoot.Click += new System.EventHandler(this.btnHelpTroubleshoot_Click);
            this.btnHelpTroubleshoot.MouseEnter += new System.EventHandler(this.btnHelpTroubleshoot_MouseEnter);
            this.btnHelpTroubleshoot.MouseLeave += new System.EventHandler(this.btnHelpTroubleshoot_MouseLeave);
            // 
            // btnUserManagement
            // 
            this.btnUserManagement.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.btnUserManagement.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.btnUserManagement.Appearance.Options.UseFont = true;
            this.btnUserManagement.Appearance.Options.UseForeColor = true;
            this.btnUserManagement.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnUserManagement.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnUserManagement.ImageOptions.Image")));
            this.btnUserManagement.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btnUserManagement.Location = new System.Drawing.Point(416, 2);
            this.btnUserManagement.Margin = new System.Windows.Forms.Padding(2);
            this.btnUserManagement.Name = "btnUserManagement";
            this.btnUserManagement.Size = new System.Drawing.Size(136, 81);
            this.btnUserManagement.TabIndex = 4;
            this.btnUserManagement.Text = "User Management";
            this.btnUserManagement.Click += new System.EventHandler(this.btnUserManagement_Click);
            this.btnUserManagement.MouseEnter += new System.EventHandler(this.btnUserManagement_MouseEnter);
            this.btnUserManagement.MouseLeave += new System.EventHandler(this.btnUserManagement_MouseLeave);
            // 
            // btnConfiguration
            // 
            this.btnConfiguration.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.btnConfiguration.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.btnConfiguration.Appearance.Options.UseFont = true;
            this.btnConfiguration.Appearance.Options.UseForeColor = true;
            this.btnConfiguration.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnConfiguration.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfiguration.ImageOptions.Image")));
            this.btnConfiguration.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btnConfiguration.Location = new System.Drawing.Point(278, 2);
            this.btnConfiguration.Margin = new System.Windows.Forms.Padding(2);
            this.btnConfiguration.Name = "btnConfiguration";
            this.btnConfiguration.Size = new System.Drawing.Size(136, 81);
            this.btnConfiguration.TabIndex = 4;
            this.btnConfiguration.Text = "System Configuration";
            this.btnConfiguration.Click += new System.EventHandler(this.btnConfiguration_Click);
            this.btnConfiguration.MouseEnter += new System.EventHandler(this.btnConfiguration_MouseEnter);
            this.btnConfiguration.MouseLeave += new System.EventHandler(this.btnConfiguration_MouseLeave);
            // 
            // btnAlarm
            // 
            this.btnAlarm.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.btnAlarm.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.btnAlarm.Appearance.Options.UseFont = true;
            this.btnAlarm.Appearance.Options.UseForeColor = true;
            this.btnAlarm.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnAlarm.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAlarm.ImageOptions.Image")));
            this.btnAlarm.ImageOptions.ImageIndex = 0;
            this.btnAlarm.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btnAlarm.Location = new System.Drawing.Point(140, 2);
            this.btnAlarm.Margin = new System.Windows.Forms.Padding(2);
            this.btnAlarm.Name = "btnAlarm";
            this.btnAlarm.Size = new System.Drawing.Size(136, 81);
            this.btnAlarm.TabIndex = 4;
            this.btnAlarm.Text = "Visit Log";
            this.btnAlarm.Click += new System.EventHandler(this.btnAlarm_Click);
            this.btnAlarm.MouseEnter += new System.EventHandler(this.btnAlarm_MouseEnter);
            this.btnAlarm.MouseLeave += new System.EventHandler(this.btnAlarm_MouseLeave);
            // 
            // btnHMI
            // 
            this.btnHMI.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.btnHMI.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.btnHMI.Appearance.Options.UseFont = true;
            this.btnHMI.Appearance.Options.UseForeColor = true;
            this.btnHMI.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnHMI.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnHMI.ImageOptions.Image")));
            this.btnHMI.ImageOptions.ImageList = this.imageListIndications;
            this.btnHMI.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btnHMI.Location = new System.Drawing.Point(2, 2);
            this.btnHMI.Margin = new System.Windows.Forms.Padding(2);
            this.btnHMI.Name = "btnHMI";
            this.btnHMI.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.True;
            this.btnHMI.Size = new System.Drawing.Size(136, 81);
            this.btnHMI.TabIndex = 4;
            this.btnHMI.Text = "Main Layout";
            this.btnHMI.Click += new System.EventHandler(this.btnHMI_Click);
            this.btnHMI.MouseEnter += new System.EventHandler(this.btnHMI_MouseEnter);
            this.btnHMI.MouseLeave += new System.EventHandler(this.btnHMI_MouseLeave);
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBoxLogo.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLogo.Image")));
            this.pictureBoxLogo.Location = new System.Drawing.Point(4, 4);
            this.pictureBoxLogo.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(236, 86);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxLogo.TabIndex = 8;
            this.pictureBoxLogo.TabStop = false;
            // 
            // panelTopStraignLine
            // 
            this.panelTopStraignLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panelTopStraignLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTopStraignLine.Location = new System.Drawing.Point(2, 132);
            this.panelTopStraignLine.Margin = new System.Windows.Forms.Padding(2);
            this.panelTopStraignLine.MinimumSize = new System.Drawing.Size(75, 2);
            this.panelTopStraignLine.Name = "panelTopStraignLine";
            this.panelTopStraignLine.Size = new System.Drawing.Size(1508, 2);
            this.panelTopStraignLine.TabIndex = 5;
            // 
            // panelBottomStraignLine
            // 
            this.panelBottomStraignLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panelBottomStraignLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBottomStraignLine.Location = new System.Drawing.Point(2, 969);
            this.panelBottomStraignLine.Margin = new System.Windows.Forms.Padding(2);
            this.panelBottomStraignLine.Name = "panelBottomStraignLine";
            this.panelBottomStraignLine.Size = new System.Drawing.Size(1508, 1);
            this.panelBottomStraignLine.TabIndex = 6;
            // 
            // cisServiceController
            // 
            this.cisServiceController.ServiceName = "OrionAutoVDSServer";
            // 
            // imageListButton
            // 
            this.imageListButton.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListButton.ImageStream")));
            this.imageListButton.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListButton.Images.SetKeyName(0, "alarmEvents.png");
            this.imageListButton.Images.SetKeyName(1, "alarmEventsHover.png");
            this.imageListButton.Images.SetKeyName(2, "hmiLayout.png");
            this.imageListButton.Images.SetKeyName(3, "hmiLayoutHover.png");
            this.imageListButton.Images.SetKeyName(4, "logs.png");
            this.imageListButton.Images.SetKeyName(5, "logsHover.png");
            this.imageListButton.Images.SetKeyName(6, "systemConfiguration.png");
            this.imageListButton.Images.SetKeyName(7, "systemConfiguratoinHover.png");
            this.imageListButton.Images.SetKeyName(8, "userManagement.png");
            this.imageListButton.Images.SetKeyName(9, "userManagementHover.png");
            // 
            // backgroundWorkerServiceStatus
            // 
            this.backgroundWorkerServiceStatus.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerServiceStatus_DoWork);
            this.backgroundWorkerServiceStatus.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerServiceStatus_RunWorkerCompleted);
            // 
            // tableLayoutPanelParent
            // 
            this.tableLayoutPanelParent.ColumnCount = 2;
            this.tableLayoutPanelParent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 244F));
            this.tableLayoutPanelParent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelParent.Controls.Add(this.panelControlLMenu, 0, 0);
            this.tableLayoutPanelParent.Controls.Add(this.panelDisplay, 1, 0);
            this.tableLayoutPanelParent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelParent.Location = new System.Drawing.Point(2, 136);
            this.tableLayoutPanelParent.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelParent.Name = "tableLayoutPanelParent";
            this.tableLayoutPanelParent.RowCount = 1;
            this.tableLayoutPanelParent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelParent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 829F));
            this.tableLayoutPanelParent.Size = new System.Drawing.Size(1508, 829);
            this.tableLayoutPanelParent.TabIndex = 2;
            // 
            // panelControlLMenu
            // 
            this.panelControlLMenu.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panelControlLMenu.Appearance.Options.UseBackColor = true;
            this.panelControlLMenu.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControlLMenu.ContentImage = ((System.Drawing.Image)(resources.GetObject("panelControlLMenu.ContentImage")));
            this.panelControlLMenu.ContentImageAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.panelControlLMenu.Controls.Add(this.tabCameraGroups);
            this.panelControlLMenu.Controls.Add(this.lblMessageDateSearch);
            this.panelControlLMenu.Controls.Add(this.lblMessage);
            this.panelControlLMenu.Controls.Add(this.label3);
            this.panelControlLMenu.Controls.Add(this.label2);
            this.panelControlLMenu.Controls.Add(this.btnShowOutwardCam);
            this.panelControlLMenu.Controls.Add(this.btnShowInwardCam);
            this.panelControlLMenu.Controls.Add(this.lbTimeOut);
            this.panelControlLMenu.Controls.Add(this.lbTimeIN);
            this.panelControlLMenu.Controls.Add(this.dateEditFrom);
            this.panelControlLMenu.Controls.Add(this.label1);
            this.panelControlLMenu.Controls.Add(this.lbVIN);
            this.panelControlLMenu.Controls.Add(this.label16);
            this.panelControlLMenu.Controls.Add(this.txtSearch);
            this.panelControlLMenu.Controls.Add(this.panelServerName);
            this.panelControlLMenu.Controls.Add(this.panel8);
            this.panelControlLMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlLMenu.Location = new System.Drawing.Point(2, 2);
            this.panelControlLMenu.Margin = new System.Windows.Forms.Padding(2);
            this.panelControlLMenu.Name = "panelControlLMenu";
            this.panelControlLMenu.Size = new System.Drawing.Size(240, 825);
            this.panelControlLMenu.TabIndex = 8;
            // 
            // tabCameraGroups
            // 
            this.tabCameraGroups.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.tabCameraGroups.Appearance.Options.UseBackColor = true;
            this.tabCameraGroups.Controls.Add(this.tabInwardCamGrp);
            this.tabCameraGroups.Controls.Add(this.tabOutwardCamGrp);
            this.tabCameraGroups.Location = new System.Drawing.Point(10, 584);
            this.tabCameraGroups.Name = "tabCameraGroups";
            this.tabCameraGroups.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabInwardCamGrp,
            this.tabOutwardCamGrp});
            this.tabCameraGroups.RegularSize = new System.Drawing.Size(216, 267);
            this.tabCameraGroups.SelectedPage = this.tabOutwardCamGrp;
            this.tabCameraGroups.Size = new System.Drawing.Size(216, 267);
            this.tabCameraGroups.TabIndex = 503;
            this.tabCameraGroups.Text = "tabPane1";
            // 
            // tabInwardCamGrp
            // 
            this.tabInwardCamGrp.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabInwardCamGrp.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("tabInwardCamGrp.Appearance.Image")));
            this.tabInwardCamGrp.Appearance.Options.UseBackColor = true;
            this.tabInwardCamGrp.Appearance.Options.UseImage = true;
            this.tabInwardCamGrp.Caption = "Select Inward Camera   ";
            this.tabInwardCamGrp.Controls.Add(this.btnCloseInwardSeletion);
            this.tabInwardCamGrp.Controls.Add(this.btnSelectInwardCam);
            this.tabInwardCamGrp.Controls.Add(this.lbInwardCam);
            this.tabInwardCamGrp.Name = "tabInwardCamGrp";
            this.tabInwardCamGrp.Size = new System.Drawing.Size(198, 222);
            // 
            // btnCloseInwardSeletion
            // 
            this.btnCloseInwardSeletion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCloseInwardSeletion.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnCloseInwardSeletion.ImageOptions.ImageUri.Uri = "Cancel;Office2013";
            this.btnCloseInwardSeletion.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCloseInwardSeletion.Location = new System.Drawing.Point(49, 196);
            this.btnCloseInwardSeletion.Name = "btnCloseInwardSeletion";
            this.btnCloseInwardSeletion.Size = new System.Drawing.Size(20, 20);
            this.btnCloseInwardSeletion.TabIndex = 2;
            this.btnCloseInwardSeletion.Click += new System.EventHandler(this.btnCloseInwardSeletion_Click);
            // 
            // btnSelectInwardCam
            // 
            this.btnSelectInwardCam.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnSelectInwardCam.ImageOptions.ImageUri.Uri = "Apply;Size32x32;Office2013";
            this.btnSelectInwardCam.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSelectInwardCam.Location = new System.Drawing.Point(8, 196);
            this.btnSelectInwardCam.Name = "btnSelectInwardCam";
            this.btnSelectInwardCam.Size = new System.Drawing.Size(20, 20);
            this.btnSelectInwardCam.TabIndex = 1;
            this.btnSelectInwardCam.Click += new System.EventHandler(this.btnSelectInwardCam_Click);
            // 
            // lbInwardCam
            // 
            this.lbInwardCam.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lbInwardCam.Appearance.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbInwardCam.Appearance.Options.UseBackColor = true;
            this.lbInwardCam.Appearance.Options.UseFont = true;
            this.lbInwardCam.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbInwardCam.HighlightedItemStyle = DevExpress.XtraEditors.HighlightStyle.Standard;
            this.lbInwardCam.HotTrackSelectMode = DevExpress.XtraEditors.HotTrackSelectMode.SelectItemOnClick;
            this.lbInwardCam.Location = new System.Drawing.Point(2, 4);
            this.lbInwardCam.Name = "lbInwardCam";
            this.lbInwardCam.Size = new System.Drawing.Size(195, 186);
            this.lbInwardCam.TabIndex = 0;
            this.lbInwardCam.DrawItem += new DevExpress.XtraEditors.ListBoxDrawItemEventHandler(this.lbInwardCam_DrawItem);
            // 
            // tabOutwardCamGrp
            // 
            this.tabOutwardCamGrp.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabOutwardCamGrp.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("tabOutwardCamGrp.Appearance.Image")));
            this.tabOutwardCamGrp.Appearance.Options.UseBackColor = true;
            this.tabOutwardCamGrp.Appearance.Options.UseImage = true;
            this.tabOutwardCamGrp.Caption = "Select Outward Camera ";
            this.tabOutwardCamGrp.Controls.Add(this.btnCloseOutwardSeletion);
            this.tabOutwardCamGrp.Controls.Add(this.btnSelectOutwardCam);
            this.tabOutwardCamGrp.Controls.Add(this.lbOutwardCam);
            this.tabOutwardCamGrp.Name = "tabOutwardCamGrp";
            this.tabOutwardCamGrp.Size = new System.Drawing.Size(198, 222);
            // 
            // btnCloseOutwardSeletion
            // 
            this.btnCloseOutwardSeletion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCloseOutwardSeletion.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnCloseOutwardSeletion.ImageOptions.ImageUri.Uri = "Cancel;Office2013";
            this.btnCloseOutwardSeletion.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCloseOutwardSeletion.Location = new System.Drawing.Point(49, 196);
            this.btnCloseOutwardSeletion.Name = "btnCloseOutwardSeletion";
            this.btnCloseOutwardSeletion.Size = new System.Drawing.Size(20, 20);
            this.btnCloseOutwardSeletion.TabIndex = 5;
            this.btnCloseOutwardSeletion.Click += new System.EventHandler(this.btnCloseOutwardSeletion_Click);
            // 
            // btnSelectOutwardCam
            // 
            this.btnSelectOutwardCam.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnSelectOutwardCam.ImageOptions.ImageUri.Uri = "Apply;Size32x32;Office2013";
            this.btnSelectOutwardCam.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSelectOutwardCam.Location = new System.Drawing.Point(8, 196);
            this.btnSelectOutwardCam.Name = "btnSelectOutwardCam";
            this.btnSelectOutwardCam.Size = new System.Drawing.Size(20, 20);
            this.btnSelectOutwardCam.TabIndex = 4;
            this.btnSelectOutwardCam.Click += new System.EventHandler(this.btnSelectOutwardCam_Click);
            // 
            // lbOutwardCam
            // 
            this.lbOutwardCam.AllowDrop = true;
            this.lbOutwardCam.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lbOutwardCam.Appearance.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOutwardCam.Appearance.Options.UseBackColor = true;
            this.lbOutwardCam.Appearance.Options.UseFont = true;
            this.lbOutwardCam.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbOutwardCam.HighlightedItemStyle = DevExpress.XtraEditors.HighlightStyle.Standard;
            this.lbOutwardCam.HotTrackSelectMode = DevExpress.XtraEditors.HotTrackSelectMode.SelectItemOnClick;
            this.lbOutwardCam.Location = new System.Drawing.Point(2, 4);
            this.lbOutwardCam.Name = "lbOutwardCam";
            this.lbOutwardCam.Size = new System.Drawing.Size(195, 186);
            this.lbOutwardCam.TabIndex = 3;
            this.lbOutwardCam.DrawItem += new DevExpress.XtraEditors.ListBoxDrawItemEventHandler(this.lbOutwardCam_DrawItem);
            // 
            // lblMessageDateSearch
            // 
            this.lblMessageDateSearch.AutoSize = true;
            this.lblMessageDateSearch.ForeColor = System.Drawing.Color.Yellow;
            this.lblMessageDateSearch.Location = new System.Drawing.Point(8, 285);
            this.lblMessageDateSearch.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMessageDateSearch.Name = "lblMessageDateSearch";
            this.lblMessageDateSearch.Size = new System.Drawing.Size(112, 13);
            this.lblMessageDateSearch.TabIndex = 502;
            this.lblMessageDateSearch.Text = "No record has found !";
            this.lblMessageDateSearch.Visible = false;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.ForeColor = System.Drawing.Color.Yellow;
            this.lblMessage.Location = new System.Drawing.Point(8, 215);
            this.lblMessage.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(112, 13);
            this.lblMessage.TabIndex = 501;
            this.lblMessage.Text = "No record has found !";
            this.lblMessage.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(116, 306);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 231;
            this.label3.Text = "OUTWARDS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(6, 306);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 17);
            this.label2.TabIndex = 230;
            this.label2.Text = "INWARDS";
            // 
            // btnShowOutwardCam
            // 
            this.btnShowOutwardCam.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnShowOutwardCam.Appearance.Options.UseBackColor = true;
            this.btnShowOutwardCam.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnShowOutwardCam.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnShowOutwardCam.ImageOptions.Image = global::VideoViewer.Properties.Resources.video_camera1;
            this.btnShowOutwardCam.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnShowOutwardCam.Location = new System.Drawing.Point(121, 540);
            this.btnShowOutwardCam.Margin = new System.Windows.Forms.Padding(2);
            this.btnShowOutwardCam.Name = "btnShowOutwardCam";
            this.btnShowOutwardCam.Size = new System.Drawing.Size(105, 27);
            this.btnShowOutwardCam.TabIndex = 10;
            this.btnShowOutwardCam.Text = "Outward ";
            this.btnShowOutwardCam.Click += new System.EventHandler(this.btnShowOutwardCam_Click);
            // 
            // btnShowInwardCam
            // 
            this.btnShowInwardCam.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnShowInwardCam.Appearance.Options.UseBackColor = true;
            this.btnShowInwardCam.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnShowInwardCam.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnShowInwardCam.ImageOptions.Image = global::VideoViewer.Properties.Resources.video_camera;
            this.btnShowInwardCam.Location = new System.Drawing.Point(10, 540);
            this.btnShowInwardCam.Margin = new System.Windows.Forms.Padding(2);
            this.btnShowInwardCam.Name = "btnShowInwardCam";
            this.btnShowInwardCam.Size = new System.Drawing.Size(105, 27);
            this.btnShowInwardCam.TabIndex = 9;
            this.btnShowInwardCam.Text = "Inward";
            this.btnShowInwardCam.Click += new System.EventHandler(this.btnShowInwardCam_Click);
            // 
            // lbTimeOut
            // 
            this.lbTimeOut.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbTimeOut.Font = new System.Drawing.Font("Calibri", 8F);
            this.lbTimeOut.FormattingEnabled = true;
            this.lbTimeOut.Location = new System.Drawing.Point(121, 325);
            this.lbTimeOut.Margin = new System.Windows.Forms.Padding(2);
            this.lbTimeOut.Name = "lbTimeOut";
            this.lbTimeOut.Size = new System.Drawing.Size(105, 195);
            this.lbTimeOut.TabIndex = 8;
            this.lbTimeOut.SelectedIndexChanged += new System.EventHandler(this.lbTimeOut_SelectedIndexChanged);
            // 
            // lbTimeIN
            // 
            this.lbTimeIN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbTimeIN.Font = new System.Drawing.Font("Calibri", 8F);
            this.lbTimeIN.FormattingEnabled = true;
            this.lbTimeIN.Location = new System.Drawing.Point(10, 325);
            this.lbTimeIN.Margin = new System.Windows.Forms.Padding(2);
            this.lbTimeIN.Name = "lbTimeIN";
            this.lbTimeIN.Size = new System.Drawing.Size(105, 195);
            this.lbTimeIN.TabIndex = 7;
            this.lbTimeIN.SelectedIndexChanged += new System.EventHandler(this.lbTimeIN_SelectedIndexChanged);
            // 
            // dateEditFrom
            // 
            this.dateEditFrom.EditValue = new System.DateTime(2018, 6, 26, 0, 0, 0, 0);
            this.dateEditFrom.Location = new System.Drawing.Point(16, 258);
            this.dateEditFrom.Margin = new System.Windows.Forms.Padding(2);
            this.dateEditFrom.Name = "dateEditFrom";
            this.dateEditFrom.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.dateEditFrom.Properties.Appearance.Options.UseFont = true;
            this.dateEditFrom.Properties.AppearanceCalendar.DayCellSelected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dateEditFrom.Properties.AppearanceCalendar.DayCellSelected.Options.UseBackColor = true;
            this.dateEditFrom.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.dateEditFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFrom.Size = new System.Drawing.Size(82, 20);
            this.dateEditFrom.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(8, 234);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 19);
            this.label1.TabIndex = 223;
            this.label1.Text = "Trip History";
            // 
            // lbVIN
            // 
            this.lbVIN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbVIN.Font = new System.Drawing.Font("Calibri", 10F);
            this.lbVIN.FormattingEnabled = true;
            this.lbVIN.ItemHeight = 15;
            this.lbVIN.Location = new System.Drawing.Point(10, 73);
            this.lbVIN.Margin = new System.Windows.Forms.Padding(2);
            this.lbVIN.Name = "lbVIN";
            this.lbVIN.Size = new System.Drawing.Size(216, 135);
            this.lbVIN.TabIndex = 3;
            this.lbVIN.SelectedIndexChanged += new System.EventHandler(this.lbVIN_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(7, 20);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(194, 19);
            this.label16.TabIndex = 221;
            this.label16.Text = "Enter (Search) VIN Number";
            // 
            // txtSearch
            // 
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSearch.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtSearch.Location = new System.Drawing.Point(15, 48);
            this.txtSearch.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(180, 17);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // panelServerName
            // 
            this.panelServerName.BackColor = System.Drawing.Color.White;
            this.panelServerName.Controls.Add(this.btnSearch);
            this.panelServerName.Location = new System.Drawing.Point(10, 42);
            this.panelServerName.Margin = new System.Windows.Forms.Padding(2);
            this.panelServerName.Name = "panelServerName";
            this.panelServerName.Size = new System.Drawing.Size(216, 28);
            this.panelServerName.TabIndex = 500;
            // 
            // btnSearch
            // 
            this.btnSearch.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSearch.Appearance.Options.UseBackColor = true;
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSearch.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnSearch.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.ImageOptions.Image")));
            this.btnSearch.Location = new System.Drawing.Point(191, 2);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(24, 24);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.Controls.Add(this.btnGetVisits);
            this.panel8.Controls.Add(this.dateEditTo);
            this.panel8.Location = new System.Drawing.Point(10, 254);
            this.panel8.Margin = new System.Windows.Forms.Padding(2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(216, 28);
            this.panel8.TabIndex = 221;
            // 
            // btnGetVisits
            // 
            this.btnGetVisits.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnGetVisits.Appearance.Options.UseBackColor = true;
            this.btnGetVisits.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnGetVisits.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnGetVisits.Enabled = false;
            this.btnGetVisits.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGetVisits.ImageOptions.Image")));
            this.btnGetVisits.Location = new System.Drawing.Point(191, 3);
            this.btnGetVisits.Margin = new System.Windows.Forms.Padding(2);
            this.btnGetVisits.Name = "btnGetVisits";
            this.btnGetVisits.Size = new System.Drawing.Size(24, 22);
            this.btnGetVisits.TabIndex = 6;
            this.btnGetVisits.Click += new System.EventHandler(this.btnGetVisits_Click);
            // 
            // dateEditTo
            // 
            this.dateEditTo.EditValue = new System.DateTime(2018, 6, 27, 0, 0, 0, 0);
            this.dateEditTo.Location = new System.Drawing.Point(103, 4);
            this.dateEditTo.Margin = new System.Windows.Forms.Padding(2);
            this.dateEditTo.Name = "dateEditTo";
            this.dateEditTo.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.dateEditTo.Properties.Appearance.Options.UseFont = true;
            this.dateEditTo.Properties.AppearanceCalendar.DayCellSelected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dateEditTo.Properties.AppearanceCalendar.DayCellSelected.Options.UseBackColor = true;
            this.dateEditTo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.dateEditTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTo.Size = new System.Drawing.Size(82, 20);
            this.dateEditTo.TabIndex = 5;
            // 
            // panelDisplay
            // 
            this.panelDisplay.AutoSize = true;
            this.panelDisplay.BackColor = System.Drawing.Color.Transparent;
            this.panelDisplay.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelDisplay.BackgroundImage")));
            this.panelDisplay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelDisplay.Controls.Add(this.panel7);
            this.panelDisplay.Controls.Add(this.gridEvent);
            this.panelDisplay.Controls.Add(this.panel6);
            this.panelDisplay.Controls.Add(this.panel3);
            this.panelDisplay.Controls.Add(this.panel5);
            this.panelDisplay.Controls.Add(this.panel2);
            this.panelDisplay.Controls.Add(this.panel1);
            this.panelDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDisplay.Location = new System.Drawing.Point(246, 2);
            this.panelDisplay.Margin = new System.Windows.Forms.Padding(2);
            this.panelDisplay.MinimumSize = new System.Drawing.Size(600, 488);
            this.panelDisplay.Name = "panelDisplay";
            this.panelDisplay.Padding = new System.Windows.Forms.Padding(0, 0, 38, 0);
            this.panelDisplay.Size = new System.Drawing.Size(1260, 825);
            this.panelDisplay.TabIndex = 7;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panel7.Controls.Add(this.btnPlayBackward02);
            this.panel7.Controls.Add(this.btnPauseBackward02);
            this.panel7.Controls.Add(this.trackBar02);
            this.panel7.Controls.Add(this.btnExport2);
            this.panel7.Controls.Add(this.btnPlayForward02);
            this.panel7.Controls.Add(this.btnPauseForward02);
            this.panel7.Controls.Add(this.btnFrameBackward02);
            this.panel7.Controls.Add(this.btnFrameForward02);
            this.panel7.Controls.Add(this.btnReset02);
            this.panel7.Location = new System.Drawing.Point(835, 626);
            this.panel7.Margin = new System.Windows.Forms.Padding(2);
            this.panel7.MaximumSize = new System.Drawing.Size(800, 32);
            this.panel7.MinimumSize = new System.Drawing.Size(800, 32);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(800, 32);
            this.panel7.TabIndex = 233;
            // 
            // btnPlayBackward02
            // 
            this.btnPlayBackward02.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPlayBackward02.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPlayBackward02.ImageOptions.Image")));
            this.btnPlayBackward02.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPlayBackward02.Location = new System.Drawing.Point(315, 3);
            this.btnPlayBackward02.Margin = new System.Windows.Forms.Padding(2);
            this.btnPlayBackward02.Name = "btnPlayBackward02";
            this.btnPlayBackward02.Size = new System.Drawing.Size(24, 26);
            this.btnPlayBackward02.TabIndex = 231;
            this.btnPlayBackward02.ToolTip = "Play";
            this.btnPlayBackward02.Click += new System.EventHandler(this.btnPlayBackward02_Click);
            // 
            // btnPauseBackward02
            // 
            this.btnPauseBackward02.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPauseBackward02.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPauseBackward02.ImageOptions.Image")));
            this.btnPauseBackward02.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPauseBackward02.Location = new System.Drawing.Point(315, 3);
            this.btnPauseBackward02.Margin = new System.Windows.Forms.Padding(2);
            this.btnPauseBackward02.Name = "btnPauseBackward02";
            this.btnPauseBackward02.Size = new System.Drawing.Size(24, 26);
            this.btnPauseBackward02.TabIndex = 232;
            this.btnPauseBackward02.ToolTip = "Pause";
            this.btnPauseBackward02.Click += new System.EventHandler(this.btnPauseBackward02_Click);
            // 
            // trackBar02
            // 
            this.trackBar02.EditValue = 2;
            this.trackBar02.Location = new System.Drawing.Point(373, 5);
            this.trackBar02.Name = "trackBar02";
            this.trackBar02.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.trackBar02.Properties.Appearance.Options.UseBackColor = true;
            this.trackBar02.Properties.AutoSize = false;
            this.trackBar02.Properties.Maximum = 6;
            this.trackBar02.Properties.Middle = 3;
            this.trackBar02.Properties.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.Bar;
            this.trackBar02.Size = new System.Drawing.Size(104, 23);
            this.trackBar02.TabIndex = 230;
            this.trackBar02.Value = 2;
            this.trackBar02.EditValueChanged += new System.EventHandler(this.trackBar02_EditValueChanged);
            // 
            // btnExport2
            // 
            this.btnExport2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExport2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnExport2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnExport2.ImageOptions.Image")));
            this.btnExport2.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnExport2.ImageOptions.SvgImageSize = new System.Drawing.Size(24, 24);
            this.btnExport2.Location = new System.Drawing.Point(762, 0);
            this.btnExport2.Margin = new System.Windows.Forms.Padding(2);
            this.btnExport2.Name = "btnExport2";
            this.btnExport2.Size = new System.Drawing.Size(32, 32);
            toolTipTitleItem1.Text = "Export Image";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Save Current Image to Default Folder";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.btnExport2.SuperTip = superToolTip1;
            this.btnExport2.TabIndex = 227;
            this.btnExport2.ToolTip = "Save Current Image";
            this.btnExport2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.btnExport2.Click += new System.EventHandler(this.btnExport2_Click);
            // 
            // btnPlayForward02
            // 
            this.btnPlayForward02.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPlayForward02.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPlayForward02.ImageOptions.Image")));
            this.btnPlayForward02.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPlayForward02.Location = new System.Drawing.Point(511, 3);
            this.btnPlayForward02.Margin = new System.Windows.Forms.Padding(2);
            this.btnPlayForward02.Name = "btnPlayForward02";
            this.btnPlayForward02.Size = new System.Drawing.Size(24, 26);
            this.btnPlayForward02.TabIndex = 35;
            this.btnPlayForward02.ToolTip = "Play";
            this.btnPlayForward02.Click += new System.EventHandler(this.btnPlayForward02_Click);
            // 
            // btnPauseForward02
            // 
            this.btnPauseForward02.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPauseForward02.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPauseForward02.ImageOptions.Image")));
            this.btnPauseForward02.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPauseForward02.Location = new System.Drawing.Point(511, 3);
            this.btnPauseForward02.Margin = new System.Windows.Forms.Padding(2);
            this.btnPauseForward02.Name = "btnPauseForward02";
            this.btnPauseForward02.Size = new System.Drawing.Size(24, 26);
            this.btnPauseForward02.TabIndex = 36;
            this.btnPauseForward02.ToolTip = "Pause";
            this.btnPauseForward02.Click += new System.EventHandler(this.btnPauseForward02_Click);
            // 
            // btnFrameBackward02
            // 
            this.btnFrameBackward02.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnFrameBackward02.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFrameBackward02.ImageOptions.Image")));
            this.btnFrameBackward02.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnFrameBackward02.Location = new System.Drawing.Point(278, 3);
            this.btnFrameBackward02.Margin = new System.Windows.Forms.Padding(2);
            this.btnFrameBackward02.Name = "btnFrameBackward02";
            this.btnFrameBackward02.Size = new System.Drawing.Size(24, 26);
            this.btnFrameBackward02.TabIndex = 34;
            this.btnFrameBackward02.ToolTip = "Backward";
            this.btnFrameBackward02.Click += new System.EventHandler(this.btnFrameBackward02_Click);
            // 
            // btnFrameForward02
            // 
            this.btnFrameForward02.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnFrameForward02.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFrameForward02.ImageOptions.Image")));
            this.btnFrameForward02.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnFrameForward02.Location = new System.Drawing.Point(548, 3);
            this.btnFrameForward02.Margin = new System.Windows.Forms.Padding(2);
            this.btnFrameForward02.Name = "btnFrameForward02";
            this.btnFrameForward02.Size = new System.Drawing.Size(24, 26);
            this.btnFrameForward02.TabIndex = 33;
            this.btnFrameForward02.ToolTip = "Forward";
            this.btnFrameForward02.Click += new System.EventHandler(this.btnFrameForward02_Click);
            // 
            // btnReset02
            // 
            this.btnReset02.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnReset02.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnReset02.ImageOptions.Image")));
            this.btnReset02.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnReset02.Location = new System.Drawing.Point(6, 3);
            this.btnReset02.Margin = new System.Windows.Forms.Padding(2);
            this.btnReset02.Name = "btnReset02";
            this.btnReset02.Size = new System.Drawing.Size(24, 26);
            this.btnReset02.TabIndex = 32;
            this.btnReset02.ToolTip = "Reset";
            this.btnReset02.Click += new System.EventHandler(this.btnReset02_Click);
            // 
            // gridEvent
            // 
            this.gridEvent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.gridEvent.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridEvent.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridEvent.Location = new System.Drawing.Point(-179, 662);
            this.gridEvent.MainView = this.gridViewEvent;
            this.gridEvent.Margin = new System.Windows.Forms.Padding(2);
            this.gridEvent.Name = "gridEvent";
            this.gridEvent.Size = new System.Drawing.Size(1610, 156);
            this.gridEvent.TabIndex = 34;
            this.gridEvent.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEvent});
            // 
            // gridViewEvent
            // 
            this.gridViewEvent.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridViewEvent.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridViewEvent.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridViewEvent.Appearance.Empty.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.Empty.Options.UseFont = true;
            this.gridViewEvent.Appearance.EvenRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.EvenRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.FilterCloseButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridViewEvent.Appearance.FilterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FilterPanel.Options.UseFont = true;
            this.gridViewEvent.Appearance.FixedLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FixedLine.Options.UseFont = true;
            this.gridViewEvent.Appearance.FocusedCell.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FocusedCell.Options.UseFont = true;
            this.gridViewEvent.Appearance.FocusedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.FooterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewEvent.Appearance.GroupButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.GroupButton.Options.UseFont = true;
            this.gridViewEvent.Appearance.GroupFooter.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.GroupFooter.Options.UseFont = true;
            this.gridViewEvent.Appearance.GroupPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.GroupRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.HeaderPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewEvent.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.HorzLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.HorzLine.Options.UseFont = true;
            this.gridViewEvent.Appearance.OddRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.OddRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.Preview.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.Preview.Options.UseFont = true;
            this.gridViewEvent.Appearance.Row.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.Row.Options.UseFont = true;
            this.gridViewEvent.Appearance.RowSeparator.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.RowSeparator.Options.UseFont = true;
            this.gridViewEvent.Appearance.SelectedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.SelectedRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.TopNewRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.TopNewRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.VertLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.VertLine.Options.UseFont = true;
            this.gridViewEvent.Appearance.ViewCaption.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.ViewCaption.Options.UseFont = true;
            this.gridViewEvent.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSrNo,
            this.colVIN,
            this.colMarker,
            this.colComment,
            this.colTimeStamp});
            this.gridViewEvent.GridControl = this.gridEvent;
            this.gridViewEvent.Name = "gridViewEvent";
            this.gridViewEvent.OptionsView.ShowGroupPanel = false;
            // 
            // colSrNo
            // 
            this.colSrNo.Caption = "Sr #.";
            this.colSrNo.FieldName = "SrNo";
            this.colSrNo.MaxWidth = 50;
            this.colSrNo.Name = "colSrNo";
            this.colSrNo.OptionsColumn.AllowEdit = false;
            this.colSrNo.Visible = true;
            this.colSrNo.VisibleIndex = 0;
            this.colSrNo.Width = 50;
            // 
            // colVIN
            // 
            this.colVIN.Caption = "VIN";
            this.colVIN.FieldName = "Vin";
            this.colVIN.MaxWidth = 300;
            this.colVIN.MinWidth = 300;
            this.colVIN.Name = "colVIN";
            this.colVIN.OptionsColumn.AllowEdit = false;
            this.colVIN.Visible = true;
            this.colVIN.VisibleIndex = 1;
            this.colVIN.Width = 300;
            // 
            // colMarker
            // 
            this.colMarker.Caption = "Marker";
            this.colMarker.FieldName = "Marker";
            this.colMarker.MaxWidth = 250;
            this.colMarker.MinWidth = 250;
            this.colMarker.Name = "colMarker";
            this.colMarker.OptionsColumn.AllowEdit = false;
            this.colMarker.Visible = true;
            this.colMarker.VisibleIndex = 2;
            this.colMarker.Width = 250;
            // 
            // colComment
            // 
            this.colComment.Caption = "Comment";
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            this.colComment.OptionsColumn.AllowEdit = false;
            this.colComment.Visible = true;
            this.colComment.VisibleIndex = 3;
            // 
            // colTimeStamp
            // 
            this.colTimeStamp.Caption = "Timestamp";
            this.colTimeStamp.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm:ss";
            this.colTimeStamp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeStamp.FieldName = "Timestamp";
            this.colTimeStamp.MaxWidth = 250;
            this.colTimeStamp.MinWidth = 250;
            this.colTimeStamp.Name = "colTimeStamp";
            this.colTimeStamp.OptionsColumn.AllowEdit = false;
            this.colTimeStamp.Visible = true;
            this.colTimeStamp.VisibleIndex = 4;
            this.colTimeStamp.Width = 250;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panel6.Controls.Add(this.btnPlayBackward01);
            this.panel6.Controls.Add(this.btnPauseBackward01);
            this.panel6.Controls.Add(this.trackBar01);
            this.panel6.Controls.Add(this.btnExport1);
            this.panel6.Controls.Add(this.btnPlayForward01);
            this.panel6.Controls.Add(this.btnPauseForward01);
            this.panel6.Controls.Add(this.btnFrameBackward01);
            this.panel6.Controls.Add(this.btnFrameForward01);
            this.panel6.Controls.Add(this.btnReset01);
            this.panel6.Location = new System.Drawing.Point(25, 626);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.MaximumSize = new System.Drawing.Size(800, 32);
            this.panel6.MinimumSize = new System.Drawing.Size(800, 32);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(800, 32);
            this.panel6.TabIndex = 32;
            // 
            // btnPlayBackward01
            // 
            this.btnPlayBackward01.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPlayBackward01.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPlayBackward01.ImageOptions.Image")));
            this.btnPlayBackward01.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPlayBackward01.Location = new System.Drawing.Point(315, 3);
            this.btnPlayBackward01.Margin = new System.Windows.Forms.Padding(2);
            this.btnPlayBackward01.Name = "btnPlayBackward01";
            this.btnPlayBackward01.Size = new System.Drawing.Size(24, 26);
            this.btnPlayBackward01.TabIndex = 231;
            this.btnPlayBackward01.ToolTip = "Play";
            this.btnPlayBackward01.Click += new System.EventHandler(this.btnPlayBackward01_Click);
            // 
            // btnPauseBackward01
            // 
            this.btnPauseBackward01.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPauseBackward01.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPauseBackward01.ImageOptions.Image")));
            this.btnPauseBackward01.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPauseBackward01.Location = new System.Drawing.Point(315, 3);
            this.btnPauseBackward01.Margin = new System.Windows.Forms.Padding(2);
            this.btnPauseBackward01.Name = "btnPauseBackward01";
            this.btnPauseBackward01.Size = new System.Drawing.Size(24, 26);
            this.btnPauseBackward01.TabIndex = 232;
            this.btnPauseBackward01.ToolTip = "Pause";
            this.btnPauseBackward01.Click += new System.EventHandler(this.btnPauseBackward01_Click);
            // 
            // trackBar01
            // 
            this.trackBar01.EditValue = 2;
            this.trackBar01.Location = new System.Drawing.Point(373, 5);
            this.trackBar01.Name = "trackBar01";
            this.trackBar01.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.trackBar01.Properties.Appearance.Options.UseBackColor = true;
            this.trackBar01.Properties.AutoSize = false;
            this.trackBar01.Properties.Maximum = 6;
            this.trackBar01.Properties.Middle = 3;
            this.trackBar01.Properties.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.Bar;
            this.trackBar01.Size = new System.Drawing.Size(104, 23);
            this.trackBar01.TabIndex = 230;
            this.trackBar01.Value = 2;
            this.trackBar01.EditValueChanged += new System.EventHandler(this.trackBar01_EditValueChanged);
            // 
            // btnExport1
            // 
            this.btnExport1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExport1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnExport1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnExport1.ImageOptions.Image")));
            this.btnExport1.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnExport1.ImageOptions.SvgImageSize = new System.Drawing.Size(24, 24);
            this.btnExport1.Location = new System.Drawing.Point(762, 0);
            this.btnExport1.Margin = new System.Windows.Forms.Padding(2);
            this.btnExport1.Name = "btnExport1";
            this.btnExport1.Size = new System.Drawing.Size(32, 32);
            toolTipTitleItem2.Text = "Export Image";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Save Current Image to Default Folder";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.btnExport1.SuperTip = superToolTip2;
            this.btnExport1.TabIndex = 227;
            this.btnExport1.ToolTip = "Save Current Image";
            this.btnExport1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.btnExport1.Click += new System.EventHandler(this.btnExport1_Click);
            // 
            // btnPlayForward01
            // 
            this.btnPlayForward01.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPlayForward01.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPlayForward01.ImageOptions.Image")));
            this.btnPlayForward01.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPlayForward01.Location = new System.Drawing.Point(511, 3);
            this.btnPlayForward01.Margin = new System.Windows.Forms.Padding(2);
            this.btnPlayForward01.Name = "btnPlayForward01";
            this.btnPlayForward01.Size = new System.Drawing.Size(24, 26);
            this.btnPlayForward01.TabIndex = 35;
            this.btnPlayForward01.ToolTip = "Play";
            this.btnPlayForward01.Click += new System.EventHandler(this.btnPlayForward01_Click);
            // 
            // btnPauseForward01
            // 
            this.btnPauseForward01.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPauseForward01.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPauseForward01.ImageOptions.Image")));
            this.btnPauseForward01.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPauseForward01.Location = new System.Drawing.Point(511, 3);
            this.btnPauseForward01.Margin = new System.Windows.Forms.Padding(2);
            this.btnPauseForward01.Name = "btnPauseForward01";
            this.btnPauseForward01.Size = new System.Drawing.Size(24, 26);
            this.btnPauseForward01.TabIndex = 36;
            this.btnPauseForward01.ToolTip = "Pause";
            this.btnPauseForward01.Click += new System.EventHandler(this.btnPauseForward01_Click);
            // 
            // btnFrameBackward01
            // 
            this.btnFrameBackward01.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnFrameBackward01.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFrameBackward01.ImageOptions.Image")));
            this.btnFrameBackward01.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnFrameBackward01.Location = new System.Drawing.Point(278, 3);
            this.btnFrameBackward01.Margin = new System.Windows.Forms.Padding(2);
            this.btnFrameBackward01.Name = "btnFrameBackward01";
            this.btnFrameBackward01.Size = new System.Drawing.Size(24, 26);
            this.btnFrameBackward01.TabIndex = 34;
            this.btnFrameBackward01.ToolTip = "Backward";
            this.btnFrameBackward01.Click += new System.EventHandler(this.btnFrameBackward01_Click);
            // 
            // btnFrameForward01
            // 
            this.btnFrameForward01.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnFrameForward01.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFrameForward01.ImageOptions.Image")));
            this.btnFrameForward01.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnFrameForward01.Location = new System.Drawing.Point(548, 3);
            this.btnFrameForward01.Margin = new System.Windows.Forms.Padding(2);
            this.btnFrameForward01.Name = "btnFrameForward01";
            this.btnFrameForward01.Size = new System.Drawing.Size(24, 26);
            this.btnFrameForward01.TabIndex = 33;
            this.btnFrameForward01.ToolTip = "Forward";
            this.btnFrameForward01.Click += new System.EventHandler(this.btnFrameForward01_Click);
            // 
            // btnReset01
            // 
            this.btnReset01.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnReset01.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnReset01.ImageOptions.Image")));
            this.btnReset01.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnReset01.Location = new System.Drawing.Point(6, 3);
            this.btnReset01.Margin = new System.Windows.Forms.Padding(2);
            this.btnReset01.Name = "btnReset01";
            this.btnReset01.Size = new System.Drawing.Size(24, 26);
            this.btnReset01.TabIndex = 32;
            this.btnReset01.ToolTip = "Reset";
            this.btnReset01.Click += new System.EventHandler(this.btnReset01_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panel3.Controls.Add(this.lblTime01);
            this.panel3.Controls.Add(this.lblSpeed01);
            this.panel3.Location = new System.Drawing.Point(25, 6);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.MaximumSize = new System.Drawing.Size(800, 20);
            this.panel3.MinimumSize = new System.Drawing.Size(800, 20);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(800, 20);
            this.panel3.TabIndex = 30;
            // 
            // lblTime01
            // 
            this.lblTime01.AutoSize = true;
            this.lblTime01.ForeColor = System.Drawing.Color.White;
            this.lblTime01.Location = new System.Drawing.Point(4, 3);
            this.lblTime01.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTime01.Name = "lblTime01";
            this.lblTime01.Size = new System.Drawing.Size(0, 13);
            this.lblTime01.TabIndex = 31;
            // 
            // lblSpeed01
            // 
            this.lblSpeed01.AutoSize = true;
            this.lblSpeed01.ForeColor = System.Drawing.Color.White;
            this.lblSpeed01.Location = new System.Drawing.Point(772, 3);
            this.lblSpeed01.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSpeed01.Name = "lblSpeed01";
            this.lblSpeed01.Size = new System.Drawing.Size(0, 13);
            this.lblSpeed01.TabIndex = 32;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panel5.Controls.Add(this.lblSpeed02);
            this.panel5.Controls.Add(this.lblTime02);
            this.panel5.Location = new System.Drawing.Point(835, 6);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.MaximumSize = new System.Drawing.Size(800, 20);
            this.panel5.MinimumSize = new System.Drawing.Size(800, 20);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(800, 20);
            this.panel5.TabIndex = 31;
            // 
            // lblSpeed02
            // 
            this.lblSpeed02.AutoSize = true;
            this.lblSpeed02.ForeColor = System.Drawing.Color.White;
            this.lblSpeed02.Location = new System.Drawing.Point(772, 3);
            this.lblSpeed02.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSpeed02.Name = "lblSpeed02";
            this.lblSpeed02.Size = new System.Drawing.Size(0, 13);
            this.lblSpeed02.TabIndex = 32;
            // 
            // lblTime02
            // 
            this.lblTime02.AutoSize = true;
            this.lblTime02.ForeColor = System.Drawing.Color.White;
            this.lblTime02.Location = new System.Drawing.Point(2, 3);
            this.lblTime02.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTime02.Name = "lblTime02";
            this.lblTime02.Size = new System.Drawing.Size(0, 13);
            this.lblTime02.TabIndex = 32;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(835, 26);
            this.panel2.MaximumSize = new System.Drawing.Size(800, 600);
            this.panel2.MinimumSize = new System.Drawing.Size(800, 600);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(800, 600);
            this.panel2.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Location = new System.Drawing.Point(25, 26);
            this.panel1.MaximumSize = new System.Drawing.Size(800, 600);
            this.panel1.MinimumSize = new System.Drawing.Size(800, 600);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 600);
            this.panel1.TabIndex = 2;
            // 
            // tableLayoutMain
            // 
            this.tableLayoutMain.BackColor = System.Drawing.Color.White;
            this.tableLayoutMain.ColumnCount = 1;
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutMain.Controls.Add(this.tableLayoutPanelParent, 0, 3);
            this.tableLayoutMain.Controls.Add(this.panel4, 0, 5);
            this.tableLayoutMain.Controls.Add(this.topPanelCloseMenu, 0, 0);
            this.tableLayoutMain.Controls.Add(this.tableLayoutTopControlsMenu, 0, 1);
            this.tableLayoutMain.Controls.Add(this.panelTopStraignLine, 0, 2);
            this.tableLayoutMain.Controls.Add(this.panelBottomStraignLine, 0, 4);
            this.tableLayoutMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutMain.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutMain.Name = "tableLayoutMain";
            this.tableLayoutMain.RowCount = 6;
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 98F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 4F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 4F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutMain.Size = new System.Drawing.Size(1512, 1012);
            this.tableLayoutMain.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.btnBrowseFolder);
            this.panel4.Controls.Add(this.btnService);
            this.panel4.Controls.Add(this.lblOrion);
            this.panel4.Location = new System.Drawing.Point(2, 973);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1508, 36);
            this.panel4.TabIndex = 1;
            // 
            // btnBrowseFolder
            // 
            this.btnBrowseFolder.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnBrowseFolder.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnBrowseFolder.Appearance.Options.UseBackColor = true;
            this.btnBrowseFolder.Appearance.Options.UseForeColor = true;
            this.btnBrowseFolder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnBrowseFolder.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnBrowseFolder.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBrowseFolder.ImageOptions.Image")));
            this.btnBrowseFolder.Location = new System.Drawing.Point(10, 4);
            this.btnBrowseFolder.Margin = new System.Windows.Forms.Padding(2);
            this.btnBrowseFolder.Name = "btnBrowseFolder";
            this.btnBrowseFolder.Size = new System.Drawing.Size(123, 27);
            this.btnBrowseFolder.TabIndex = 232;
            this.btnBrowseFolder.Text = "Browse Folder";
            this.btnBrowseFolder.Click += new System.EventHandler(this.btnBrowseFolder_Click);
            // 
            // btnService
            // 
            this.btnService.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnService.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnService.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.btnService.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnService.Appearance.Options.UseBackColor = true;
            this.btnService.Appearance.Options.UseFont = true;
            this.btnService.Appearance.Options.UseForeColor = true;
            this.btnService.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnService.ImageOptions.ImageList = this.imageListIndications;
            this.btnService.Location = new System.Drawing.Point(1379, 4);
            this.btnService.Margin = new System.Windows.Forms.Padding(2);
            this.btnService.Name = "btnService";
            this.btnService.Size = new System.Drawing.Size(118, 28);
            this.btnService.TabIndex = 8;
            this.btnService.Text = "Orion Server";
            this.btnService.Click += new System.EventHandler(this.btnService_Click);
            // 
            // lblOrion
            // 
            this.lblOrion.Location = new System.Drawing.Point(12, 11);
            this.lblOrion.Margin = new System.Windows.Forms.Padding(2);
            this.lblOrion.Name = "lblOrion";
            this.lblOrion.Size = new System.Drawing.Size(0, 13);
            this.lblOrion.TabIndex = 0;
            // 
            // show_recording
            // 
            this.show_recording.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            this.show_recording.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.show_recording.Name = "show_recording";
            this.show_recording.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // backgroundWorkerVMSStatus
            // 
            this.backgroundWorkerVMSStatus.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerVMSStatus_DoWork);
            this.backgroundWorkerVMSStatus.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerVMSStatus_RunWorkerCompleted);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1512, 1012);
            this.Controls.Add(this.tableLayoutMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMain";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.topPanelCloseMenu.ResumeLayout(false);
            this.tableLayoutTopControlsMenu.ResumeLayout(false);
            this.panelMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.tableLayoutPanelParent.ResumeLayout(false);
            this.tableLayoutPanelParent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlLMenu)).EndInit();
            this.panelControlLMenu.ResumeLayout(false);
            this.panelControlLMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabCameraGroups)).EndInit();
            this.tabCameraGroups.ResumeLayout(false);
            this.tabInwardCamGrp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbInwardCam)).EndInit();
            this.tabOutwardCamGrp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbOutwardCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).EndInit();
            this.panelServerName.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).EndInit();
            this.panelDisplay.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar02.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEvent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEvent)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar01.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar01)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tableLayoutMain.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.show_recording)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimeChartRangeControlClient1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.ImageList imageListIndications;
        private System.Windows.Forms.Panel topPanelCloseMenu;
        private System.Windows.Forms.Button btnCloseApplication;
        private System.Windows.Forms.TableLayoutPanel tableLayoutTopControlsMenu;
        private System.Windows.Forms.Panel panelMenu;
        private DevExpress.XtraEditors.SimpleButton btnHelpTroubleshoot;
        private DevExpress.XtraEditors.SimpleButton btnUserManagement;
        private DevExpress.XtraEditors.SimpleButton btnConfiguration;
        private DevExpress.XtraEditors.SimpleButton btnAlarm;
        private DevExpress.XtraEditors.SimpleButton btnHMI;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.Panel panelTopStraignLine;
        private System.Windows.Forms.Panel panelBottomStraignLine;
        private System.ServiceProcess.ServiceController cisServiceController;
        private System.Windows.Forms.ImageList imageListButton;
        private System.ComponentModel.BackgroundWorker backgroundWorkerServiceStatus;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelParent;
        private DevExpress.XtraEditors.PanelControl panelControlLMenu;
        private System.Windows.Forms.Panel panelDisplay;
        private System.Windows.Forms.TableLayoutPanel tableLayoutMain;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.SimpleButton btnService;
        private DevExpress.XtraEditors.LabelControl lblOrion;
        private System.Windows.Forms.Panel panel6;
        private DevExpress.XtraEditors.SimpleButton btnPlayForward01;
        private DevExpress.XtraEditors.SimpleButton btnPauseForward01;
        private DevExpress.XtraEditors.SimpleButton btnFrameBackward01;
        private DevExpress.XtraEditors.SimpleButton btnFrameForward01;
        private DevExpress.XtraEditors.SimpleButton btnReset01;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblTime01;
        private System.Windows.Forms.Label lblSpeed01;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblSpeed02;
        private System.Windows.Forms.Label lblTime02;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit show_recording;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.GridControl gridEvent;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEvent;
        private DevExpress.XtraGrid.Columns.GridColumn colSrNo;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeStamp;
        private DevExpress.XtraGrid.Columns.GridColumn colVIN;
        private DevExpress.XtraGrid.Columns.GridColumn colMarker;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private System.Windows.Forms.MaskedTextBox txtSearch;
        private System.Windows.Forms.Panel panelServerName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lbVIN;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel8;
        private DevExpress.XtraEditors.DateEdit dateEditFrom;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
        private System.Windows.Forms.ListBox lbTimeOut;
        private System.Windows.Forms.ListBox lbTimeIN;
        private DevExpress.XtraEditors.SimpleButton btnShowOutwardCam;
        private DevExpress.XtraEditors.SimpleButton btnShowInwardCam;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.SimpleButton btnExport1;
        private DevExpress.XtraEditors.SimpleButton btnBrowseFolder;
        private DevExpress.XtraEditors.DateEdit dateEditTo;
        private DevExpress.XtraEditors.SimpleButton btnGetVisits;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblMessageDateSearch;
        private DevExpress.XtraBars.Navigation.TabPane tabCameraGroups;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabInwardCamGrp;
        private DevExpress.XtraEditors.SimpleButton btnCloseInwardSeletion;
        private DevExpress.XtraEditors.SimpleButton btnSelectInwardCam;
        private DevExpress.XtraEditors.ListBoxControl lbInwardCam;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabOutwardCamGrp;
        private DevExpress.XtraEditors.ListBoxControl lbOutwardCam;
        private DevExpress.XtraEditors.ZoomTrackBarControl trackBar01;
        private DevExpress.XtraEditors.SimpleButton btnPlayBackward01;
        private DevExpress.XtraEditors.SimpleButton btnPauseBackward01;
        private System.Windows.Forms.Panel panel7;
        private DevExpress.XtraEditors.SimpleButton btnPlayBackward02;
        private DevExpress.XtraEditors.SimpleButton btnPauseBackward02;
        private DevExpress.XtraEditors.ZoomTrackBarControl trackBar02;
        private DevExpress.XtraEditors.SimpleButton btnExport2;
        private DevExpress.XtraEditors.SimpleButton btnPlayForward02;
        private DevExpress.XtraEditors.SimpleButton btnPauseForward02;
        private DevExpress.XtraEditors.SimpleButton btnFrameBackward02;
        private DevExpress.XtraEditors.SimpleButton btnFrameForward02;
        private DevExpress.XtraEditors.SimpleButton btnReset02;
        private DevExpress.XtraEditors.SimpleButton btnCloseOutwardSeletion;
        private DevExpress.XtraEditors.SimpleButton btnSelectOutwardCam;
        private DevExpress.XtraEditors.DateTimeChartRangeControlClient dateTimeChartRangeControlClient1;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraEditors.SimpleButton btnReconnect;
        private System.ComponentModel.BackgroundWorker backgroundWorkerVMSStatus;
    }
}