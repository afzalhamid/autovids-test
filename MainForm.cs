﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Windows.Forms;
using System.Linq;
using VideoOS.Platform;
using VideoOS.Platform.Client;
using VideoOS.Platform.Messaging;
using VideoOS.Platform.UI;
using System.Collections;
using DataAccessLayer;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace VideoViewer
{
	public partial class MainForm : Form
	{
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private bool chkServiceStatus { get; set; }
        Regex rgx = new Regex("[^a-zA-Z0-9 -]");

        #region private fields

        DAL DAL = new DAL();
		private Item _selectItem1;
		private ImageViewerControl _imageViewerControl1;
		private Item _selectItem2;
		private ImageViewerControl _imageViewerControl2;
		private double _speed1 = 0.0;
		private double _speed2 = 0.0;

		private FQID _playbackControllerFQID;

        private List<string> Group01;
        private List<string> Group02;

        private string camera01 { get { return comboBoxCamera.SelectedIndex == 0 ? Group01[0] : Group01[1]; } }
        private string camera02 { get { return comboBoxCamera.SelectedIndex == 0 ? Group02[0] : Group02[1]; } }
        private string ParentFolder { get { return System.Configuration.ConfigurationManager.AppSettings["FolderPath"]; } }

        private bool IsAdvanceEdt { get { return System.Configuration.ConfigurationManager.AppSettings["AdvanceEdt"] == "1" ? true : false; } }

        private bool DebuggingEnabled { get { return System.Configuration.ConfigurationManager.AppSettings["DebugMode"] == "1" ? true : false; } }
        private int PreRecPlayback { get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PreRecPlayback"]); } }

        public DateTime recStartTime01 { get; set; }
        public DateTime recStartTime02 { get; set; }

        #endregion

        #region construction and close

        public MainForm()
		{
			InitializeComponent();

			EnvironmentManager.Instance.RegisterReceiver(PlaybackTimeChangedHandler,
			                                             new MessageIdFilter(MessageId.SmartClient.PlaybackCurrentTimeIndication));
			//EnvironmentManager.Instance.RegisterReceiver(PlaybackIndicationHandler,
			//											 new MessageIdFilter(MessageId.SmartClient.PlaybackIndication));
            		
		}

		private void OnClose(object sender, EventArgs e)
		{
			Close();
		}
		#endregion

		#region Time changed event handler

		private object PlaybackTimeChangedHandler(VideoOS.Platform.Messaging.Message message, FQID dest, FQID sender)
		{
			DateTime time = (DateTime)message.Data;
            int utcDiff = DateTime.Now.Subtract(DateTime.UtcNow).Hours;
            time = time.AddHours(utcDiff);

            // The built-in PlaybackController has FQID==null
            if (sender == null)
            {
                lblTime01.Text = time.ToShortDateString() + "  " + time.ToLongTimeString();
                if (_imageViewerControl1 != null)
                    _imageViewerControl1.Invalidate();
            }

            // The PlaybackController on the right hand side has a specific FQID
            if (sender != null && _playbackControllerFQID != null && sender.ObjectId == _playbackControllerFQID.ObjectId)
            {
                lblTime02.Text = time.ToShortDateString() + "  " + time.ToLongTimeString();
                if (_imageViewerControl2 != null)
                    _imageViewerControl2.Invalidate();
            }
                return null;
        }

		//private object PlaybackIndicationHandler(VideoOS.Platform.Messaging.Message message, FQID dest, FQID sender)
		//{
		//	if ((sender==null && _playbackControllerFQID==null) ||  _playbackControllerFQID.EqualGuids(sender))		// If message from right hand side control
		//	{
		//		if (message.Data is PlaybackCommandData)
		//		{
		//			PlaybackCommandData data = (PlaybackCommandData) message.Data;
		//			if (data.Command == PlaybackData.PlayStop)
		//			{
		//				_speed2 = 0;
		//			}
		//		}
		//	}
		//	return null;
		//}

		#endregion

		#region Playback Click handling - Left hand camera
		private void SelectCamera01(string cameraName)
        {
            try
            {
                _speed1 = 0.0;

                if (_imageViewerControl1 != null)
                {
                    _imageViewerControl1.Disconnect();
                }

                List<Item> camCollection = null;

                //List<Item> camCollection = EnvironmentManager.Instance.MasterSite.ServerId.UserContext.Configuration.GetItems()[0].GetChildren()[4].GetChildren()[0].GetChildren();
                if (IsAdvanceEdt)
                {
                    string[] vals = cameraName.Split('#');
                    string[] camGrps = vals[1].Split('|');
                    cameraName = vals[0].Trim();

                    camCollection = EnvironmentManager.Instance.MasterSite.ServerId.UserContext.Configuration.GetItems()[0].GetChildren()[0].GetChildren();

                    int idx = 0;

                    foreach (string grp in camGrps)
                    {
                        if (idx == 0)
                        {
                            camCollection = camCollection.Where(x => x.Name.Trim() == grp.Trim()).ToList();
                            idx = 1;
                        }
                        else
                        {
                            camCollection = camCollection[0].GetChildren().Where(x => x.Name.Trim() == grp.Trim()).ToList();
                        }
                    }

                    camCollection = camCollection[0].GetChildren();

                }
                else
                {
                    camCollection = EnvironmentManager.Instance.MasterSite.ServerId.UserContext.Configuration.GetItems()[0].GetChildren()[4].GetChildren()[0].GetChildren();
                }

                List<FQID> fqids = new List<FQID>();
                foreach (Item cam in camCollection)
                {
                    if (cam.Name == cameraName)
                        _selectItem1 = cam;
                }

                using (_imageViewerControl1 = ClientControl.Instance.GenerateImageViewerControl())
                {
                    _imageViewerControl1 = ClientControl.Instance.GenerateImageViewerControl();
                    _imageViewerControl1.Dock = DockStyle.Fill;
                    _imageViewerControl1.ClickEvent += new EventHandler(ImageViewerControl1Click);
                    panel1.Controls.Clear();
                    panel1.Controls.Add(_imageViewerControl1);
                    _imageViewerControl1.CameraFQID = _selectItem1.FQID;
                    _imageViewerControl1.Initialize();
                    _imageViewerControl1.Connect();
                    _imageViewerControl1.Selected = true;
                    _imageViewerControl1.EnableMouseControlledPtz = true;
                    _imageViewerControl1.EnableMousePtzEmbeddedHandler = true;
                    _imageViewerControl1.EnableDigitalZoom = true;
                    _imageViewerControl1.BackColor = System.Drawing.Color.Black;

                    EnvironmentManager.Instance.Mode = Mode.ClientPlayback;

                    EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                           MessageId.SmartClient.PlaybackCommand,
                                                           new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = recStartTime01, Speed = _speed1 }));

                    //EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                    //                                        MessageId.SmartClient.PlaybackCommand,
                    //                                        new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed1 }));

                    lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);

                    //btnPlay01.Visible = false;
                    //btnPause01.Visible = true;
                    //btnForward01.Enabled = true;
                    //btnBackward01.Enabled = true;
                }
            }

            catch (Exception ex)
            {
                if (DebuggingEnabled)
                    MessageBox.Show(ex.ToString());
            }

            }

		void ImageViewerControl1Click(object sender, EventArgs e)
		{
            if (_imageViewerControl2 != null)
                _imageViewerControl2.Selected = false;
            _imageViewerControl1.Selected = true;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayStop }), _playbackControllerFQID
                                                       );

            btnPlay01.Visible = true;
            btnPlay01.Enabled = true;
            btnPause01.Visible = false;
            btnForward01.Enabled = false;
            btnBackward01.Enabled = false;
            btnReset01.Enabled = false;
            btnNxtFrame1.Enabled = false;
            btnPrvFrame1.Enabled = false;

            btnPlay02.Visible = true;
            btnPlay02.Enabled = false;
            btnPause02.Visible = false;
            btnForward02.Enabled = false;
            btnBackward02.Enabled = false;
            btnReset02.Enabled = false;
            btnNxtFrame2.Enabled = false;
            btnPrvFrame2.Enabled = false;
        }


		private void OnStop1Click(object sender, EventArgs e)
		{

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayStop }));
            btnForward01.Enabled = false;
            btnBackward01.Enabled = false;
            btnPlay01.Visible = true;
            btnPause01.Visible = false;
            btnPrvFrame1.Enabled = true;
            btnNxtFrame1.Enabled = true;

            _speed1 = 0.0;
            lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);
        }

		private void OnReverse1Click(object sender, EventArgs e)
		{
			if (_speed1 == 0.0)
				_speed1 = 1.0;
			else
				_speed1 *= 2;
			EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
														MessageId.SmartClient.PlaybackCommand, 
														new PlaybackCommandData() { Command=PlaybackData.PlayReverse, Speed=_speed1}));

            lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);
        }

		private void OnForward1Click(object sender, EventArgs e)
		{
            if (_speed1 <= 32)
            {
                if (_speed1 == 0.0)
                    _speed1 = 1.0;
                else
                    _speed1 *= 2;

                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                            MessageId.SmartClient.PlaybackCommand,
                                                            new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed1 }));

                lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);
            }
        }

		#endregion

		#region Playback Click handling - Right hand camera

        private void SelectCamera02(string cameraName)
		{
            try
            {
                _speed2 = 0.0;

                if (_imageViewerControl2 != null)
                {
                    _imageViewerControl2.Disconnect();
                }

                List<Item> camCollection = null;

                //List<Item> camCollection = EnvironmentManager.Instance.MasterSite.ServerId.UserContext.Configuration.GetItems()[0].GetChildren()[4].GetChildren()[0].GetChildren();
                if (IsAdvanceEdt)
                {
                    string[] vals = cameraName.Split('#');
                    string[] camGrps = vals[1].Split('|');
                    cameraName = vals[0].Trim();

                    camCollection = EnvironmentManager.Instance.MasterSite.ServerId.UserContext.Configuration.GetItems()[0].GetChildren()[0].GetChildren();

                    int idx = 0;

                    foreach (string grp in camGrps)
                    {
                        if (idx == 0)
                        {
                            camCollection = camCollection.Where(x => x.Name.Trim() == grp.Trim()).ToList();
                            idx = 1;
                        }
                        else
                        {
                            camCollection = camCollection[0].GetChildren().Where(x => x.Name.Trim() == grp.Trim()).ToList();
                        }
                    }

                    camCollection = camCollection[0].GetChildren();

                }
                else
                {
                    camCollection = EnvironmentManager.Instance.MasterSite.ServerId.UserContext.Configuration.GetItems()[0].GetChildren()[4].GetChildren()[0].GetChildren();
                }

                List<FQID> fqids = new List<FQID>();
                foreach (Item cam in camCollection)
                {
                    if (cam.Name == cameraName)
                        _selectItem2 = cam;
                }

                if (_playbackControllerFQID == null)
                    _playbackControllerFQID = ClientControl.Instance.GeneratePlaybackController();

                using (_imageViewerControl2 = ClientControl.Instance.GenerateImageViewerControl())
                {

                    _imageViewerControl2 = ClientControl.Instance.GenerateImageViewerControl();
                    _imageViewerControl2.PlaybackControllerFQID = _playbackControllerFQID;
                    _imageViewerControl2.Dock = DockStyle.Fill;
                    _imageViewerControl2.ClickEvent += new EventHandler(ImageViewerControl2Click);
                    panel2.Controls.Clear();
                    panel2.Controls.Add(_imageViewerControl2);
                    _imageViewerControl2.CameraFQID = _selectItem2.FQID;
                    _imageViewerControl2.Initialize();
                    _imageViewerControl2.Connect();
                    _imageViewerControl2.Selected = true;
                    _imageViewerControl2.EnableMouseControlledPtz = true;
                    _imageViewerControl2.EnableMousePtzEmbeddedHandler = true;
                    _imageViewerControl2.EnableDigitalZoom = true;
                    _imageViewerControl2.BackColor = System.Drawing.Color.Black;
                                        
                    EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                           MessageId.SmartClient.PlaybackCommand,
                                                           new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = recStartTime02, Speed = _speed2 }), _playbackControllerFQID);

                    //EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                    //                                        MessageId.SmartClient.PlaybackCommand,
                    //                                        new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed2 }), _playbackControllerFQID);

                    lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);

                    //btnPlay02.Visible = false;
                    //btnPause02.Visible = true;
                    //btnForward02.Enabled = true;
                    //btnBackward02.Enabled = true;

                    
                }
            }
            catch (Exception ex)
            {
                if (DebuggingEnabled)
                    MessageBox.Show(ex.ToString());
            }

        }

		void ImageViewerControl2Click(object sender, EventArgs e)
		{
            if (_imageViewerControl1 != null)
                _imageViewerControl1.Selected = false;
            _imageViewerControl2.Selected = true;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayStop })
                                                       );

            btnPlay02.Visible = true;
            btnPlay02.Enabled = true;
            btnPause02.Visible = false;
            btnForward02.Enabled = false;
            btnBackward02.Enabled = false;
            btnReset02.Enabled = false;
            btnNxtFrame2.Enabled = false;
            btnPrvFrame2.Enabled = false;

            btnPlay01.Visible = true;
            btnPlay01.Enabled = false;
            btnPause01.Visible = false;
            btnForward01.Enabled = false;
            btnBackward01.Enabled = false;
            btnReset01.Enabled = false;
            btnNxtFrame1.Enabled = false;
            btnPrvFrame1.Enabled = false;
        }


		private void OnStop2Click(object sender, EventArgs e)
		{
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayStop }), _playbackControllerFQID);

            _speed2 = 0.0;
            btnForward02.Enabled = false;
            btnBackward02.Enabled = false;
            btnPlay02.Visible = true;
            btnPause02.Visible = false;
            btnPrvFrame2.Enabled = true;
            btnNxtFrame2.Enabled = true;
            lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);
        }

		private void OnReverse2Click(object sender, EventArgs e)
		{
            if (_speed2 <= 32)
            {
                if (_speed2 == 0.0)
                    _speed2 = 1.0;
                else
                    _speed2 *= 2;

                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                            MessageId.SmartClient.PlaybackCommand,
                                                            new PlaybackCommandData() { Command = PlaybackData.PlayReverse, Speed = _speed2 }),
                                                            _playbackControllerFQID);
                lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);
            }
        }

		private void OnForward2Click(object sender, EventArgs e)
		{
            if (_speed2 <= 32)
            {
                if (_speed2 == 0.0)
                    _speed2 = 1.0;
                else
                    _speed2 *= 2;

                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                            MessageId.SmartClient.PlaybackCommand,
                                                            new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed2 }),
                                                            _playbackControllerFQID);
                lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);
            }
        }

		#endregion

        private void MainForm_Load(object sender, EventArgs e)
        {
            string status = System.Configuration.ConfigurationManager.AppSettings["ServiceStatus"];
            chkServiceStatus = status == "1" ? true : false;

            btnService.Visible = chkServiceStatus;

            Group01 = new List<string>();
            Group02 = new List<string>();
                        
            Group01.AddRange(System.Configuration.ConfigurationManager.AppSettings["Group01"].Split(','));
            Group02.AddRange(System.Configuration.ConfigurationManager.AppSettings["Group02"].Split(','));
              
            try
            {
                if (!Directory.Exists(ParentFolder))
                    Directory.CreateDirectory(ParentFolder);
            }
            catch { }

            comboBoxCamera.SelectedIndex = 0;

            listBoxIN.Enabled = false;
            listBoxOUT.Enabled = false;

            lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);
            lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);

            SetButtonDefaultStatus();

            timer.Tick += Timer_Tick;
            timer.Interval = 1000 * 30 * 1;
            timer.Start();

        }

        void SetButtonDefaultStatus()
        {
            btnPlay01.Visible = true;
            btnPlay01.Enabled = false;
            btnPause01.Visible = false;
            btnForward01.Enabled = false;
            btnBackward01.Enabled = false;
            btnReset01.Enabled = false;
            btnPrvFrame1.Enabled = false;
            btnNxtFrame1.Enabled = false;

            btnPlay02.Visible = true;
            btnPlay02.Enabled = false;
            btnPause02.Visible = false;
            btnForward02.Enabled = false;
            btnBackward02.Enabled = false;
            btnReset02.Enabled = false;
            btnPrvFrame2.Enabled = false;
            btnNxtFrame2.Enabled = false;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (chkServiceStatus)
                {
                    vidsServiceController.Refresh();
                    if (vidsServiceController.Status == System.ServiceProcess.ServiceControllerStatus.Running)
                        btnService.ImageIndex = 0;
                    else if (vidsServiceController.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                        btnService.ImageIndex = 2;
                    else
                        btnService.ImageIndex = 1;
                }
            }
            catch { }
        }
        private void listBoxIN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_imageViewerControl1 != null)
            {
                _imageViewerControl1.Disconnect();
            }

            if (_imageViewerControl2 != null)
            {
                _imageViewerControl2.Disconnect();
            }
        }

        private void listBoxOUT_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_imageViewerControl1 != null)
            {
                _imageViewerControl1.Disconnect();
            }

            if (_imageViewerControl2 != null)
            {
                _imageViewerControl2.Disconnect();
            }
        }

        void ConnectCameras()
        {
            if (listBoxIN.SelectedItems.Count > 0 && listBoxIN.SelectedValue.ToString() != "System.Data.DataRowView")
            {
                frmLoading frmLoad = new frmLoading();
                frmLoad.StartPosition = FormStartPosition.CenterScreen;
                frmLoad.Show();
                Application.DoEvents();

                Thread.Sleep(1000);

                recStartTime01 = Convert.ToDateTime(listBoxIN.SelectedValue).AddSeconds(-PreRecPlayback);
                _speed1 = 0.0;
                SelectCamera01(camera01.Trim());

                frmLoad.Hide();
            }

            if (listBoxOUT.SelectedItems.Count > 0 && listBoxOUT.SelectedValue.ToString() != "System.Data.DataRowView")
            {
                frmLoading frmLoad = new frmLoading();
                frmLoad.StartPosition = FormStartPosition.CenterScreen;
                frmLoad.Show();
                Application.DoEvents();

                Thread.Sleep(1000);

                recStartTime02 = Convert.ToDateTime(listBoxOUT.SelectedValue).AddSeconds(-PreRecPlayback); 
                _speed2 = 0.0;
                SelectCamera02(camera02.Trim());

                frmLoad.Hide();
            }
        }

        private void comboBoxCamera_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBoxIN.DataSource = null;
            listBoxOUT.DataSource = null;

            if (_imageViewerControl1 != null)
            {
                _imageViewerControl1.Disconnect();
            }

            if (_imageViewerControl2 != null)
            {
                _imageViewerControl2.Disconnect();
            }
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            string searchKeyword = textBoxSearch.Text.Trim();

            try
            {
                
                lbVehicles.DataSource = null;
                listBoxIN.DataSource = null;
                listBoxOUT.DataSource = null;

                if (_imageViewerControl1 != null)
                {
                    _imageViewerControl1.Disconnect();
                    _imageViewerControl1 = null;
                    panel1.Controls.Clear();
                }

                if (_imageViewerControl2 != null)
                {
                    _imageViewerControl2.Disconnect();
                    _imageViewerControl2 = null;
                    panel2.Controls.Clear();
                }

                SetButtonDefaultStatus();

            }
            catch { }

            if (searchKeyword.Length > 0)
            {
                DataSet dsVehicle = DAL.SearchVehicle(searchKeyword);
                if (dsVehicle.Tables[0].Rows.Count > 0)
                {
                    lbVehicles.DataSource = dsVehicle.Tables[0];
                    lbVehicles.ValueMember = "ID";
                    lbVehicles.DisplayMember = "RegistrationNumber";
                    buttonHistory.Enabled = true;
                }
                else
                {
                    buttonHistory.Enabled = false;
                    MessageBox.Show("No record found.", "Information!");
                }
            }
        }

        private void buttonHistory_Click(object sender, EventArgs e)
        {
            try
            {
                SetButtonDefaultStatus();

                listBoxIN.DataSource = null;
                listBoxOUT.DataSource = null;

                if (_imageViewerControl1 != null)
                {
                    _imageViewerControl1.Disconnect();
                    _imageViewerControl1 = null;
                    panel1.Controls.Clear();
                }

                if (_imageViewerControl2 != null)
                {
                    _imageViewerControl2.Disconnect();
                    _imageViewerControl2 = null;
                    panel2.Controls.Clear();
                }

                DataTable dtEvents = DAL.GetEventsByVehicleID(lbVehicles.SelectedValue.ToString(), DateTime.Now, DateTime.Now);
                if (dtEvents.Rows.Count > 0)
                {
                    var inEvents = from evn in dtEvents.AsEnumerable() where evn.Field<string>("Gate") == "IN" select evn;
                    var outEvents = from evn in dtEvents.AsEnumerable() where evn.Field<string>("Gate") == "OUT" select evn;

                    if (inEvents.Any())
                    {
                        listBoxIN.DataSource = inEvents.OrderByDescending(x => x.Field<DateTime>("Timestamp")).CopyToDataTable();
                        listBoxIN.DisplayMember = "Event";
                        listBoxIN.ValueMember = "Timestamp";
                        listBoxIN.ClearSelected();
                        listBoxIN.Enabled = true;
                    }
                    else
                    {
                        listBoxIN.DataSource = null;
                        listBoxIN.Enabled = false;
                    }

                    if (outEvents.Any())
                    {
                        listBoxOUT.DataSource = outEvents.OrderByDescending(x => x.Field<DateTime>("Timestamp")).CopyToDataTable();
                        listBoxOUT.DisplayMember = "Event";
                        listBoxOUT.ValueMember = "Timestamp";
                        listBoxOUT.ClearSelected();
                        listBoxOUT.Enabled = true;
                    }
                    else
                    {
                        listBoxOUT.DataSource = null;
                        listBoxOUT.Enabled = false;
                    }
                }
                else
                {
                    MessageBox.Show("No record found.", "Information!");
                }
            }
            catch (Exception ex)
            {
                if (DebuggingEnabled)
                    MessageBox.Show(ex.ToString());
            }
        }

        void LoadCameras()
        {
            if (listBoxIN.SelectedItems.Count > 0)
                SelectCamera01(camera01.Trim());
            if (listBoxOUT.SelectedItems.Count > 0)
                SelectCamera02(camera02.Trim());
        }

        private void buttonFrameForward2_Click(object sender, EventArgs e)
        {
            if (_speed2 == 0.0)
                _speed2 = 1.0;
            else if (_speed2 < 32)
                _speed2 *= 2;

            if (_speed2 <= 32)
            {
                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                            MessageId.SmartClient.PlaybackCommand,
                                                            new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed2 }),_playbackControllerFQID);
                lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);
            }
        }
       
        private void buttonFrameReverse2_Click(object sender, EventArgs e)
        {
            if (_speed2 == 0.0)
                _speed2 = 1.0;
            else if (_speed2 < 32)
                _speed2 *= 2;

            if (_speed2 <= 32)
            {
                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                            MessageId.SmartClient.PlaybackCommand,
                                                            new PlaybackCommandData() { Command = PlaybackData.PlayReverse, Speed = _speed2 }), _playbackControllerFQID);
                lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);
            }
        }

        private void buttonFrameForward_Click(object sender, EventArgs e)
        {
            if (_speed1 <= 32)
            {
                if (_speed1 == 0.0)
                    _speed1 = 1.0;
                else if (_speed1 < 32)
                    _speed1 *= 2;

                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                            MessageId.SmartClient.PlaybackCommand,
                                                            new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed1 }));
                lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);
            }
        }

        private void buttonFrameReverse_Click(object sender, EventArgs e)
        {            

            if (_speed1 <= 32)
            {
                if (_speed1 == 0.0)
                    _speed1 = 1.0;
                else if (_speed1 < 32)
                    _speed1 *= 2;

                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                            MessageId.SmartClient.PlaybackCommand,
                                                            new PlaybackCommandData() { Command = PlaybackData.PlayReverse, Speed = _speed1 }));
                lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);
            }
        }

        private void buttonPlay2_Click(object sender, EventArgs e)
        {
            _speed2 = 0.0;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed2 }),_playbackControllerFQID);

            btnForward02.Enabled = true;
            btnBackward02.Enabled = true;
            btnPlay02.Visible = false;
            btnPause02.Visible = true;
            btnReset02.Enabled = true;
            btnNxtFrame2.Enabled = false;
            btnPrvFrame2.Enabled = false;
        }

        private void buttonPaly_Click(object sender, EventArgs e)
        {
            _speed1 = 0.0;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                       MessageId.SmartClient.PlaybackCommand,
                                                       new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed1 }));
            btnForward01.Enabled = true;
            btnBackward01.Enabled = true;
            btnPlay01.Visible = false;
            btnPause01.Visible = true;
            btnReset01.Enabled = true;
            btnPrvFrame1.Enabled = false;
            btnNxtFrame1.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string folder = String.Format("{0}\\{1}", ParentFolder, lbVehicles.Text.Trim());
                if (!System.IO.Directory.Exists(folder))
                    System.IO.Directory.CreateDirectory(folder);

                string cameraName = string.Empty;
                if (IsAdvanceEdt)
                {
                    string[] vals = camera01.Split('#');
                    string[] camGrps = vals[1].Split('|');
                    cameraName = vals[0].Trim();

                }
                else
                {
                    cameraName = camera01;
                }


                if (_imageViewerControl1 != null)
                {
                    try
                    {
                        if (!Directory.Exists(folder))
                            Directory.CreateDirectory(folder);
                    }
                    catch { }

                    folder = String.Format("{0}\\{1} {2}.jpg", folder,rgx.Replace(cameraName,"-"), Convert.ToDateTime(lblTime01.Text).ToString("MM-dd-yy Hmmss"));

                    Bitmap bitmap = _imageViewerControl1.GetCurrentDisplayedImageAsBitmap(false);
                    EnlargeImage form = new EnlargeImage();
                    form.SetBitmap(bitmap, folder);
                    form.ShowDialog();
                }
            }
            catch(Exception ex) {
                if (DebuggingEnabled)
                    MessageBox.Show(ex.ToString());
            }
        }

        private void buttonExplore_Click(object sender, EventArgs e)
        {
            try
            {
                string dir = string.Format("{0}\\{1}", ParentFolder, lbVehicles.Text.Trim());
                if (!System.IO.Directory.Exists(dir))
                    System.IO.Directory.CreateDirectory(dir);
                    Process.Start(string.Format("{0}\\{1}", ParentFolder, lbVehicles.Text.Trim()));
            }
            catch
            {
                Process.Start(ParentFolder);
            }
        }

        private void buttonExport2_Click(object sender, EventArgs e)
        {
            try
            {
                                

                string folder = String.Format("{0}\\{1}", ParentFolder, lbVehicles.Text.Trim());
                if (!System.IO.Directory.Exists(folder))
                    System.IO.Directory.CreateDirectory(folder);

                string cameraName = string.Empty;
                if (IsAdvanceEdt)
                {
                    string[] vals = camera02.Split('#');
                    string[] camGrps = vals[1].Split('|');
                    cameraName = vals[0].Trim();

                }
                else
                {
                    cameraName = camera02;
                }

                if (_imageViewerControl2 != null)
                {
                    try
                    {
                        if (!Directory.Exists(folder))
                            Directory.CreateDirectory(folder);
                    }
                    catch { }
                    
                    folder = String.Format("{0}\\{1} {2}.jpg", folder, rgx.Replace(cameraName, "-"), Convert.ToDateTime(lblTime02.Text).ToString("MM-dd-yy Hmmss"));

                    Bitmap bitmap = _imageViewerControl2.GetCurrentDisplayedImageAsBitmap(false);
                    EnlargeImage form = new EnlargeImage();
                    form.SetBitmap(bitmap, folder);
                    form.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (DebuggingEnabled)
                    MessageBox.Show(ex.ToString());
            }
        }

        private void btnReset01_Click(object sender, EventArgs e)
        {
            _speed1 = 0.0;


            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                        MessageId.SmartClient.PlaybackCommand,
                                                        new PlaybackCommandData() { Command = PlaybackData.PlayStop }));

            EnvironmentManager.Instance.Mode = Mode.ClientPlayback;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                   MessageId.SmartClient.PlaybackCommand,
                                                   new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = recStartTime01, Speed = _speed1 }));

            lblSpeed01.Text = String.Format("{0} x", _speed1 == 0.0 ? 1 : _speed1);
        }

        private void btnReset02_Click(object sender, EventArgs e)
        {
            _speed2 = 0.0;


            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                        MessageId.SmartClient.PlaybackCommand,
                                                        new PlaybackCommandData() { Command = PlaybackData.PlayStop }), _playbackControllerFQID);
                       

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                   MessageId.SmartClient.PlaybackCommand,
                                                   new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = recStartTime02, Speed = _speed2 }), _playbackControllerFQID);

            lblSpeed02.Text = String.Format("{0} x", _speed2 == 0.0 ? 1 : _speed2);
        }

        private void btnService_Click(object sender, EventArgs e)
        {
            try
            {
                vidsServiceController.Refresh();

                if (vidsServiceController.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                {
                    vidsServiceController.Start();
                    vidsServiceController.WaitForStatus(System.ServiceProcess.ServiceControllerStatus.Running);
                    btnService.ImageIndex = 0;
                }
                else if (vidsServiceController.Status == System.ServiceProcess.ServiceControllerStatus.Paused)
                    vidsServiceController.Continue();

            }
            catch (Exception ex)
            {
                if (DebuggingEnabled)
                    MessageBox.Show(ex.ToString());
            }
        }

        private void lbVehicles_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            SetButtonDefaultStatus();
            
            if (_imageViewerControl1 != null)
            {
                _imageViewerControl1.Disconnect();
                _imageViewerControl1 = null;
                panel1.Controls.Clear();
            }

            if (_imageViewerControl2 != null)
            {
                _imageViewerControl2.Disconnect();
                _imageViewerControl2 = null;
                panel2.Controls.Clear();
            }

            ConnectCameras();
        }

        private void btnPrvFrame1_Click(object sender, EventArgs e)
        {
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                      MessageId.SmartClient.PlaybackCommand,
                                                      new PlaybackCommandData() { Command = PlaybackData.Previous }));
        }

        private void btnNxtFrame1_Click(object sender, EventArgs e)
        {
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                      MessageId.SmartClient.PlaybackCommand,
                                                      new PlaybackCommandData() { Command = PlaybackData.Next }));
        }

        private void btnPrvFrame2_Click(object sender, EventArgs e)
        {
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                      MessageId.SmartClient.PlaybackCommand,
                                                      new PlaybackCommandData() { Command = PlaybackData.Previous }),
                                                      _playbackControllerFQID);
        }

        private void btnNxtFrame2_Click(object sender, EventArgs e)
        {
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                      MessageId.SmartClient.PlaybackCommand,
                                                      new PlaybackCommandData() { Command = PlaybackData.Next }),
                                                      _playbackControllerFQID);
        }
    }
}
