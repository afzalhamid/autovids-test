﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoViewer
{
    public partial class frmHistory : Form
    {
        DAL dataAccess = new DAL();
        private string ParentFolder { get { return System.Configuration.ConfigurationManager.AppSettings["FolderPath"]; } }
        public int vehicleID { get; set; }
        public string VIN { get; set; }
        public frmHistory()
        {
            InitializeComponent();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                string folder = String.Format("{0}\\Excel", ParentFolder);
                if (!System.IO.Directory.Exists(folder))
                    System.IO.Directory.CreateDirectory(folder);

                gridEvent.ExportToXlsx(string.Format("{0}\\Log_{0}.xlsx", folder, VIN));

            }
            catch
            {
                MessageBox.Show("Export Failed");
            }
        }

        private void frmHistory_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable dtHistory = dataAccess.GetHistoryByVehicleID(vehicleID);
                if (dtHistory.Rows.Count > 0)
                    VIN = dtHistory.Rows[0]["Vin"].ToString();

                gridEvent.DataSource = dataAccess.GetHistoryByVehicleID(vehicleID);
            }
            catch
            {

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
