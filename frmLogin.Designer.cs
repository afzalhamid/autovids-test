﻿namespace VideoViewer
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelControlsMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelControls = new System.Windows.Forms.TableLayoutPanel();
            this.panelLogo = new System.Windows.Forms.Panel();
            this.panelControls = new System.Windows.Forms.Panel();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.checkBoxAutoLogin = new System.Windows.Forms.CheckBox();
            this.panelPassword = new System.Windows.Forms.Panel();
            this.tableLayoutPanelPassword = new System.Windows.Forms.TableLayoutPanel();
            this.panelPwdIcon = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.panelUsername = new System.Windows.Forms.Panel();
            this.tableLayoutPanelUsername = new System.Windows.Forms.TableLayoutPanel();
            this.panelUserIcon = new System.Windows.Forms.Panel();
            this.panelUserTextBox = new System.Windows.Forms.Panel();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.btnReset = new DevExpress.XtraEditors.SimpleButton();
            this.btnLogin = new DevExpress.XtraEditors.SimpleButton();
            this.lblTitle = new System.Windows.Forms.Label();
            this.panelResetControls = new System.Windows.Forms.Panel();
            this.lblMessage = new System.Windows.Forms.Label();
            this.tableLayoutPanelMain.SuspendLayout();
            this.tableLayoutPanelControlsMain.SuspendLayout();
            this.tableLayoutPanelControls.SuspendLayout();
            this.panelControls.SuspendLayout();
            this.panelPassword.SuspendLayout();
            this.tableLayoutPanelPassword.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelUsername.SuspendLayout();
            this.tableLayoutPanelUsername.SuspendLayout();
            this.panelUserTextBox.SuspendLayout();
            this.panelResetControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelMain.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanelMain.BackgroundImage")));
            this.tableLayoutPanelMain.ColumnCount = 3;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelControlsMain, 1, 0);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 1;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1920, 1080);
            this.tableLayoutPanelMain.TabIndex = 1;
            // 
            // tableLayoutPanelControlsMain
            // 
            this.tableLayoutPanelControlsMain.ColumnCount = 1;
            this.tableLayoutPanelControlsMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelControlsMain.Controls.Add(this.tableLayoutPanelControls, 0, 0);
            this.tableLayoutPanelControlsMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelControlsMain.Location = new System.Drawing.Point(194, 2);
            this.tableLayoutPanelControlsMain.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelControlsMain.Name = "tableLayoutPanelControlsMain";
            this.tableLayoutPanelControlsMain.RowCount = 2;
            this.tableLayoutPanelControlsMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanelControlsMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanelControlsMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanelControlsMain.Size = new System.Drawing.Size(668, 1076);
            this.tableLayoutPanelControlsMain.TabIndex = 0;
            // 
            // tableLayoutPanelControls
            // 
            this.tableLayoutPanelControls.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.tableLayoutPanelControls.ColumnCount = 1;
            this.tableLayoutPanelControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelControls.Controls.Add(this.panelLogo, 0, 0);
            this.tableLayoutPanelControls.Controls.Add(this.panelControls, 0, 1);
            this.tableLayoutPanelControls.Controls.Add(this.panelResetControls, 0, 2);
            this.tableLayoutPanelControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelControls.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanelControls.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelControls.Name = "tableLayoutPanelControls";
            this.tableLayoutPanelControls.RowCount = 3;
            this.tableLayoutPanelControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanelControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanelControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelControls.Size = new System.Drawing.Size(664, 641);
            this.tableLayoutPanelControls.TabIndex = 0;
            // 
            // panelLogo
            // 
            this.panelLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelLogo.BackgroundImage")));
            this.panelLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLogo.Location = new System.Drawing.Point(2, 2);
            this.panelLogo.Margin = new System.Windows.Forms.Padding(2);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(660, 252);
            this.panelLogo.TabIndex = 0;
            // 
            // panelControls
            // 
            this.panelControls.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelControls.BackgroundImage")));
            this.panelControls.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panelControls.Controls.Add(this.btnClose);
            this.panelControls.Controls.Add(this.checkBoxAutoLogin);
            this.panelControls.Controls.Add(this.panelPassword);
            this.panelControls.Controls.Add(this.panelUsername);
            this.panelControls.Controls.Add(this.btnReset);
            this.panelControls.Controls.Add(this.btnLogin);
            this.panelControls.Controls.Add(this.lblTitle);
            this.panelControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControls.Location = new System.Drawing.Point(2, 258);
            this.panelControls.Margin = new System.Windows.Forms.Padding(2);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(660, 348);
            this.panelControls.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(179)))), ((int)(((byte)(179)))));
            this.btnClose.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnClose.Appearance.Options.UseBackColor = true;
            this.btnClose.Appearance.Options.UseFont = true;
            this.btnClose.Appearance.Options.UseForeColor = true;
            this.btnClose.AutoWidthInLayoutControl = true;
            this.btnClose.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnClose.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.ImageOptions.Image")));
            this.btnClose.Location = new System.Drawing.Point(456, 277);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(56, 16);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // checkBoxAutoLogin
            // 
            this.checkBoxAutoLogin.AutoSize = true;
            this.checkBoxAutoLogin.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxAutoLogin.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.checkBoxAutoLogin.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.checkBoxAutoLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.checkBoxAutoLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.checkBoxAutoLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxAutoLogin.Font = new System.Drawing.Font("Calibri", 9F);
            this.checkBoxAutoLogin.ForeColor = System.Drawing.Color.White;
            this.checkBoxAutoLogin.Location = new System.Drawing.Point(156, 277);
            this.checkBoxAutoLogin.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxAutoLogin.Name = "checkBoxAutoLogin";
            this.checkBoxAutoLogin.Size = new System.Drawing.Size(80, 18);
            this.checkBoxAutoLogin.TabIndex = 12;
            this.checkBoxAutoLogin.Text = "Auto Login";
            this.checkBoxAutoLogin.UseVisualStyleBackColor = false;
            // 
            // panelPassword
            // 
            this.panelPassword.BackColor = System.Drawing.Color.White;
            this.panelPassword.Controls.Add(this.tableLayoutPanelPassword);
            this.panelPassword.Location = new System.Drawing.Point(154, 164);
            this.panelPassword.Margin = new System.Windows.Forms.Padding(2);
            this.panelPassword.Name = "panelPassword";
            this.panelPassword.Size = new System.Drawing.Size(357, 50);
            this.panelPassword.TabIndex = 16;
            // 
            // tableLayoutPanelPassword
            // 
            this.tableLayoutPanelPassword.ColumnCount = 2;
            this.tableLayoutPanelPassword.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelPassword.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelPassword.Controls.Add(this.panelPwdIcon, 0, 0);
            this.tableLayoutPanelPassword.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanelPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelPassword.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelPassword.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelPassword.Name = "tableLayoutPanelPassword";
            this.tableLayoutPanelPassword.RowCount = 1;
            this.tableLayoutPanelPassword.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelPassword.Size = new System.Drawing.Size(357, 50);
            this.tableLayoutPanelPassword.TabIndex = 1;
            // 
            // panelPwdIcon
            // 
            this.panelPwdIcon.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelPwdIcon.BackgroundImage")));
            this.panelPwdIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panelPwdIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPwdIcon.Location = new System.Drawing.Point(2, 2);
            this.panelPwdIcon.Margin = new System.Windows.Forms.Padding(2);
            this.panelPwdIcon.Name = "panelPwdIcon";
            this.panelPwdIcon.Size = new System.Drawing.Size(26, 46);
            this.panelPwdIcon.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtPassword);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(32, 2);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(323, 46);
            this.panel2.TabIndex = 1;
            // 
            // txtPassword
            // 
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPassword.Font = new System.Drawing.Font("Calibri", 18F);
            this.txtPassword.Location = new System.Drawing.Point(4, 8);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(2);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(316, 30);
            this.txtPassword.TabIndex = 14;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // panelUsername
            // 
            this.panelUsername.BackColor = System.Drawing.Color.White;
            this.panelUsername.Controls.Add(this.tableLayoutPanelUsername);
            this.panelUsername.Location = new System.Drawing.Point(154, 105);
            this.panelUsername.Margin = new System.Windows.Forms.Padding(2);
            this.panelUsername.Name = "panelUsername";
            this.panelUsername.Size = new System.Drawing.Size(357, 50);
            this.panelUsername.TabIndex = 15;
            // 
            // tableLayoutPanelUsername
            // 
            this.tableLayoutPanelUsername.ColumnCount = 2;
            this.tableLayoutPanelUsername.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelUsername.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelUsername.Controls.Add(this.panelUserIcon, 0, 0);
            this.tableLayoutPanelUsername.Controls.Add(this.panelUserTextBox, 1, 0);
            this.tableLayoutPanelUsername.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelUsername.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelUsername.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelUsername.Name = "tableLayoutPanelUsername";
            this.tableLayoutPanelUsername.RowCount = 1;
            this.tableLayoutPanelUsername.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelUsername.Size = new System.Drawing.Size(357, 50);
            this.tableLayoutPanelUsername.TabIndex = 0;
            // 
            // panelUserIcon
            // 
            this.panelUserIcon.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelUserIcon.BackgroundImage")));
            this.panelUserIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panelUserIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUserIcon.Location = new System.Drawing.Point(2, 2);
            this.panelUserIcon.Margin = new System.Windows.Forms.Padding(2);
            this.panelUserIcon.Name = "panelUserIcon";
            this.panelUserIcon.Size = new System.Drawing.Size(26, 46);
            this.panelUserIcon.TabIndex = 0;
            // 
            // panelUserTextBox
            // 
            this.panelUserTextBox.Controls.Add(this.txtUserName);
            this.panelUserTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUserTextBox.Location = new System.Drawing.Point(32, 2);
            this.panelUserTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.panelUserTextBox.Name = "panelUserTextBox";
            this.panelUserTextBox.Size = new System.Drawing.Size(323, 46);
            this.panelUserTextBox.TabIndex = 1;
            // 
            // txtUserName
            // 
            this.txtUserName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUserName.Font = new System.Drawing.Font("Calibri", 18F);
            this.txtUserName.Location = new System.Drawing.Point(3, 7);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(2);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(316, 30);
            this.txtUserName.TabIndex = 13;
            // 
            // btnReset
            // 
            this.btnReset.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(179)))), ((int)(((byte)(179)))));
            this.btnReset.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnReset.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnReset.Appearance.Options.UseBackColor = true;
            this.btnReset.Appearance.Options.UseFont = true;
            this.btnReset.Appearance.Options.UseForeColor = true;
            this.btnReset.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnReset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnReset.ImageOptions.Image")));
            this.btnReset.Location = new System.Drawing.Point(394, 277);
            this.btnReset.Margin = new System.Windows.Forms.Padding(2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(56, 16);
            this.btnReset.TabIndex = 10;
            this.btnReset.Text = "Reset";
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.btnLogin.Appearance.Font = new System.Drawing.Font("Calibri", 16F);
            this.btnLogin.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnLogin.Appearance.Options.UseBackColor = true;
            this.btnLogin.Appearance.Options.UseFont = true;
            this.btnLogin.Appearance.Options.UseForeColor = true;
            this.btnLogin.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnLogin.Location = new System.Drawing.Point(154, 222);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(2);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(357, 50);
            this.btnLogin.TabIndex = 9;
            this.btnLogin.Text = "LOGIN";
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblTitle.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(253, 27);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(159, 27);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "MEMBER LOGIN";
            // 
            // panelResetControls
            // 
            this.panelResetControls.Controls.Add(this.lblMessage);
            this.panelResetControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelResetControls.Location = new System.Drawing.Point(2, 610);
            this.panelResetControls.Margin = new System.Windows.Forms.Padding(2);
            this.panelResetControls.Name = "panelResetControls";
            this.panelResetControls.Size = new System.Drawing.Size(660, 29);
            this.panelResetControls.TabIndex = 2;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.ForeColor = System.Drawing.Color.Red;
            this.lblMessage.Location = new System.Drawing.Point(66, 3);
            this.lblMessage.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(0, 13);
            this.lblMessage.TabIndex = 8;
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmLogin";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelControlsMain.ResumeLayout(false);
            this.tableLayoutPanelControls.ResumeLayout(false);
            this.panelControls.ResumeLayout(false);
            this.panelControls.PerformLayout();
            this.panelPassword.ResumeLayout(false);
            this.tableLayoutPanelPassword.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panelUsername.ResumeLayout(false);
            this.tableLayoutPanelUsername.ResumeLayout(false);
            this.panelUserTextBox.ResumeLayout(false);
            this.panelUserTextBox.PerformLayout();
            this.panelResetControls.ResumeLayout(false);
            this.panelResetControls.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblMessage;
        private DevExpress.XtraEditors.SimpleButton btnReset;
        private DevExpress.XtraEditors.SimpleButton btnLogin;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Panel panelUserIcon;
        private System.Windows.Forms.Panel panelUserTextBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelUsername;
        private System.Windows.Forms.Panel panelUsername;
        private System.Windows.Forms.Panel panelPwdIcon;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelPassword;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private System.Windows.Forms.CheckBox checkBoxAutoLogin;
        private System.Windows.Forms.Panel panelPassword;
        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.Panel panelControls;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelControls;
        private System.Windows.Forms.Panel panelResetControls;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelControlsMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
    }
}