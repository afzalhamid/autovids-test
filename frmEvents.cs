﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoViewer
{
    public partial class frmEvents : Form
    {
        DAL dataAccess = new DAL();
        private DateTime reportDate { get; set; }
        private string ParentFolder { get { return System.Configuration.ConfigurationManager.AppSettings["FolderPath"]; } }

        public frmEvents()
        {
            InitializeComponent();
        }

        private void frmEvents_Load(object sender, EventArgs e)
        {
            dateEdit.DateTime = DateTime.Now;
            reportDate = DateTime.Now.Date;
            LoadData();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            reportDate = dateEdit.DateTime;
            LoadData();
        }

        void LoadData()
        {
            try
            {
                gridEvent.DataSource = dataAccess.GetEventsByDate(reportDate);
            }
            catch 
            {

            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                string folder = String.Format("{0}\\Excel", ParentFolder);
                if (!System.IO.Directory.Exists(folder))
                    System.IO.Directory.CreateDirectory(folder);

                gridEvent.ExportToXlsx(string.Format("{0}\\EventLog_{1}_{2}.xlsx", folder, reportDate.ToString("ddMMyyyy"), DateTime.Now.ToString("hhmmss")));

            }
            catch
            {
                MessageBox.Show("Export Failed");
            }
        }
    }
}
