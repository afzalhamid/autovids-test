﻿namespace VideoViewer
{
    partial class frmUserManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUserManagement));
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelUser = new System.Windows.Forms.TableLayoutPanel();
            this.gridControlUser = new DevExpress.XtraGrid.GridControl();
            this.gridViewUser = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnFName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnRole = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnRID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tableLayoutPanelAddUserControl = new System.Windows.Forms.TableLayoutPanel();
            this.panelSeqForm = new System.Windows.Forms.Panel();
            this.txtUserID = new System.Windows.Forms.MaskedTextBox();
            this.ddlRole = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.MaskedTextBox();
            this.txtEmail = new System.Windows.Forms.MaskedTextBox();
            this.txtConfirmPassword = new System.Windows.Forms.MaskedTextBox();
            this.txtPassword = new System.Windows.Forms.MaskedTextBox();
            this.txtUserName = new System.Windows.Forms.MaskedTextBox();
            this.txtFirstName = new System.Windows.Forms.MaskedTextBox();
            this.btnResetUser = new DevExpress.XtraEditors.SimpleButton();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnDeleteUser = new DevExpress.XtraEditors.SimpleButton();
            this.lblUserName = new System.Windows.Forms.Label();
            this.btnAddUpdateUser = new DevExpress.XtraEditors.SimpleButton();
            this.lblSeqNo = new System.Windows.Forms.Label();
            this.panelUserRole = new System.Windows.Forms.Panel();
            this.panelPassword = new System.Windows.Forms.Panel();
            this.panelConfirmPassword = new System.Windows.Forms.Panel();
            this.panelUserName = new System.Windows.Forms.Panel();
            this.panelEmail = new System.Windows.Forms.Panel();
            this.panelFirstName = new System.Windows.Forms.Panel();
            this.panelLastName = new System.Windows.Forms.Panel();
            this.panelUserControlTitle = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanelRoles = new System.Windows.Forms.TableLayoutPanel();
            this.gridControlRole = new DevExpress.XtraGrid.GridControl();
            this.gridViewRole = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPermission = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnRoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPermissionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tableLayoutPanelUserRoleControls = new System.Windows.Forms.TableLayoutPanel();
            this.panelStepForm = new System.Windows.Forms.Panel();
            this.txtRoleID = new System.Windows.Forms.MaskedTextBox();
            this.clbPermission = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.txtRoleName = new System.Windows.Forms.MaskedTextBox();
            this.btnResetRole = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteRole = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddUpdateRole = new DevExpress.XtraEditors.SimpleButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panelRoleTitle = new System.Windows.Forms.Panel();
            this.panelUserRoleControlTitle = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanelMain.SuspendLayout();
            this.tableLayoutPanelUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.tableLayoutPanelAddUserControl.SuspendLayout();
            this.panelSeqForm.SuspendLayout();
            this.panelUserControlTitle.SuspendLayout();
            this.tableLayoutPanelRoles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRole)).BeginInit();
            this.tableLayoutPanelUserRoleControls.SuspendLayout();
            this.panelStepForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clbPermission)).BeginInit();
            this.panelUserRoleControlTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelMain.ColumnCount = 1;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelUser, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelRoles, 0, 1);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 2;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1829, 1028);
            this.tableLayoutPanelMain.TabIndex = 0;
            // 
            // tableLayoutPanelUser
            // 
            this.tableLayoutPanelUser.ColumnCount = 2;
            this.tableLayoutPanelUser.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanelUser.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelUser.Controls.Add(this.gridControlUser, 1, 0);
            this.tableLayoutPanelUser.Controls.Add(this.tableLayoutPanelAddUserControl, 0, 0);
            this.tableLayoutPanelUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelUser.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelUser.Name = "tableLayoutPanelUser";
            this.tableLayoutPanelUser.RowCount = 1;
            this.tableLayoutPanelUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelUser.Size = new System.Drawing.Size(1823, 610);
            this.tableLayoutPanelUser.TabIndex = 3;
            // 
            // gridControlUser
            // 
            this.gridControlUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlUser.Location = new System.Drawing.Point(403, 3);
            this.gridControlUser.MainView = this.gridViewUser;
            this.gridControlUser.Name = "gridControlUser";
            this.gridControlUser.Size = new System.Drawing.Size(1417, 604);
            this.gridControlUser.TabIndex = 3;
            this.gridControlUser.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewUser,
            this.gridView1});
            // 
            // gridViewUser
            // 
            this.gridViewUser.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridViewUser.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridViewUser.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridViewUser.Appearance.DetailTip.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.DetailTip.Options.UseFont = true;
            this.gridViewUser.Appearance.Empty.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.Empty.Options.UseFont = true;
            this.gridViewUser.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.gridViewUser.Appearance.EvenRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridViewUser.Appearance.EvenRow.Options.UseFont = true;
            this.gridViewUser.Appearance.FilterCloseButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridViewUser.Appearance.FilterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.FilterPanel.Options.UseFont = true;
            this.gridViewUser.Appearance.FixedLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.FixedLine.Options.UseFont = true;
            this.gridViewUser.Appearance.FocusedCell.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.FocusedCell.Options.UseFont = true;
            this.gridViewUser.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.gridViewUser.Appearance.FocusedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridViewUser.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewUser.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewUser.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewUser.Appearance.FooterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewUser.Appearance.GroupButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.GroupButton.Options.UseFont = true;
            this.gridViewUser.Appearance.GroupFooter.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.GroupFooter.Options.UseFont = true;
            this.gridViewUser.Appearance.GroupPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewUser.Appearance.GroupRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewUser.Appearance.HeaderPanel.Font = new System.Drawing.Font("Calibri", 12F);
            this.gridViewUser.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewUser.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridViewUser.Appearance.HorzLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.HorzLine.Options.UseFont = true;
            this.gridViewUser.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.gridViewUser.Appearance.OddRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewUser.Appearance.OddRow.Options.UseFont = true;
            this.gridViewUser.Appearance.Preview.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.Preview.Options.UseFont = true;
            this.gridViewUser.Appearance.Row.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.Row.Options.UseFont = true;
            this.gridViewUser.Appearance.RowSeparator.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.RowSeparator.Options.UseFont = true;
            this.gridViewUser.Appearance.SelectedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.SelectedRow.Options.UseFont = true;
            this.gridViewUser.Appearance.TopNewRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.TopNewRow.Options.UseFont = true;
            this.gridViewUser.Appearance.VertLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.VertLine.Options.UseFont = true;
            this.gridViewUser.Appearance.ViewCaption.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewUser.Appearance.ViewCaption.Options.UseFont = true;
            this.gridViewUser.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnFName,
            this.gridColumnLName,
            this.gridColumnUserID,
            this.gridColumnEmail,
            this.gridColumnRole,
            this.gridColumnPassword,
            this.gridColumnID,
            this.gridColumnRID});
            this.gridViewUser.GridControl = this.gridControlUser;
            this.gridViewUser.Name = "gridViewUser";
            this.gridViewUser.OptionsView.EnableAppearanceOddRow = true;
            this.gridViewUser.OptionsView.ShowGroupPanel = false;
            this.gridViewUser.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridViewUser_RowClick);
            // 
            // gridColumnFName
            // 
            this.gridColumnFName.Caption = "First Name";
            this.gridColumnFName.FieldName = "FirstName";
            this.gridColumnFName.MinWidth = 150;
            this.gridColumnFName.Name = "gridColumnFName";
            this.gridColumnFName.OptionsColumn.AllowEdit = false;
            this.gridColumnFName.Visible = true;
            this.gridColumnFName.VisibleIndex = 0;
            this.gridColumnFName.Width = 150;
            // 
            // gridColumnLName
            // 
            this.gridColumnLName.Caption = "Last Name";
            this.gridColumnLName.FieldName = "LastName";
            this.gridColumnLName.MinWidth = 150;
            this.gridColumnLName.Name = "gridColumnLName";
            this.gridColumnLName.OptionsColumn.AllowEdit = false;
            this.gridColumnLName.Visible = true;
            this.gridColumnLName.VisibleIndex = 1;
            this.gridColumnLName.Width = 150;
            // 
            // gridColumnUserID
            // 
            this.gridColumnUserID.Caption = "User Name";
            this.gridColumnUserID.FieldName = "UserID";
            this.gridColumnUserID.MinWidth = 200;
            this.gridColumnUserID.Name = "gridColumnUserID";
            this.gridColumnUserID.OptionsColumn.AllowEdit = false;
            this.gridColumnUserID.Visible = true;
            this.gridColumnUserID.VisibleIndex = 2;
            this.gridColumnUserID.Width = 200;
            // 
            // gridColumnEmail
            // 
            this.gridColumnEmail.Caption = "Email";
            this.gridColumnEmail.FieldName = "Email";
            this.gridColumnEmail.MinWidth = 200;
            this.gridColumnEmail.Name = "gridColumnEmail";
            this.gridColumnEmail.OptionsColumn.AllowEdit = false;
            this.gridColumnEmail.Visible = true;
            this.gridColumnEmail.VisibleIndex = 3;
            this.gridColumnEmail.Width = 200;
            // 
            // gridColumnRole
            // 
            this.gridColumnRole.Caption = "Role";
            this.gridColumnRole.FieldName = "Role";
            this.gridColumnRole.MinWidth = 150;
            this.gridColumnRole.Name = "gridColumnRole";
            this.gridColumnRole.OptionsColumn.AllowEdit = false;
            this.gridColumnRole.Visible = true;
            this.gridColumnRole.VisibleIndex = 4;
            this.gridColumnRole.Width = 150;
            // 
            // gridColumnPassword
            // 
            this.gridColumnPassword.Caption = "Password";
            this.gridColumnPassword.FieldName = "Password";
            this.gridColumnPassword.Name = "gridColumnPassword";
            this.gridColumnPassword.OptionsColumn.AllowEdit = false;
            // 
            // gridColumnID
            // 
            this.gridColumnID.Caption = "ID";
            this.gridColumnID.FieldName = "UID";
            this.gridColumnID.Name = "gridColumnID";
            this.gridColumnID.OptionsColumn.AllowEdit = false;
            // 
            // gridColumnRID
            // 
            this.gridColumnRID.Caption = "RID";
            this.gridColumnRID.FieldName = "RID";
            this.gridColumnRID.Name = "gridColumnRID";
            this.gridColumnRID.OptionsColumn.AllowEdit = false;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControlUser;
            this.gridView1.Name = "gridView1";
            // 
            // tableLayoutPanelAddUserControl
            // 
            this.tableLayoutPanelAddUserControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.tableLayoutPanelAddUserControl.ColumnCount = 1;
            this.tableLayoutPanelAddUserControl.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelAddUserControl.Controls.Add(this.panelSeqForm, 0, 1);
            this.tableLayoutPanelAddUserControl.Controls.Add(this.panelUserControlTitle, 0, 0);
            this.tableLayoutPanelAddUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelAddUserControl.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelAddUserControl.Name = "tableLayoutPanelAddUserControl";
            this.tableLayoutPanelAddUserControl.RowCount = 2;
            this.tableLayoutPanelAddUserControl.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelAddUserControl.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelAddUserControl.Size = new System.Drawing.Size(394, 604);
            this.tableLayoutPanelAddUserControl.TabIndex = 4;
            // 
            // panelSeqForm
            // 
            this.panelSeqForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panelSeqForm.Controls.Add(this.txtUserID);
            this.panelSeqForm.Controls.Add(this.ddlRole);
            this.panelSeqForm.Controls.Add(this.label11);
            this.panelSeqForm.Controls.Add(this.txtLastName);
            this.panelSeqForm.Controls.Add(this.txtEmail);
            this.panelSeqForm.Controls.Add(this.txtConfirmPassword);
            this.panelSeqForm.Controls.Add(this.txtPassword);
            this.panelSeqForm.Controls.Add(this.txtUserName);
            this.panelSeqForm.Controls.Add(this.txtFirstName);
            this.panelSeqForm.Controls.Add(this.btnResetUser);
            this.panelSeqForm.Controls.Add(this.label10);
            this.panelSeqForm.Controls.Add(this.label3);
            this.panelSeqForm.Controls.Add(this.label2);
            this.panelSeqForm.Controls.Add(this.btnDeleteUser);
            this.panelSeqForm.Controls.Add(this.lblUserName);
            this.panelSeqForm.Controls.Add(this.btnAddUpdateUser);
            this.panelSeqForm.Controls.Add(this.lblSeqNo);
            this.panelSeqForm.Controls.Add(this.panelUserRole);
            this.panelSeqForm.Controls.Add(this.panelPassword);
            this.panelSeqForm.Controls.Add(this.panelConfirmPassword);
            this.panelSeqForm.Controls.Add(this.panelUserName);
            this.panelSeqForm.Controls.Add(this.panelEmail);
            this.panelSeqForm.Controls.Add(this.panelFirstName);
            this.panelSeqForm.Controls.Add(this.panelLastName);
            this.panelSeqForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSeqForm.Font = new System.Drawing.Font("Calibri", 9F);
            this.panelSeqForm.Location = new System.Drawing.Point(3, 53);
            this.panelSeqForm.Name = "panelSeqForm";
            this.panelSeqForm.Size = new System.Drawing.Size(388, 548);
            this.panelSeqForm.TabIndex = 4;
            // 
            // txtUserID
            // 
            this.txtUserID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUserID.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserID.Location = new System.Drawing.Point(231, 8);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(37, 21);
            this.txtUserID.TabIndex = 21;
            this.txtUserID.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // ddlRole
            // 
            this.ddlRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlRole.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ddlRole.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlRole.FormattingEnabled = true;
            this.ddlRole.Location = new System.Drawing.Point(24, 383);
            this.ddlRole.Name = "ddlRole";
            this.ddlRole.Size = new System.Drawing.Size(275, 29);
            this.ddlRole.TabIndex = 56;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 10F);
            this.label11.Location = new System.Drawing.Point(17, 354);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 21);
            this.label11.TabIndex = 19;
            this.label11.Text = "Role";
            // 
            // txtLastName
            // 
            this.txtLastName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLastName.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastName.Location = new System.Drawing.Point(168, 47);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(130, 21);
            this.txtLastName.TabIndex = 51;
            this.txtLastName.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // txtEmail
            // 
            this.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmail.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(24, 114);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(274, 21);
            this.txtEmail.TabIndex = 52;
            this.txtEmail.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // txtConfirmPassword
            // 
            this.txtConfirmPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtConfirmPassword.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfirmPassword.Location = new System.Drawing.Point(23, 320);
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.PasswordChar = '*';
            this.txtConfirmPassword.Size = new System.Drawing.Size(274, 21);
            this.txtConfirmPassword.TabIndex = 55;
            this.txtConfirmPassword.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtConfirmPassword.UseSystemPasswordChar = true;
            this.txtConfirmPassword.Leave += new System.EventHandler(this.txtConfirmPassword_Leave);
            // 
            // txtPassword
            // 
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPassword.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(24, 250);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(274, 21);
            this.txtPassword.TabIndex = 54;
            this.txtPassword.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // txtUserName
            // 
            this.txtUserName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUserName.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserName.Location = new System.Drawing.Point(24, 183);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(274, 21);
            this.txtUserName.TabIndex = 53;
            this.txtUserName.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // txtFirstName
            // 
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFirstName.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.Location = new System.Drawing.Point(23, 47);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(130, 21);
            this.txtFirstName.TabIndex = 50;
            this.txtFirstName.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // btnResetUser
            // 
            this.btnResetUser.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnResetUser.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnResetUser.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnResetUser.Appearance.Options.UseBackColor = true;
            this.btnResetUser.Appearance.Options.UseFont = true;
            this.btnResetUser.Appearance.Options.UseForeColor = true;
            this.btnResetUser.Image = ((System.Drawing.Image)(resources.GetObject("btnResetUser.Image")));
            this.btnResetUser.Location = new System.Drawing.Point(226, 427);
            this.btnResetUser.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.btnResetUser.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnResetUser.Name = "btnResetUser";
            this.btnResetUser.Size = new System.Drawing.Size(79, 30);
            this.btnResetUser.TabIndex = 59;
            this.btnResetUser.Text = "Reset";
            this.btnResetUser.Click += new System.EventHandler(this.btnResetUser_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 10F);
            this.label10.Location = new System.Drawing.Point(17, 285);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(137, 21);
            this.label10.TabIndex = 4;
            this.label10.Text = "Confirm Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 10F);
            this.label3.Location = new System.Drawing.Point(17, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 21);
            this.label3.TabIndex = 4;
            this.label3.Text = "E-Mail";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 10F);
            this.label2.Location = new System.Drawing.Point(17, 217);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 21);
            this.label2.TabIndex = 4;
            this.label2.Text = "Password";
            // 
            // btnDeleteUser
            // 
            this.btnDeleteUser.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnDeleteUser.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnDeleteUser.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnDeleteUser.Appearance.Options.UseBackColor = true;
            this.btnDeleteUser.Appearance.Options.UseFont = true;
            this.btnDeleteUser.Appearance.Options.UseForeColor = true;
            this.btnDeleteUser.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteUser.Image")));
            this.btnDeleteUser.Location = new System.Drawing.Point(125, 427);
            this.btnDeleteUser.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.btnDeleteUser.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnDeleteUser.Name = "btnDeleteUser";
            this.btnDeleteUser.Size = new System.Drawing.Size(79, 30);
            this.btnDeleteUser.TabIndex = 58;
            this.btnDeleteUser.Text = "Delete";
            this.btnDeleteUser.Click += new System.EventHandler(this.btnDeleteUser_Click);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblUserName.Location = new System.Drawing.Point(17, 149);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(87, 21);
            this.lblUserName.TabIndex = 4;
            this.lblUserName.Text = "User Name";
            // 
            // btnAddUpdateUser
            // 
            this.btnAddUpdateUser.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnAddUpdateUser.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnAddUpdateUser.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnAddUpdateUser.Appearance.Options.UseBackColor = true;
            this.btnAddUpdateUser.Appearance.Options.UseFont = true;
            this.btnAddUpdateUser.Appearance.Options.UseForeColor = true;
            this.btnAddUpdateUser.Image = ((System.Drawing.Image)(resources.GetObject("btnAddUpdateUser.Image")));
            this.btnAddUpdateUser.Location = new System.Drawing.Point(17, 427);
            this.btnAddUpdateUser.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.btnAddUpdateUser.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAddUpdateUser.Name = "btnAddUpdateUser";
            this.btnAddUpdateUser.Size = new System.Drawing.Size(86, 30);
            this.btnAddUpdateUser.TabIndex = 57;
            this.btnAddUpdateUser.Text = "Add / Edit";
            this.btnAddUpdateUser.Click += new System.EventHandler(this.btnAddUpdateUser_Click);
            // 
            // lblSeqNo
            // 
            this.lblSeqNo.AutoSize = true;
            this.lblSeqNo.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblSeqNo.Location = new System.Drawing.Point(17, 14);
            this.lblSeqNo.Name = "lblSeqNo";
            this.lblSeqNo.Size = new System.Drawing.Size(122, 21);
            this.lblSeqNo.TabIndex = 4;
            this.lblSeqNo.Text = "First & Last Name";
            // 
            // panelUserRole
            // 
            this.panelUserRole.BackColor = System.Drawing.Color.White;
            this.panelUserRole.Location = new System.Drawing.Point(17, 380);
            this.panelUserRole.Name = "panelUserRole";
            this.panelUserRole.Size = new System.Drawing.Size(288, 35);
            this.panelUserRole.TabIndex = 237;
            // 
            // panelPassword
            // 
            this.panelPassword.BackColor = System.Drawing.Color.White;
            this.panelPassword.Location = new System.Drawing.Point(17, 243);
            this.panelPassword.Name = "panelPassword";
            this.panelPassword.Size = new System.Drawing.Size(288, 35);
            this.panelPassword.TabIndex = 237;
            // 
            // panelConfirmPassword
            // 
            this.panelConfirmPassword.BackColor = System.Drawing.Color.White;
            this.panelConfirmPassword.Location = new System.Drawing.Point(17, 313);
            this.panelConfirmPassword.Name = "panelConfirmPassword";
            this.panelConfirmPassword.Size = new System.Drawing.Size(288, 35);
            this.panelConfirmPassword.TabIndex = 236;
            // 
            // panelUserName
            // 
            this.panelUserName.BackColor = System.Drawing.Color.White;
            this.panelUserName.Location = new System.Drawing.Point(17, 176);
            this.panelUserName.Name = "panelUserName";
            this.panelUserName.Size = new System.Drawing.Size(288, 35);
            this.panelUserName.TabIndex = 236;
            // 
            // panelEmail
            // 
            this.panelEmail.BackColor = System.Drawing.Color.White;
            this.panelEmail.Location = new System.Drawing.Point(17, 107);
            this.panelEmail.Name = "panelEmail";
            this.panelEmail.Size = new System.Drawing.Size(288, 35);
            this.panelEmail.TabIndex = 235;
            // 
            // panelFirstName
            // 
            this.panelFirstName.BackColor = System.Drawing.Color.White;
            this.panelFirstName.Location = new System.Drawing.Point(17, 40);
            this.panelFirstName.Name = "panelFirstName";
            this.panelFirstName.Size = new System.Drawing.Size(143, 35);
            this.panelFirstName.TabIndex = 234;
            // 
            // panelLastName
            // 
            this.panelLastName.BackColor = System.Drawing.Color.White;
            this.panelLastName.Location = new System.Drawing.Point(162, 40);
            this.panelLastName.Name = "panelLastName";
            this.panelLastName.Size = new System.Drawing.Size(143, 35);
            this.panelLastName.TabIndex = 235;
            // 
            // panelUserControlTitle
            // 
            this.panelUserControlTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panelUserControlTitle.Controls.Add(this.label1);
            this.panelUserControlTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUserControlTitle.Font = new System.Drawing.Font("Calibri", 10F);
            this.panelUserControlTitle.ForeColor = System.Drawing.Color.White;
            this.panelUserControlTitle.Location = new System.Drawing.Point(0, 0);
            this.panelUserControlTitle.Margin = new System.Windows.Forms.Padding(0);
            this.panelUserControlTitle.Name = "panelUserControlTitle";
            this.panelUserControlTitle.Size = new System.Drawing.Size(394, 50);
            this.panelUserControlTitle.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(6, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "User:";
            // 
            // tableLayoutPanelRoles
            // 
            this.tableLayoutPanelRoles.ColumnCount = 2;
            this.tableLayoutPanelRoles.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanelRoles.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelRoles.Controls.Add(this.gridControlRole, 1, 0);
            this.tableLayoutPanelRoles.Controls.Add(this.tableLayoutPanelUserRoleControls, 0, 0);
            this.tableLayoutPanelRoles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelRoles.Location = new System.Drawing.Point(3, 619);
            this.tableLayoutPanelRoles.Name = "tableLayoutPanelRoles";
            this.tableLayoutPanelRoles.RowCount = 1;
            this.tableLayoutPanelRoles.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelRoles.Size = new System.Drawing.Size(1823, 406);
            this.tableLayoutPanelRoles.TabIndex = 5;
            // 
            // gridControlRole
            // 
            this.gridControlRole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRole.Location = new System.Drawing.Point(403, 3);
            this.gridControlRole.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControlRole.MainView = this.gridViewRole;
            this.gridControlRole.Name = "gridControlRole";
            this.gridControlRole.Size = new System.Drawing.Size(1417, 400);
            this.gridControlRole.TabIndex = 5;
            this.gridControlRole.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewRole});
            // 
            // gridViewRole
            // 
            this.gridViewRole.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridViewRole.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridViewRole.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridViewRole.Appearance.DetailTip.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.DetailTip.Options.UseFont = true;
            this.gridViewRole.Appearance.Empty.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.Empty.Options.UseFont = true;
            this.gridViewRole.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.gridViewRole.Appearance.EvenRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridViewRole.Appearance.EvenRow.Options.UseFont = true;
            this.gridViewRole.Appearance.FilterCloseButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridViewRole.Appearance.FilterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.FilterPanel.Options.UseFont = true;
            this.gridViewRole.Appearance.FixedLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.FixedLine.Options.UseFont = true;
            this.gridViewRole.Appearance.FocusedCell.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.FocusedCell.Options.UseFont = true;
            this.gridViewRole.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.gridViewRole.Appearance.FocusedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridViewRole.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewRole.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewRole.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewRole.Appearance.FooterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewRole.Appearance.GroupButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.GroupButton.Options.UseFont = true;
            this.gridViewRole.Appearance.GroupFooter.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.GroupFooter.Options.UseFont = true;
            this.gridViewRole.Appearance.GroupPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewRole.Appearance.GroupRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewRole.Appearance.HeaderPanel.Font = new System.Drawing.Font("Calibri", 12F);
            this.gridViewRole.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewRole.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridViewRole.Appearance.HorzLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.HorzLine.Options.UseFont = true;
            this.gridViewRole.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.gridViewRole.Appearance.OddRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewRole.Appearance.OddRow.Options.UseFont = true;
            this.gridViewRole.Appearance.Preview.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.Preview.Options.UseFont = true;
            this.gridViewRole.Appearance.Row.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.Row.Options.UseFont = true;
            this.gridViewRole.Appearance.RowSeparator.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.RowSeparator.Options.UseFont = true;
            this.gridViewRole.Appearance.SelectedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.SelectedRow.Options.UseFont = true;
            this.gridViewRole.Appearance.TopNewRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.TopNewRow.Options.UseFont = true;
            this.gridViewRole.Appearance.VertLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.VertLine.Options.UseFont = true;
            this.gridViewRole.Appearance.ViewCaption.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewRole.Appearance.ViewCaption.Options.UseFont = true;
            this.gridViewRole.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnTitle,
            this.gridColumnPermission,
            this.gridColumnRoleID,
            this.gridColumnPermissionID});
            this.gridViewRole.GridControl = this.gridControlRole;
            this.gridViewRole.Name = "gridViewRole";
            this.gridViewRole.OptionsView.EnableAppearanceOddRow = true;
            this.gridViewRole.OptionsView.ShowGroupPanel = false;
            this.gridViewRole.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridViewRole_RowClick);
            // 
            // gridColumnTitle
            // 
            this.gridColumnTitle.Caption = "Title";
            this.gridColumnTitle.FieldName = "Role";
            this.gridColumnTitle.MaxWidth = 200;
            this.gridColumnTitle.MinWidth = 10;
            this.gridColumnTitle.Name = "gridColumnTitle";
            this.gridColumnTitle.OptionsColumn.AllowEdit = false;
            this.gridColumnTitle.Visible = true;
            this.gridColumnTitle.VisibleIndex = 0;
            this.gridColumnTitle.Width = 200;
            // 
            // gridColumnPermission
            // 
            this.gridColumnPermission.Caption = "Permission";
            this.gridColumnPermission.FieldName = "Permissions";
            this.gridColumnPermission.Name = "gridColumnPermission";
            this.gridColumnPermission.OptionsColumn.AllowEdit = false;
            this.gridColumnPermission.Visible = true;
            this.gridColumnPermission.VisibleIndex = 1;
            this.gridColumnPermission.Width = 20;
            // 
            // gridColumnRoleID
            // 
            this.gridColumnRoleID.Caption = "ID";
            this.gridColumnRoleID.FieldName = "ID";
            this.gridColumnRoleID.MinWidth = 10;
            this.gridColumnRoleID.Name = "gridColumnRoleID";
            this.gridColumnRoleID.OptionsColumn.AllowEdit = false;
            this.gridColumnRoleID.Width = 10;
            // 
            // gridColumnPermissionID
            // 
            this.gridColumnPermissionID.Caption = "PermissoinID";
            this.gridColumnPermissionID.FieldName = "PermissionID";
            this.gridColumnPermissionID.Name = "gridColumnPermissionID";
            this.gridColumnPermissionID.OptionsColumn.AllowEdit = false;
            // 
            // tableLayoutPanelUserRoleControls
            // 
            this.tableLayoutPanelUserRoleControls.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.tableLayoutPanelUserRoleControls.ColumnCount = 1;
            this.tableLayoutPanelUserRoleControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelUserRoleControls.Controls.Add(this.panelStepForm, 0, 1);
            this.tableLayoutPanelUserRoleControls.Controls.Add(this.panelUserRoleControlTitle, 0, 0);
            this.tableLayoutPanelUserRoleControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelUserRoleControls.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelUserRoleControls.Name = "tableLayoutPanelUserRoleControls";
            this.tableLayoutPanelUserRoleControls.RowCount = 2;
            this.tableLayoutPanelUserRoleControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelUserRoleControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelUserRoleControls.Size = new System.Drawing.Size(394, 400);
            this.tableLayoutPanelUserRoleControls.TabIndex = 6;
            // 
            // panelStepForm
            // 
            this.panelStepForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelStepForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panelStepForm.Controls.Add(this.txtRoleID);
            this.panelStepForm.Controls.Add(this.clbPermission);
            this.panelStepForm.Controls.Add(this.txtRoleName);
            this.panelStepForm.Controls.Add(this.btnResetRole);
            this.panelStepForm.Controls.Add(this.btnDeleteRole);
            this.panelStepForm.Controls.Add(this.btnAddUpdateRole);
            this.panelStepForm.Controls.Add(this.label4);
            this.panelStepForm.Controls.Add(this.label5);
            this.panelStepForm.Controls.Add(this.panelRoleTitle);
            this.panelStepForm.Font = new System.Drawing.Font("Calibri", 9F);
            this.panelStepForm.Location = new System.Drawing.Point(3, 53);
            this.panelStepForm.Name = "panelStepForm";
            this.panelStepForm.Size = new System.Drawing.Size(388, 344);
            this.panelStepForm.TabIndex = 6;
            // 
            // txtRoleID
            // 
            this.txtRoleID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRoleID.Location = new System.Drawing.Point(225, 6);
            this.txtRoleID.Name = "txtRoleID";
            this.txtRoleID.Size = new System.Drawing.Size(37, 19);
            this.txtRoleID.TabIndex = 22;
            this.txtRoleID.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // clbPermission
            // 
            this.clbPermission.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.clbPermission.Appearance.Options.UseFont = true;
            this.clbPermission.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.clbPermission.CheckOnClick = true;
            this.clbPermission.Cursor = System.Windows.Forms.Cursors.Default;
            this.clbPermission.Location = new System.Drawing.Point(17, 105);
            this.clbPermission.Name = "clbPermission";
            this.clbPermission.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.clbPermission.Size = new System.Drawing.Size(288, 122);
            this.clbPermission.TabIndex = 61;
            // 
            // txtRoleName
            // 
            this.txtRoleName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRoleName.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtRoleName.Location = new System.Drawing.Point(24, 43);
            this.txtRoleName.Name = "txtRoleName";
            this.txtRoleName.Size = new System.Drawing.Size(275, 21);
            this.txtRoleName.TabIndex = 60;
            this.txtRoleName.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtRoleName.ValidatingType = typeof(int);
            // 
            // btnResetRole
            // 
            this.btnResetRole.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnResetRole.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnResetRole.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnResetRole.Appearance.Options.UseBackColor = true;
            this.btnResetRole.Appearance.Options.UseFont = true;
            this.btnResetRole.Appearance.Options.UseForeColor = true;
            this.btnResetRole.Image = ((System.Drawing.Image)(resources.GetObject("btnResetRole.Image")));
            this.btnResetRole.Location = new System.Drawing.Point(226, 242);
            this.btnResetRole.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.btnResetRole.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnResetRole.Name = "btnResetRole";
            this.btnResetRole.Size = new System.Drawing.Size(79, 30);
            this.btnResetRole.TabIndex = 64;
            this.btnResetRole.Text = "Reset";
            this.btnResetRole.Click += new System.EventHandler(this.btnResetRole_Click);
            // 
            // btnDeleteRole
            // 
            this.btnDeleteRole.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnDeleteRole.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnDeleteRole.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnDeleteRole.Appearance.Options.UseBackColor = true;
            this.btnDeleteRole.Appearance.Options.UseFont = true;
            this.btnDeleteRole.Appearance.Options.UseForeColor = true;
            this.btnDeleteRole.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteRole.Image")));
            this.btnDeleteRole.Location = new System.Drawing.Point(125, 242);
            this.btnDeleteRole.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.btnDeleteRole.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnDeleteRole.Name = "btnDeleteRole";
            this.btnDeleteRole.Size = new System.Drawing.Size(79, 30);
            this.btnDeleteRole.TabIndex = 63;
            this.btnDeleteRole.Text = "Delete";
            this.btnDeleteRole.Click += new System.EventHandler(this.btnDeleteRole_Click);
            // 
            // btnAddUpdateRole
            // 
            this.btnAddUpdateRole.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.btnAddUpdateRole.Appearance.Font = new System.Drawing.Font("Calibri", 9F);
            this.btnAddUpdateRole.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnAddUpdateRole.Appearance.Options.UseBackColor = true;
            this.btnAddUpdateRole.Appearance.Options.UseFont = true;
            this.btnAddUpdateRole.Appearance.Options.UseForeColor = true;
            this.btnAddUpdateRole.Image = ((System.Drawing.Image)(resources.GetObject("btnAddUpdateRole.Image")));
            this.btnAddUpdateRole.Location = new System.Drawing.Point(17, 242);
            this.btnAddUpdateRole.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.btnAddUpdateRole.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAddUpdateRole.Name = "btnAddUpdateRole";
            this.btnAddUpdateRole.Size = new System.Drawing.Size(86, 30);
            this.btnAddUpdateRole.TabIndex = 62;
            this.btnAddUpdateRole.Text = "Add / Edit";
            this.btnAddUpdateRole.Click += new System.EventHandler(this.btnAddUpdateRole_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 10F);
            this.label4.Location = new System.Drawing.Point(17, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 21);
            this.label4.TabIndex = 8;
            this.label4.Text = "Title";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 10F);
            this.label5.Location = new System.Drawing.Point(17, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 21);
            this.label5.TabIndex = 9;
            this.label5.Text = "Permission";
            // 
            // panelRoleTitle
            // 
            this.panelRoleTitle.BackColor = System.Drawing.Color.White;
            this.panelRoleTitle.Location = new System.Drawing.Point(17, 36);
            this.panelRoleTitle.Name = "panelRoleTitle";
            this.panelRoleTitle.Size = new System.Drawing.Size(288, 35);
            this.panelRoleTitle.TabIndex = 238;
            // 
            // panelUserRoleControlTitle
            // 
            this.panelUserRoleControlTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panelUserRoleControlTitle.Controls.Add(this.label6);
            this.panelUserRoleControlTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUserRoleControlTitle.Font = new System.Drawing.Font("Calibri", 10F);
            this.panelUserRoleControlTitle.ForeColor = System.Drawing.Color.White;
            this.panelUserRoleControlTitle.Location = new System.Drawing.Point(0, 0);
            this.panelUserRoleControlTitle.Margin = new System.Windows.Forms.Padding(0);
            this.panelUserRoleControlTitle.Name = "panelUserRoleControlTitle";
            this.panelUserRoleControlTitle.Size = new System.Drawing.Size(394, 50);
            this.panelUserRoleControlTitle.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(6, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 21);
            this.label6.TabIndex = 0;
            this.label6.Text = "Role:";
            // 
            // frmUserManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1829, 1028);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmUserManagement";
            this.Text = "User Management";
            this.Load += new System.EventHandler(this.frmUserManagement_Load);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelUser.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.tableLayoutPanelAddUserControl.ResumeLayout(false);
            this.panelSeqForm.ResumeLayout(false);
            this.panelSeqForm.PerformLayout();
            this.panelUserControlTitle.ResumeLayout(false);
            this.panelUserControlTitle.PerformLayout();
            this.tableLayoutPanelRoles.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRole)).EndInit();
            this.tableLayoutPanelUserRoleControls.ResumeLayout(false);
            this.panelStepForm.ResumeLayout(false);
            this.panelStepForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clbPermission)).EndInit();
            this.panelUserRoleControlTitle.ResumeLayout(false);
            this.panelUserRoleControlTitle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRoles;
        private DevExpress.XtraGrid.GridControl gridControlRole;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewRole;
        private System.Windows.Forms.Panel panelSeqForm;
        private System.Windows.Forms.Panel panelStepForm;
        private DevExpress.XtraEditors.SimpleButton btnAddUpdateUser;
        private System.Windows.Forms.Label lblSeqNo;
        private DevExpress.XtraEditors.SimpleButton btnAddUpdateRole;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTitle;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPermission;
        private DevExpress.XtraEditors.SimpleButton btnDeleteUser;
        private DevExpress.XtraEditors.SimpleButton btnDeleteRole;
        private DevExpress.XtraEditors.SimpleButton btnResetUser;
        private DevExpress.XtraEditors.SimpleButton btnResetRole;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnRoleID;
        private System.Windows.Forms.MaskedTextBox txtFirstName;
        private System.Windows.Forms.MaskedTextBox txtRoleName;
        private System.Windows.Forms.MaskedTextBox txtLastName;
        private System.Windows.Forms.MaskedTextBox txtEmail;
        private System.Windows.Forms.MaskedTextBox txtUserName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox txtConfirmPassword;
        private System.Windows.Forms.MaskedTextBox txtPassword;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ddlRole;
        private DevExpress.XtraEditors.CheckedListBoxControl clbPermission;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPermissionID;
        private System.Windows.Forms.MaskedTextBox txtUserID;
        private System.Windows.Forms.MaskedTextBox txtRoleID;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelUser;
        private DevExpress.XtraGrid.GridControl gridControlUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewUser;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnFName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnUserID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnEmail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnRole;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPassword;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnRID;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelAddUserControl;
        private System.Windows.Forms.Panel panelUserControlTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelUserRoleControls;
        private System.Windows.Forms.Panel panelUserRoleControlTitle;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.Panel panelFirstName;
        private System.Windows.Forms.Panel panelUserRole;
        private System.Windows.Forms.Panel panelPassword;
        private System.Windows.Forms.Panel panelConfirmPassword;
        private System.Windows.Forms.Panel panelUserName;
        private System.Windows.Forms.Panel panelEmail;
        private System.Windows.Forms.Panel panelLastName;
        private System.Windows.Forms.Panel panelRoleTitle;
    }
}