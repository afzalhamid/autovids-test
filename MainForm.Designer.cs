﻿namespace VideoViewer
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbVehicles = new System.Windows.Forms.ListBox();
            this.buttonHistory = new System.Windows.Forms.Button();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.comboBoxCamera = new System.Windows.Forms.ComboBox();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.listBoxOUT = new System.Windows.Forms.ListBox();
            this.listBoxIN = new System.Windows.Forms.ListBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnService = new DevExpress.XtraEditors.SimpleButton();
            this.imageListIndications = new System.Windows.Forms.ImageList(this.components);
            this.vidsServiceController = new System.ServiceProcess.ServiceController();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblSpeed02 = new System.Windows.Forms.Label();
            this.lblTime02 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblTime01 = new System.Windows.Forms.Label();
            this.lblSpeed01 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.groupBoxVideo = new System.Windows.Forms.GroupBox();
            this.btnPrvFrame1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnNxtFrame1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPlay01 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPause01 = new DevExpress.XtraEditors.SimpleButton();
            this.btnBackward01 = new DevExpress.XtraEditors.SimpleButton();
            this.btnForward01 = new DevExpress.XtraEditors.SimpleButton();
            this.btnReset01 = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExport1 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnPrvFrame2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPause02 = new DevExpress.XtraEditors.SimpleButton();
            this.btnNxtFrame2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnForward02 = new DevExpress.XtraEditors.SimpleButton();
            this.btnReset02 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPlay02 = new DevExpress.XtraEditors.SimpleButton();
            this.btnBackward02 = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExport2 = new System.Windows.Forms.Button();
            this.buttonExplore = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.groupBoxVideo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbVehicles);
            this.groupBox2.Controls.Add(this.buttonHistory);
            this.groupBox2.Controls.Add(this.buttonSearch);
            this.groupBox2.Controls.Add(this.comboBoxCamera);
            this.groupBox2.Controls.Add(this.textBoxSearch);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(48, 118);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(850, 228);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Vehicle Details";
            // 
            // lbVehicles
            // 
            this.lbVehicles.FormattingEnabled = true;
            this.lbVehicles.ItemHeight = 16;
            this.lbVehicles.Location = new System.Drawing.Point(159, 56);
            this.lbVehicles.Name = "lbVehicles";
            this.lbVehicles.Size = new System.Drawing.Size(372, 132);
            this.lbVehicles.TabIndex = 2;
            this.lbVehicles.SelectedIndexChanged += new System.EventHandler(this.lbVehicles_SelectedIndexChanged);
            // 
            // buttonHistory
            // 
            this.buttonHistory.BackColor = System.Drawing.Color.Teal;
            this.buttonHistory.ForeColor = System.Drawing.Color.White;
            this.buttonHistory.Location = new System.Drawing.Point(551, 176);
            this.buttonHistory.Name = "buttonHistory";
            this.buttonHistory.Size = new System.Drawing.Size(123, 39);
            this.buttonHistory.TabIndex = 3;
            this.buttonHistory.Text = "Get Visits";
            this.buttonHistory.UseVisualStyleBackColor = false;
            this.buttonHistory.Click += new System.EventHandler(this.buttonHistory_Click);
            // 
            // buttonSearch
            // 
            this.buttonSearch.BackColor = System.Drawing.Color.Teal;
            this.buttonSearch.ForeColor = System.Drawing.Color.White;
            this.buttonSearch.Location = new System.Drawing.Point(551, 32);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(123, 39);
            this.buttonSearch.TabIndex = 3;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // comboBoxCamera
            // 
            this.comboBoxCamera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCamera.FormattingEnabled = true;
            this.comboBoxCamera.Items.AddRange(new object[] {
            "Front Cameras",
            "Rear Cameras"});
            this.comboBoxCamera.Location = new System.Drawing.Point(159, 191);
            this.comboBoxCamera.Name = "comboBoxCamera";
            this.comboBoxCamera.Size = new System.Drawing.Size(372, 24);
            this.comboBoxCamera.TabIndex = 2;
            this.comboBoxCamera.SelectedIndexChanged += new System.EventHandler(this.comboBoxCamera_SelectedIndexChanged);
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Location = new System.Drawing.Point(159, 32);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(372, 22);
            this.textBoxSearch.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 195);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Camera:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Registration No:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Search Vehicle:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnConnect);
            this.groupBox3.Controls.Add(this.listBoxOUT);
            this.groupBox3.Controls.Add(this.listBoxIN);
            this.groupBox3.Location = new System.Drawing.Point(998, 118);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(850, 228);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Visit History (Entry / Exit)";
            // 
            // listBoxOUT
            // 
            this.listBoxOUT.FormattingEnabled = true;
            this.listBoxOUT.ItemHeight = 16;
            this.listBoxOUT.Location = new System.Drawing.Point(403, 32);
            this.listBoxOUT.Name = "listBoxOUT";
            this.listBoxOUT.Size = new System.Drawing.Size(375, 180);
            this.listBoxOUT.TabIndex = 1;
            this.listBoxOUT.SelectedIndexChanged += new System.EventHandler(this.listBoxOUT_SelectedIndexChanged);
            // 
            // listBoxIN
            // 
            this.listBoxIN.FormattingEnabled = true;
            this.listBoxIN.ItemHeight = 16;
            this.listBoxIN.Location = new System.Drawing.Point(8, 32);
            this.listBoxIN.Name = "listBoxIN";
            this.listBoxIN.Size = new System.Drawing.Size(375, 180);
            this.listBoxIN.TabIndex = 0;
            this.listBoxIN.SelectedIndexChanged += new System.EventHandler(this.listBoxIN_SelectedIndexChanged);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Location = new System.Drawing.Point(1, 1);
            this.panel3.MaximumSize = new System.Drawing.Size(1901, 100);
            this.panel3.MinimumSize = new System.Drawing.Size(1901, 100);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1901, 100);
            this.panel3.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(806, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(294, 42);
            this.label4.TabIndex = 2;
            this.label4.Text = "AUTO VDS 360";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Teal;
            this.panel6.Controls.Add(this.btnPrvFrame1);
            this.panel6.Controls.Add(this.btnNxtFrame1);
            this.panel6.Controls.Add(this.btnPlay01);
            this.panel6.Controls.Add(this.btnPause01);
            this.panel6.Controls.Add(this.btnBackward01);
            this.panel6.Controls.Add(this.btnForward01);
            this.panel6.Controls.Add(this.btnReset01);
            this.panel6.Controls.Add(this.buttonExport1);
            this.panel6.Location = new System.Drawing.Point(95, 930);
            this.panel6.MaximumSize = new System.Drawing.Size(800, 40);
            this.panel6.MinimumSize = new System.Drawing.Size(800, 40);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(800, 40);
            this.panel6.TabIndex = 29;
            // 
            // btnService
            // 
            this.btnService.ImageList = this.imageListIndications;
            this.btnService.Location = new System.Drawing.Point(1685, 974);
            this.btnService.Name = "btnService";
            this.btnService.Size = new System.Drawing.Size(117, 25);
            this.btnService.TabIndex = 30;
            this.btnService.Text = "VIDS Server";
            this.btnService.Click += new System.EventHandler(this.btnService_Click);
            // 
            // imageListIndications
            // 
            this.imageListIndications.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIndications.ImageStream")));
            this.imageListIndications.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIndications.Images.SetKeyName(0, "circle_green.png");
            this.imageListIndications.Images.SetKeyName(1, "circle_grey.png");
            this.imageListIndications.Images.SetKeyName(2, "circle_red.png");
            // 
            // vidsServiceController
            // 
            this.vidsServiceController.ServiceName = "OrionAutoVDSServer";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Teal;
            this.panel5.Controls.Add(this.lblSpeed02);
            this.panel5.Controls.Add(this.lblTime02);
            this.panel5.Location = new System.Drawing.Point(954, 53);
            this.panel5.MaximumSize = new System.Drawing.Size(800, 25);
            this.panel5.MinimumSize = new System.Drawing.Size(800, 25);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(800, 25);
            this.panel5.TabIndex = 29;
            // 
            // lblSpeed02
            // 
            this.lblSpeed02.AutoSize = true;
            this.lblSpeed02.ForeColor = System.Drawing.Color.White;
            this.lblSpeed02.Location = new System.Drawing.Point(734, 3);
            this.lblSpeed02.Name = "lblSpeed02";
            this.lblSpeed02.Size = new System.Drawing.Size(0, 17);
            this.lblSpeed02.TabIndex = 32;
            // 
            // lblTime02
            // 
            this.lblTime02.AutoSize = true;
            this.lblTime02.ForeColor = System.Drawing.Color.White;
            this.lblTime02.Location = new System.Drawing.Point(3, 4);
            this.lblTime02.Name = "lblTime02";
            this.lblTime02.Size = new System.Drawing.Size(0, 17);
            this.lblTime02.TabIndex = 32;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Teal;
            this.panel4.Controls.Add(this.lblTime01);
            this.panel4.Controls.Add(this.lblSpeed01);
            this.panel4.Location = new System.Drawing.Point(47, 53);
            this.panel4.MaximumSize = new System.Drawing.Size(800, 25);
            this.panel4.MinimumSize = new System.Drawing.Size(800, 25);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(800, 25);
            this.panel4.TabIndex = 28;
            // 
            // lblTime01
            // 
            this.lblTime01.AutoSize = true;
            this.lblTime01.ForeColor = System.Drawing.Color.White;
            this.lblTime01.Location = new System.Drawing.Point(5, 4);
            this.lblTime01.Name = "lblTime01";
            this.lblTime01.Size = new System.Drawing.Size(0, 17);
            this.lblTime01.TabIndex = 31;
            // 
            // lblSpeed01
            // 
            this.lblSpeed01.AutoSize = true;
            this.lblSpeed01.ForeColor = System.Drawing.Color.White;
            this.lblSpeed01.Location = new System.Drawing.Point(734, 4);
            this.lblSpeed01.Name = "lblSpeed01";
            this.lblSpeed01.Size = new System.Drawing.Size(0, 17);
            this.lblSpeed01.TabIndex = 32;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(47, 77);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.MaximumSize = new System.Drawing.Size(800, 500);
            this.panel1.MinimumSize = new System.Drawing.Size(800, 500);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 500);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel2.Location = new System.Drawing.Point(954, 77);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.MaximumSize = new System.Drawing.Size(800, 500);
            this.panel2.MinimumSize = new System.Drawing.Size(800, 500);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(800, 500);
            this.panel2.TabIndex = 3;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Teal;
            this.panel7.Controls.Add(this.btnPrvFrame2);
            this.panel7.Controls.Add(this.btnPause02);
            this.panel7.Controls.Add(this.btnNxtFrame2);
            this.panel7.Controls.Add(this.btnForward02);
            this.panel7.Controls.Add(this.btnReset02);
            this.panel7.Controls.Add(this.btnPlay02);
            this.panel7.Controls.Add(this.btnBackward02);
            this.panel7.Controls.Add(this.buttonExport2);
            this.panel7.Location = new System.Drawing.Point(954, 577);
            this.panel7.MaximumSize = new System.Drawing.Size(800, 40);
            this.panel7.MinimumSize = new System.Drawing.Size(800, 40);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(800, 40);
            this.panel7.TabIndex = 30;
            // 
            // groupBoxVideo
            // 
            this.groupBoxVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxVideo.Controls.Add(this.panel7);
            this.groupBoxVideo.Controls.Add(this.buttonExplore);
            this.groupBoxVideo.Controls.Add(this.panel2);
            this.groupBoxVideo.Controls.Add(this.panel1);
            this.groupBoxVideo.Controls.Add(this.panel4);
            this.groupBoxVideo.Controls.Add(this.panel5);
            this.groupBoxVideo.Location = new System.Drawing.Point(48, 353);
            this.groupBoxVideo.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxVideo.MaximumSize = new System.Drawing.Size(1800, 750);
            this.groupBoxVideo.MinimumSize = new System.Drawing.Size(1800, 650);
            this.groupBoxVideo.Name = "groupBoxVideo";
            this.groupBoxVideo.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxVideo.Size = new System.Drawing.Size(1800, 650);
            this.groupBoxVideo.TabIndex = 12;
            this.groupBoxVideo.TabStop = false;
            this.groupBoxVideo.Text = "Playback and Export Image";
            // 
            // btnPrvFrame1
            // 
            this.btnPrvFrame1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPrvFrame1.Image = ((System.Drawing.Image)(resources.GetObject("btnPrvFrame1.Image")));
            this.btnPrvFrame1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPrvFrame1.Location = new System.Drawing.Point(320, 4);
            this.btnPrvFrame1.Name = "btnPrvFrame1";
            this.btnPrvFrame1.Size = new System.Drawing.Size(32, 32);
            this.btnPrvFrame1.TabIndex = 38;
            this.btnPrvFrame1.ToolTip = "Previous Frame";
            this.btnPrvFrame1.Click += new System.EventHandler(this.btnPrvFrame1_Click);
            // 
            // btnNxtFrame1
            // 
            this.btnNxtFrame1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnNxtFrame1.Image = ((System.Drawing.Image)(resources.GetObject("btnNxtFrame1.Image")));
            this.btnNxtFrame1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnNxtFrame1.Location = new System.Drawing.Point(497, 4);
            this.btnNxtFrame1.Name = "btnNxtFrame1";
            this.btnNxtFrame1.Size = new System.Drawing.Size(32, 32);
            this.btnNxtFrame1.TabIndex = 37;
            this.btnNxtFrame1.ToolTip = "Next Frame";
            this.btnNxtFrame1.Click += new System.EventHandler(this.btnNxtFrame1_Click);
            // 
            // btnPlay01
            // 
            this.btnPlay01.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPlay01.Image = ((System.Drawing.Image)(resources.GetObject("btnPlay01.Image")));
            this.btnPlay01.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPlay01.Location = new System.Drawing.Point(378, 4);
            this.btnPlay01.Name = "btnPlay01";
            this.btnPlay01.Size = new System.Drawing.Size(32, 32);
            this.btnPlay01.TabIndex = 35;
            this.btnPlay01.ToolTip = "Play";
            this.btnPlay01.Click += new System.EventHandler(this.buttonPaly_Click);
            // 
            // btnPause01
            // 
            this.btnPause01.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPause01.Image = ((System.Drawing.Image)(resources.GetObject("btnPause01.Image")));
            this.btnPause01.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPause01.Location = new System.Drawing.Point(378, 4);
            this.btnPause01.Name = "btnPause01";
            this.btnPause01.Size = new System.Drawing.Size(32, 32);
            this.btnPause01.TabIndex = 36;
            this.btnPause01.ToolTip = "Pause";
            this.btnPause01.Click += new System.EventHandler(this.OnStop1Click);
            // 
            // btnBackward01
            // 
            this.btnBackward01.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnBackward01.Image = ((System.Drawing.Image)(resources.GetObject("btnBackward01.Image")));
            this.btnBackward01.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBackward01.Location = new System.Drawing.Point(220, 4);
            this.btnBackward01.Name = "btnBackward01";
            this.btnBackward01.Size = new System.Drawing.Size(32, 32);
            this.btnBackward01.TabIndex = 34;
            this.btnBackward01.ToolTip = "Backward";
            this.btnBackward01.Visible = false;
            this.btnBackward01.Click += new System.EventHandler(this.buttonFrameReverse_Click);
            // 
            // btnForward01
            // 
            this.btnForward01.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnForward01.Image = ((System.Drawing.Image)(resources.GetObject("btnForward01.Image")));
            this.btnForward01.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnForward01.Location = new System.Drawing.Point(597, 4);
            this.btnForward01.Name = "btnForward01";
            this.btnForward01.Size = new System.Drawing.Size(32, 32);
            this.btnForward01.TabIndex = 33;
            this.btnForward01.ToolTip = "Forward";
            this.btnForward01.Visible = false;
            this.btnForward01.Click += new System.EventHandler(this.OnForward1Click);
            // 
            // btnReset01
            // 
            this.btnReset01.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnReset01.Image = ((System.Drawing.Image)(resources.GetObject("btnReset01.Image")));
            this.btnReset01.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnReset01.Location = new System.Drawing.Point(433, 4);
            this.btnReset01.Name = "btnReset01";
            this.btnReset01.Size = new System.Drawing.Size(32, 32);
            this.btnReset01.TabIndex = 32;
            this.btnReset01.ToolTip = "Reset";
            this.btnReset01.Click += new System.EventHandler(this.btnReset01_Click);
            // 
            // buttonExport1
            // 
            this.buttonExport1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExport1.Image = global::VideoViewer.Properties.Resources.image_export;
            this.buttonExport1.Location = new System.Drawing.Point(760, 4);
            this.buttonExport1.Name = "buttonExport1";
            this.buttonExport1.Size = new System.Drawing.Size(32, 32);
            this.buttonExport1.TabIndex = 26;
            this.buttonExport1.UseVisualStyleBackColor = true;
            this.buttonExport1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.InitialImage = global::VideoViewer.Properties.Resources.altayer_logo;
            this.pictureBox2.Location = new System.Drawing.Point(1526, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(363, 100);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = global::VideoViewer.Properties.Resources.orion_logo_70;
            this.pictureBox1.InitialImage = global::VideoViewer.Properties.Resources.orion_logo_70;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(323, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnConnect
            // 
            this.btnConnect.BackColor = System.Drawing.Color.Teal;
            this.btnConnect.ForeColor = System.Drawing.Color.Black;
            this.btnConnect.Image = ((System.Drawing.Image)(resources.GetObject("btnConnect.Image")));
            this.btnConnect.Location = new System.Drawing.Point(794, 164);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(48, 48);
            this.btnConnect.TabIndex = 31;
            this.btnConnect.UseVisualStyleBackColor = false;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnPrvFrame2
            // 
            this.btnPrvFrame2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPrvFrame2.Image = ((System.Drawing.Image)(resources.GetObject("btnPrvFrame2.Image")));
            this.btnPrvFrame2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPrvFrame2.Location = new System.Drawing.Point(318, 4);
            this.btnPrvFrame2.Name = "btnPrvFrame2";
            this.btnPrvFrame2.Size = new System.Drawing.Size(32, 32);
            this.btnPrvFrame2.TabIndex = 40;
            this.btnPrvFrame2.ToolTip = "Previous Frame";
            this.btnPrvFrame2.Click += new System.EventHandler(this.btnPrvFrame2_Click);
            // 
            // btnPause02
            // 
            this.btnPause02.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPause02.Image = ((System.Drawing.Image)(resources.GetObject("btnPause02.Image")));
            this.btnPause02.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPause02.Location = new System.Drawing.Point(376, 2);
            this.btnPause02.Name = "btnPause02";
            this.btnPause02.Size = new System.Drawing.Size(32, 32);
            this.btnPause02.TabIndex = 37;
            this.btnPause02.ToolTip = "Pause";
            this.btnPause02.Click += new System.EventHandler(this.OnStop2Click);
            // 
            // btnNxtFrame2
            // 
            this.btnNxtFrame2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnNxtFrame2.Image = ((System.Drawing.Image)(resources.GetObject("btnNxtFrame2.Image")));
            this.btnNxtFrame2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnNxtFrame2.Location = new System.Drawing.Point(492, 4);
            this.btnNxtFrame2.Name = "btnNxtFrame2";
            this.btnNxtFrame2.Size = new System.Drawing.Size(32, 32);
            this.btnNxtFrame2.TabIndex = 39;
            this.btnNxtFrame2.ToolTip = "Next Frame";
            this.btnNxtFrame2.Click += new System.EventHandler(this.btnNxtFrame2_Click);
            // 
            // btnForward02
            // 
            this.btnForward02.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnForward02.Image = ((System.Drawing.Image)(resources.GetObject("btnForward02.Image")));
            this.btnForward02.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnForward02.Location = new System.Drawing.Point(592, 4);
            this.btnForward02.Name = "btnForward02";
            this.btnForward02.Size = new System.Drawing.Size(32, 32);
            this.btnForward02.TabIndex = 37;
            this.btnForward02.ToolTip = "Forward";
            this.btnForward02.Visible = false;
            this.btnForward02.Click += new System.EventHandler(this.OnForward2Click);
            // 
            // btnReset02
            // 
            this.btnReset02.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnReset02.Image = ((System.Drawing.Image)(resources.GetObject("btnReset02.Image")));
            this.btnReset02.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnReset02.Location = new System.Drawing.Point(429, 4);
            this.btnReset02.Name = "btnReset02";
            this.btnReset02.Size = new System.Drawing.Size(32, 32);
            this.btnReset02.TabIndex = 37;
            this.btnReset02.ToolTip = "Reset";
            this.btnReset02.Click += new System.EventHandler(this.btnReset02_Click);
            // 
            // btnPlay02
            // 
            this.btnPlay02.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPlay02.Image = ((System.Drawing.Image)(resources.GetObject("btnPlay02.Image")));
            this.btnPlay02.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnPlay02.Location = new System.Drawing.Point(376, 4);
            this.btnPlay02.Name = "btnPlay02";
            this.btnPlay02.Size = new System.Drawing.Size(32, 32);
            this.btnPlay02.TabIndex = 37;
            this.btnPlay02.ToolTip = "Play";
            this.btnPlay02.Click += new System.EventHandler(this.buttonPlay2_Click);
            // 
            // btnBackward02
            // 
            this.btnBackward02.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnBackward02.Image = ((System.Drawing.Image)(resources.GetObject("btnBackward02.Image")));
            this.btnBackward02.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBackward02.Location = new System.Drawing.Point(218, 4);
            this.btnBackward02.Name = "btnBackward02";
            this.btnBackward02.Size = new System.Drawing.Size(32, 32);
            this.btnBackward02.TabIndex = 37;
            this.btnBackward02.ToolTip = "Backward";
            this.btnBackward02.Visible = false;
            this.btnBackward02.Click += new System.EventHandler(this.OnReverse2Click);
            // 
            // buttonExport2
            // 
            this.buttonExport2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExport2.Image = global::VideoViewer.Properties.Resources.image_export;
            this.buttonExport2.Location = new System.Drawing.Point(762, 3);
            this.buttonExport2.Name = "buttonExport2";
            this.buttonExport2.Size = new System.Drawing.Size(32, 32);
            this.buttonExport2.TabIndex = 25;
            this.buttonExport2.UseVisualStyleBackColor = true;
            this.buttonExport2.Click += new System.EventHandler(this.buttonExport2_Click);
            // 
            // buttonExplore
            // 
            this.buttonExplore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExplore.Image = global::VideoViewer.Properties.Resources.folder_explore;
            this.buttonExplore.Location = new System.Drawing.Point(1761, 25);
            this.buttonExplore.Name = "buttonExplore";
            this.buttonExplore.Size = new System.Drawing.Size(32, 32);
            this.buttonExplore.TabIndex = 27;
            this.buttonExplore.UseVisualStyleBackColor = true;
            this.buttonExplore.Click += new System.EventHandler(this.buttonExplore_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1902, 1033);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.btnService);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBoxVideo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1918, 1080);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Auto VDS 360 Solution";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.groupBoxVideo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox listBoxOUT;
        private System.Windows.Forms.ListBox listBoxIN;
        private System.Windows.Forms.Button buttonHistory;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.ComboBox comboBoxCamera;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonExport1;
        private System.Windows.Forms.Panel panel6;
        private DevExpress.XtraEditors.SimpleButton btnReset01;
        private DevExpress.XtraEditors.SimpleButton btnPlay01;
        private DevExpress.XtraEditors.SimpleButton btnPause01;
        private DevExpress.XtraEditors.SimpleButton btnBackward01;
        private DevExpress.XtraEditors.SimpleButton btnForward01;
        private DevExpress.XtraEditors.SimpleButton btnService;
        private System.Windows.Forms.ImageList imageListIndications;
        private System.ServiceProcess.ServiceController vidsServiceController;
        private System.Windows.Forms.ListBox lbVehicles;
        private System.Windows.Forms.Button btnConnect;
        private DevExpress.XtraEditors.SimpleButton btnPrvFrame1;
        private DevExpress.XtraEditors.SimpleButton btnNxtFrame1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblSpeed02;
        private System.Windows.Forms.Label lblTime02;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblTime01;
        private System.Windows.Forms.Label lblSpeed01;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonExplore;
        private System.Windows.Forms.Panel panel7;
        private DevExpress.XtraEditors.SimpleButton btnPrvFrame2;
        private DevExpress.XtraEditors.SimpleButton btnPause02;
        private DevExpress.XtraEditors.SimpleButton btnNxtFrame2;
        private DevExpress.XtraEditors.SimpleButton btnForward02;
        private DevExpress.XtraEditors.SimpleButton btnReset02;
        private DevExpress.XtraEditors.SimpleButton btnPlay02;
        private DevExpress.XtraEditors.SimpleButton btnBackward02;
        private System.Windows.Forms.Button buttonExport2;
        private System.Windows.Forms.GroupBox groupBoxVideo;
    }
}

