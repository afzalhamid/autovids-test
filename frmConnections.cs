﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoViewer
{
    public partial class frmConnections : Form
    {
        DAL dataAccess = new DAL();
        private static string imgFilePath { get; set; }
        private static string lastPassword { get; set; }
        public frmConnections()
        {
            InitializeComponent();
        }

        private void frmConnections_Load(object sender, EventArgs e)
        {
            LoadConfiguration();
        }

        private void btnAddUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                var result = MessageBox.Show("Clik OK to continue. ?", "Save Configuration", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    Dictionary<string, string> paraCollection = new Dictionary<string, string>();
                    
                    paraCollection.Add("@VMSServerName", txtVMSServerName.Text);
                    paraCollection.Add("@VMSUserName", txtVMSuserName.Text);
                    if (lastPassword == txtVMSPassword.Text.Trim())
                        paraCollection.Add("@VMSPassword", lastPassword);
                    else
                        paraCollection.Add("@VMSPassword", dataAccess.Encrypt(txtVMSPassword.Text.Trim()));
                    paraCollection.Add("@VMSPort", txtVMSPortNumber.Text);
                    paraCollection.Add("@VMSIP", txtVMSIP.Text);

                    paraCollection.Add("@VMSEventServerPort", txtEventServerPortNumber.Text);
                    paraCollection.Add("@VMSRetention", txtVMSRetentionTime.Text);
                    
                    paraCollection.Add("@TCPIP", txtTCPServerIP.Text);
                    paraCollection.Add("@TCPPort", txtANPRPortNumber.Text);
                    paraCollection.Add("@Edition", ddlVMSEdition.Text);

                    bool isUpdate = dataAccess.SaveConfiguration(paraCollection);

                    if (isUpdate)
                    {
                        LoadConfiguration();
                    }
                    else
                    {
                        MessageBox.Show("Update Failed.");
                    }
                }
            }
            catch (Exception ex)
            {
                dataAccess.Logging(Module.Application, LogClass.Error, ex.ToString());
            }
        }

        void LoadConfiguration()
        {
            try
            {
                Dictionary<string, string> fields = dataAccess.GetConfiguration();

                txtVMSServerName.Text = fields["VMSServerName"];
                txtVMSPortNumber.Text = fields["VMSPort"];
                txtVMSIP.Text = fields["VMSIP"];
                txtVMSuserName.Text = fields["VMSUserName"];
                txtVMSPassword.Text = fields["VMSPassword"];

                txtEventServerPortNumber.Text = fields["VMSEventServerPort"];
                txtVMSRetentionTime.Text = fields["VMSRetentionTime"];

                txtTCPServerIP.Text = fields["TCPServerIP"];
                txtANPRPortNumber.Text = fields["TCPPort"];
                lastPassword = fields["VMSPassword"];

                if (fields["VMSEdition"] == "Advanced")
                    ddlVMSEdition.SelectedIndex = 1;
                else
                    ddlVMSEdition.SelectedIndex = 0;

                

            }
            catch (Exception ex)
            {
                dataAccess.Logging(Module.Application, LogClass.Error, ex.ToString());
            }
        }

        private void txtANPRPortNumber_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
