﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VideoOS.Platform;
using System.Collections;
using DevExpress.Utils.Menu;
using DataAccessLayer;
namespace VideoViewer
{

    public partial class frmVIN : Form
    {
        DAL dataAccess = new DAL();
        private string ParentFolder { get { return System.Configuration.ConfigurationManager.AppSettings["FolderPath"]; } }

        public frmVIN()
        {
            InitializeComponent();
        }

        private void frmVIN_Load(object sender, EventArgs e)
        {
            LoadFormData();
        }

        void LoadFormData()
        {
            try
            {
                gridControlVIN.DataSource = dataAccess.GetAllVehicles().Tables[0];
            }
            catch
            {

            }
        }

        private void show_Description_Click(object sender, EventArgs e)
        {
            int vehicleID = Convert.ToInt32(((DevExpress.XtraEditors.TextEdit)sender).Text.Trim());
            frmVINUpdate frmupdate = new frmVINUpdate();
            frmupdate.FormClosing += Frmupdate_FormClosing;
            frmupdate.vehicleID = vehicleID;
            frmupdate.StartPosition = FormStartPosition.CenterParent;
            frmupdate.ShowDialog();
        }

        private void Frmupdate_FormClosing(object sender, FormClosingEventArgs e)
        {
            LoadFormData();
        }

        private void show_history_Click(object sender, EventArgs e)
        {
            int vehicleID = Convert.ToInt32(((DevExpress.XtraEditors.TextEdit)sender).Text.Trim());
            frmHistory frmVehicleHistory = new frmHistory();
            frmVehicleHistory.vehicleID = vehicleID;
            frmVehicleHistory.StartPosition = FormStartPosition.CenterParent;
            frmVehicleHistory.ShowDialog();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                string folder = String.Format("{0}\\Excel", ParentFolder);
                if (!System.IO.Directory.Exists(folder))
                    System.IO.Directory.CreateDirectory(folder);

                gridViewVIN.ExportToXlsx(string.Format("{0}\\VINLog_{1}.xlsx", folder, DateTime.Now.ToString("ddMMyyyyhhmmss")));

            }
            catch
            {
                MessageBox.Show("Export Failed");
            }
        }
    }
}
