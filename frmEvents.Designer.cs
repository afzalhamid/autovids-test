﻿namespace VideoViewer
{
    partial class frmEvents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEvents));
            this.panelDropdown = new System.Windows.Forms.Panel();
            this.panelServerName = new System.Windows.Forms.Panel();
            this.dateEdit = new DevExpress.XtraEditors.DateEdit();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.label16 = new System.Windows.Forms.Label();
            this.btnExportExcel = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanelDopdownMenu = new System.Windows.Forms.TableLayoutPanel();
            this.panelMain = new System.Windows.Forms.Panel();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.gridEvent = new DevExpress.XtraGrid.GridControl();
            this.gridViewEvent = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSrNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVIN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarker = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeStamp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelDropdown.SuspendLayout();
            this.panelServerName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).BeginInit();
            this.tableLayoutPanelDopdownMenu.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.tableLayoutPanelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEvent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEvent)).BeginInit();
            this.SuspendLayout();
            // 
            // panelDropdown
            // 
            this.panelDropdown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(185)))));
            this.panelDropdown.Controls.Add(this.panelServerName);
            this.panelDropdown.Controls.Add(this.label16);
            this.panelDropdown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDropdown.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.panelDropdown.ForeColor = System.Drawing.Color.White;
            this.panelDropdown.Location = new System.Drawing.Point(0, 0);
            this.panelDropdown.Margin = new System.Windows.Forms.Padding(0);
            this.panelDropdown.Name = "panelDropdown";
            this.panelDropdown.Size = new System.Drawing.Size(300, 41);
            this.panelDropdown.TabIndex = 0;
            // 
            // panelServerName
            // 
            this.panelServerName.BackColor = System.Drawing.Color.White;
            this.panelServerName.Controls.Add(this.dateEdit);
            this.panelServerName.Controls.Add(this.btnSearch);
            this.panelServerName.Location = new System.Drawing.Point(59, 6);
            this.panelServerName.Margin = new System.Windows.Forms.Padding(2);
            this.panelServerName.Name = "panelServerName";
            this.panelServerName.Size = new System.Drawing.Size(216, 28);
            this.panelServerName.TabIndex = 504;
            // 
            // dateEdit
            // 
            this.dateEdit.EditValue = new System.DateTime(2018, 12, 11, 17, 57, 37, 0);
            this.dateEdit.Location = new System.Drawing.Point(5, 4);
            this.dateEdit.Margin = new System.Windows.Forms.Padding(2);
            this.dateEdit.Name = "dateEdit";
            this.dateEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            this.dateEdit.Properties.Appearance.Options.UseFont = true;
            this.dateEdit.Properties.AppearanceCalendar.DayCellSelected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dateEdit.Properties.AppearanceCalendar.DayCellSelected.Options.UseBackColor = true;
            this.dateEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.dateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit.Size = new System.Drawing.Size(171, 20);
            this.dateEdit.TabIndex = 5;
            // 
            // btnSearch
            // 
            this.btnSearch.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSearch.Appearance.Options.UseBackColor = true;
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSearch.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnSearch.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.ImageOptions.Image")));
            this.btnSearch.Location = new System.Drawing.Point(190, 2);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(24, 24);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(4, 12);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 17);
            this.label16.TabIndex = 502;
            this.label16.Text = "Date :";
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Appearance.Image")));
            this.btnExportExcel.Appearance.Options.UseImage = true;
            this.btnExportExcel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExportExcel.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnExportExcel.Location = new System.Drawing.Point(1169, 3);
            this.btnExportExcel.MaximumSize = new System.Drawing.Size(32, 32);
            this.btnExportExcel.MinimumSize = new System.Drawing.Size(32, 32);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(32, 32);
            this.btnExportExcel.TabIndex = 1;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // tableLayoutPanelDopdownMenu
            // 
            this.tableLayoutPanelDopdownMenu.ColumnCount = 3;
            this.tableLayoutPanelDopdownMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanelDopdownMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelDopdownMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelDopdownMenu.Controls.Add(this.panelDropdown, 0, 0);
            this.tableLayoutPanelDopdownMenu.Controls.Add(this.btnExportExcel, 2, 0);
            this.tableLayoutPanelDopdownMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelDopdownMenu.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelDopdownMenu.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelDopdownMenu.Name = "tableLayoutPanelDopdownMenu";
            this.tableLayoutPanelDopdownMenu.RowCount = 1;
            this.tableLayoutPanelDopdownMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelDopdownMenu.Size = new System.Drawing.Size(1216, 41);
            this.tableLayoutPanelDopdownMenu.TabIndex = 0;
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.Transparent;
            this.panelMain.Controls.Add(this.tableLayoutPanelDopdownMenu);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(2, 14);
            this.panelMain.Margin = new System.Windows.Forms.Padding(2);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1216, 41);
            this.panelMain.TabIndex = 4;
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelMain.ColumnCount = 1;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.gridEvent, 0, 3);
            this.tableLayoutPanelMain.Controls.Add(this.panelMain, 0, 1);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 4;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1220, 612);
            this.tableLayoutPanelMain.TabIndex = 5;
            // 
            // gridEvent
            // 
            this.gridEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEvent.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridEvent.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridEvent.Location = new System.Drawing.Point(2, 71);
            this.gridEvent.MainView = this.gridViewEvent;
            this.gridEvent.Margin = new System.Windows.Forms.Padding(2);
            this.gridEvent.Name = "gridEvent";
            this.gridEvent.Size = new System.Drawing.Size(1216, 539);
            this.gridEvent.TabIndex = 35;
            this.gridEvent.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEvent});
            // 
            // gridViewEvent
            // 
            this.gridViewEvent.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridViewEvent.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridViewEvent.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridViewEvent.Appearance.Empty.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.Empty.Options.UseFont = true;
            this.gridViewEvent.Appearance.EvenRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.EvenRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.FilterCloseButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridViewEvent.Appearance.FilterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FilterPanel.Options.UseFont = true;
            this.gridViewEvent.Appearance.FixedLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FixedLine.Options.UseFont = true;
            this.gridViewEvent.Appearance.FocusedCell.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FocusedCell.Options.UseFont = true;
            this.gridViewEvent.Appearance.FocusedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.FooterPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewEvent.Appearance.GroupButton.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.GroupButton.Options.UseFont = true;
            this.gridViewEvent.Appearance.GroupFooter.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.GroupFooter.Options.UseFont = true;
            this.gridViewEvent.Appearance.GroupPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.GroupRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.HeaderPanel.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewEvent.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.HorzLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.HorzLine.Options.UseFont = true;
            this.gridViewEvent.Appearance.OddRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.OddRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.Preview.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.Preview.Options.UseFont = true;
            this.gridViewEvent.Appearance.Row.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.Row.Options.UseFont = true;
            this.gridViewEvent.Appearance.RowSeparator.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.RowSeparator.Options.UseFont = true;
            this.gridViewEvent.Appearance.SelectedRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.SelectedRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.TopNewRow.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.TopNewRow.Options.UseFont = true;
            this.gridViewEvent.Appearance.VertLine.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.VertLine.Options.UseFont = true;
            this.gridViewEvent.Appearance.ViewCaption.Font = new System.Drawing.Font("Calibri", 10F);
            this.gridViewEvent.Appearance.ViewCaption.Options.UseFont = true;
            this.gridViewEvent.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSrNo,
            this.colVIN,
            this.colMarker,
            this.colComment,
            this.colTimeStamp});
            this.gridViewEvent.GridControl = this.gridEvent;
            this.gridViewEvent.Name = "gridViewEvent";
            // 
            // colSrNo
            // 
            this.colSrNo.Caption = "Sr #.";
            this.colSrNo.FieldName = "SrNo";
            this.colSrNo.MaxWidth = 50;
            this.colSrNo.Name = "colSrNo";
            this.colSrNo.OptionsColumn.AllowEdit = false;
            this.colSrNo.Visible = true;
            this.colSrNo.VisibleIndex = 0;
            this.colSrNo.Width = 50;
            // 
            // colVIN
            // 
            this.colVIN.Caption = "VIN";
            this.colVIN.FieldName = "Vin";
            this.colVIN.MaxWidth = 300;
            this.colVIN.MinWidth = 300;
            this.colVIN.Name = "colVIN";
            this.colVIN.OptionsColumn.AllowEdit = false;
            this.colVIN.Visible = true;
            this.colVIN.VisibleIndex = 1;
            this.colVIN.Width = 300;
            // 
            // colMarker
            // 
            this.colMarker.Caption = "Marker";
            this.colMarker.FieldName = "Marker";
            this.colMarker.MaxWidth = 250;
            this.colMarker.MinWidth = 250;
            this.colMarker.Name = "colMarker";
            this.colMarker.OptionsColumn.AllowEdit = false;
            this.colMarker.Visible = true;
            this.colMarker.VisibleIndex = 2;
            this.colMarker.Width = 250;
            // 
            // colComment
            // 
            this.colComment.Caption = "Comment";
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            this.colComment.OptionsColumn.AllowEdit = false;
            this.colComment.Visible = true;
            this.colComment.VisibleIndex = 3;
            // 
            // colTimeStamp
            // 
            this.colTimeStamp.Caption = "Timestamp";
            this.colTimeStamp.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm:ss";
            this.colTimeStamp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeStamp.FieldName = "Timestamp";
            this.colTimeStamp.MaxWidth = 250;
            this.colTimeStamp.MinWidth = 250;
            this.colTimeStamp.Name = "colTimeStamp";
            this.colTimeStamp.OptionsColumn.AllowEdit = false;
            this.colTimeStamp.Visible = true;
            this.colTimeStamp.VisibleIndex = 4;
            this.colTimeStamp.Width = 250;
            // 
            // frmEvents
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1220, 612);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmEvents";
            this.Text = "frmEvents";
            this.Load += new System.EventHandler(this.frmEvents_Load);
            this.panelDropdown.ResumeLayout(false);
            this.panelDropdown.PerformLayout();
            this.panelServerName.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).EndInit();
            this.tableLayoutPanelDopdownMenu.ResumeLayout(false);
            this.panelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEvent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEvent)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelDropdown;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.SimpleButton btnExportExcel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelDopdownMenu;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private DevExpress.XtraGrid.GridControl gridEvent;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEvent;
        private DevExpress.XtraGrid.Columns.GridColumn colSrNo;
        private DevExpress.XtraGrid.Columns.GridColumn colVIN;
        private DevExpress.XtraGrid.Columns.GridColumn colMarker;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeStamp;
        private System.Windows.Forms.Panel panelServerName;
        private DevExpress.XtraEditors.DateEdit dateEdit;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
    }
}